'\" te
.\" Copyright 2008 Sun Microsystems, Inc.
.\" All rights reserved. Use is subject to license terms.
.TH scha_cluster_get 1HA "25 Aug 2008" "Sun Cluster 3.2" "Sun Cluster Commands"
.SH NAME
scha_cluster_get \- access cluster information
.SH SYNOPSIS
.LP
.nf
\fBscha_cluster_get\fR  \fB-O\fR \fIoptag\fR [\fIargs\fR]
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscha_cluster_get\fR command accesses and
generates information about a cluster. You can access information
about the cluster, nodes, zones, host names, resource groups, resource
types, and states.
.sp
.LP
The command is intended for use in shell script implementations
of callback methods for resource types. These callback methods for
resource types represent services that are controlled by the cluster's
Resource Group Manager (\fBRGM\fR). This command provides
the same information as the \fBscha_resource_get()\fR function.
.sp
.LP
This command sends output to the standard output (\fBstdout\fR)
in formatted strings on separate lines, as described in the \fBscha_cmds\fR(1HA) man page. You can
store the output in shell variables. You can also parse the output
with shell commands such as the \fBawk\fR command.
.SH OPTIONS
.sp
.LP
The following options are supported: 
.sp
.ne 2
.mk
.na
\fB\fB-O\fR \fIoptag\fR\fR
.ad
.RS 20n
.rt  
The \fIoptag\fR argument indicates
the information to be accessed. Depending on the \fIoptag\fR,
an additional argument may be needed to indicate the cluster node
or zone for which information is to be retrieved.
.LP
Note - 
.sp
.RS 2
\fIoptag\fR options, such as \fBNODENAME_LOCAL\fR and \fBNODENAME_NODEID\fR, are \fBnot\fR case
sensitive. You can use any combination of uppercase and lowercase
letters when you specify \fIoptag\fR options.
.RE
The following \fIoptag\fR values are supported: 
.sp
.ne 2
.mk
.na
\fB\fBALL_NODEIDS\fR\fR
.ad
.sp .6
.RS 4n
Generates on successive lines the numeric node identifiers
of all nodes in the cluster.
.RE

.sp
.ne 2
.mk
.na
\fB\fBALL_NODENAMES\fR\fR
.ad
.sp .6
.RS 4n
Generates on successive lines the names of all nodes
in the cluster.
.RE

.sp
.ne 2
.mk
.na
\fB\fBALL_NONGLOBAL_ZONES\fR\fR
.ad
.sp .6
.RS 4n
Generates on successive lines the \fBnodename:zonename\fR string of all the non-global zones on all nodes in the
cluster.
.sp
Only
if the following conditions occur is a non-global zone included in
the output of this query:
.RS +4
.TP
.ie t \(bu
.el o
The non-global zone booted at least once since the
cluster was brought online.
.RE
.RS +4
.TP
.ie t \(bu
.el o
The non-global zone successfully started the Service
Management Facility (SMF) service \fB/system/cluster/sc_ng_zones\fR.
.RE
Non-global
zones that do not execute the SMF service \fB/system/cluster/sc_ng_zones\fR cannot master resource groups, and are therefore not included
in the output.
.RE

.sp
.ne 2
.mk
.na
\fB\fBALL_NONGLOBAL_ZONES_NODEID\fR\fR
.ad
.sp .6
.RS 4n
Generates on successive lines the \fBnodename:zonename\fR string of all non-global zones on the cluster node whose
numeric node identifier is given as the argument.
.sp
Only if the following conditions occur is a non-global zone included
in the output of this query:
.RS +4
.TP
.ie t \(bu
.el o
The non-global zone booted at least once since the
cluster was brought online.
.RE
.RS +4
.TP
.ie t \(bu
.el o
The non-global zone successfully started the Service
Management Facility (SMF) service \fB/system/cluster/sc_ng_zones\fR.
.RE
Non-global zones that do not execute the SMF
service \fB/system/cluster/sc_ng_zones\fR cannot master
resource groups, and are therefore not included in the output.
.RE

.sp
.ne 2
.mk
.na
\fB\fBALL_PRIVATELINK_HOSTNAMES\fR\fR
.ad
.sp .6
.RS 4n
Generates on successive lines the host names by which
all cluster nodes are addressed on the cluster interconnect.
.RE

.sp
.ne 2
.mk
.na
\fB\fBALL_RESOURCEGROUPS\fR\fR
.ad
.sp .6
.RS 4n
Generates on successive lines the names of all the
resource groups that are being managed in the cluster.
.RE

.sp
.ne 2
.mk
.na
\fB\fBALL_RESOURCETYPES\fR\fR
.ad
.sp .6
.RS 4n
Generates on successive lines the names of all the
resource types that are registered in the cluster.
.RE

.sp
.ne 2
.mk
.na
\fB\fBALL_ZONES\fR\fR
.ad
.sp .6
.RS 4n
Generates on successive lines the \fBnodename:zonename\fR string of all zones, including the global zone, on all
nodes in the cluster.
.sp
Only if the following conditions occur is a non-global zone included
in the output of this query:
.RS +4
.TP
.ie t \(bu
.el o
The non-global zone booted at least once since the
cluster was brought online.
.RE
.RS +4
.TP
.ie t \(bu
.el o
The non-global zone successfully started the Service
Management Facility (SMF) service \fB/system/cluster/sc_ng_zones\fR.
.RE
Non-global zones that do not execute the SMF
service \fB/system/cluster/sc_ng_zones\fR cannot master
resource groups, and are therefore not included in the output.
.RE

.sp
.ne 2
.mk
.na
\fB\fBALL_ZONES_NODEID\fR\fR
.ad
.sp .6
.RS 4n
Generates on successive lines the \fBnodename:zonename\fR string of all zones, including the global zone, on the
cluster node whose numeric node identifier is given as the argument.
.sp
Only
if the following conditions occur is a non-global zone included in
the output of this query:
.RS +4
.TP
.ie t \(bu
.el o
The non-global zone booted at least once since the
cluster was brought online.
.RE
.RS +4
.TP
.ie t \(bu
.el o
The non-global zone successfully started the Service
Management Facility (SMF) service \fB/system/cluster/sc_ng_zones\fR.
.RE
Non-global zones that do not execute the SMF
service \fB/system/cluster/sc_ng_zones\fR cannot master
resource groups, and are therefore not included in the output.
.RE

.sp
.ne 2
.mk
.na
\fB\fBCLUSTERNAME\fR\fR
.ad
.sp .6
.RS 4n
Generates the name of the cluster.
.RE

.sp
.ne 2
.mk
.na
\fB\fBNODEID_LOCAL\fR\fR
.ad
.sp .6
.RS 4n
Generates the numeric node identifier for the node
where the command is executed.
.RE

.sp
.ne 2
.mk
.na
\fB\fBNODEID_NODENAME\fR\fR
.ad
.sp .6
.RS 4n
Generates the numeric node identifier of the node
indicated by the name. Requires an additional unflagged argument that
is the name of a cluster node.
.RE

.sp
.ne 2
.mk
.na
\fB\fBNODENAME_LOCAL\fR\fR
.ad
.sp .6
.RS 4n
Generates the name of the cluster node where the command
is executed.
.RE

.sp
.ne 2
.mk
.na
\fB\fBNODENAME_NODEID\fR\fR
.ad
.sp .6
.RS 4n
Generates the name of the cluster node indicated by
the numeric identifier. Requires an additional unflagged argument
that is a numeric cluster node identifier.
.RE

.sp
.ne 2
.mk
.na
\fB\fBNODESTATE_LOCAL\fR\fR
.ad
.sp .6
.RS 4n
Generates \fBUP\fR or \fBDOWN\fR depending
on the state of the node where the command is executed.
.RE

.sp
.ne 2
.mk
.na
\fB\fBNODESTATE_NODE\fR\fR
.ad
.sp .6
.RS 4n
Generates \fBUP\fR or \fBDOWN\fR depending
on the state of the named node. Requires an additional unflagged argument
that is the name of a cluster node.
.RE

.sp
.ne 2
.mk
.na
\fB\fBPRIVATELINK_HOSTNAME_LOCAL\fR\fR
.ad
.sp .6
.RS 4n
Generates the host name by which the node on which
the command is run is addressed over the cluster interconnect.
.RE

.sp
.ne 2
.mk
.na
\fB\fBPRIVATELINK_HOSTNAME_NODE\fR\fR
.ad
.sp .6
.RS 4n
Generates the host name by which the named node is
addressed on the cluster interconnect. Requires an additional unflagged
argument that is the name of a cluster node.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSYSLOG_FACILITY\fR\fR
.ad
.sp .6
.RS 4n
Generates the number of the \fBsyslog\fR(3C) facility that the
RGM uses for log messages. The value is \fB24\fR, which
corresponds to the \fBdaemon\fR facility. You can use
this value as the facility level in the \fBlogger\fR(1) command to log messages
in the cluster log.
.RE

.sp
.ne 2
.mk
.na
\fB\fBZONE_LOCAL\fR\fR
.ad
.sp .6
.RS 4n
Generates the name of the zone from which the command
is issued. The format of the zone name that is returned is \fBnodename:zonename\fR.
.RE

.RE

.SH EXAMPLES
.LP
\fBExample 1 \fRUsing the \fBscha_cluster\fR Command
in a Shell Script
.sp
.LP
The following shell script uses the \fBscha_cluster\fR command
to print whether each cluster node is up or down:

.sp
.in +2
.nf
#!/bin/sh
nodenames=`scha_cluster_get -O All_Nodenames`
for node in $nodenames
do
    state=`scha_cluster_get -O NodeState_Node $node`
    printf "State of node: %s\n exit: %d\n value: %s\n" "$node" $? "$state"
done
.fi
.in -2

.SH EXIT STATUS
.sp
.LP
The following exit status codes are returned: 
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 13n
.rt  
Successful completion.
.RE

.sp
.ne 2
.mk
.na
\fBnonzero\fR
.ad
.RS 13n
.rt  
An error occurred.
.sp
Error codes are described in \fBscha_calls\fR(3HA).
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityStable
.TE

.SH SEE ALSO
.sp
.LP
\fBawk\fR(1), \fBlogger\fR(1), \fBsh\fR(1), \fBscha_cmds\fR(1HA), \fBsyslog\fR(3C), \fBscha_calls\fR(3HA), \fBscha_cluster_get\fR(3HA), \fBattributes\fR(5)
