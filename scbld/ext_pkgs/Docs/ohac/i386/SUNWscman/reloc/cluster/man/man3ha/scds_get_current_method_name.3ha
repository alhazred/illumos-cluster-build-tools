'\" te
.\" Copyright 2008 Sun Microsystems, Inc.
.\" All rights reserved. Use is subject to license terms.
.TH scds_get_current_method_name 3HA "15 Aug 2008" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scds_get_current_method_name \- retrieve
the last element of the path name by which a data service method was
called
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev
#include <rgm/libdsdev.h>

\fBconst char *\fR\fBscds_get_current_method_name\fR(\fBscds_handle_t\fR \fIhandle\fR);
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscds_get_current_method_name()\fR function
returns a pointer to a character string. This character string contains
the last element of the path by which a data service method was called.
.sp
.LP
See the \fBbasename\fR(3C) man page for more information.
.sp
.LP
The pointer to the character string points to memory that belongs
to the Data Service Development Library (DSDL). Do not modify this
memory. A call to \fBscds_close()\fR invalidates this
pointer.
.SH PARAMETERS
.sp
.LP
The following parameters are supported:
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
The handle that is returned from \fBscds_initialize\fR(3HA).
.RE

.SH ERRORS
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR \fR
.ad
.RS 28n
.rt  
The function succeeded.
.RE

.sp
.LP
See  \fBscha_calls\fR(3HA) for a description of other error codes.
.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBscds_close\fR(3HA), \fBscds_initialize\fR(3HA), \fBscha_calls\fR(3HA), \fBattributes\fR(5)
