'\" te
.\" Copyright 2008 Sun Microsystems, Inc.
.\" All rights reserved. Use is subject to license terms.
.TH scha_resource_setstatus 3HA "11 Sep 2008" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scha_resource_setstatus, scha_resource_setstatus_zone \- set resource
status functions
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR scha
#include <scha.h>

\fBscha_err_t\fR \fBscha_resource_setstatus\fR(\fBconst char *\fR\fIrname\fR, \fBconst char *\fR\fIrgname\fR,
    \fBscha_rsstatus_t\fR \fIstatus\fR, \fBconst char *\fR\fIstatus_msg\fR);
.fi

.LP
.nf
\fBscha_err_t\fR \fBscha_resource_setstatus_zone\fR(\fBconst char *\fR\fIrname\fR, \fBconst char *\fR\fIrgname\fR,
    \fBconst char *\fR\fIzonename\fR, \fBscha_rsstatus_t\fR \fIstatus\fR, \fBconst char *\fR\fIstatus_msg\fR);
.fi

.SH DESCRIPTION
.sp
.LP
The  \fBscha_resource_setstatus()\fR and \fBscha_resource_setstatus_zone()\fR functions set the \fBStatus\fR and \fBStatus_msg\fR properties of a resource that is managed by the Resource
Group Manager (RGM). A resource's monitor uses these functions to
indicate the resource's state as perceived by the monitor.
.sp
.LP
Use the \fBscha_resource_setstatus_zone()\fR function only
for resource types whose \fBGlobal_zone\fR property is
set to \fBTRUE\fR.This function is not needed if
the \fBGlobal_zone\fR property is set to \fBFALSE\fR.For
more information, see the \fBrt_properties\fR(5) man page.
.sp
.LP
The  \fIrname\fR argument names the resource
whose status is to be set.
.sp
.LP
The  \fIrgname\fR argument is the name
of the resource group that contains the resource.
.sp
.LP
The  \fIzonename\fR argument is the name
of the non-global zone in which the resource group is configured to
run. If the \fBGlobal_zone\fR property is set to \fBTRUE\fR, methods execute in the global zone even if the resource
group that contains the resource runs in a non-global zone.
.sp
.LP
The \fIstatus\fR argument is an \fBenum\fR value
of type \fBscha_rsstatus_t\fR: \fBSCHA_RSSTATUS_OK\fR, \fBSCHA_RSSTATUS_OFFLINE\fR, \fBSCHA_RSSTATUS_FAULTED\fR, \fBSCHA_RSSTATUS_DEGRADED\fR, or \fBSCHA_RSSTATUS_UNKNOWN\fR.
.sp
.LP
The \fIstatus-msg\fR argument is the new
value for the \fBStatus_msg\fR property. The \fIstatus-msg\fR argument can be \fINULL\fR.
.sp
.LP
A successful call to the \fBscha_resource_setstatus()\fR or \fBscha_resource_setstatus_zone()\fR function causes the \fBStatus\fR and \fBStatus_msg\fR properties of the resource
to be updated with the supplied values. The update of the resource
status is logged in the cluster system log and is accessible by cluster
administration tools.
.SH RETURN VALUES
.sp
.LP
The \fBscha_resource_setstatus()\fR and \fBscha_resource_setstatus_zone()\fR functions return the following values:
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 28n
.rt  
The function succeeded.
.RE

.sp
.ne 2
.mk
.na
\fBnonzero\fR
.ad
.RS 28n
.rt  
The function failed.
.RE

.SH ERRORS
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 28n
.rt  
The function succeeded.
.RE

.sp
.LP
See  \fBscha_calls\fR(3HA) for a description of other error codes.
.SH EXAMPLES
.LP
\fBExample 1 \fRUsing the \fBscha_resource_setstatus_zone()\fR Function
.sp
.in +2
.nf
#include <scha.h>

scha_err_t err_code;
const char *rname = "example_R";
const char *rgname = "example_RG";

err_code = scha_resource_setstatus_zone(rname, rgname,
          SCHA_RSSTATUS_OK, "No problems");
.fi
.in -2

.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/scha.h\fR\fR
.ad
.RS 36n
.rt  
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libscha.so\fR\fR
.ad
.RS 36n
.rt  
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBscha_resource_setstatus\fR(1HA), \fBscha_calls\fR(3HA), \fBscha_strerror\fR(3HA), \fBattributes\fR(5), \fBrt_properties\fR(5)
