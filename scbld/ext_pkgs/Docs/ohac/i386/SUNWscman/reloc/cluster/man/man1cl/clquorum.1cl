'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. Use is subject to license terms.
.TH clquorum 1CL "11 Sep 2008" "Sun Cluster 3.2" "Sun Cluster Maintenance Commands"
.SH NAME
clquorum, clq \- manage Sun Cluster quorum devices and properties
.SH SYNOPSIS
.LP
.nf
\fB/usr/cluster/bin/clquorum\fR  \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum \fIsubcommand\fR\fR 
\fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum \fIsubcommand\fR\fR 
[\fIoptions\fR] \fB-v\fR \fIdevicename\fR[ \&.\|.\|.]
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum add\fR  [\fB-a\fR] [\fB-t\fR \fItype\fR] [\fB-p\fR  \fIname\fR=\fIvalue\fR[,\&.\|.\|.]]
\fIdevicename\fR[ \&.\|.\|.]
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum add\fR  \fB-i\fR {- |  \fIclconfigfile\fR}
[\fB-t\fR \fItype\fR] [\fB-p\fR  \fIname\fR=\fIvalue\fR[,\&.\|.\|.]] {+ |  \fIdevicename\fR[ \&.\|.\|.]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum disable\fR  [\fB-t\fR  \fItype\fR[,\&.\|.\|.]]
{+ |  \fIdevicename\fR...}
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum enable\fR  [\fB-t\fR  \fItype\fR[,\&.\|.\|.]]
{+ |  \fIdevicename\fR[ \&.\|.\|.]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum export\fR  [\fB-o\fR  {- |  \fIclconfigfile\fR}]
[\fB-t\fR  \fItype\fR[,\&.\|.\|.]] {+ |  \fIdevicename\fR[ \&.\|.\|.]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum list\fR  [\fB-t\fR  \fItype\fR[,\&.\|.\|.]]
[\fB-n\fR  \fInode\fR[,\&.\|.\|.]] [+ |  \fIdevicename\fR[ \&.\|.\|.]]
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum remove\fR  [\fB-t\fR  \fItype\fR[,\&.\|.\|.]]
{+ |  \fIdevicename\fR[ \&.\|.\|.]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum reset\fR 
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum show\fR  [\fB-t\fR  \fItype\fR[,\&.\|.\|.]]
[\fB-n\fR  \fInode\fR[,\&.\|.\|.]] [+ |  \fIdevicename\fR[ \&.\|.\|.]]
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum status\fR  [\fB-t\fR  \fItype\fR[,\&.\|.\|.]]
[\fB-n\fR  \fInode\fR[,\&.\|.\|.]] [+ |  \fIdevicename\fR[ \&.\|.\|.]]
.fi

.SH DESCRIPTION
.sp
.LP
The \fBclquorum\fR command manages cluster quorum devices and cluster quorum properties. The \fBclq\fR command is the short form of the \fBclquorum\fR command. The \fBclquorum\fR command and the \fBclq\fR command are identical.
You can use either form of the command.
.sp
.LP
You can use some forms of this command
in a non-global zone. For more information about valid uses of this command
in zones, see the descriptions of the individual subcommands. For ease of administration, use this command in the global zone.
.sp
.LP
The general form of this command is as follows:
.sp
.LP
\fBclquorum\fR [\fIsubcommand\fR] [\fIoptions\fR] [\fIoperands\fR]
.sp
.LP
You can omit \fIsubcommand\fR only if \fIoptions\fR specifies the \fB-?\fR option or  the \fB-V\fR option.
.sp
.LP
Each option of this command has a long form and a short form. Both forms of each option are provided with the description of the option in the OPTIONS section of this man page.
.sp
.LP
Quorum devices are necessary to protect the cluster from split-brain and amnesia situations. (For information about split-brain and amnesia situations, see the section on quorum and quorum devices in the \fISun Cluster Concepts Guide for Solaris OS\fR.) Each quorum device
must be connected, either by a SCSI cable or through an IP network, to at least two nodes. 
.sp
.LP
A quorum device can be a shared SCSI storage device, a shared NAS storage device, or a quorum server. If the quorum device stores user data, you do not affect this data if you add or remove such a device as a quorum device. However, if you are using replicated storage devices, the quorum
device must be on an unreplicated volume. 
.sp
.LP
Both nodes and quorum devices participate in cluster quorum formation, unless the nodes and quorum devices are in the maintenance state. If a node or a quorum device is in the maintenance state, its vote count is always zero and it does not participate in quorum formation.
.sp
.LP
You can use the \fBclquorum\fR command to perform the following tasks:
.RS +4
.TP
.ie t \(bu
.el o
Add a quorum device to the Sun Cluster configuration
.RE
.RS +4
.TP
.ie t \(bu
.el o
Remove a quorum device from the Sun Cluster configuration
.RE
.RS +4
.TP
.ie t \(bu
.el o
Manage quorum properties
.RE
.SH SUBCOMMANDS
.sp
.LP
The following subcommands are supported:
.sp
.ne 2
.mk
.na
\fB\fBadd\fR\fR
.ad
.sp .6
.RS 4n
Adds the specified shared device as a quorum device. 
.sp
You can use this subcommand only in the global zone.
.sp
Each quorum device must be connected to at least two nodes in the cluster. The quorum device is added with connection paths in the cluster configuration to every node to which the device is connected. Later, if the connection between the quorum device and the cluster nodes changes, you must
update the paths. Update the paths by removing the quorum device and then adding it back to the configuration. This situation could arise if you add more nodes that are connected to the quorum device or if you disconnect the quorum device from one or more nodes. For more information about quorum
administration,  see the Sun Cluster administration guide.
.sp
Quorum devices have several types. See the \fB-t\fR option in the OPTIONS section for a complete description. The \fBshared_disk\fR type is the default type.
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR role-based access control (\fBRBAC\fR) authorization.  See \fBrbac\fR(5).
.sp
See also the description of the \fBremove\fR subcommand.
.RE

.sp
.ne 2
.mk
.na
\fB\fBdisable\fR\fR
.ad
.sp .6
.RS 4n
Puts a quorum device or node in the quorum maintenance state.
.sp
You can use this subcommand only in the global zone.
.sp
In the maintenance state, a shared device or node has a vote count of zero. This shared device or node no longer participates in quorum formation. In addition, for a node that is in the maintenance state, any quorum devices that are connected to the node have their vote counts decremented
by one.
.sp
This feature is useful when you need to shut down a node or a device for an extended period of time for maintenance. After a node boots back into the cluster, the node removes itself from maintenance mode unless the \fBinstallmode\fR is set.
.sp
You must shut down a node before you can put the node in the maintenance state.
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR \fBRBAC\fR authorization.  See \fBrbac\fR(5).
.sp
See also the description of the \fBenable\fR subcommand.
.RE

.sp
.ne 2
.mk
.na
\fB\fBenable\fR\fR
.ad
.sp .6
.RS 4n
Removes a quorum device or a node from the quorum maintenance state. 
.sp
You can use this subcommand only in the global zone.
.sp
The \fBenable\fR subcommand removes a quorum device or node from maintenance mode. The subcommand resets the configured quorum vote count of a quorum device or node to the default. The shared device or node can then participate in quorum formation. 
.sp
After resetting a quorum device, the vote count for the quorum device is changed to \fIN\fR-1. In this calculation, \fIN\fR is the number of nodes with nonzero vote counts that are connected to the device. After resetting a node, the vote count is
reset to its default. Then the quorum devices that are connected to the node have their vote counts incremented by one.
.sp
Unless the install mode setting \fBinstallmode\fR is enabled, the quorum configuration for each node is automatically enabled at boot time.
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR \fBRBAC\fR authorization.  See \fBrbac\fR(5).
.sp
See also the description of the \fBdisable\fR subcommand.
.RE

.sp
.ne 2
.mk
.na
\fB\fBexport\fR\fR
.ad
.sp .6
.RS 4n
Exports the configuration information for the cluster quorum.
.sp
You can use this subcommand only in the global zone.
.sp
If you specify a file by using the \fB-o\fR option, the configuration information is written to that file. If you do not specify a file, the information is written to standard output (\fBstdout\fR).
.sp
The \fBexport\fR subcommand does not modify any cluster configuration data.
.sp
Users other than superuser require \fBsolaris.cluster.read\fR \fBRBAC\fR authorization.  See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fBlist\fR\fR
.ad
.sp .6
.RS 4n
Displays the names of quorum devices that are configured in the cluster.
.sp
You can use this subcommand in the global zone or in a non-global zone. For ease of administration, use this form of the command in the global zone.
.sp
If you do not specify options, the \fBlist\fR subcommand displays all the quorum devices that are configured in the cluster. If you specify the \fB-t\fR option, the subcommand displays only quorum devices of the specified type. If you specify the \fB-n\fR option,
the subcommand displays the names of all quorum devices that are connected to any of the specified nodes. 
.sp
Users other than superuser require \fBsolaris.cluster.read\fR \fBRBAC\fR authorization.  See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fBremove\fR\fR
.ad
.sp .6
.RS 4n
Removes the specified quorum device or devices from the Sun Cluster quorum configuration.
.sp
You can use this subcommand only in the global zone.
.sp
The \fBremove\fR subcommand does not disconnect and remove the physical device. The subcommand also does not affect the user data on the device, if any data exists. The last quorum device in a two-node cluster cannot be removed, unless the \fBinstallmode\fR is enabled. 
.sp
You can remove only a quorum device. You cannot use this subcommand to remove cluster nodes.
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR \fBRBAC\fR authorization.  See \fBrbac\fR(5).
.sp
See also the description of the \fBadd\fR subcommand.
.RE

.sp
.ne 2
.mk
.na
\fB\fBreset\fR\fR
.ad
.sp .6
.RS 4n
Resets the entire quorum configuration to the default vote count settings.
.sp
You can use this subcommand only in the global zone.
.sp
If \fBinstallmode\fR is enabled, the mode is cleared by resetting. \fBinstallmode\fR cannot be reset on a two-node cluster unless at least one quorum device has been successfully configured.
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR \fBRBAC\fR authorization.  See \fBrbac\fR(5).
.sp
See also  the \fB-p\fR option in \fBcluster\fR(1CL) for the description of the \fBinstallmode\fR property.
.RE

.sp
.ne 2
.mk
.na
\fB\fBshow\fR\fR
.ad
.sp .6
.RS 4n
Displays the properties of quorum devices. 
.sp
You can use this subcommand only in the global zone.
.sp
 If you do not specify options, the \fBshow\fR subcommand displays the properties of all the quorum devices in the cluster. 
.sp
If you specify the type by using the \fB-t\fR option, the subcommand displays properties of devices of that type only. See \fB-t\fR in OPTIONS.
.sp
If you specify nodes by using the \fB-n\fR option, this subcommand displays the properties of the quorum devices that are connected to any of the specified nodes. 
.sp
Users other than superuser require \fBsolaris.cluster.read\fR \fBRBAC\fR authorization.  See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fBstatus\fR\fR
.ad
.sp .6
.RS 4n
Displays the status and vote counts of quorum devices. 
.sp
You can use this subcommand in the global zone or in a non-global zone. For ease of administration, use this form of the command in the global zone.
.sp
If you do not specify options, the \fBstatus\fR subcommand displays information about all the quorum devices in the cluster. 
.sp
If you specify the type by using the \fB-t\fR option, the subcommand displays information about devices of that type only. See \fB-t\fR in OPTIONS.
.sp
If you specify nodes by using the \fB-n\fR option, this subcommand displays the properties of the quorum devices that are connected to any of the specified nodes. 
.sp
Users other than superuser require \fBsolaris.cluster.read\fR \fBRBAC\fR authorization.  See \fBrbac\fR(5).
.RE

.SH OPTIONS
.sp
.LP
The following options are supported:
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB-\fB-help\fR\fR
.ad
.sp .6
.RS 4n
Displays help information. When this option is used, no other processing is performed.
.sp
You can specify this option without a subcommand or with a subcommand.
.sp
If you specify this option without a subcommand, the list of subcommands of this command is displayed.
.sp
If you specify this option with a subcommand, the usage options for the subcommand are displayed.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-a\fR\fR
.ad
.br
.na
\fB\fB--autoconfig\fR\fR
.ad
.sp .6
.RS 4n
For a two-node cluster that uses shared disks, automatically chooses and configures one quorum device if no quorum devices are configured.
.sp
All shared disks in the cluster must be qualified to be a quorum device. The \fBautoconfig\fR subcommand does not check whether an available device is qualified to be a quorum device. The \fBautoconfig\fR subcommand checks only for shared disks.
.sp
Users other than superuser require \fBsolaris.cluster.modify\fR \fBRBAC\fR authorization.  See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fB-i\fR \fIclconfigfile\fR\fR
.ad
.br
.na
\fB-\fB-input=\fR\fIclconfigfile\fR\fR
.ad
.br
.na
\fB-\fB-input \fR\fIclconfigfile\fR\fR
.ad
.sp .6
.RS 4n
Specifies configuration information that is to be used for managing the quorum devices. This information must conform to the format that is defined in the \fBclconfiguration\fR(5CL) man page.
.sp
When \fB-i\fR is used with a subcommand along with other command-line options, the arguments of the command-line options overwrite the settings in the configuration file.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-n\fR \fInode-name\fR\fR
.ad
.br
.na
\fB-\fB-node=\fR\fInode-name\fR\fR
.ad
.br
.na
\fB-\fB-node \fR\fInode-name\fR\fR
.ad
.sp .6
.RS 4n
Specifies the node name to which the quorum devices are connected. This option is used in the \fBlist\fR, \fBstatus\fR, and \fBshow\fR subcommands to limit the information that is displayed to those quorum devices that are connected to the
specified nodes.
.sp
You can specify either a node name or a node ID for the \fInode-name\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-o\fR {- | \fIclconfigfile\fR}\fR
.ad
.br
.na
\fB-\fB-output={- | \fR\fIclconfigfile\fR\fB-}\fR\fR
.ad
.br
.na
\fB-\fB-output {- | \fR\fIclconfigfile\fR\fB-}\fR\fR
.ad
.sp .6
.RS 4n
Writes quorum-device-configuration information to a file or to the standard output (\fBstdout\fR). The format of this configuration information conforms to the format that is described in the \fBclconfiguration\fR(5CL) man page. To specify the standard output, specify \fB-\fR instead of a file name.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-p\fR \fIname\fR=\fIvalue\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB-\fB-property \fR\fIname\fR\fB-=\fR\fIvalue\fR\fB-[,...]\fR\fR
.ad
.br
.na
\fB-\fB-property \fR\fIname\fR\fB- \fR\fIvalue\fR\fB-[,...]\fR\fR
.ad
.sp .6
.RS 4n
Specifies properties of a quorum device that are specific to a device type.   You use this option with the \fBadd\fR subcommand. See the description of the \fB-t\fR option for a list and a description of these properties.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-t\fR \fIdevice-type\fR\fR
.ad
.br
.na
\fB-\fB-type=\fR\fIdevice-type\fR\fR
.ad
.br
.na
\fB-\fB-type \fR\fIdevice-type\fR\fR
.ad
.sp .6
.RS 4n
Specifies the quorum device type. When this option is specified, the operands must be of the specified type.
.sp
For the \fBadd\fR, \fBexport\fR, and \fBremove\fR subcommands, the current supported quorum device types are as follows:
.RS +4
.TP
.ie t \(bu
.el o
Shared local disks, specified by \fBshared_disk\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
Network Attached Storage device from Network Appliance, Inc., specified by \fBnetapp_nas\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
A quorum server process that runs on the Sun Cluster Quorum Server machine, specified by \fBquorum_server\fR
.RE
The default type is \fBshared_disk\fR.
.sp
The \fBadd\fR subcommand does not accept \fB-t\fR\fBnode\fR as a quorum type.
.sp
For the \fBenable\fR, \fBdisable\fR, \fBlist\fR, \fBshow\fR, and \fBstatus\fR subcommands, you can specify the types \fBnode\fR, \fBshared_disk\fR, \fBnetapp_nas\fR, or \fBquorum_server\fR. These different types of quorum devices have the following properties:
.sp
.ne 2
.mk
.na
\fB\fBnetapp_nas\fR\fR
.ad
.sp .6
.RS 4n
 The \fBnetapp_nas\fR type of quorum device has the following properties:
.sp
\fBfiler=\fIfiler-name\fR\fR: Specifies the name of the device on the network. The cluster uses this name to access the NAS device. 
.sp
\fBlun_id=\fIlun-id\fR\fR: Specifies the logical unit number (LUN) on the NAS device that will be a NAS quorum device. The \fBlun_id\fR property defaults to 0. If you have configured LUN 0 on your device for the quorum device, you do not need
to specify this property.
.sp
These properties are required when using the \fBadd\fR subcommand to add a NAS device as a quorum device. 
.sp
Before you can add a quorum device, the NAS device must be set up and operational. The NAS device must be already booted and running, and the LUN to be used as a NAS quorum must be already created.
.sp
To provide support for Network Appliance, Inc. NAS devices as quorum devices, you must install the quorum device support module that Network Appliance, Inc. provides. If the module is not installed on the cluster nodes, the \fBadd\fR subcommand fails . See \fISun Cluster
With Network-Attached Storage Devices Manual for Solaris OS\fR for instructions about obtaining the support module. Additionally, the iSCSI license must be valid for the Network Appliance, Inc. NAS device.
.sp
After the cluster administrator performs the required procedures, you can use the \fBclquorum add\fR subcommand to add the NAS device as a quorum device.
.RE

.sp
.ne 2
.mk
.na
\fB\fBnode\fR\fR
.ad
.sp .6
.RS 4n
No specific properties are set for nodes to participate in quorum formation.
.sp
This type is used  only with the \fBenable\fR, \fBdisable\fR, \fBlist\fR, \fBstatus\fR, and \fBshow\fR subcommands. It cannot be used to add a quorum device of type \fBnode\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBquorum_server\fR\fR
.ad
.sp .6
.RS 4n
 The \fBquorum_server\fR type of quorum device has the following properties:
.sp
\fBqshost=\fIquorum-server-host\fR\fR: Specifies the name of the machine where the quorum server runs. This host can be the IP address of the machine or the hostname on the network. If you specify the hostname, the IP address of the machine must be specified
in the \fB/etc/hosts\fR file, the \fB/etc/inet/ipnodes\fR file, or both.
.sp
\fBport=\fIport\fR\fR: Specifies the port number used by the quorum server to communicate with the cluster nodes.
.sp
Before you can add a quorum server, the quorum server software must be installed on the host machine and the quorum server must be started and running. Refer to the \fBSun Cluster Quorum Server User Guide\fR for details.
.RE

.sp
.ne 2
.mk
.na
\fB\fBshared_disk\fR \fR
.ad
.sp .6
.RS 4n
No specific properties are set for \fBshared_disk\fR quorum devices. The \fBautoconfig\fR subcommand accepts only this quorum device type.
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB-\fB-version\fR\fR
.ad
.sp .6
.RS 4n
Displays the version of the command.
.sp
Do not specify this option with other subcommands, options, or operands. The  subcommands, options, or operands are ignored. The \fB-V\fR option displays only the version of the command. No other operations are performed.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR\fR
.ad
.br
.na
\fB-\fB-verbose\fR\fR
.ad
.sp .6
.RS 4n
Displays verbose information to standard output (\fBstdout\fR).
.RE

.SH OPERANDS
.sp
.LP
The following operands are supported:
.sp
.ne 2
.mk
.na
\fB\fIdevicename\fR\fR
.ad
.sp .6
.RS 4n
For the \fBadd\fR, \fBexport\fR, and \fBremove\fR subcommands only, the operand is the name of a shared disk (SCSI, quorum server, or NAS quorum device). For the \fBadd\fR subcommand, if you do not specify a \fBclconfigurationfile\fR by
using \fB-i\fR, you must specify at least one quorum device as the operand.
.sp
For the \fBdisable\fR, \fBenable\fR, \fBlist\fR, \fBstatus\fR, and \fBshow\fR subcommands only, the operand can be the name of a node or of a shared disk (SCSI, quorum server, or NAS quorum device). 
.sp
In every case, the operand type must match the value of the \fB-t\fR option, if you specify that option. 
.sp
Use the following values as the \fIdevicename\fR operand:
.RS +4
.TP
.ie t \(bu
.el o
For nodes, the operand must be the node name or the node ID.
.RE
.RS +4
.TP
.ie t \(bu
.el o
For SCSI quorum devices, the operand must be the device identifier or the full DID path name, for example, \fBd1\fR or \fB/dev/did/rdsk/d1\fR. 
.RE
.RS +4
.TP
.ie t \(bu
.el o
For NAS quorum devices, the operand must be the device name as defined when you added the device to the cluster configuration. 
.RE
.RS +4
.TP
.ie t \(bu
.el o
For quorum server quorum devices, the operand must specify an identifier for the quorum server or servers. This can be the quorum server instance name, and must be unique across all quorum devices. 
.RE
.RE

.sp
.ne 2
.mk
.na
\fB+\fR
.ad
.sp .6
.RS 4n
For the \fBdisable\fR, \fBenable\fR, \fBlist\fR, \fBstatus\fR, and \fBshow\fR subcommands only, specifies all quorum devices configured for the cluster. If you use the \fB-t\fR option, the plus sign (\fB+\fR) operand specifies all devices of that type. 
.RE

.SH EXIT STATUS
.sp
.LP
If the command is successful for all specified operands, it 
returns zero (\fBCL_NOERR\fR). If an error occurs for an
operand, the command processes the next operand in the operand list. The
returned exit code always reflects the error that occurred first.
.sp
.LP
The following exit values can be returned:
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fBCL_NOERR\fR\fR
.ad
.sp .6
.RS 4n
No error
.sp
The command that you issued completed successfully.
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR \fBCL_ENOMEM\fR\fR
.ad
.sp .6
.RS 4n
Not enough swap space
.sp
A cluster node ran out of swap memory or ran out of other operating system resources.
.RE

.sp
.ne 2
.mk
.na
\fB\fB3\fR \fBCL_EINVAL\fR\fR
.ad
.sp .6
.RS 4n
Invalid argument
.sp
You typed the command incorrectly, or the syntax of the cluster
configuration information that you supplied with the \fB-i\fR option
was incorrect.
.RE

.sp
.ne 2
.mk
.na
\fB\fB6\fR \fBCL_EACCESS\fR\fR
.ad
.sp .6
.RS 4n
Permission denied
.sp
The object that you specified is inaccessible. You might need superuser 
or RBAC access to issue the command. See the 
\fBsu\fR(1M) 
and 
\fBrbac\fR(5) 
man pages for more information.
.RE

.sp
.ne 2
.mk
.na
\fB\fB18\fR \fBCL_EINTERNAL\fR\fR
.ad
.sp .6
.RS 4n
Internal error was encountered
.sp
An internal error indicates a software defect or other defect.
.RE

.sp
.ne 2
.mk
.na
\fB\fB35\fR \fBCL_EIO\fR\fR
.ad
.sp .6
.RS 4n
I/O error
.sp
A physical input/output error has occurred.
.RE

.sp
.ne 2
.mk
.na
\fB\fB36\fR \fBCL_ENOENT\fR\fR
.ad
.sp .6
.RS 4n
No such object
.sp
The object that you specified cannot be found for one of the following reasons:
.RS +4
.TP
.ie t \(bu
.el o
The object does not exist.
.RE
.RS +4
.TP
.ie t \(bu
.el o
A directory in the path to the configuration file that you attempted to create with the \fB-o\fR option does not exist.
.RE
.RS +4
.TP
.ie t \(bu
.el o
The configuration file that you attempted to access with the \fB-i\fR option contains errors.
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fB39\fR \fBCL_EEXIST\fR\fR
.ad
.sp .6
.RS 4n
Object exists
.sp
The device, device group, cluster interconnect component, node, cluster, resource, resource type, or resource group that you specified already exists.
.RE

.sp
.ne 2
.mk
.na
\fB\fB41\fR \fBCL_ETYPE\fR\fR
.ad
.sp .6
.RS 4n
Invalid type
.sp
The type that you specified with the \fB-t\fR 
or \fB-p\fR option does not exist.
.RE

.SH EXAMPLES
.LP
\fBExample 1 \fRAdding a SCSI Quorum Device
.sp
.LP
The following \fBclquorum\fR command configures a SCSI quorum device that is connected to all the cluster nodes. 

.sp
.in +2
.nf
# \fBclquorum add /dev/did/rdsk/d4s2\fR
.fi
.in -2
.sp

.sp
.LP
When you use the \fBadd\fR subcommand, the \fBshared_disk\fR type is the default. To add a \fBshared_disk\fR quorum device, you do not need to specify \fB-t\ shared_disk\fR.

.LP
\fBExample 2 \fRAdding a Network Appliance NAS Quorum Device
.sp
.LP
The following \fBclquorum\fR command adds the Network Appliance NAS quorum device \fBqd1\fR that is connected to all the cluster nodes.

.sp
.in +2
.nf
# \fBclquorum -t netapp_nas -p filer=nas1.sun.com -p lun_id=0 qd1\fR
.fi
.in -2
.sp

.sp
.LP
The name of the NAS quorum device must be unique across all cluster quorum devices.

.LP
\fBExample 3 \fRAdding a Quorum Server
.sp
.LP
The following \fBclquorum\fR command configures a quorum server, \fBqs1\fR:

.sp
.in +2
.nf
# \fBclquorum add -t quorum_server -p qshost=10.11.114.81 -p port=9000 qs1\fR
.fi
.in -2
.sp

.LP
\fBExample 4 \fRRemoving a Quorum Device
.sp
.LP
The following \fBclquorum\fR command removes the \fBd4\fR quorum device.

.sp
.in +2
.nf
# \fBclquorum remove d4\fR
.fi
.in -2
.sp

.sp
.LP
The command that you use to remove a quorum device is the same, whether your device has a type of \fBshared_disk\fR, \fBnas\fR, or \fBquorum_server\fR. 

.LP
\fBExample 5 \fRPutting a Quorum Device into a Maintenance State
.sp
.LP
The following \fBclquorum\fR command puts a quorum device, \fBqs1\fR into a maintenance state and verifies that the device is in a maintenance state.

.sp
.in +2
.nf
# \fBclquorum disable qs1\fR
# \fBclquorum status qs1\fR
\ 
=== Cluster Quorum ===

--- Quorum Votes by Device ---

Device Name       Present      Possible      Status
-----------       -------      --------      ------
qs1                1            1             Offline
.fi
.in -2
.sp

.LP
\fBExample 6 \fRResetting the Quorum Votes of a Quorum Device
.sp
.LP
The following \fBclquorum\fR command resets the configured quorum vote count of a quorum device, \fBd4\fR, to the default.

.sp
.in +2
.nf
# \fBclquorum enable d4\fR
.fi
.in -2
.sp

.LP
\fBExample 7 \fRDisplaying the Configured Quorum Devices in the Cluster
.sp
.LP
The following \fBclquorum\fR commands display the quorum devices in concise format and verbose format.

.sp
.in +2
.nf
# \fBclquorum list\fR
d4
qd1
pcow1
pcow2
.fi
.in -2
.sp

.sp
.in +2
.nf
# \fBclquorum list -v\fR
Quorums               Type
-------               ----
d4                    shared_disk
qd1                   netapp_nas
pcow1                 node
pcow2                 node
.fi
.in -2
.sp

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWsczu
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBIntro\fR(1CL),  \fBcluster\fR(1CL), \fBclconfiguration\fR(5CL)
.SH NOTES
.sp
.LP
The superuser can run all forms of this command.
.sp
.LP
Any user can run this command with the following options:
.RS +4
.TP
.ie t \(bu
.el o
\fB-?\fR option
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-V\fR option
.RE
.sp
.LP
To run this command with other subcommands, users other than superuser require \fBRBAC\fR authorizations. See the following table.
.sp

.sp
.TS
tab() box;
cw(0i) |cw(5.5i) 
lw(0i) |lw(5.5i) 
.
Subcommand\fBRBAC\fR Authorization
_
\fBadd\fR\fBsolaris.cluster.modify\fR
_
\fBdisable\fR\fBsolaris.cluster.modify\fR
_
\fBenable\fR\fBsolaris.cluster.modify\fR
_
\fBexport\fR\fBsolaris.cluster.read\fR
_
\fBlist\fR\fBsolaris.cluster.read\fR
_
\fBremove\fR\fBsolaris.cluster.modify\fR
_
\fBreset\fR\fBsolaris.cluster.modify\fR
_
\fBshow\fR\fBsolaris.cluster.read\fR
_
\fBstatus\fR\fBsolaris.cluster.read\fR
.TE

