'\" te
.\" Copyright 2007 Sun Microsystems, Inc. All rights
.\" reserved. Use is subject to license terms.
.TH scha_resourcegroup_open 3HA "7 Sep 2007" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scha_resourcegroup_open, scha_resourcegroup_close, scha_resourcegroup_get \- resource information
access functions
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR scha
#include <scha.h>

\fBscha_err_t\fR \fBscha_resourcegroup_open\fR(\fBconst char *\fR\fIrgname\fR, \fBscha_resourcegroup_t *\fR\fIhandle\fR);
.fi

.LP
.nf
\fBscha_err_t\fR \fBscha_resourcegroup_close\fR(\fBscha_resourcegroup_t\fR \fIhandle\fR);
.fi

.LP
.nf
\fBscha_err_t\fR \fBscha_resourcegroup_get\fR(\fBscha_resourcegroup_t\fR \fIhandle\fR, \fBconst char *\fR\fItag\fR...);
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscha_resourcegroup_open()\fR, \fBscha_resourcegroup_get()\fR, and \fBscha_resourcegroup_close()\fR functions are
used together to access information about a resource group that is managed
by the Resource Group Manager (RGM) cluster facility.
.sp
.LP
\fBscha_resourcegroup_open()\fR initializes access of the
resource group and returns a handle to be used by \fBscha_resourcegroup_get()\fR. 
.sp
.LP
The \fIrgname\fR argument names the resource group
to be accessed.
.sp
.LP
The \fIhandle\fR argument is the address of a variable
to hold the value that is returned by the function.
.sp
.LP
\fBscha_resourcegroup_get()\fR accesses resource group
information as indicated by the \fItag\fR argument. The \fItag\fR should be a string value that is defined by a macro in the \fBscha_tags.h\fR header file. Arguments following the tag depend on
the value of \fItag\fR. An additional argument following
the tag might be needed to indicate a cluster node from which the information
is to be retrieved.
.sp
.LP
The last argument in the argument list is to be of a type suitable to
hold the information indicated by \fItag\fR. This parameter
is the output argument for the resource group information that is to be retrieved.
No value is returned for the output argument if the function fails. Memory
that is allocated to hold information that is returned by \fBscha_resourcegroup_get()\fR remains intact until \fBscha_resourcegroup_close()\fR is
called on the handle that is used for \fBscha_resourcegroup_get()\fR.
.sp
.LP
\fBscha_resourcegroup_close()\fR takes a \fIhandle\fR argument
that is returned from a previous call to \fBscha_resourcegroup_open()\fR.
It invalidates the handle and frees memory that is allocated to return values
to \fBscha_resourcegroup_get()\fR calls that were made with the
handle. Note that memory, if needed to return a value, is allocated for each \fBget\fR call. Space that is allocated to return a value in one call
is not overwritten or reused by subsequent calls.
.SS "Macros That You Can Use for \fItag\fR Arguments"
.sp
.LP
 You can use the following macros that are defined in \fBscha_tags.h\fR as \fItag\fR arguments to the \fBscha_resourcegroup_get()\fR function. These macros name resource group properties. The value
of the property of the resource group is generated. The \fBRG_STATE\fR property
refers to the value on the node or zone where the function is called.
.sp
.LP
The type of the output argument and any additional arguments are indicated.
Structure and \fBenum\fR types are described in the \fBscha_calls\fR(3HA) man page.
.sp
.ne 2
.mk
.na
\fB\fBSCHA_DESIRED_PRIMARIES\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBint*\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_FAILBACK\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBboolean_t*\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_GLOBAL_RESOURCES_USED\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBscha_str_array_t**\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_IMPL_NET_DEPEND\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBboolean_t*\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_MAXIMUM_PRIMARIES\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBint*\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_NODELIST\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBscha_str_array_t**\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_PATHPREFIX\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBchar**\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_PINGPONG_INTERVAL\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBint*\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RESOURCE_LIST\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBscha_str_array_t**\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_AFFINITIES\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBchar**\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_AUTO_START\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBboolean_t*\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_DEPENDENCIES\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBscha_str_array_t**\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_DESCRIPTION\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBchar**\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_IS_FROZEN\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBboolean_t*\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_MODE\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBscha_rgmode_t*\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_PROJECT_NAME\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBchar**\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_SLM_CPU\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBchar**\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_SLM_CPU_MIN\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBchar**\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_SLM_PSET_TYPE\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBchar**\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_SLM_TYPE\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBchar**\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_STATE\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBscha_rgstate_t*\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_STATE_NODE\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBscha_rgstate_t*\fR.
An additional argument type is \fBchar*\fR. The additional argument
names a cluster node and returns the state of the resource group on that node.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_SUSP_AUTO_RECOVERY\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBboolean_t*\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_SYSTEM\fR\fR
.ad
.sp .6
.RS 4n
The output argument type is \fBboolean_t*\fR.
.RE

.SH RETURN VALUES
.sp
.LP
These functions return the following:
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
The function succeeded.
.RE

.sp
.ne 2
.mk
.na
\fBnonzero\fR
.ad
.RS 20n
.rt  
The function failed.
.RE

.SH ERRORS
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR \fR
.ad
.RS 36n
.rt  
The function succeeded.
.RE

.sp
.LP
See \fBscha_calls\fR(3HA) for a description
of other error codes.
.SH EXAMPLES
.LP
\fBExample 1 \fRUsing the \fBscha_resourcegroup_get()\fR Function
.sp
.LP
The following example uses \fBscha_resourcegroup_get()\fR to
get the list of resources in the resource group \fBexample_RG\fR.

.sp
.in +2
.nf
main() {
  #include <scha.h>

  scha_err_t err;
  scha_str_array_t *resource_list;
  scha_resourcegroup_t handle;
  int ix;

  char * rgname = "example_RG";	

  err = scha_resourcegroup_open(rgname, &handle);

  err = scha_resourcegroup_get(handle, SCHA_RESOURCE_LIST, \e
        &resource_list);

  if (err == SCHA_ERR_NOERR) {
   for (ix = 0; ix < resource_list->array_cnt; ix++) {
       printf("Group: %s contains resource %s\n", rgname,
               resource_list->str_array[ix]);
       }
   }

/* resource_list memory freed */
err = scha_resourcegroup_close(handle);	
}
.fi
.in -2

.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/scha.h\fR\fR
.ad
.RS 36n
.rt  
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libscha.so\fR\fR
.ad
.RS 36n
.rt  
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for
descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBscha_resourcegroup_get\fR(1HA) , \fBscha_calls\fR(3HA), \fBattributes\fR(5)
