'\" te
.\" Copyright 2008 Sun Microsystems, Inc.
.\" All rights reserved. Use is subject to license terms.
.TH scha_cmds 1HA "11 Sep 2008" "Sun Cluster 3.2" "Sun Cluster Commands"
.SH NAME
scha_cmds \- command standard output for \fBscha_cluster_get\fR, \fBscha_control\fR, \fBscha_resource_get\fR, \fBscha_resourcegroup_get\fR, \fBscha_resourcetype_get\fR, \fBscha_resource_setstatus\fR
.SH SYNOPSIS
.LP
.nf
 \fIscha-command\fR \fB-O\fR \fIoptag\fR...
.fi

.SH DESCRIPTION
.sp
.LP
The Sun Cluster \fBscha_cluster_get\fR(1HA), \fBscha_control\fR(1HA), \fBscha_resource_get\fR(1HA), \fBscha_resourcegroup_get\fR(1HA), \fBscha_resourcetype_get\fR(1HA), and \fBscha_resource_setstatus\fR(1HA) commands
are command-line implementations of the callback methods for resource
types. See \fBrt_callbacks\fR(1HA). 
.sp
.LP
Resource types represent services that are controlled by the
cluster's Resource Group Manager (\fBRGM\fR) facility.
These commands provide a command-line interface to the functionality
of the \fBscha_calls\fR(3HA) C
functions.
.sp
.LP
The \fBget\fR commands access cluster configuration
information. All of these commands have the same general interface.
These commands all take an \fB-O\fR \fIoptag\fR operand.
This operand indicates the information to be accessed. These commands
all send the results to the standard output (\fBstdout\fR)
as formatted strings. Additional arguments might be needed depending
on the command and the value of \fIoptag\fR.
For information about the format of different \fIoptag\fR results,
see the "Results Format" section.
.LP
Note - 
.sp
.RS 2
\fIoptag\fR options, for all \fBscha\fR commands, are \fBnot\fR case sensitive.
You can use any combination of uppercase and lowercase letters when
you specify \fIoptag\fR options.
.RE
.sp
.LP
The \fBscha_control\fR(1HA) command also takes an \fB-O\fR \fIoptag\fR option that indicates a control operation, but
does not produce output to standard output.
.sp
.LP
The \fBscha_resource_setstatus\fR(1HA) command sets the \fBSTATUS\fR and \fBSTATUS_MSG\fR properties of a resource that is managed by the
RGM.
.SS "Result Formats"
.sp
.LP
The format of strings that are output to the standard output
by the commands depends on the type of the result that is indicated
by the \fIoptag\fR that you include with the \fB-O\fR option. Formats for each type are specified in the following
table. Format notation is described in \fBformats\fR(5).
.sp

.sp
.TS
tab();
cw(2.2i) cw(3.3i) 
lw(2.2i) lw(3.3i) 
.
Result TypeFormat on Standard Output
\fBboolean\fR\fBTRUE\en\fR or \fBFALSE\en\fR
\fBenum\fR\fB%s\en\fR, the string name of an \fBenum\fR value.
\fBextension\fRT{
\fB%s\en\fR, the type attribute of the extension
property, which is one of the following values: \fBSTRING\fR, \fBINT\fR, \fBBOOLEAN\fR, \fBENUM\fR,
or \fBSTRINGARRAY\fR.Following the type information, the property value is output
according to the formats for each type as follows:  \fBSTRING\fR as \fBstring\fR, \fBINT\fR as \fBint\fR, \fBBOOLEAN\fR as \fBboolean\fR, \fBENUM\fR as \fBenum\fR,  \fBSTRINGARRAY\fR as \fBstring_array\fR.
T}
\fBint\fR\fB%d\en\fR
\fBstatus\fRT{
\fB%s\en%s\en\fR, the first string is the
status, which is one of the following \fBenum\fR values: \fBDEGRADED\fR, \fBFAULTED\fR, \fBOFFLINE\fR, \fBONLINE\fR, or \fBUNKNOWN\fR.The second string is the status message.
T}
\fBstring\fR\fB%s\en\fR
\fBstring_array\fRT{
Each element in the array is output in the format \fB%s\en\fR. An asterisk, indicating all nodes, zones, or resources,
can be returned for the \fBGLOBAL_RESOURCES_USED\fR and \fBINSTALLED_NODES\fR properties.
T}
\fBunsigned_int\fR\fB%u\en\fR
\fBunsigned_int_array\fRT{
Each element in the array is output in the format \fB%u\en\fR
T}
.TE

.SS "\fIoptag\fR Result Types"
.sp
.LP
The following table specifies the valid \fIoptag\fR values
for different commands as well as the type of the result that is output
according to the formats specified in the previous table.
.sp

.sp
.TS
tab();
cw(2.2i) cw(3.3i) 
lw(2.2i) lw(3.3i) 
.
\fIoptag\fR Values for \fBscha_cluster_get\fR(1HA) Result Type
\fBALL_NODEIDS\fR\fBunsigned_int_array\fR
\fBALL_NODENAMES\fR\fBstring_array\fR
\fBALL_NONGLOBAL_ZONES\fR\fBstring\fR
\fBALL_NONGLOBAL_ZONES_NODEID\fR\fBstring\fR
\fBALL_PRIVATELINK_HOSTNAMES\fR\fBstring_array\fR
\fBALL_RESOURCEGROUPS\fR\fBstring_array\fR
\fBALL_RESOURCETYPES\fR\fBstring_array\fR
\fBALL_ZONES\fR\fBstring\fR
\fBALL_ZONES_NODEID\fR\fBstring\fR
\fBCLUSTERNAME\fR\fBstring\fR
\fBNODEID_LOCAL\fR\fBunsigned_int\fR
\fBNODEID_NODENAME\fR\fBunsigned_int\fR
\fBNODENAME_LOCAL\fR\fBstring\fR
\fBNODENAME_NODEID\fR\fBstring\fR
\fBNODESTATE_LOCAL\fR\fBenum\fR (\fBUP\fR, \fBDOWN\fR)
\fBNODESTATE_NODE\fR\fBenum\fR (\fBUP\fR, \fBDOWN\fR)
\fBPRIVATELINK_HOSTNAME_LOCAL\fR\fBstring\fR
\fBPRIVATELINK_HOSTNAME_NODE\fR\fBstring\fR
\fBSYSLOG_FACILITY\fR\fBint\fR
\fBZONE_LOCAL\fR\fBstring\fR
.TE

.sp

.sp
.TS
tab();
cw(5.5i) 
lw(5.5i) 
.
\fIoptag\fR Values for \fBscha_control\fR(1HA) 
_
\fBCHANGE_STATE_OFFLINE\fR
\fBCHANGE_STATE_ONLINE\fR
\fBCHECK_GIVEOVER\fR
\fBCHECK_RESTART\fR
\fBGIVEOVER\fR
\fBIGNORE_FAILED_START\fR
\fBRESOURCE_DISABLE\fR
\fBRESOURCE_IS_RESTARTED\fR
\fBRESOURCE_RESTART\fR
\fBRESTART\fR
.TE

.sp

.sp
.TS
tab();
cw(2.2i) cw(3.3i) 
lw(2.2i) lw(3.3i) 
.
\fIoptag\fR Values for \fBscha_resource_get\fR(1HA) Result Type
\fBAFFINITY_TIMEOUT\fR\fBint\fR
\fBALL_EXTENSIONS\fR\fBstring_array\fR
\fBBOOT_TIMEOUT\fR\fBint\fR
\fBCHEAP_PROBE_INTERVAL\fR\fBint\fR
\fBCHEAP_PROBE_INTERVAL\fR\fBint\fR
\fBEXTENSION\fR\fBextension\fR
\fBEXTENSION_NODE\fR\fBextension\fR
\fBFAILOVER_MODE\fRT{
\fBenum\fR (\fBNONE\fR, \fBHARD\fR, \fBSOFT\fR, \fBRESTART_ONLY\fR, \fBLOG_ONLY\fR )
T}
\fBFINI_TIMEOUT\fR\fBint\fR
\fBGROUP\fR\fBstring\fR
\fBINIT_TIMEOUT\fR\fBint\fR
\fBLOAD_BALANCING_POLICY\fR\fBstring\fR
\fBLOAD_BALANCING_WEIGHTS\fR\fBstring_array\fR
\fBMONITORED_SWITCH\fR\fBenum\fR (\fBDISABLED\fR, \fBENABLED\fR)
\fBMONITORED_SWITCH_NODE\fR\fBenum\fR (\fBDISABLED\fR, \fBENABLED\fR)
\fBMONITOR_CHECK_TIMEOUT\fR\fBint\fR
\fBMONITOR_START_TIMEOUT\fR\fBint\fR
\fBMONITOR_STOP_TIMEOUT\fR\fBint\fR
\fBNETWORK_RESOURCES_USED\fR\fBstring_array\fR
\fBNUM_RESOURCE_RESTARTS\fR\fBint\fR
\fBNUM_RESOURCE_RESTARTS_ZONE\fR\fBint\fR
\fBNUM_RG_RESTARTS\fR\fBint\fR
\fBNUM_RG_RESTARTS_ZONE\fR\fBint\fR
\fBON_OFF_SWITCH\fR\fBenum\fR (\fBDISABLED\fR, \fBENABLED\fR)
\fBON_OFF_SWITCH_NODE\fR\fBenum\fR (\fBDISABLED\fR, \fBENABLED\fR)
\fBPORT_LIST\fR\fBstring_array\fR
\fBPOSTNET_STOP_TIMEOUT\fR\fBint\fR
\fBPRENET_START_TIMEOUT\fR\fBint\fR
\fBR_DESCRIPTION\fR\fBstring\fR
\fBRESOURCE_DEPENDENCIES\fR\fBstring_array\fR
\fBRESOURCE_DEPENDENCIES_OFFLINE_RESTART\fR\fBstring_array\fR
\fBRESOURCE_DEPENDENCIES_RESTART\fR\fBstring_array\fR
\fBRESOURCE_DEPENDENCIES_WEAK\fR\fBstring_array\fR
\fBRESOURCE_PROJECT_NAME\fR\fBstring\fR
\fBRESOURCE_STATE\fRT{
\fBenum\fR (\fBONLINE\fR, \fBOFFLINE\fR, \fBSTART_FAILED\fR, \fBSTOP_FAILED\fR, \fBMONITOR_FAILED\fR, \fBONLINE_NOT_MONITORED\fR, \fBSTARTING\fR, \fBSTOPPING\fR)
T}
\fBRESOURCE_STATE_NODE\fR\fBenum\fR (see \fBRESOURCE_STATE\fR for
values)
\fBRETRY_COUNT\fR\fBint\fR
\fBRETRY_INTERVAL\fR\fBint\fR
\fBSCALABLE\fR\fBboolean\fR
\fBSTART_TIMEOUT\fR\fBint\fR
\fBSTATUS\fR\fBstatus\fR
\fBSTATUS_NODE\fR\fBstatus\fR
\fBSTOP_TIMEOUT\fR\fBint\fR
\fBTHOROUGH_PROBE_INTERVAL\fR\fBint\fR
\fBTYPE\fR\fBstring\fR
\fBTYPE_VERSION\fR\fBstring\fR
\fBUDP_AFFINITY\fR\fBboolean\fR
\fBUPDATE_TIMEOUT\fR\fBint\fR
\fBVALIDATE_TIMEOUT\fR\fBint\fR
\fBWEAK_AFFINITY\fR\fBboolean\fR
.TE

.sp

.sp
.TS
tab();
cw(2.2i) cw(3.3i) 
lw(2.2i) lw(3.3i) 
.
T{
\fIoptag\fR Values for \fBscha_resource_get\fR(1HA) and \fBscha_resourcetype_get\fR(1HA) 
T}Result Type
\fBAPI_VERSION\fR\fBint\fR
\fBBOOT\fR\fBstring\fR
\fBFAILOVER\fR\fBboolean\fR
\fBFINI\fR\fBstring\fR
\fBGLOBAL_ZONE\fR\fBboolean\fR
\fBINIT\fR\fBstring\fR
\fBINIT_NODES\fR\fBenum\fR (\fBRG_PRIMARIES\fR, \fBRT_INSTALLED_NODES\fR )
\fBINSTALLED_NODES\fRT{
\fBstring_array\fR. An asterisk (\fB*\fR)
is returned to indicate all nodes.
T}
\fBIS_LOGICAL_HOSTNAME\fR\fBboolean\fR
\fBIS_SHARED_ADDRESS\fR\fBboolean\fR
\fBMONITOR_CHECK\fR\fBstring\fR
\fBMONITOR_START\fR\fBstring\fR
\fBMONITOR_STOP\fR\fBstring\fR
\fBPER_NODE\fR\fBboolean\fR
\fBPKGLIST\fR\fBstring_array\fR
\fBPOSTNET_STOP\fR\fBstring\fR
\fBPRENET_START\fR\fBstring\fR
\fBPROXY\fR\fBboolean\fR
\fBRT_BASEDIR\fR\fBstring\fR
\fBRT_DESCRIPTION\fR\fBstring\fR
\fBRT_SYSTEM\fR\fBboolean\fR
\fBRT_VERSION\fR\fBstring\fR
\fBSINGLE_INSTANCE\fR\fBboolean\fR
\fBSTART\fR\fBstring\fR
\fBSTOP\fR\fBstring\fR
\fBUPDATE\fR\fBstring\fR
\fBVALIDATE\fR\fBstring\fR
.TE

.sp

.sp
.TS
tab();
cw(2.75i) cw(2.75i) 
lw(2.75i) lw(2.75i) 
.
T{
\fIoptag\fR Values for \fBscha_resourcegroup_get\fR(1HA)
T}Result Type
\fBAUTO_START_ON_NEW_CLUSTER\fR\fBboolean\fR
\fBDESIRED_PRIMARIES\fR\fBint\fR
\fBFAILBACK\fR\fBboolean\fR
\fBGLOBAL_RESOURCES_USED\fRT{
\fBstring_array\fR (an asterisk (\fB*\fR)
is returned to indicate all resources)
T}
\fBIMPLICIT_NETWORK_DEPENDENCIES\fR\fBboolean\fR
\fBMAXIMUM_PRIMARIES\fR\fBint\fR
\fBNODELIST\fR\fBstring_array\fR
\fBPATHPREFIX\fR\fBstring\fR
\fBPINGPONG_INTERVAL\fR\fBint\fR
\fBRESOURCE_LIST\fR\fBstring_array\fR
\fBRG_AFFINITIES\fR\fBstring_array\fR
\fBRG_DEPENDENCIES\fR\fBstring_array\fR
\fBRG_DESCRIPTION\fR\fBstring\fR
\fBRG_IS_FROZEN\fR\fBboolean\fR
\fBRG_MODE\fR\fBenum\fR (\fBFAILOVER\fR, \fBSCALABLE\fR)
\fBRG_PROJECT_NAME\fR\fBstring\fR
\fBRG_SLM_CPU\fR\fBdecimal\fR
\fBRG_SLM_CPU_MIN\fR\fBdecimal\fR
\fBRG_SLM_PSET_TYPE\fRT{
\fBenum\fR (\fBDEFAULT\fR, \fBDEDICATED_STRONG\fR, \fBDEDICATED_WEAK\fR)
T}
\fBRG_SLM_TYPE\fR\fBenum\fR (\fBAUTOMATED\fR, \fBMANUAL\fR)
\fBRG_STATE\fRT{
\fBenum\fR (\fBUNMANAGED\fR, \fBONLINE\fR, \fBOFFLINE\fR, \fBPENDING_ONLINE\fR, \fBPENDING_OFFLINE\fR, \fBERROR_STOP_FAILED\fR, \fBONLINE_FAULTED\fR, \fBPENDING_ONLINE_BLOCKED\fR)
T}
\fBRG_STATE_NODE\fR\fBenum\fR (see \fBRG_STATE\fR for
values)
\fBRG_SYSTEM\fR\fBboolean\fR
\fBSUSPEND_AUTOMATIC_RECOVERY\fR\fBboolean\fR
.TE

.SH EXIT STATUS
.sp
.LP
One set of exit status codes is used for all \fBscha\fR commands.
.sp
.LP
The exit status codes are the numeric values of the \fBscha_err_t\fR return codes of the corresponding C functions as described
in \fBscha_calls\fR(3HA).
.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
Availability\fBSUNWscdev\fR
_
Interface StabilityStable
.TE

.SH SEE ALSO
.sp
.LP
\fBawk\fR(1), \fBrt_callbacks\fR(1HA), \fBscha_cluster_get\fR(1HA), \fBscha_control\fR(1HA), \fBscha_resource_get\fR(1HA), \fBscha_resourcegroup_get\fR(1HA), \fBscha_resourcetype_get\fR(1HA), \fBscha_resource_setstatus\fR(1HA), \fBscha_calls\fR(3HA), \fBattributes\fR(5), \fBformats\fR(5), \fBr_properties\fR(5), \fBrg_properties\fR(5), \fBrt_properties\fR(5)
