'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. Use is subject to license terms.
.TH sccheck 1M "15 May 2008" "Sun Cluster 3.2" "System Administration Commands"
.SH NAME
sccheck \- check for and report on vulnerable Sun Cluster configurations
.SH SYNOPSIS
.LP
.nf
\fBsccheck\fR [\fB-b\fR] [\fB-h\fR \fInodename\fR[,\fInodename\fR]\&.\|.\|.] [\fB-o\fR \fIoutput-dir\fR]
[\fB-s \fIseverity\fR\fR] [\fB-v \fIverbosity\fR\fR]
.fi

.LP
.nf
\fBsccheck\fR [\fB-b\fR] [\fB-W\fR] [\fB-h\fR \fInodename\fR[,\fInodename\fR]\&.\|.\|.] [\fB-o\fR \fIoutput-dir\fR] [\fB-v \fIverbosity\fR\fR]
.fi

.SH DESCRIPTION
.LP
Note - 
.sp
.RS 2
Beginning with the Sun Cluster 3.2 release, Sun Cluster software includes an object-oriented command set. Although Sun Cluster software still supports the original command set, Sun Cluster procedural documentation uses only the object-oriented command set. For more information about the object-oriented command set, see the \fBIntro\fR(1CL) man page.
.RE
.sp
.LP
The \fBsccheck\fR utility examines Sun Cluster nodes for known vulnerabilities and configuration problems, and it delivers reports that describe all failed checks, if any. The utility runs one of these two sets of checks, depending on the state of the node that issues the command: 
.RS +4
.TP
.ie t \(bu
.el o
\fBPreinstallation checks -\fR When issued from a node that is not running as an active cluster member, the \fBsccheck\fR utility runs preinstallation checks on that node. These checks ensure that the node meets the minimum requirements
to be successfully configured with Sun Cluster software. 
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBCluster configuration checks -\fR When issued from an active member of a running cluster, the \fBsccheck\fR utility runs configuration checks on the specified or default set of nodes. These checks ensure that the cluster meets
the basic configuration required for a cluster to be functional. The \fBsccheck\fR utility produces the same results for this set of checks regardless of which cluster node issues the command.
.RE
.sp
.LP
The \fBsccheck\fR utility runs configuration checks and uses the \fBexplorer\fR(1M) utility to gather system data for check processing. The \fBsccheck\fR utility first runs single-node checks
on each \fInodename\fR specified, then runs multiple-node checks on the specified or default set of nodes.
.sp
.LP
Each configuration check produces a set of reports that are saved in the specified or default output directory. For each specified \fInodename\fR, the \fBsccheck\fR utility produces a report of any single-node checks that failed on that node. Then the node
from which \fBsccheck\fR was run produces an additional report for the multiple-node checks. Each report contains a summary that shows the total number of checks executed and the number of failures, grouped by check severity level.
.sp
.LP
Each report is produced in both ordinary text and in XML. The DTD for the XML format is available in the \fB/usr/cluster/lib/sccheck/checkresults.dtd\fR file. The reports are produced in English only.
.sp
.LP
The \fBsccheck\fR utility is a client-server program in which the server is started when needed by the \fBinetd\fR daemon. Environment variables in the user's shell are not available to this server. Also, some environment variables, in particular those that specify
the non-default locations of Java and Sun Explorer software, can be overridden by entries in the \fB/etc/default/sccheck\fR file. The ports used by the \fBsccheck\fR utility can also be overridden by entries in this file, as can the setting for required minimum available
disk space. The server logs error messages to \fBsyslog\fR and the console.
.sp
.LP
You can use this command only in the global zone.
.SH OPTIONS
.sp
.LP
The following options are supported: 
.sp
.ne 2
.mk
.na
\fB\fB-b\fR\fR
.ad
.sp .6
.RS 4n
Specifies a brief report. This report contains only the summary of the problem and the severity level. Analysis and recommendations are omitted.
.sp
You can use this option only in the global zone.
.sp
You need \fBsolaris.cluster.system.read\fR RBAC authorization to use this command option. See \fBrbac\fR(5).
.RE

.sp
.ne 2
.mk
.na
\fB\fB-h\fR \fInodename\fR[,\fInodename\fR]\&.\|.\|.\fR
.ad
.sp .6
.RS 4n
Specifies the nodes on which to run checks. If the \fB-h\fR option is not specified, the \fBsccheck\fR utility reports on all active cluster members.
.sp
You can use this option only in the global zone.
.sp
This option is only legal when issued from an active cluster member.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-o \fR\fIoutput-dir\fR\fR
.ad
.sp .6
.RS 4n
Specifies the directory in which to save reports.
.sp
You can use this option only in the global zone.
.sp
The \fIoutput-dir\fR must already exist or be able to be created by the \fBsccheck\fR utility. Any previous reports in \fIoutput-dir\fR are overwritten by the new reports.
.sp
If the \fB-o\fR option is not specified, \fB/var/cluster/sccheck/reports.\fIyyyy\fR-\fImm\fR-\fIdd\fR:\fIhh\fR:\fImm\fR:\fIss\fR\fR is
used as \fIoutput-dir\fR by default, where \fIyyyy\fR-\fImm\fR-\fIdd\fR:\fIhh\fR:\fImm\fR:\fIss\fR is the year-month-day:hour:minute:second
when the directory was created.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-s \fR\fIseverity\fR\fR
.ad
.sp .6
.RS 4n
Specifies the minimum severity level to report on.
.sp
You can use this option only in the global zone.
.sp
The value of \fIseverity\fR is a number in the range of 1 to 4 that indicates one of the following severity levels:
.RS +4
.TP
1.
Low
.RE
.RS +4
.TP
2.
Medium
.RE
.RS +4
.TP
3.
High
.RE
.RS +4
.TP
4.
Critical
.RE
Each check has an assigned severity level. Specifying a severity level will exclude any failed checks of lesser severity levels from the report. When the \fB-s\fR option is not specified, the default severity level is 0, which means that failed checks of all severity
levels are reported.
.sp
The \fB-s\fR option is mutually exclusive with the \fB-W\fR option.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v \fR\fIverbosity\fR\fR
.ad
.sp .6
.RS 4n
Specifies the \fBsccheck\fR utility's level of verbosity.
.sp
You can use this option only in the global zone.
.sp
The value of \fIverbosity\fR is a number in the range of 0 to 2 that indicates one of the following verbosity levels:
.RS +4
.TP
.ie t \(bu
.el o
0: No progress messages. This level is the default.
.RE
.RS +4
.TP
.ie t \(bu
.el o
1: Issues \fBsccheck\fR progress messages.
.RE
.RS +4
.TP
.ie t \(bu
.el o
2: Issues Sun Explorer and more detailed \fBsccheck\fR progress messages.
.RE
You need \fBsolaris.cluster.system.read\fR RBAC authorization to use this command option. See \fBrbac\fR(5).
.sp
The \fB-v\fR option has no effect on report contents.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-W\fR\fR
.ad
.sp .6
.RS 4n
Disables any warnings. The report generated is equivalent to \fB-s 3\fR.
.sp
You can use this option only in the global zone.
.sp
The \fB-W\fR option is mutually exclusive with the \fB-s\fR option. The \fB-W\fR option is retained for compatibility with prior versions of the \fBsccheck\fR utility.
.sp
You need \fBsolaris.cluster.system.read\fR RBAC authorization to use this command option. See \fBrbac\fR(5).
.RE

.SH EXIT STATUS
.sp
.LP
The following exit values are returned:
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 13n
.rt  
The command completed successfully. No violations were reported.
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR-\fB4\fR\fR
.ad
.RS 13n
.rt  
The code indicates that the highest severity level of all violations was reported.
.RE

.sp
.ne 2
.mk
.na
\fB\fB100+\fR\fR
.ad
.RS 13n
.rt  
An error has occurred. Some reports might have been generated.
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWsczu, SUNWscsck
_
Interface StabilityObsolete
.TE

.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/etc/default/sccheck\fR\fR
.ad
.sp .6
.RS 4n
 
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/sccheck/checkresults.dtd\fR\fR
.ad
.sp .6
.RS 4n
 
.RE

.sp
.ne 2
.mk
.na
\fB\fB/var/cluster/sccheck/reports.\fIyyyy\fR-\fImm\fR-\fIdd\fR:\fIhh\fR:\fImm\fR:\fIss\fR\fR\fR
.ad
.sp .6
.RS 4n
 
.RE

.SH SEE ALSO
.sp
.LP
\fBIntro\fR(1CL), \fBexplorer\fR(1M), \fBsccheckd\fR(1M), \fBscinstall\fR(1M), \fBattributes\fR(5)
.sp
.LP
\fISun Cluster Software Installation Guide\fR, \fISun Cluster System Administration Guide\fR
