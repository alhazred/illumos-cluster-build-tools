'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. Use is subject to license terms.
.TH clzonecluster 1CL "17 Sep 2008" "Sun Cluster 3.2" "Sun Cluster Maintenance Commands"
.SH NAME
clzonecluster, clzc \- create and manage zone clusters
.SH SYNOPSIS
.LP
.nf
\fB/usr/cluster/bin/clzonecluster [\fIsubcommand\fR]\fR \fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster\fR \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster \fIsubcommand\fR\fR [\fIoptions\fR] \fB-v\fR [\fIzoneclustername\fR]
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster boot\fR [\fB-n\fR \fInodename\fR[,...]] {+ | \fIzoneclustername\fR [...]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster clone\fR \fB-Z\fR \fIsource-zoneclustername\fR [\fB-m\fR \fImethod\fR] [\fB-n\fR \fInodename\fR[,...]] { 
\fIzoneclustername\fR }
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster configure\fR [\fB-f\fR \fIcommandfile\fR] \fIzoneclustername\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster delete\fR [\fB-F\fR] \fIzoneclustername\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster halt\fR [\fB-n\fR \fInodename\fR[,...]] {+ | \fIzoneclustername\fR}
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster install\fR [\fB-n\fR \fInodename\fR[,...]] \fIzoneclustername\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster list\fR [+ | \fIzoneclustername\fR [...]]
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster move\fR \fB-f\fR \fIzonepath\fR \fIzoneclustername\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster ready\fR [\fB-n\fR \fInodename\fR[,...]] {+ | \fIzoneclustername\fR [...]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster reboot\fR [\fB-n\fR \fInodename\fR[,...]] {+ | \fIzoneclustername\fR [...]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster show\fR [+ | \fIzoneclustername\fR [...]]
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster status\fR [+ | \fIzoneclustername\fR [...]]
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster uninstall\fR [\fB-F\fR] [\fB-n\fR \fInodename\fR[,...]] \fIzoneclustername\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clzonecluster verify\fR [\fB-n\fR \fInodename\fR[,...]] {+ | \fIzoneclustername\fR [...]}
.fi

.SH DESCRIPTION
.sp
.LP
The \fBclzonecluster\fR command creates and modifies zone clusters for Sun Cluster configurations. The \fBclzc\fR command is the short form of the  \fBclzonecluster\fR command; the commands are identical. The \fBclzonecluster\fR command
is cluster-aware and supports a single source of administration. You can issue all forms of the command from one node to affect a single zone-cluster node or all nodes.
.sp
.LP
You can omit \fIsubcommand\fR only if \fIoptions\fR is the \fB-?\fR option or the \fB-V\fR option.
.sp
.LP
The subcommands require at least one operand, except for the \fBlist\fR, \fBshow\fR, and \fBstatus\fR subcommands. However, many subcommands accept the plus sign operand (\fB+\fR) to apply the subcommand to all applicable objects. The \fBclzonecluster\fR commands can be run on any node of a zone cluster and can affect any or all of the zone cluster.
.sp
.LP
Each option has a long and a short form. Both forms of each option are given with the description of the option in OPTIONS.
.SH SUBCOMMANDS
.sp
.LP
The following subcommands are supported:
.sp
.ne 2
.mk
.na
\fB\fBboot\fR\fR
.ad
.sp .6
.RS 4n
Boots the zone cluster.
.sp
The \fBboot\fR subcommand boots the zone cluster. The \fBboot\fR subcommand  uses the \fB-n\fR flag to boot the  zone cluster for a specified list of nodes. You can use the \fBboot\fR subcommand only from a global-cluster node.
.RE

.sp
.ne 2
.mk
.na
\fB\fBclone\fR\fR
.ad
.sp .6
.RS 4n
Clones the zone cluster.
.sp
The \fBclone\fR command clones the zone cluster. You can use the \fBclone\fR subcommand only from a global-cluster node.
.RE

.sp
.ne 2
.mk
.na
\fB\fBconfigure\fR\fR
.ad
.sp .6
.RS 4n
Launches an interactive utility to configure a zone cluster.
.sp
The \fBconfigure\fR subcommand uses the \fBzonecfg\fR command to configure a zone on each specified machine.  The \fBconfigure\fR subcommand lets you specify properties that apply to each node of the zone cluster. These properties have the same meaning
as established by the \fBzonecfg\fR command for individual zones. The \fBconfigure\fR subcommand supports the configuration  of properties that are unknown to the \fBzonecfg\fR command. 
.sp
The \fBconfigure\fR subcommand launches an interactive shell if you do not specify the \fB-f\fR option. The \fB-f\fR option takes a command file as its argument. The \fBconfigure\fR subcommand uses this file to create or modify zone clusters
non-interactively. You can use the \fBconfigure\fR subcommand only from a global-cluster node.
.sp
Both the interactive and non-interactive forms of the \fBconfigure\fR command support several subcommands to edit the zone cluster configuration. See zonecfg(1M) for a list of available configuration subcommands.
.sp
The interactive \fBconfigure\fR utility enables you to create and modify the configuration of a zone cluster. Zone-cluster configuration consists of a number of resource types and properties. The \fBconfigure\fR utility uses the concept of \fBscope\fR to
determine where the subcommand applies. There are three levels of scope that are used by the \fBconfigure\fR utility: cluster, resource, and node-specific resource. The default scope is cluster. The following list describes the three levels of scope:
.RS +4
.TP
.ie t \(bu
.el o
Cluster scope - Properties that affect the entire zone cluster. If the \fIzoneclustername\fR is \fBsczone\fR, the interactive shell of the \fBclzonecluster\fR command looks similar to the following:
.sp
.in +2
.nf
clzc:sczone>
.fi
.in -2
.sp

.RE
.RS +4
.TP
.ie t \(bu
.el o
Node scope - A special resource scope that is nested inside the node resource scope. Settings inside the node scope affect a specific node in the zone cluster. For example, you can add a net resource to a specific node in the zone cluster. The interactive shell of the \fBclzonecluster\fR command looks similar to the following:
.sp
.in +2
.nf
clzc:sczone:node:net>
.fi
.in -2
.sp

.RE
.RS +4
.TP
.ie t \(bu
.el o
Resource scope - Properties that apply to one specific resource. A resource scope prompt has the name of the resource type appended. For example, the interactive shell of the \fBclzonecluster\fR command looks similar to the following:
.sp
.in +2
.nf
clzc:sczone:net>
.fi
.in -2
.sp

.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fBdelete\fR\fR
.ad
.sp .6
.RS 4n
Removes a specific zone cluster.
.sp
This subcommand deletes a specific zone cluster. When you use a wild card operand (*), the \fBdelete\fR command removes the zone clusters that are configured on the global cluster. The zone cluster must be in the configured state before you run the \fBdelete\fR subcommand.
.RE

.sp
.ne 2
.mk
.na
\fB\fBhalt\fR\fR
.ad
.sp .6
.RS 4n
Stops a zone cluster or a specific node on the zone cluster.
.sp
When you specify a specific zone cluster, the \fBhalt\fR subcommand applies only to that specific zone cluster. You can halt the entire zone cluster or just halt specific nodes of a zone cluster. If you do not specify a zone cluster, the \fBhalt\fR subcommand applies
to all zone clusters. You can also halt all zone clusters on specified machines.
.sp
The \fBhalt\fR subcommand uses the \fB-n\fR option to halt zone clusters on specific nodes. By default, the \fBhalt\fR subcommand stops all zone clusters on all nodes. If you specify the \fB+\fR operand in place of a zone name, all the zone
clusters are stopped. You can use the \fBhalt\fR subcommand only from a global-cluster node.
.RE

.sp
.ne 2
.mk
.na
\fB\fBinstall\fR\fR
.ad
.sp .6
.RS 4n
Installs a zone cluster.
.sp
This subcommand installs a zone cluster. You can use the \fBinstall\fR subcommand only from a global-cluster node.
.RE

.sp
.ne 2
.mk
.na
\fB\fBlist\fR\fR
.ad
.sp .6
.RS 4n
Displays the names of configured zone clusters.
.sp
This subcommand reports the names of zone clusters that are configured in the cluster. If you run the \fBlist\fR subcommand from a global-cluster node, the subcommand displays a list of all the zone clusters in the global cluster. If you run the \fBlist\fR subcommand
from a zone-cluster node, the subcommand displays only the name of the zone cluster. To see the list of nodes where the zone cluster is configured, use the \fB-v\fR option.
.RE

.sp
.ne 2
.mk
.na
\fB\fBmove\fR\fR
.ad
.sp .6
.RS 4n
Moves the zonepath to a new zonepath.
.sp
This subcommand moves the zonepath to a new zonepath. You can use the \fBmove\fR subcommand only from a global-cluster node.
.RE

.sp
.ne 2
.mk
.na
\fB\fBready\fR\fR
.ad
.sp .6
.RS 4n
Prepares the zone for applications.
.sp
This subcommand prepares the zone for running applications. You can use the \fBready\fR subcommand only from a global-cluster node.
.RE

.sp
.ne 2
.mk
.na
\fB\fBreboot\fR\fR
.ad
.sp .6
.RS 4n
Reboots a zone cluster.
.sp
This subcommand reboots the zone cluster and is similar to issuing a \fBhalt\fR subcommand, followed by a \fBboot\fR subcommand. See the \fBhalt\fR subcommand and the \fBboot\fR subcommand for more information.
.RE

.sp
.ne 2
.mk
.na
\fB\fBshow\fR\fR
.ad
.sp .6
.RS 4n
Displays the properties of zone clusters.
.sp
Properties for a zone cluster include zone cluster name, brand, IP type, node list, and zonepath. The \fBshow\fR subcommand runs from a zone cluster but applies only to that particular zone cluster. The zonepath is always \fB/\fR when you use this subcommand from
a zone cluster. If zone cluster name is specified, this command applies only for that zone cluster.
.RE

.sp
.ne 2
.mk
.na
\fB\fBstatus\fR\fR
.ad
.sp .6
.RS 4n
Determines whether the zone cluster node is a member of the zone cluster.
.sp
The zone state can be one of the following: Configured, Installed, Ready, Running, and Shutting Down. When you run the \fBstatus\fR subcommand from a global-cluster zone, the state of all the zone clusters in the global cluster is displayed so you can see the state of your virtual
cluster. When you run the \fBstatus\fR subcommand from a zone cluster, the state of only that particular zone cluster is displayed. Use the \fBzoneadm\fR command to check zone activity.
.RE

.sp
.ne 2
.mk
.na
\fB\fBuninstall\fR\fR
.ad
.sp .6
.RS 4n
Uninstalls a zone cluster.
.sp
This subcommand uninstalls a zone cluster. The \fBuninstall\fR subcommand uses the \fBzoneadm\fR command.
.RE

.sp
.ne 2
.mk
.na
\fB\fBverify\fR\fR
.ad
.sp .6
.RS 4n
Checks that the syntax of the specified information is correct.
.sp
This subcommand invokes the \fBzoneadm verify\fR command on each node in the zone cluster to ensure that each zone cluster member can be installed safely. For more information, see zoneadm(1M).
.RE

.SH OPTIONS
.LP
Note - 
.sp
.RS 2
The short and long form of each option are shown in this section.
.RE
.sp
.LP
The following options are supported:
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB\fB--help\fR\fR
.ad
.sp .6
.RS 4n
Displays help information.
.sp
You can specify this option with or without a \fIsubcommand\fR.
.sp
If you do not specify a \fIsubcommand\fR, the list of all available subcommands is displayed.
.sp
If you specify a \fIsubcommand\fR, the usage for that subcommand is displayed.
.sp
If you specify this option and other options, the other options are ignored.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-f\fR{\fIcommandfile\fR | \fIzonepath\fR}\fR
.ad
.br
.na
\fB\fB--file-argument\fR {\fIcommandfile\fR | \fIzonepath\fR}\fR
.ad
.sp .6
.RS 4n
When used with the \fBconfigure\fR subcommand, the \fB-f\fR option specifies the command file argument. For example, \fBclzonecluster configure\fR \fB-f\fR \fIcommandfile\fR. When used with the \fBmove\fR subcommand,
the \fB-f\fR option specifies the \fIzonepath\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-F\fR\fR
.ad
.br
.na
\fB\fB--force\fR\fR
.ad
.sp .6
.RS 4n
You can use the \fB-F\fR option during \fBdelete\fR and \fBuninstall\fR operations. The \fB-F\fR option forcefully suppresses the \fBAre you sure you want to \fIdo this operation\fR [y/n]?\fR questions.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-m\fR \fImethod\fR \fR
.ad
.br
.na
\fB\fB--method\fR \fIcopymethod\fR\fR
.ad
.sp .6
.RS 4n
Use the \fImethod\fR option to clone a zone cluster. The only valid method for cloning is the \fBcopy\fR command. Before you run the \fBclone\fR subcommand, you must halt the source zone cluster.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-n\fR \fInodename\fR[\&.\|.\|.] \fR
.ad
.br
.na
\fB\fB--nodelist\fR \fInodename\fR[,\&.\|.\|.]\fR
.ad
.sp .6
.RS 4n
Specifies the node list for the subcommand.
.sp
For example, \fBclzonecluster boot\fR \fB-n\fR \fIphys-schost-1\fR, \fIphys-schost-2\fR \fIzoneclustername\fR.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR\fR
.ad
.br
.na
\fB-\fB--verbose\fR\fR
.ad
.sp .6
.RS 4n
Displays verbose information on the standard output (\fBstdout\fR) .
.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB \fB--version\fR\fR
.ad
.sp .6
.RS 4n
Displays the version of the command.
.sp
If you specify this option with other options, with subcommands, or with operands, they are all ignored. Only the version of the command is displayed. No other processing occurs.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-Z\fR \fIsource-zoneclustername\fR \fR
.ad
.br
.na
\fB \fB--zonecluster\fR \fIsource-zoneclustername\fR\fR
.ad
.sp .6
.RS 4n
The zone cluster name that you want to clone.
.sp
Use the source zone-cluster name for cloning. The source zone cluster must be halted before you use this subcommand.
.RE

.SH RESOURCES AND PROPERTIES
.sp
.LP
The \fBclzonecluster\fR command supports several resources and properties for zone clusters.
.SS "Resources"
.sp
.LP
The following lists the resource types that are supported in the resource scope and where to find more information:
.sp
.ne 2
.mk
.na
\fB\fBcapped-cpu\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBcapped-memory\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBdataset\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M). Use this resource to export a ZFS data set to be used in the zone cluster for a highly-available ZFS file system. The exported data set is managed by the Sun Cluster software, and is not passed down to the
individual Solaris zone level when specified in the cluster scope. A data set cannot be shared between zone clusters.
.RE

.sp
.ne 2
.mk
.na
\fB\fBdedicated-cpu\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M). You can use a fixed number of CPUs that are dedicated to the zone cluster on each node.
.RE

.sp
.ne 2
.mk
.na
\fB\fBdevice\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M). You can add a device to only one zone cluster.
.RE

.sp
.ne 2
.mk
.na
\fB\fBfs\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M). Use this resource to export a file system to be used in the zone cluster. The file system types supported are UFS, Vxfs, single-machine QFS, shared QFS, ZFS (exported as a data set), and loopback file systems.
.sp
Highly-available file systems (for example, UFS, Vxfs, and single-machine QFS) are always specified in the cluster context. Sun Cluster manages highly-available file systems, and this information is not passed to the \fBzonecfg\fR command.
.sp
The administrator can specify a loopback mount in the cluster scope and that loopback mount is done on each zone cluster node. This approach is particularly useful for read-only mounts of common local directories, such as directories that contain executable files. This information is passed
to the \fBzonecfg\fR command, which does the actual mounts.
.sp
Shared QFS, UFS, Vxfs, single-machine QFS, and ZFS are configured in at most one zone cluster.
.RE

.sp
.ne 2
.mk
.na
\fB\fBinherit-pkg-dir\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBnet\fR\fR
.ad
.sp .6
.RS 4n
 See zonecfg(1M) for information about net resources.
.sp
Any net resource managed by Sun Cluster, such as Logical Host or Shared Address, is specified in the cluster scope.     Any net resource managed by an application, such as an Oracle RAC VIP, is specified in the cluster scope. These net resources are not passed to the individual Solaris zone
level. 
.sp
The administrator can specify the Network Interface Card (NIC) to use with the specified IP Address. The system automatically selects a NIC that satisfies the following two requirements:
.RS +4
.TP
.ie t \(bu
.el o
The NIC already connects to the same subnet.
.RE
.RS +4
.TP
.ie t \(bu
.el o
The NIC has been configured for this zone cluster.
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fBnode\fR\fR
.ad
.sp .6
.RS 4n
The node resource performs the following two purposes:
.RS +4
.TP
.ie t \(bu
.el o
Identifies a scope level. Any resource specified in a node scope belongs exclusively to this specific node.
.RE
.RS +4
.TP
.ie t \(bu
.el o
Identifies a node of the zone cluster. The administrator identifies the machine where the zone will run by identifying the global cluster global zone on that machine. The administrator also specifies information identifying network information for reaching this node.
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fBrctl\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBsysid\fR\fR
.ad
.sp .6
.RS 4n
See \fBsysidcfg\fR(4). This resource specifies the system identification parameters for all zones of the zone cluster.
.RE

.SS "Properties"
.sp
.LP
Each resource type has one or more properties. The following properties are supported for cluster:
.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
\fBzonename\fR
.sp
The name of the zone cluster, as well as the name of each zone in the zone cluster.
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
\fBzonepath\fR
.sp
The zonepath of each zone in the zone cluster.
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
\fBautoboot\fR
.sp
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
bootargs
.sp
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
limitpriv
.sp
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
brand
.sp
See zonecfg(1M). Cluster is the only brand type supported.
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
ip-type
.sp
See zonecfg(1M). IP-type is the only value supported.
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
pool
.sp
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
cpu-shares
.sp
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
max-lwps
.sp
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
max-msg-ids
.sp
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
max-sem-ids
.sp
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
max-shm-ids
.sp
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
max-shm-memory
.sp
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fB(cluster)\fR\fR
.ad
.sp .6
.RS 4n
enable_priv_net
.sp
When set to true, Sun Cluster private network communication is enabled between the nodes of the zone cluster. The Sun Cluster private hostnames and IP addresses for the zone cluster nodes are automatically generated by the system. Private network is disabled if the value is set to false.
The default value is true.
.RE

.sp
.ne 2
.mk
.na
\fB\fBfs\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBinherit pkg-dir\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBnet\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBdevice\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBrctl\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBdataset\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBdedicated-cpu\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBcapped-memory\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBcapped-cpu\fR\fR
.ad
.sp .6
.RS 4n
See zonecfg(1M).
.RE

.sp
.ne 2
.mk
.na
\fB\fBnode\fR\fR
.ad
.sp .6
.RS 4n
Includes physical-host, hostname, and net.
.RS +4
.TP
.ie t \(bu
.el o
physical-host - This property specifies a global cluster node that will host a zone cluster node.
.RE
.RS +4
.TP
.ie t \(bu
.el o
hostname - This property specifies the public host name of the zone cluster node on the global cluster node specified by the physical-host property.
.RE
.RS +4
.TP
.ie t \(bu
.el o
net - This resource specifies a network address and physical interface name for public network communication by the zone cluster node on the global cluster node specified by physical-host.
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fBsysid\fR\fR
.ad
.sp .6
.RS 4n
 See \fBsysidcfg\fR(4). Includes root_password, name_service, security_policy, system_locale, timezone, terminal, and nfs4_domain. The administrator can later manually change any \fBsysidcfg\fR value following the normal Solaris procedures one node at a time.
.RS +4
.TP
.ie t \(bu
.el o
root_password - This property specifies the encrypted value of the common root password for all nodes of the zone cluster. Do not specify a clear text password. Encrypted password string from /etc/shadow must be used. This is a required property. 
.RE
.RS +4
.TP
.ie t \(bu
.el o
name_service -  This property specifies the naming service to be used in the zone cluster. It is an optional property, and the setting in the global zone is used by default. 
.RE
.RS +4
.TP
.ie t \(bu
.el o
security_policy - The value is set to none by default.
.RE
.RS +4
.TP
.ie t \(bu
.el o
system_locale - The value is obtained from the environment of the \fBclzonecluster\fR command by default.
.RE
.RS +4
.TP
.ie t \(bu
.el o
timezone - The time zone to be used in the zone cluster. The global zone setting is used by default.
.RE
.RS +4
.TP
.ie t \(bu
.el o
terminal - The value is set to xterm by default.
.RE
.RS +4
.TP
.ie t \(bu
.el o
nfs4_domain - The value is set to dynamic by default.
.RE
.RE

.SS "Examples"
.sp
.LP
In all the examples, the \fIzoneclustername\fR is \fBsczone\fR. The first global-cluster node is \fBphys-schost-1\fR and the second node is \fBphys-schost-2\fR. The first zone-cluster node is \fBzc-host-1\fR and the second
one is \fBzc-host-2\fR.
.LP
\fBExample 1 \fRCreating a New Zone Cluster
.sp
.LP
The following example demonstrates how to create a two-node zone cluster comprised of sparse-root zones. The /usr/local is loopback mounted into the zone cluster nodes as /opt/local. Two IP addresses are exported to the zone cluster for use as highly-available IP addresses. A ZFS data set
is exported to the zone cluster for use as a highly-available ZFS file system. Memory capping is used to limit the amount of memory that can be used in the zone cluster. The \fBproc_priocnlt\fR and \fBproc_clock\fR_highres privileges are added to the zone cluster to enable
Oracle RAC to run. Default system identification values are used, except for the root password.

.sp
.LP
A UFS file system is exported to the zone cluster for use as a highly-available file system. It is assumed that the UFS file system is created on a Solaris Volume Manager metadevice.

.sp
.in +2
.nf
phys-schost-1#\fBclzonecluster configure\fR \fIsczone\fR
sczone: No such zone cluster configured
Use 'create' to begin configuring a new zone cluster.
clzc:sczone> \fBcreate\fR
clzc:sczone> \fBset zonepath=/zones/sczone\fR
clzc:sczone> \fBset limitpriv="default,proc_priocntl,proc_clock_highres"\fR
clzc:sczone> \fBadd sysid\fR
clzc:sczone:sysid> \fBset root_password=xxxxxxxxxxxxx\fR
clzc:sczone:sysid> \fBend\fR
clzc:sczone> \fBadd node\fR
clzc:sczone:node> \fBset physical-host=phys-schost-1\fR
clzc:sczone:node> \fBset hostname=zc-host-1\fR
clzc:sczone:node> \fBadd\fR \fBnet\fR
clzc:sczone:node:net> \fBset address=zc-host-1\fR
clzc:sczone:node:net> \fBset physical=bge0\fR
clzc:sczone:node:net> \fBend\fR
clzc:sczone:node> \fBend\fR
clzc:sczone> \fBadd node\fR
clzc:sczone:node> \fBset physical-host=phys-schost-2\fR
clzc:sczone:node> \fBset hostname=zc-host-2\fR
clzc:sczone:node> \fBadd net\fR
clzc:sczone:node:net> \fBset address=zc-host-2\fR
clzc:sczone:node:net> \fBset physical=bge0\fR
clzc:sczone:node:net> \fBend\fR
clzc:sczone:node> \fBend\fR
clzc:sczone> \fBadd net\fR
clzc:sczone:net> \fBset address=192.168.0.1\fR
clzc:sczone:net> \fBend\fR
clzc:sczone> \fBadd net\fR
clzc:sczone:net> \fBset address=192.168.0.2\fR
clzc:sczone:net> \fBend\fR
clzc:sczone> \fBadd fs\fR
clzc:sczone:fs> \fBset dir=/opt/local\fR
clzc:sczone:fs> \fBset special=/usr/local\fR
clzc:sczone:fs> \fBset type=lofs\fR
clzc:sczone:fs> \fBadd options [ro,nodevices]\fR
clzc:sczone:fs> \fBend\fR
clzc:sczone> \fBadd dataset\fR
clzc:sczone:dataset> \fBset name=tank/home\fR
clzc:sczone:dataset> \fBend\fR
clzc:sczone> \fBadd capped-memory\fR
clzc:sczone:capped-memory> \fBset physical=3G\fR
clzc:sczone:capped-memory> \fBset swap=4G\fR
clzc:sczone:capped-memory> \fBset locked=3G\fR
clzc:sczone:capped-memory> \fBend\fR
clzc:sczone> \fBadd fs\fR
clzc:sczone:fs> \fBset dir=/data/ha-data\fR
clzc:sczone:fs> \fBset special=/dev/md/ha-set/dsk/d10\fR
clzc:sczone:fs> \fBset raw=/dev/md/ha-set/rdsk/d10\fR
clzc:sczone:fs> \fBset type=ufs\fR
clzc:sczone:fs> \fBend\fR 
clzc:sczone> \fBverify\fR
clzc:sczone> \fBcommit\fR
clzc:sczone> \fBexit\fR
.fi
.in -2
.sp

.sp
.LP
The zone cluster is now configured. The following commands install and then boot the zone cluster from a global-cluster node:
.sp
.in +2
.nf
phys-schost-1# \fBclzonecluster install\fR \fIsczone\fR
.fi
.in -2
.sp

.sp
.in +2
.nf
phys-schost-1# \fBclzonecluster boot\fR \fIsczone\fR
.fi
.in -2
.sp

.LP
\fBExample 2 \fRModifying an Existing Zone Cluster
.sp
.LP
The following example shows how to modify the configuration of the zone cluster created in Example 1. A multi-owner SVM metadevice is added to the zone cluster. The set number of the metaset is 1, and the set name is oraset. An additional public IP address is added to the zone-cluster node
on phys-schost-2. A shared QFS file system is also added to the configuration. Note that the \fBspecial\fR property of a QFS file system must be set to the name of the MCF file. The \fBraw\fR property must be left unspecified.

.sp
.LP
A UFS file system is exported to the zone cluster for use as a highly-available file system. It is assumed that the UFS file system is created on a Solaris Volume Manager metadevice.

.sp
.in +2
.nf
phys-schost-1# \fBclzonecluster configure\fR \fIsczone\fR
clzc:sczone> \fBadd device\fR
clzc:sczone:device> \fBset match=/dev/md/1/dsk/d100\fR
clzc:sczone:device> \fBend\fR
clzc:sczone> \fBadd device\fR
clzc:sczone:device> \fBset match=/dev/md/oraset/dsk/d100\fR
clzc:sczone:device> \fBend\fR
clzc:sczone> \fBselect node physical-host=phys-schost-2\fR
clzc:sczone:node> \fBadd net\fR
clzc:sczone:node:net> \fBset address=192.168.0.3/24\fR
clzc:sczone:node:net> \fBset physical=bge0\fR
clzc:sczone:node:net> \fBend\fR
clzc:sczone:node> \fBend\fR
clzc:sczone> \fBadd fs\fR
clzc:sczone:fs> \fBset dir=/qfs/ora_home\fR
clzc:sczone:fs> \fBset special=oracle_home\fR
clzc:sczone:fs> \fBset type=samfs\fR
clzc:sczone:fs> \fBend\fR
clzc:sczone> \fBexit\fR
.fi
.in -2
.sp

.LP
\fBExample 3 \fRCreating a New Zone Cluster Using an Existing Zone Cluster as a Template
.sp
.LP
The following example shows how to create a zone cluster called \fIsczone1\fR, using the \fIsczone\fR zone cluster created in Example 1 as a template. The new zone cluster's configuration will be the same as the original zone cluster. Some properties
of the new zone cluster need to be modified to avoid conflicts. When the administrator removes a resource type without specifying a specific resource, the system removes all resources of that type. For example, \fBremove net\fR causes the removal of all net resources.

.sp
.in +2
.nf
phys-schost-1# \fBclzonecluster configure\fR \fIsczone1\fR
sczone1: No such zone cluster configured
Use 'create' to begin configuring a new zone cluster.
clzc:sczone1> \fBcreate -t\fR \fIsczone\fR
clzc:sczone1>\fBset zonepath=/zones/sczone1\fR
clzc:sczone1> \fBselect node physical-host=phys-schost-1\fR
clzc:sczone1:node> \fBset hostname=zc-host-3\fR
clzc:sczone1:node> \fBselect net address=zc-host-1\fR
clzc:sczone1:node:net> \fBset address=zc-host-3\fR
clzc:sczone1:node:net> \fBend\fR
clzc:sczone1:node> \fBend\fR
clzc:sczone1> \fBselect node physical-host=phys-schost-2\fR
clzc:sczone1:node> \fBset hostname=zc-host-4\fR
clzc:sczone1:node> \fBselect net address=zc-host-2\fR
clzc:sczone1:node:net> \fBset address=zc-host-4\fR
clzc:sczone1:node:net> \fBend\fR
clzc:sczone1:node> \fBremove net address=192.168.0.3/24\fR
clzc:sczone1:node> \fBend\fR
clzc:sczone1> \fBremove dataset name=tank/home\fR
clzc:sczone1> \fBremove net\fR
clzc:sczone1> \fBremove device\fR
clzc:sczone1> \fBremove fs dir=/qfs/ora_home\fR
clzc:sczone1> \fBexit\fR
.fi
.in -2
.sp

.LP
\fBExample 4 \fRCreating a Whole-Root Zone Cluster
.sp
.LP
The following example shows the creation of a new zone cluster, \fIsczone2\fR, but now the constituent zones will be whole-root zones.

.sp
.in +2
.nf
phys-schost-1# \fBclzonecluster configure\fR \fIsczone2\fR
sczone2: No such zone cluster configured
Use 'create' to begin configuring a new zone cluster.
clzc:sczone2> \fBcreate -b\fR
\&...
\fIFollow the steps in Example 1 for the rest of the configuration\fR
\&...
clzc:sczone2> \fBexit\fR
.fi
.in -2
.sp

.SH OPERANDS
.sp
.LP
The following operands are supported:
.sp
.ne 2
.mk
.na
\fB\fIzoneclustername\fR\fR
.ad
.RS 20n
.rt  
The name of the zone cluster. You specify the name of the new zone cluster. The \fIzoneclustername\fR operand is supported for all subcommands.
.RE

.sp
.ne 2
.mk
.na
\fB\fB+\fR\fR
.ad
.RS 20n
.rt  
All nodes in the cluster. The \fB+\fR operand is supported only for a subset of subcommands.
.RE

.SH EXIT STATUS
.sp
.LP
The complete set of exit status codes for all commands in this command set are listed on the \fBIntro\fR(1CL) man page.
.sp
.LP
If the command is successful for all specified operands, it 
returns zero (\fBCL_NOERR\fR). If an error occurs for an
operand, the command processes the next operand in the operand list. The
returned exit code always reflects the error that occurred first.
.sp
.LP
This command returns the following exit status codes:
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fBCL_NOERR\fR\fR
.ad
.sp .6
.RS 4n
No error.
.sp
The command that you issued completed successfully.
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR \fBCL_ENOMEM\fR\fR
.ad
.sp .6
.RS 4n
Not enough swap space.
.sp
A cluster node ran out of swap memory or ran out of other operating system resources.
.RE

.sp
.ne 2
.mk
.na
\fB\fB3\fR \fBCL_EINVAL\fR\fR
.ad
.sp .6
.RS 4n
Invalid argument.
.sp
You typed the command incorrectly, or the syntax of the cluster
configuration information that you supplied with the \fB-i\fR option
was incorrect.
.RE

.sp
.ne 2
.mk
.na
\fB\fB18\fR\fBCL_EINTERNAL\fR\fR
.ad
.sp .6
.RS 4n
Internal error was encountered.
.RE

.sp
.ne 2
.mk
.na
\fB\fB36\fR \fBCL_ENOENT\fR\fR
.ad
.sp .6
.RS 4n
No such object
.sp
The object that you specified cannot be found for one of the following reasons:
.RS +4
.TP
.ie t \(bu
.el o
The object does not exist.
.RE
.RS +4
.TP
.ie t \(bu
.el o
A directory in the path to the configuration file that you attempted to create with the \fB-o\fR option does not exist.
.RE
.RS +4
.TP
.ie t \(bu
.el o
The configuration file that you attempted to access with the \fB-i\fR option contains errors.
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fB37\fR \fBCL_EOP\fR\fR
.ad
.sp .6
.RS 4n
Operation not allowed
.sp
You tried to perform an operation on an unsupported configuration, 
or you performed an unsupported operation.
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
Availability\fBSUNWsczu\fR
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBcluster\fR(1CL), \fBIntro\fR(1CL), \fBscinstall\fR(1M), \fBclnode\fR(1CL), zoneadm(1M), \fBzonecfg\fR(1M).
.SH NOTES
.sp
.LP
The superuser can run all forms of this command.
.sp
.LP
All users can run this command with the \fB-?\fR (help) or \fB-V\fR (version) option.
.sp
.LP
To run the \fBclzonecluster\fR command with subcommands, users other than superuser require RBAC authorizations. See the following table.
.sp

.sp
.TS
tab() box;
cw(0i) |cw(5.5i) 
lw(0i) |lw(5.5i) 
.
SubcommandRBAC Authorization
_
\fBboot\fR\fBsolaris.cluster.admin\fR
_
\fBcheck\fR\fBsolaris.cluster.read\fR
_
\fBclone\fR\fBsolaris.cluster.admin\fR
_
\fBconfigure\fR\fBsolaris.cluster.admin\fR
_
\fBdelete\fR\fBsolaris.cluster.admin\fR
_
\fBexport\fR\fBsolaris.cluster.admin\fR
_
\fBhalt\fR\fBsolaris.cluster.admin\fR
_
\fBinstall\fR\fBsolaris.cluster.admin\fR
_
\fBlist\fR\fBsolaris.cluster.read\fR
_
\fBmonitor\fR\fBsolaris.cluster.modify\fR
_
\fBmove\fR\fBsolaris.cluster.admin\fR
_
\fBready\fR\fBsolaris.cluster.admin\fR
_
\fBreboot\fR\fBsolaris.cluster.admin\fR
_
\fBshow\fR\fBsolaris.cluster.read\fR
_
\fBstatus\fR\fBsolaris.cluster.read\fR
_
\fBuninstall\fR\fBsolaris.cluster.admin\fR
_
\fBunmonitor\fR\fBsolaris.cluster.modify\fR
_
\fBverify\fR\fBsolaris.cluster.admin\fR
.TE

