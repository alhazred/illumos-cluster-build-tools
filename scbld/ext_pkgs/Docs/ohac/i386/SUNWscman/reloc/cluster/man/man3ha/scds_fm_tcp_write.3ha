'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights
.\" reserved. Use is subject to license terms.
.TH scds_fm_tcp_write 3HA "13 Aug 2008" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scds_fm_tcp_write \- write data using a TCP connection
to an application
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev
#include <rgm/libdsdev.h>

\fBscha_err_t\fR  \fBscds_fm_tcp_write\fR(\fBscds_handle_t\fR \fIhandle\fR, \fBint\fR \fIsock\fR, \fBchar *\fR\fIbuffer\fR,
    \fBsize_t *\fR\fIsize\fR, \fBtime_t\fR \fItimeout\fR);
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscds_fm_tcp_write()\fR function writes data by means
of a TCP connection to a process that is being monitored.
.sp
.LP
The \fIsize\fR argument is an input and output argument.
On input, you specify the number of bytes to be written. On output, the function
returns the number of bytes actually written. If the input and output values
of \fIsize\fR are not equal, an error has occurred. The function
returns \fBSCHA_ERR_TIMEOUT\fR if it times out before writing
all the requested data.
.SH PARAMETERS
.sp
.LP
The following parameters are supported:
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
The handle returned from \fBscds_initialize\fR(3HA).
.RE

.sp
.ne 2
.mk
.na
\fB\fIsock\fR\fR
.ad
.RS 20n
.rt  
The socket number returned by a previous call to \fBscds_fm_tcp_connect\fR(3HA).
.RE

.sp
.ne 2
.mk
.na
\fB\fIbuffer\fR\fR
.ad
.RS 20n
.rt  
Data buffer.
.RE

.sp
.ne 2
.mk
.na
\fB\fIsize\fR\fR
.ad
.RS 20n
.rt  
Data buffer size. On input, you specify the number of bytes
to be written. On output, the function returns the number of bytes that were
actually written.
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
Timeout value in seconds.
.RE

.SH RETURN VALUES
.sp
.LP
The \fBscds_fm_tcp_write()\fR function returns the following:
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
The function succeeded.
.RE

.sp
.ne 2
.mk
.na
\fBnonzero\fR
.ad
.RS 20n
.rt  
The function failed.
.RE

.SH ERRORS
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 28n
.rt  
Indicates that the function succeeded.
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_TIMEOUT\fR\fR
.ad
.RS 28n
.rt  
Indicates that the function timed out.
.RE

.sp
.ne 2
.mk
.na
\fBOther values\fR
.ad
.RS 28n
.rt  
Indicate that the function failed. See \fBscha_calls\fR(3HA) for  the meaning of failure
codes.
.RE

.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) 
for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBscds_fm_tcp_connect\fR(3HA), \fBscds_fm_tcp_read\fR(3HA), \fBscds_initialize\fR(3HA), \fBscha_calls\fR(3HA), \fBattributes\fR(5)
