'\" te
.\" Copyright 2008 Sun Microsystems, Inc.  All
.\" rights reserved. Use is subject to license terms.
.TH scds_pmf_stop 3HA "13 Aug 2008" "Sun Cluster 3.2" "Sun Cluster HA and Data Services"
.SH NAME
scds_pmf_stop \- terminate a process that is running
under PMF control
.SH SYNOPSIS
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev
#include <rgm/libdsdev.h>

\fBscha_err_t\fR \fB scds_pmf_stop\fR(\fBscds_handle_t\fR \fIhandle\fR, \fBscds_pmf_type_t\fR \fIprogram_type\fR,
    \fBint\fR \fIinstance\fR, \fBint\fR \fIsignal\fR, \fBtime_t\fR \fItimeout\fR);
.fi

.SH DESCRIPTION
.sp
.LP
The \fBscds_pmf_stop()\fR function stops a program that
is running under PMF control. It is equivalent to the \fBpmfadm\fR(1M) command with the \fB-s\fR option.
.sp
.LP
If the requested instance is not running, \fBscds_pmf_stop()\fR returns
with value \fBSCHA_ERR_NOERR\fR.
.sp
.LP
If the requested instance is running, then the specified signal is sent
to the instance. If the instance fails to die within a period of time equal
to 80 percent of the timeout value, \fBSIGKILL\fR is sent to
the instance. If the instance then fails to die within a period of time equal
to 15 percent of the timeout value, the function is considered to have failed
and returns \fBSCHA_ERR_TIMEOUT\fR. The remaining 5 percent of
the timeout argument is presumed to have been absorbed by this function's
overhead.
.SH PARAMETERS
.sp
.LP
The following parameters are supported:
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
The handle returned from \fBscds_initialize\fR(3HA)
.RE

.sp
.ne 2
.mk
.na
\fB\fIprogram_type\fR\fR
.ad
.RS 20n
.rt  
Type of program to execute.  Valid types are:
.sp
.ne 2
.mk
.na
\fB\fBSCDS_PMF_TYPE_SVC\fR\fR
.ad
.RS 28n
.rt  
Data service application
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCDS_PMF_TYPE_MON\fR\fR
.ad
.RS 28n
.rt  
Fault monitor
.RE

.sp
.ne 2
.mk
.na
\fB\fB\fR\fBSCDS_PMF_TYPE_OTHER\fR\fR
.ad
.RS 28n
.rt  
Other
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fIinstance\fR\fR
.ad
.RS 20n
.rt  
For resources with multiple instances, this integer, starting
at 0, uniquely identifies the instance. For single instance resources, use
0. 
.RE

.sp
.ne 2
.mk
.na
\fB\fIsignal\fR\fR
.ad
.RS 20n
.rt  
Solaris signal to send kill the instance. See \fBsignal\fR(3HEAD). Use \fBSIGKILL\fR if
the specified signal fails to kill the instance.
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
Timeout period measured in seconds.
.RE

.SH RETURN VALUES
.sp
.LP
The \fBscds_pmf_stop()\fR function returns the following:
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
The function succeeded.
.RE

.sp
.ne 2
.mk
.na
\fB\fBnon-zero\fR\fR
.ad
.RS 20n
.rt  
The function failed.
.RE

.SH ERRORS
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_TIMEOUT\fR\fR
.ad
.RS 28n
.rt  
The function timed out. 
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 28n
.rt  
The function succeeded. 
.RE

.sp
.ne 2
.mk
.na
\fBOther values\fR
.ad
.RS 28n
.rt  
Indicate the function failed. See  \fBscha_calls\fR(3HA) for a description of other
error codes.
.RE

.SH FILES
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
Include file
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
Library
.RE

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) 
for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscdev
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBpmfadm\fR(1M),  \fBscds_initialize\fR(3HA), \fBscds_pmf_start\fR(3HA), \fBscha_calls\fR(3HA), \fBsignal\fR(3HEAD), \fBattributes\fR(5)
