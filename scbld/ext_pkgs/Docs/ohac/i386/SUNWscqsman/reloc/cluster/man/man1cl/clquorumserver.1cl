'\" te
.\" Copyright 2005 Sun Microsystems, Inc. All rights reserved. Use is subject to license terms.
.TH clquorumserver 1CL "04 Oct 2005" "Sun Cluster 3.2" "Sun Cluster Maintenance Commands"
.SH NAME
clquorumserver, clqs \- manage quorum servers
.SH SYNOPSIS
.LP
.nf
\fB/usr/cluster/bin/clquorumserver\fR \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorumserver \fIsubcommand\fR\fR 
\fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorumserver \fIsubcommand\fR\fR 
[\fB-v\fR] [\fIquorumserver\fR]
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorumserver clear\fR  \fB-c\fR \fIclustername\fR
\fB-I\fR \fIclusterID\fR [\fB-y\fR] \fIquorumserver\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorumserver show\fR  [+ |  \fIquorumserver\fR...]
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorumserver start\fR  + |  \fIquorumserver\fR...
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorumserver stop\fR  + |  \fIquorumserver\fR...
.fi

.SH DESCRIPTION
.sp
.LP
 Use the \fBclquorumserver\fR command for the following tasks:
.RS +4
.TP
.ie t \(bu
.el o
to clean up stale configuration information of one or more quorum servers
.RE
.RS +4
.TP
.ie t \(bu
.el o
to display the configuration of one or more quorum servers
.RE
.RS +4
.TP
.ie t \(bu
.el o
to start one or more quorum servers
.RE
.RS +4
.TP
.ie t \(bu
.el o
to stop one or more quorum servers
.RE
.sp
.LP
The \fBclqs\fR command is the short form of  the \fBclquorumserver\fR command. You can use either form of the command.  
.sp
.LP
The general form of this command is as follows:
.sp
.LP
 \fBclquorumserver\fR [\fIsubcommand\fR] [\fIoptions\fR]
.sp
.LP
 You can omit \fIsubcommand\fR only if \fIoptions\fR specifies the \fB-?\fR, \fB-v\fR, or  \fB-V\fR options.
.sp
.LP
The quorum server must be configured as a quorum device for the cluster. For information about configuring the quorum server, see \fBscqsd.conf\fR(4)  and  \fBscqsd\fR(1M). For information about adding a \fBquorum_server\fR type of quorum device to  the cluster, see \fBclquorum\fR(1CL).
.SH SUBCOMMANDS
.sp
.LP
The following subcommands are supported:
.sp
.ne 2
.mk
.na
\fB\fBclear\fR\fR
.ad
.sp .6
.RS 4n
Removes outdated cluster information from the quorum server. The quorum server keeps information about the cluster which it serves as a quorum device. This information can become invalid in the following circumstances:
.RS +4
.TP
.ie t \(bu
.el o
When a cluster is decommissioned without first removing the cluster quorum device using the \fBclquorum remove\fR command
.RE
.RS +4
.TP
.ie t \(bu
.el o
When a \fBquorum_server\fR type quorum device is removed from a cluster while the quorum server host is down
.RE
.LP
Caution - 
.sp
.RS 2
If a quorum server is not yet removed from the cluster, using this subcommand to clean up a valid quorum server could compromise the cluster quorum.
.RE
You must specify the cluster name and cluster ID for a particular quorum server. See the \fB-c\fR and \fB-I\fR options for details.
.sp
Users other than superuser require \fBsolaris.cluster.admin\fR RBAC authorization to use this subcommand. See  \fBrbac\fR(5) for more information.
.RE

.sp
.ne 2
.mk
.na
\fB\fBshow\fR\fR
.ad
.sp .6
.RS 4n
Displays the configuration information about the quorum server. For every cluster that configured the quorum server as a quorum device, this subcommand shows the corresponding cluster name, cluster ID, list of reservation keys, and list of registration keys.
.sp
You can use the plus sign (\fB+\fR) to specify more than one quorum server.
.sp
If no operand is given, or if the plus sign (\fB+\fR) is specified with the operand, the command prints the configuration of all running quorum servers.
.sp
Users other than superuser require \fBsolaris.cluster.read\fR RBAC authorization to use this subcommand. See  \fBrbac\fR(5) for more information.
.RE

.sp
.ne 2
.mk
.na
\fB\fBstart\fR\fR
.ad
.sp .6
.RS 4n
Starts the quorum server
.RE

.sp
.ne 2
.mk
.na
\fB\fBstop\fR\fR
.ad
.sp .6
.RS 4n
Stops the quorum server
.RE

.SH OPTIONS
.sp
.LP
The following options are supported:
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB\fB--help\fR\fR
.ad
.sp .6
.RS 4n
Prints help information.
.sp
This option can be used alone or with a subcommand.
.RS +4
.TP
.ie t \(bu
.el o
If you use this option alone, the list of available subcommands is printed.
.RE
.RS +4
.TP
.ie t \(bu
.el o
If you use this option with a subcommand, the usage options for that subcommand are printed.
.RE
When this option is used, no other processing is performed.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-c\fR \fIclustername\fR\fR
.ad
.br
.na
\fB\fB--clustername\fR \fIclustername\fR\fR
.ad
.sp .6
.RS 4n
Specifies the name of the cluster that uses the quorum server as a quorum device. You can get the cluster name by running some Sun Cluster commands on the cluster nodes, such as \fBcluster show\fR.
.sp
This option is required with the \fBclear\fR subcommand.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-I\fR \fIclusterID\fR\fR
.ad
.br
.na
\fB\fB--clusterID\fR \fIclusterID\fR\fR
.ad
.sp .6
.RS 4n
Specifies the cluster ID. The cluster ID is an 8-digit hexadecimal number. You can get the cluster ID by running some Sun Cluster commands on the cluster nodes, such as \fBcluster show\fR.
.sp
This option is required with the \fBclear\fR subcommand.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB\fB--version\fR\fR
.ad
.sp .6
.RS 4n
Prints the version of the command.
.sp
Do not specify this option with subcommands, operands, or other options. The subcommand, operands, or other options are ignored. The \fB-V\fR option only prints the version of the command. No other operations are performed.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR\fR
.ad
.br
.na
\fB-\fB--verbose\fR\fR
.ad
.sp .6
.RS 4n
Prints verbose information to standard output, \fBstdout\fR.
.sp
You can specify this option with any form of this command.
.RE

.sp
.ne 2
.mk
.na
\fB\fB-y\fR\fR
.ad
.br
.na
\fB\fB--yes\fR\fR
.ad
.sp .6
.RS 4n
This option is only used with the \fBclear\fR subcommand. It pre-answers the confirmation question that is prompted by \fBclear\fR subcommand. Without this option, the \fBclear\fR subcommand asks a question to confirm whether you want to clean
up the cluster information, and you need to answer \fByes\fR or \fBno\fR. The subcommand only continues processing if you answer \fByes\fR. When use this option, the \fBclear\fR subcommand will not ask any confirmation question, and instead directly
removes the cluster information for the specified quorum server.
.RE

.SH OPERANDS
.sp
.LP
The following operand is supported:
.sp
.ne 2
.mk
.na
\fB\fIquorumserver\fR\fR
.ad
.sp .6
.RS 4n
 Specifies an identifier for the quorum server or servers. A quorum server can be identified by either a port number or an instance name. The port number is used by the cluster nodes to communicate with the quorum server. The instance name can be specified in the quorum server
configuration file, \fB/etc/scqsd/scqsd.conf\fR. See \fBscqsd.conf\fR(4).
.RE

.SH EXIT STATUS
.sp
.LP
If the command is successful for all specified operands, it 
returns zero (\fBCL_NOERR\fR). If an error occurs for an
operand, the command processes the next operand in the operand list. The
returned exit code always reflects the error that occurred first.
.sp
.LP
The following exit values are returned:
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fBCL_NOERR\fR\fR
.ad
.sp .6
.RS 4n
No error
.sp
The command that you issued completed successfully.
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR \fBCL_ENOMEM\fR\fR
.ad
.sp .6
.RS 4n
Not enough swap space
.sp
A cluster node ran out of swap memory or ran out of other operating system resources.
.RE

.sp
.ne 2
.mk
.na
\fB\fB3\fR \fBCL_EINVAL\fR\fR
.ad
.sp .6
.RS 4n
Invalid argument
.sp
You typed the command incorrectly, or the syntax of the cluster
configuration information that you supplied with the \fB-i\fR option
was incorrect.
.RE

.sp
.ne 2
.mk
.na
\fB\fB6\fR \fBCL_EACCESS\fR\fR
.ad
.sp .6
.RS 4n
Permission denied
.sp
The object that you specified is inaccessible. You might need superuser 
or RBAC access to issue the command. See the 
\fBsu\fR(1M) 
and 
\fBrbac\fR(5) 
man pages for more information.
.RE

.sp
.ne 2
.mk
.na
\fB\fB18\fR \fBCL_EINTERNAL\fR\fR
.ad
.sp .6
.RS 4n
Internal error was encountered
.sp
An internal error indicates a software defect or other defect.
.RE

.sp
.ne 2
.mk
.na
\fB\fB35\fR \fBCL_EIO\fR\fR
.ad
.sp .6
.RS 4n
I/O error
.sp
A physical input/output error has occurred.
.RE

.sp
.ne 2
.mk
.na
\fB\fB36\fR \fBCL_ENOENT\fR\fR
.ad
.sp .6
.RS 4n
No such object
.sp
The object that you specified cannot be found for one of the following reasons:
.RS +4
.TP
.ie t \(bu
.el o
The object does not exist.
.RE
.RS +4
.TP
.ie t \(bu
.el o
A directory in the path to the configuration file that you attempted to create with the \fB-o\fR option does not exist.
.RE
.RS +4
.TP
.ie t \(bu
.el o
The configuration file that you attempted to access with the \fB-i\fR option contains errors.
.RE
.RE

.SH EXAMPLES
.LP
\fBExample 1 \fRDisplaying the Configuration of One Quorum Server
.sp
.LP
The following command displays the configuration information for the quorum server that uses port 9000.

.sp
.in +2
.nf
# \fBclquorumserver show 9000\fR
.fi
.in -2
.sp

.LP
\fBExample 2 \fRDisplaying the Configuration of Several Quorum Servers
.sp
.LP
The following command displays the configuration information for the quorum servers listed by their instance names. 

.sp
.in +2
.nf
# \fBclquorumserver show qs1 qs2 qs3\fR
.fi
.in -2
.sp

.LP
\fBExample 3 \fRDisplaying the Configuration of All Running Quorum Servers
.sp
.LP
The following command displays the configuration information of all running quorum servers. 

.sp
.in +2
.nf
# \fBclquorumserver show +\fR
.fi
.in -2
.sp

.LP
\fBExample 4 \fRStarting Quorum Servers
.sp
.LP
The following command starts all the configured quorum servers.

.sp
.in +2
.nf
# \fBclquorumserver start +\fR
.fi
.in -2
.sp

.sp
.LP
The following command starts a quorum server that is listening on port 9000.

.sp
.in +2
.nf
# \fBclquorumserver start 9000\fR
.fi
.in -2
.sp

.sp
.LP
The following command starts the quorum server instance \fBqs1\fR. 

.sp
.in +2
.nf
# \fBclquorumserver start qs1\fR
.fi
.in -2
.sp

.LP
\fBExample 5 \fRStopping a Quorum Server By Port Number
.sp
.LP
The following command stops a quorum server that is listening on port 9000.

.sp
.in +2
.nf
# \fBclquorumserver stop 9000\fR
.fi
.in -2
.sp

.LP
\fBExample 6 \fRCleaning Up Outdated Cluster Information From the Quorum Server
.sp
.LP
This example removes information about the cluster named \fBsc-cluster\fR from the quorum server.

.sp
.LP
Use caution when unconfiguring a quorum server in this way. Generally, you should use \fBclquorum remove\fR to remove the quorum server device from cluster configuration and clean up the configuration information on the quorum server in a single set. You should only need to
use this command if communications were lost between the cluster and the quorum server host during the \fBclquorum remove\fR operation. 

.sp
.in +2
.nf
# \fBclquorumserver clear -c sc-cluster -I 0x4308D2CF 9000\fR
The quorum server to be unconfigured must have been removed from
the cluster. Unconfiguring a valid quorum server could compromise
the cluster quorum. Do you want to continue? (yes or no)
.fi
.in -2
.sp

.SH ATTRIBUTES
.sp
.LP
See \fBattributes\fR(5) for descriptions of the following attributes:
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
ATTRIBUTE TYPEATTRIBUTE VALUE
_
AvailabilitySUNWscu
_
Interface StabilityEvolving
.TE

.SH SEE ALSO
.sp
.LP
\fBIntro\fR(1CL), \fBclquorum\fR(1CL),  \fBcluster\fR(1CL),  \fBscqsd\fR(1M),
\fBscqsd.conf\fR(4).
.SH NOTES
.sp
.LP
The superuser can run all forms of this command.
.sp
.LP
Any user can run this command with the following options:
.RS +4
.TP
.ie t \(bu
.el o
\fB-?\fR (help) option
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-V\fR (version) option
.RE
.sp
.LP
To run this command with other subcommands, users other than superuser require RBAC authorizations. See the following table.
.sp

.sp
.TS
tab();
cw(0i) |cw(5.5i) 
lw(0i) |lw(5.5i) 
.
SubcommandRBAC Authorization
_
\fBclear\fR\fBsolaris.cluster.admin\fR
_
\fBshow\fR\fBsolaris.cluster.read\fR
_
\fBstart\fR\fBsolaris.cluster.admin\fR
_
\fBstop\fR\fBsolaris.cluster.admin\fR
.TE

