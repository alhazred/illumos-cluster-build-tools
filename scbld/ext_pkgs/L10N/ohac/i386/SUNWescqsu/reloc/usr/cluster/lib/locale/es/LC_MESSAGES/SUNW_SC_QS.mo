!   D   �
  u  @  ��������               .   :   ��������i   {         �   �   ���������   �           /  ��������?  _        p  �  ���������  �     
   �  �  ���������  �  	      �    ���������  $          ?  ����   �    ���������  C          �  ��������G  �        o    ���������  X        �  �  ��������  �        2  %  ��������f  k        �  �  ���������  �        �  �  ���������            2  ��������  R        0  b  ����    ~  �  ���������        2   �  2  ���������  k  "   $     �  ��������7  �  #   '   O  �  ��������v  %  &   (   �  ^  ���������  �  %   -   �  �  ��������  �  *   ,     �  ��������2  	  +   /   Q  A	  ��������u  e	  .   0   �  	  ����1   �  �	  ���������  �	  )   ;      
  ��������?  o
  3   5   Y  �
  ��������t  �
  4   8   �    ���������    7   9   �  @  ����:   �  [  ���������  r  6   ?   1	  �  ��������C	  �  <   >   �	    ���������	  5  =   A   �	  Z  ���������	  �  @   B   #
  �  ����C   ?
    ��������p
  C  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 Debe especificar el servidor del qu�rum que va a borrar.
 Debe especificar "%s" o una lista de los servidores del qu�rum.
 Debe especificar tanto el nombre del cl�ster como su Id.
 No se pueden especificar  "%s" y el servidor del qu�rum al mismo tiempo.
 S�lo puede especificar la opci�n "-c" una vez.
 S�lo puede especificar la opci�n "-I" una vez.
 S�lo puede borrar servidores del qu�rum de uno en uno.
 Sintaxis: %s <subcommand> [<quorumserver> ...]
 Error de sintaxis.
 Sintaxis Subcomando no reconocido - "%s".

 Opci�n no reconocida: "%s".
 Opci�n inesperada - "%s".
 El servidor del qu�rum que se va a borrar debe haberse eliminado previamente del cl�ster. Si se borra un servidor del qu�rum v�lido, esta acci�n podr�a poner en peligro el qu�rum del cl�ster .
 Ya se est� utilizando el directorio del qu�rum de la l�nea "%s".
 El nombre o Id. de cl�ster no se ha configurado para el servidor del qu�rum en el puerto "%s".
 Se ha detenido el servidor del qu�rum en el puerto "%s".
 Se ha iniciado el servidor del qu�rum en el puerto "%s".
 El servidor del qu�rum en el puerto "%s" no se est� ejecutando.
 El servidor del qu�rum en el puerto "%s" no se ha configurado en ning�n cl�ster.
 Se ha borrado el servidor del qu�rum en el puerto "%s".
 El servidor del qu�rum a�n no se ha iniciado en el puerto "%s".

 El servidor del qu�rum "%s" no se ha configurado en el archivo "%s".
 El directorio del qu�rum "%s" de la l�nea "%s" no es exclusivo.
 La opci�n "%s" necesita un argumento.
 No hay suficiente memoria.
 Puerto no v�lido "%s".
 Puerto no v�lido "%s" en el archivo "%s".
 Id. de cl�ster no v�lido "%s".
 Error interno.
 No se ha podido iniciar el servidor del qu�rum en el puerto "%s". Consulte el registro del sistema para obtener informaci�n.
 Error al deshabilitar el servidor de qu�rum en el puerto "%s".
 �Desea continuar? El Id. de cl�ster "%s" no presenta formato hexadecimal.
 No se pueden enviar mensajes en el puerto "%s".
 No se pueden recibir mensajes en el puerto "%s".
 No se puede abrir el archivo "%s".
 No se puede obtener el nodo IP para el host "%s".
 No se puede obtener la longitud de los datos recibidos.
 No se puede ejecutar la l�nea "%s" en el archivo "%s".
 No se puede crear el socket en el puerto "%s".
 === Servidor del qu�rum en el puerto %s ===
 %s %s
         Clave de reserva:		0x%llx
         Clave de registro:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Permite detener los servidores del qu�rum. 
start	Permite iniciar los servidores del qu�rum. 
show	Muestra la configuraci�n de los servidores del qu�rum. 
clear	Permite borrar la configuraci�n del cl�ster de un servidor del qu�rum.

 
Permite detener los servidores del qu�rum.
 
Permite iniciar los servidores del qu�rum.
 
Muestra la configuraci�n de los servidores del qu�rum.
 
OPCIONES:
 
Administrar servidores del qu�rum

SUBCOMANDOS:
 
Desactivado			Verdadero

 
Desactivado			Falso

 
Permite borrar la configuraci�n del cl�ster de un servidor del qu�rum.
 
  Id. de nodo:			%d
 
  -y	Responde autom�ticamente "yes" (s�) a la pregunta de confirmaci�n.
 
  -v	Salida detallada


 
  -d	Desactivar servidor de qu�rum
 
  -c <clustername>
	Permite especificar el nombre del cl�ster.
 
  -I <clusterID>
	Permite especificar el Id. de cl�ster con formato hexadecimal.
 
  -?	Muestra un mensaje de ayuda.
 
  ---  Reserva del cl�ster %s (id 0x%8.8X) ---
 
  ---  Registro del cl�ster %s (id 0x%8.8X) ---
 