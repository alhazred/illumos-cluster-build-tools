!   D   �
  �  @  ��������               .      ��������i   H         �   i   ���������   �           �   ��������?  �         p  �   ���������       
   �    ���������    	      �  0  ���������  I          ^  ����   �  �  ���������  �            ��������G  )        o  K  ���������  o        �  �  ��������  �        2  �  ��������f          �  7  ���������  O        �  [  ���������  m          �  ��������  �        0  �  ����    ~  �  ���������       2   �  #  ���������  F  "   $     d  ��������7  �  #   '   O  �  ��������v  �  &   (   �  �  ���������  �  %   -   �    ��������  9  *   ,     @  ��������2  [  +   /   Q  v  ��������u  �  .   0   �  �  ����1   �  �  ���������  �  )   ;      �  ��������?    3   5   Y  )  ��������t  :  4   8   �  Q  ���������  Z  7   9   �  u  ����:   �  �  ���������  �  6   ?   1	  �  ��������C	  �  <   >   �	  �  ���������	  �  =   A   �	    ���������	  .  @   B   #
  Z  ����C   ?
  n  ��������p
  �  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 ����ָ��Ҫ����ķ�����������
 ����ָ�� "%s" �򷨶��������б�����֮һ��
 ����ͬʱָ��Ⱥ�����ƺ�Ⱥ�� ID��
 ����ͬʱָ�� "%s" �ͷ�����������
 ֻ��ָ�� "-c" ѡ��һ�Ρ�
 ֻ��ָ�� "-I" ѡ��һ�Ρ�
 һ��ֻ�����һ��������������
 �÷���%s <������> [<����������> ...]
 �÷�����
 �÷� ����ʶ��������� - "%s"��

 ����ʶ���ѡ�� - "%s"��
 �����ѡ�� - "%s"��
 Ҫ����ķ��������������Ѵ�Ⱥ����ɾ���������Ч�ķ������������ܻ�Ӱ��Ⱥ�������豸��
 �� "%s" �е�Ĭ�Ϸ���Ŀ¼�ѱ�ʹ�á�
 δΪ�˿� "%s" �ϵķ�������������Ⱥ�����ƻ� ID��
 ��ֹͣ�˿� "%s" �ϵķ�����������
 �������˿� "%s" �ϵķ�����������
 �˿� "%s" �ϵķ���������δ�����С�
 δ���κ�Ⱥ�������ö˿� "%s" �ϵķ�����������
 ������˿� "%s" �ϵķ�����������
 ������������δ�ڶ˿� "%s" ��������

 δ���ļ� "%2$s" �����÷��������� "%1$s"��
 �� "%2$s" �еķ���Ŀ¼ "%1$s" ��Ψһ��
 ѡ�� "%s" ��Ҫ�в�����
 �ڴ治�㡣
 �˿� "%s" ��Ч��
 �ļ� "%2$s" �еĶ˿� "%1$s" ��Ч��
 Ⱥ�� ID "%s" ��Ч��
 �ڲ�����
 �޷������˿� "%s" �ϵķ������������й���ϸ��Ϣ�������ϵͳ��־��
 �ڶ˿� "%s" �Ͻ��÷���������ʧ�ܡ�
 �Ƿ�Ҫ���� Ⱥ�� ID "%s" ����ʮ��������ʽ�ġ�
 �޷��ڶ˿� "%s" �Ϸ�����Ϣ��
 �޷��ڶ˿� "%s" �Ͻ�����Ϣ��
 �޷����ļ� "%s"��
 �޷�������� "%s" �� IP �ڵ㡣
 �޷�����ѽ������ݵĳ��ȡ�
 �޷�ִ���ļ� "%2$s" �е� "%1$s" �С�
 �޷��ڶ˿� "%s" �ϴ����׽��֡�
 === �˿� %s �ϵķ��������� ===
 %s %s
     ���� �ؼ��֣�		0x%llx
     ע�� �ؼ��֣�		0x%llx
        %s <������> -? | --help
        %s -V | --version
 
stop	ֹͣ���������� 
start	�������������� 
show	��ʾ���������������� 
clear	���������������Ⱥ������

 
ֹͣ����������
 
��������������
 
��ʾ����������������
 
ѡ�
 
��������������

�����
 
�ѽ���			True

 
�ѽ���			False

 
���������������Ⱥ������
 
  �ڵ� ID��			%d
 
  -y	��ȷ�������Զ��ش� "yes"
 
  -v	��ϸ���


 
  -d	���÷���������
 
  -c <Ⱥ������>
	ָ��Ⱥ������
 
  -I <Ⱥ�� ID>
	ָ��ʮ��������ʽ��Ⱥ�� ID
 
  -?	��ʾ������Ϣ
 
  ---  Ⱥ�� %s (id 0x%8.8X) ���� ---
 
  ---  Ⱥ�� %s (id 0x%8.8X) ע�� ---
 