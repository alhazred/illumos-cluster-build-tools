!   D   �
    @  ��������               .   8   ��������i   x         �   �   ���������   �           .  ��������?  ^        p  �  ���������  �     
   �  �  ���������  �  	      �  �  ���������            :  ����   �  �  ���������             y  ��������G  �        o  �  ���������  �        �  5  ��������  \        2  �  ��������f  �        �    ���������  )        �  H  ���������  _          �  ��������  �        0  �  ����    ~    ���������  W     2   �  o  ���������  �  "   $     �  ��������7    #   '   O  B  ��������v  x  &   (   �  �  ���������  �  %   -   �    ��������  >  *   ,     E  ��������2  j  +   /   Q  �  ��������u  �  .   0   �  �  ����1   �  �  ���������  	  )   ;      7	  ��������?  y	  3   5   Y  �	  ��������t  �	  4   8   �  �	  ���������  �	  7   9   �  	
  ����:   �  
  ���������  7
  6   ?   1	  r
  ��������C	  �
  <   >   �	  �
  ���������	  �
  =   A   �	     ���������	  5  @   B   #
  {  ����C   ?
  �  ��������p
  �  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 Sie m�ssen den zu bereinigenden Quorum-Server angeben.
 Geben Sie entweder "%s" an oder eine Liste von Quorum-Servern.
 Geben Sie sowohl den Cluster-Namen als auch die Cluster-ID an.
 "%s" und Quorum-Server k�nnen nicht gleichzeitig angegeben werden.
 Sie k�nnen die "-c"-Option nur einmal festlegen.
 Sie k�nnen die Option "-I" nur einmal angeben.
 Es kann immer nur ein Quorum-Server bereinigt werden.
 Verwendung: %s <subcommand> [<quorumserver> ...]
 Syntaxfehler.
 Syntax Unbekannter Unterbefehl - "%s".

 Nicht erkannte Option - "%s".
 Unerwartete Option - "%s".
 Der zu bereinigende Quorum-Server muss aus dem Cluster entfernt worden sein. Das Bereinigen eines g�ltigen Quorum-Servers k�nnte das Cluster-Quorum gef�hrden.
 Das Standard-Quorumverzeichnis in Zeile "%s" wird bereits verwendet.
 Cluster-Name bzw. Cluster-ID ist nicht f�r den Quorum-Server an Port "%s" konfiguriert.
 Quorum-Server an Port "%s" gestoppt.
 Quorum-Server an Port "%s" wird gestartet.
 Quorum-Server an Port "%s" nicht in Betrieb.
 Quorum-Server an Port "%s" in keinem Cluster konfiguriert.
 Quorum-Server an Port "%s" bereinigt.
 Quorum-Server noch nicht an Port "%s" gestartet.

 Quorum-Server "%s" nicht in der Datei "%s" konfiguriert.
 Quorumverzeichnis "%s" in Zeile "%s" ist nicht eindeutig.
 Option "%s" erfordert ein Argument.
 Nicht gen�gend Speicherplatz.
 Ung�ltiger Port "%s".
 Ung�ltiger Port "%s" in Datei "%s".
 Ung�ltige Cluster-ID "%s".
 Interner Fehler.
 Starten des Quorum-Servers an Port "%s" fehlgeschlagen. N�heres hierzu finden Sie im Systemprotokoll.
 Deaktivieren des Quorum-Servers an Port "%s" fehlgeschlagen.
 M�chten Sie fortfahren? Cluster-ID "%s" ist keine Hexadezimal-Zahl.
 Nachrichten k�nnen nicht �ber den Port "%s" versendet werden.
 Nachrichten k�nnen nicht �ber den Port "%s" empfangen werden.
 Datei kann nicht ge�ffnet werden "%s".
 IP-Knoten f�r Host "%s" kann nicht abgerufen werden.
 L�nge der empfangenen Daten kann nicht abgerufen werden.
 Zeile "%s" in Datei "%s" kann nicht ausgef�hrt werden.
  Socket an Port "%s" kann nicht erstellt werden.
 === Quorum-Server an Port %s ===
 %s %s
     Reservierungsschl�ssel:		0x%llx
     Registrierungsschl�ssel:		0x%llx
        %s <subcommand> -? | --Hilfe
        %s -V | --Version
 
stop	Quorum-Server stoppen 
start	Quorum-Server starten 
show	Konfiguration der Quorum-Server anzeigen 
clear	Cluster-Konfiguration f�r einen Quorum-Server bereinigen

 
Quorum-Server stoppen
 
Quorum-Server starten
 
Konfiguration der Quorum-Server anzeigen
 
OPTIONEN:
 
Quorum-Server verwalten

UNTERBEFEHLE:
 
Deaktiviert			Wahr

 
Deaktiviert			Falsch

 
Cluster-Konfiguration f�r einen Quorum-Server bereinigen
 
  Knoten-ID:			%d
 
  -y	Best�tigungsfrage automatisch mit "Ja" beantworten
 
  -v	Ausf�hrliche Ausgabe


 
  -d	Quorum-Server deaktivieren
 
  -c <clustername>
	Geben Sie den Cluster-Namen an
 
  -I <Cluster-ID>
	Geben Sie die Cluster-ID als Hexadezimal-Zahl an
 
  -?	Hilfemitteilung anzeigen
 
  ---  Cluster %s (ID 0x%8.8X) Reservierung ---
 
  ---  Cluster %s (ID 0x%8.8X) Registrierungen ---
 