!   D   �
  a  @  ��������               .   0   ��������i   h         �   �   ���������   �              ��������?  *        p  _  ���������  �     
   �  �  ���������  �  	      �  �  ���������  �            ����   �  �  ���������  �          8  ��������G  h        o  �  ���������  �        �  
  ��������  >        2  s  ��������f  �        �  �  ���������          �  9  ���������  S          �  ��������  �        0  �  ����    ~    ���������  T     2   �  a  ���������  �  "   $     �  ��������7  �  #   '   O    ��������v  D  &   (   �  t  ���������  �  %   -   �  �  ��������  �  *   ,       ��������2    +   /   Q  /  ��������u  S  .   0   �  m  ����1   �  �  ���������  �  )   ;      �  ��������?  	  3   5   Y  8	  ��������t  W	  4   8   �  z	  ���������  �	  7   9   �  �	  ����:   �  �	  ���������  �	  6   ?   1	  �	  ��������C	  
  <   >   �	  ?
  ���������	  S
  =   A   �	  y
  ���������	  �
  @   B   #
  �
  ����C   ?
    ��������p
  4  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 ���ꥢ��������­�������С�����ꤷ�Ƥ���������
 ��%s�פޤ�����­�������С��Υꥹ�Ȥ���ꤷ�Ƥ���������
 ���饹��̾�ȥ��饹�� ID ��ξ������ꤷ�Ƥ���������
 ��%s�פ���­�������С���Ʊ���˻��ꤹ�뤳�ȤϤǤ��ޤ���
 ��-c�ץ��ץ����ϰ��٤�������Ǥ��ޤ���
 ��-l�ץ��ץ����ϰ��٤�������Ǥ��ޤ���
 ���٤˥��ꥢ���Ǥ�����­�������С��� 1 �ĤΤߤǤ���
 ����ˡ: %s <subcommand> [<quorumserver> ...]
 ����ˡ�θ��ꡣ
 ���� ǧ������ʤ����֥��ޥ�� - ��%s��

 ���ץ���� ��%s�פ�ǧ���Ǥ��ޤ���
 ͽ�����Ƥ��ʤ����ץ���� - ��%s��
 ���ꥢ��������­�������С��ϡ����饹������������Ƥ���ɬ�פ�����ޤ���ͭ������­�������С��򥯥ꥢ������ȡ����饹����­�����������ޤ���
 ��%s�פιԤΥǥե������­���ǥ��쥯�ȥ�Ϥ��Ǥ˻��Ѥ���Ƥ��ޤ���
 �ݡ��ȡ�%s�פ���­�������С��ˤϡ����Υ��饹��̾�ޤ��ϥ��饹�� ID �Ϲ�������Ƥ��ޤ���
 �ݡ��ȡ�%s�פ���­�������С�����ߤ���ޤ�����
 �ݡ��ȡ�%s�פ���­�������С�����ư����ޤ�����
 �ݡ��ȡ�%s�פ���­�������С��ϲ�Ư���Ƥ��ޤ���
 �ݡ��ȡ�%s�פ���­�������С��ϡ����饹���˹�������Ƥ��ޤ���
 �ݡ��ȡ�%s�פ���­�������С������ꥢ������ޤ�����
 �ݡ��ȡ�%s�פ���­�������С��ϵ�ư����Ƥ��ޤ���

 �ե������%2$s�פˤϡ���­�������С���%1$s�פϹ�������Ƥ��ޤ���
 ��%2$s�פιԤ���­���ǥ��쥯�ȥ��%1$s�פ���դǤϤ���ޤ���
 ���ץ�����%s�פˤϰ�����ɬ�פǤ���
 ���꡼����­���Ƥ��ޤ���
 �ݡ��ȡ�%s�פ�̵���Ǥ���
 �ե������%2$s�����̵���ʥݡ��ȡ�%1$s�פ����ꤵ��Ƥ��ޤ���
 ���饹�� ID��%s�פ�̵���Ǥ���
 �������顼�Ǥ���
 �ݡ��ȡ�%s�פ���­�������С��ε�ư�˼��Ԥ��ޤ������ܺ٤ϡ������ƥ�����򻲾Ȥ��Ƥ���������
 �ݡ��ȡ�%s�פ���­�������С���̵�����˼��Ԥ��ޤ�����
 ³�Ԥ��ޤ��� ���饹�� ID��%s�פ� 16 �ʤǤϤ���ޤ���
 �ݡ��ȡ�%s�פǥ�å������������Ǥ��ޤ���Ǥ�����
 �ݡ��ȡ�%s�פǥ�å�����������Ǥ��ޤ���Ǥ�����
 �ե������%s�פ򳫤����Ȥ��Ǥ��ޤ���
 �ۥ��ȡ�%s�פ� IP �Ρ��ɤ�����Ǥ��ޤ���
 �������줿Ĺ���Υǡ���������Ǥ��ޤ���Ǥ�����
 �ե������%2$s�פΡ�%1$s�פιԤϼ¹ԤǤ��ޤ���
 �ݡ��ȡ�%s�פ˥����åȤ�����Ǥ��ޤ���Ǥ�����
 === �ݡ��� %s ����­�������С� ===
 %s %s
     ͽ�󥭡�:		0x%llx
     ��Ͽ����:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	��­�������С�����ߤ��ޤ��� 
start	��­�������С���ư���ޤ��� 
show	��­�������С��ι����򼨤��ޤ��� 
clear	��­�������С��Υ��饹�������򥯥꡼�󥢥åפ��ޤ���

 
��­�������С�����ߤ��ޤ���
 
��­�������С���ư���ޤ���
 
��­�������С��ι����򼨤��ޤ���
 
���ץ����:
 
��­�������С��δ���

���֥��ޥ��:
 
̵��			True

 
̵��			False

 
��­�������С��Υ��饹��������õ�ޤ���
 
  �Ρ��� ID:			%d
 
  -y	��ǧ�μ���˼�ưŪ�ˡ�yes�פǱ������ޤ���
 
  -v	��Ĺ�ʽ���


 
  -d	��­�������С���̵���ˤ��ޤ���
 
  -c <clustername>
	���饹��̾����ꤷ�ޤ���
 
  -I <clusterID>
	���饹�� ID �� 16 �ʿ��ǻ��ꤷ�ޤ���
 
  -?	�إ�ץ�å�������ɽ�����ޤ���
 
  ---  ���饹�� %s (ID 0x%8.8X) ��ͽ�� ---
 
  ---  ���饹�� %s (ID 0x%8.8X) ����Ͽ ---
 