!   D   £
    @  ’’’’’’               .   1   ’’’’’’i   n         „   ø   ’’’’’’ē             :  ’’’’’’?  q        p  ®  ’’’’’’  ć     
   «  õ  ’’’’’’±  ü  	      Ó  )  ’’’’’’š  N          o  ’’’       ’’’’’’Õ  [          Ę  ’’’’’’G  ž        o  9  ’’’’’’  |        Ł  Ö  ’’’’’’          2  U  ’’’’’’f            ļ  ’’’’’’¼          Š  9  ’’’’’’ä  Z            ’’’’’’  ¼        0  Õ  ’’’    ~  S  ’’’’’’­       2   Å  µ  ’’’’’’ģ  ķ  "   $     #  ’’’’’’7  \  #   '   O    ’’’’’’v  Ź  &   (     	  ’’’’’’Ē  B	  %   -   ė  u	  ’’’’’’  	  *   ,      	  ’’’’’’2  ¹	  +   /   Q  Ņ	  ’’’’’’u  ł	  .   0     
  ’’’1   ­  .
  ’’’’’’Ķ  J
  )   ;      o
  ’’’’’’?  ¤
  3   5   Y  »
  ’’’’’’t  Ņ
  4   8   £  ó
  ’’’’’’®  ż
  7   9   Ō  $  ’’’:   ē  >  ’’’’’’ū  Y  6   ?   1	    ’’’’’’C	    <   >   	  Ķ  ’’’’’’	  ī  =   A   ø	    ’’’’’’ē	  I  @   B   #
    ’’’C   ?
  „  ’’’’’’p
  Õ  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 ģ§ģø ģæ¼ė¼ ģė²ė„¼ ģ§ģ ķ“ģ¼ ķ©ėė¤.
 "%s" ėė ģæ¼ė¼ ģė² ėŖ©ė”ģ ģ§ģ ķ“ģ¼ ķ©ėė¤.
 ķ“ė¬ģ¤ķ° ģ“ė¦ź³¼ ķ“ė¬ģ¤ķ° IDė„¼ ėŖØė ģ§ģ ķ“ģ¼ ķ©ėė¤.
 "%s" ė° ģæ¼ė¼ ģė²ė„¼ ėģģ ėŖØė ģ§ģ ķ  ģė ģģµėė¤.
 "-c" ģµģģ ķ ė²ė§ ģ§ģ ķ  ģ ģģµėė¤.
 "-I" ģµģģ ķ ė²ė§ ģ§ģ ķ  ģ ģģµėė¤.
 ķ ė²ģ ģæ¼ė¼ ģė² ķėė§ ģ§ģø ģ ģģµėė¤.
 ģ¬ģ©ė²: %s <ķģ ėŖė ¹> [<ģæ¼ė¼ ģė²> ...]
 ģ¬ģ©ė² ģ¤ė„
 ģ¬ģ© ģøģķ  ģ ģė ķģ ėŖė ¹ - "%s".

 ģøģķ  ģ ģė ģµģ - "%s".
 ģźø°ģ¹ ģģ ģµģ - "%s".
 ģ§ģģ¼ ķ  ģæ¼ė¼ ģė²ė„¼ ķ“ė¬ģ¤ķ°ģģ ģ ź±°ķģ“ģ¼ ķ©ėė¤. ģ ķØķ ģæ¼ė¼ ģė²ė„¼ ģ§ģ°ė©“ ķ“ė¬ģ¤ķ° ģæ¼ė¼ģ“ ģģė  ģ ģģµėė¤.
 "%s" ķģ źø°ė³ø ģæ¼ė¼ ėė ķ ė¦¬ė„¼ ģ“ėÆø ģ¬ģ©ķģµėė¤.
 ķ“ė¬ģ¤ķ° ģ“ė¦ ėė IDź° ķ¬ķø "%s"ģ ģæ¼ė¼ ģė²ģ ėķ“ źµ¬ģ±ėģ“ ģģ§ ģģµėė¤.
 ķ¬ķø "%s"ģ ģæ¼ė¼ ģė²ź° ģ¤ģ§ėģģµėė¤.
 ķ¬ķø "%s"ģģ ģæ¼ė¼ ģė²ź° ģģėģģµėė¤.
 ķ¬ķø "%s"ģ ģė ģæ¼ė¼ ģė²ź° ģ¤ķėģ§ ģģµėė¤.
 ķ¬ķø "%s"ģ ģė ģæ¼ė¼ ģė²ź° ķ“ė¬ģ¤ķ°ģ źµ¬ģ±ėģ“ ģģ§ ģģµėė¤.
 ķ¬ķø "%s"ģ ģæ¼ė¼ ģė²ź° ģ§ģģ”ģµėė¤.
 ģæ¼ė¼ ģė²ź° ķ¬ķø "%s"ģģ ģģ§ ģģėģ§ ģģģµėė¤.

 ģæ¼ė¼ ģė² "%s"ģ“(ź°) "%s" ķģ¼ģ źµ¬ģ±ėģ§ ģģģµėė¤.
 ģæ¼ė¼ ėė ķ ė¦¬ "%s"("%s" ķģ ģģ)ģ“(ź°) ź³ ģ ķģ§ ģģµėė¤.
 ģµģ "%s"ģ ģøģź° ķģķ©ėė¤.
 ė©ėŖØė¦¬ź° ė¶ģ”±ķ©ėė¤.
 ģėŖ»ė ķ¬ķø "%s"ģėė¤.
 ģėŖ»ė ķ¬ķø "%s"("%s" ķģ¼ģ ģģ)ģėė¤.
 ģėŖ»ė ķ“ė¬ģ¤ķ° ID "%s"ģėė¤.
 ė“ė¶ ģ¤ė„ģėė¤.
 ķ¬ķø "%s"ģģ ģæ¼ė¼ ģė²ė„¼ ģģķģ§ ėŖ»ķģµėė¤. ģģøķ ė“ģ©ģ ģģ¤ķ ė”ź·øė„¼ ģ°øģ”°ķģ­ģģ¤.
 "%s" ķ¬ķøģģ ģæ¼ė¼ ģė²ė„¼ ė¹ķģ±ķķģ§ ėŖ»ķģµėė¤.
 ź³ģķģź² ģµėź¹? ķ“ė¬ģ¤ķ° ID "%s"ģ“(ź°) 16ģ§ģź° ģėėė¤.
 ķ¬ķø "%s"ģ ė©ģģ§ė„¼ ė³“ė¼ ģ ģģµėė¤.
 ķ¬ķø "%s"ģģ ė©ģģ§ė„¼ ė°ģ ģ ģģµėė¤.
 ķģ¼ "%s"ģ(ė„¼) ģ“ ģ ģģµėė¤.
 ķøģ¤ķø "%s"ģ ėķ IP ėøėė„¼ ź°ģ øģ¬ ģ ģģµėė¤.
 ė°ģ ė°ģ“ķ° źøøģ“ė„¼ ź°ģ øģ¬ ģ ģģµėė¤.
 "%s" ķ("%s" ķģ¼ģ ģģ)ģ ģ¤ķķ  ģ ģģµėė¤.
 ķ¬ķø "%s"ģ ģģ¼ģ ė§ė¤ ģ ģģµėė¤.
 === ķ¬ķø %sģ ģæ¼ė¼ ģė² ===
 %s %s
     ģģ½ ķ¤:		0x%llx
     ė±ė” ķ¤:		0x%llx
        %s <ķģ ėŖė ¹> -? | --help
        %s -V | --version
 
stop	ģæ¼ė¼ ģė² ģ¤ģ§ 
start	ģæ¼ė¼ ģė² ģģ 
show	ģæ¼ė¼ ģė²ģ źµ¬ģ± ķģ 
clear	ģæ¼ė¼ ģė²ģ ķ“ė¬ģ¤ķ° źµ¬ģ± ģ ė¦¬

 
ģæ¼ė¼ ģė² ģ¤ģ§
 
ģæ¼ė¼ ģė² ģģ
 
ģæ¼ė¼ ģė²ģ źµ¬ģ± ķģ
 
ģµģ:
 
ģæ¼ė¼ ģė² ź“ė¦¬

ķģ ėŖė ¹:
 
ė¹ķģ±ķėØ			True

 
ė¹ķģ±ķėØ			False

 
ģæ¼ė¼ ģė²ģ ķ“ė¬ģ¤ķ° źµ¬ģ± ģ§ģ°źø°
 
  ėøė ID:			%d
 
  -y	ķģø ģ§ė¬øģ "ģ"ė” ģė ģėµ
 
  -v	ģģøķ ķģ ģ¶ė „


 
  -d	ģæ¼ė¼ ģė² ė¹ķģ±ķ
 
  -c <ķ“ė¬ģ¤ķ° ģ“ė¦>
	ķ“ė¬ģ¤ķ° ģ“ė¦ ģ§ģ 
 
  -I <clusterID>
	ķ“ė¬ģ¤ķ° IDė„¼ 16ģ§ģė” ģ§ģ 
 
  -?	ėģė§ ė©ģģ§ ķģ
 
  ---  ķ“ė¬ģ¤ķ° %s(id 0x%8.8X) ģģ½ ---
 
  ---  ķ“ė¬ģ¤ķ° %s(id 0x%8.8X) ė±ė” ---
 