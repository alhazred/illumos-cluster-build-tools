!   D   �
  �	  @  ��������               .   #   ��������i   P         �   �   ���������   �           �   ��������?          p  :  ���������  d     
   �  q  ���������  v  	      �  �  ���������  �          �  ����   �  E  ���������  x          �  ��������G  �        o    ���������  K        �  �  ��������  �        2  �  ��������f           �  ]  ���������  }        �  �  ���������  �          �  ��������  �        0  	  ����    ~  c  ���������  �     2   �  �  ���������  �  "   $     �  ��������7  &  #   '   O  G  ��������v  y  &   (   �  �  ���������  �  %   -   �  �  ��������    *   ,       ��������2  2  +   /   Q  H  ��������u  k  .   0   �  �  ����1   �  �  ���������  �  )   ;      �  ��������?  �  3   5   Y    ��������t    4   8   �  .  ���������  6  7   9   �  S  ����:   �  h  ���������  ~  6   ?   1	  �  ��������C	  �  <   >   �	  �  ���������	  �  =   A   �	  	  ���������	  6	  @   B   #
  f	  ����C   ?
  �	  ��������p
  �	  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 ���� ���� ������ �����ؾ� �մϴ�.
 "%s" �Ǵ� ���� ���� ����� �����ؾ� �մϴ�.
 Ŭ������ �̸��� Ŭ������ ID�� ��� �����ؾ� �մϴ�.
 "%s" �� ���� ������ ���ÿ� ��� ������ ���� �����ϴ�.
 "-c" �ɼ��� �� ���� ������ �� �ֽ��ϴ�.
 "-I" �ɼ��� �� ���� ������ �� �ֽ��ϴ�.
 �� ���� ���� ���� �ϳ��� ���� �� �ֽ��ϴ�.
 ����: %s <���� ����> [<���� ����> ...]
 ���� ����
 ��� �ν��� �� ���� ���� ���� - "%s".

 �ν��� �� ���� �ɼ� - "%s".
 ����ġ ���� �ɼ� - "%s".
 ������ �� ���� ������ Ŭ�����Ϳ��� �����߾�� �մϴ�. ��ȿ�� ���� ������ ����� Ŭ������ ������ �ջ�� �� �ֽ��ϴ�.
 "%s" ���� �⺻ ���� ���丮�� �̹� ����߽��ϴ�.
 Ŭ������ �̸� �Ǵ� ID�� ��Ʈ "%s"�� ���� ������ ���� �����Ǿ� ���� �ʽ��ϴ�.
 ��Ʈ "%s"�� ���� ������ �����Ǿ����ϴ�.
 ��Ʈ "%s"���� ���� ������ ���۵Ǿ����ϴ�.
 ��Ʈ "%s"�� �ִ� ���� ������ ������� �ʽ��ϴ�.
 ��Ʈ "%s"�� �ִ� ���� ������ Ŭ�����Ϳ� �����Ǿ� ���� �ʽ��ϴ�.
 ��Ʈ "%s"�� ���� ������ ���������ϴ�.
 ���� ������ ��Ʈ "%s"���� ���� ���۵��� �ʾҽ��ϴ�.

 ���� ���� "%s"��(��) "%s" ���Ͽ� �������� �ʾҽ��ϴ�.
 ���� ���丮 "%s"("%s" �࿡ ����)��(��) �������� �ʽ��ϴ�.
 �ɼ� "%s"�� �μ��� �ʿ��մϴ�.
 �޸𸮰� �����մϴ�.
 �߸��� ��Ʈ "%s"�Դϴ�.
 �߸��� ��Ʈ "%s"("%s" ���Ͽ� ����)�Դϴ�.
 �߸��� Ŭ������ ID "%s"�Դϴ�.
 ���� �����Դϴ�.
 ��Ʈ "%s"���� ���� ������ �������� ���߽��ϴ�. �ڼ��� ������ �ý��� �α׸� �����Ͻʽÿ�.
 "%s" ��Ʈ���� ���� ������ ��Ȱ��ȭ���� ���߽��ϴ�.
 ����Ͻðڽ��ϱ�? Ŭ������ ID "%s"��(��) 16������ �ƴմϴ�.
 ��Ʈ "%s"�� �޽����� ���� �� �����ϴ�.
 ��Ʈ "%s"���� �޽����� ���� �� �����ϴ�.
 ���� "%s"��(��) �� �� �����ϴ�.
 ȣ��Ʈ "%s"�� ���� IP ��带 ������ �� �����ϴ�.
 ���� ������ ���̸� ������ �� �����ϴ�.
 "%s" ��("%s" ���Ͽ� ����)�� ������ �� �����ϴ�.
 ��Ʈ "%s"�� ������ ���� �� �����ϴ�.
 === ��Ʈ %s�� ���� ���� ===
 %s %s
     ���� Ű:		0x%llx
     ��� Ű:		0x%llx
        %s <���� ����> -? | --help
        %s -V | --version
 
stop	���� ���� ���� 
start	���� ���� ���� 
show	���� ������ ���� ǥ�� 
clear	���� ������ Ŭ������ ���� ����

 
���� ���� ����
 
���� ���� ����
 
���� ������ ���� ǥ��
 
�ɼ�:
 
���� ���� ����

���� ����:
 
��Ȱ��ȭ��			True

 
��Ȱ��ȭ��			False

 
���� ������ Ŭ������ ���� �����
 
  ��� ID:			%d
 
  -y	Ȯ�� ������ "��"�� �ڵ� ����
 
  -v	�ڼ��� ǥ�� ���


 
  -d	���� ���� ��Ȱ��ȭ
 
  -c <Ŭ������ �̸�>
	Ŭ������ �̸� ����
 
  -I <clusterID>
	Ŭ������ ID�� 16������ ����
 
  -?	���� �޽��� ǥ��
 
  ---  Ŭ������ %s(id 0x%8.8X) ���� ---
 
  ---  Ŭ������ %s(id 0x%8.8X) ��� ---
 