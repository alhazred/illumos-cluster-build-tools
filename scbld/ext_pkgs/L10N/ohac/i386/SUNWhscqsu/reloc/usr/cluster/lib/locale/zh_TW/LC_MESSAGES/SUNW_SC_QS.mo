!   D   �
  &  @  ��������               .   !   ��������i   E         �   n   ���������   �           �   ��������?  �         p  �   ���������       
   �  %  ���������  *  	      �  E  ���������  _          w  ����   �  �  ���������  �          '  ��������G  L        o  q  ���������  �        �  �  ��������  �        2    ��������f  =        �  k  ���������  �        �  �  ���������  �          �  ��������  �        0  �  ����    ~  8  ���������  \     2   �  i  ���������  �  "   $     �  ��������7  �  #   '   O  �  ��������v     &   (   �    ���������  B  %   -   �  d  ��������  �  *   ,     �  ��������2  �  +   /   Q  �  ��������u  �  .   0   �  �  ����1   �    ���������  ,  )   ;      K  ��������?  n  3   5   Y  �  ��������t  �  4   8   �  �  ���������  �  7   9   �  �  ����:   �  �  ���������  �  6   ?   1	    ��������C	  *  <   >   �	  N  ���������	  `  =   A   �	  v  ���������	  �  @   B   #
  �  ����C   ?
  �  ��������p
  �  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 ���������������������� ��Τ�¡�
 ������������%s����������Τ�����̡�
 �������������� ������������� ID ̥ϯ��
 ����������������%s�������� ��Τ��̥ϯ��
 ��Ʒ��������-c������ġȹ��
 ��Ʒ��������-I������ġȹ��
 ��ġȹƷ������ġԶ���� ��Τ�¡�
 ����: %s <������> [<quorum ��Τ��> ...]
 ������먡�
 ���� �������������� -��%s�ס�

 ������ɱ������ -��%s�ס�
 ������������ -��%s�ס�
 ��������������Τ���������� ڷ���������桤����ȴ���� ������Τ������������������
 ���%s���� ������ ���������� �������㡤
 �����������ա�%s��ĸ ��������Τ������������� �� ID��
 �����������ա�%s��ĸ�� ������Τ�¡�
 ����ٯ �����ա�%s��ĸ��������Τ�¡�
 �����ա�%s��ĸ��������Τ�� �����硤
 ����Ƕ�����������������ա�%s��ĸ�� ������Τ�¡�
 �����������ա�%s��ĸ�� ������Τ�¡�
 ������ٯ�����ա�%s��ĸ�� ������Τ�¡�

 ������Τ�¡�%s���� ��������%s����������
 ���%$2s�������������� ��%$1s������ ��ġ����
 ������%s����ȴſ����
 ش�������͡�
 ���������ա�%s�ס�
 ������%$2s������������������ ��%$1s�ס�
 ���������� ID ��%s�ס�
 ������먡�
 ������ٯ �����ա�%s��ĸ�� ������Τ�¡�����������ع����ٶ��˷��ش����
 �����������ա�%s��ĸ��������������
 ������������ ���� ID��%s������Ĳ������֪Ȣ��
 �����������ա�%s��ĸ���ع�ɡ�
 �����������ա�%s��ĸ��ȭع�ɡ�
 ��������������%s�ס�
 ����̽ڵ����%s���� IP ������
 ����̽ڵȭ̯������������
 ���������� ��%$2s�������� ��%$1s���硤
 �����������ա�%s��ĸ��ǡ��عꦡ�
 === ������ %s ĸ�� quorum ��Τ�� ===
 %s %s
     ���� ����:		0x%llx
     ��� �:		0x%llx
        %s <������> -? | --help
        %s -V | --version
 
stop	���� quorum ��Τ�� 
start	��ٯ quorum ��Τ�� 
show	���� quorum ��Τ�������� 
clear	����������Τ���� ��������

 
���� quorum ��Τ��
 
��ٯ quorum ��Τ��
 
���� quorum ��Τ��������
 
����:
 
�ܡ quorum ��Τ��

������:
 
������			True

 
������			False

 
����������Τ���� ��������
 
  ���� ID:			%d
 
  -y	��ٯ��������������� ��yes��
 
  -v	��������


 
  -d	����������Τ��
 
  -c <�������>
	�����������
 
  -I <���� ID>
	����Ĳ������֪Ȣ�� ���� ID
 
  -?	�������ع��
 
  ---  ���� %s (id 0x%8.8X)  ���� ---
 
  ---  ���� %s (id 0x%8.8X) ��� ---
 