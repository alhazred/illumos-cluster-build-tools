!   D   �
  �  @  ��������               .   6   ��������i   |         �   �   ���������   �           4  ��������?  o        p  �  ���������  �     
   �  �  ���������    	      �  +  ���������  H          c  ����   �  �  ���������  =          �  ��������G  �        o  �  ���������  F        �  �  ��������  �        2    ��������f  K        �  �  ���������  �        �  �  ���������  �          
  ��������  *        0  ;  ����    ~  �  ���������  �     2   �    ���������  2  "   $     g  ��������7  �  #   '   O  �  ��������v  �  &   (   �  .  ���������  i  %   -   �  �  ��������  �  *   ,     �  ��������2  �  +   /   Q  	  ��������u  7	  .   0   �  Q	  ����1   �  v	  ���������  �	  )   ;      �	  ��������?  
  3   5   Y  :
  ��������t  \
  4   8   �  �
  ���������  �
  7   9   �  �
  ����:   �  �
  ���������  �
  6   ?   1	  1  ��������C	  I  <   >   �	  �  ���������	  �  =   A   �	  �  ���������	     @   B   #
  F  ����C   ?
  h  ��������p
  �  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 Vous devez sp�cifier le serveur de quorum � effacer.
 Vous devez sp�cifier soit "%s" soit une liste de serveurs de quorum.
 Vous devez sp�cifier le nom du cluster et son ID.
 Vous ne pouvez pas sp�cifier "%s" et le serveur de quorum en m�me temps.
 Vous ne pouvez sp�cifier l'option "-c" qu'une seule fois.
 Vous ne pouvez sp�cifier l'option "-I" qu'une seule fois.
 Vous ne pouvez effacer qu'un seul serveur de quorum � la fois.
 Utilisation : %s <subcommand> [<quorumserver> ...]
 Erreur d'utilisation.
 Utilisation Sous-commande non reconnue - "%s".

 Option non reconnue - "%s".
 Option inattendue - "%s".
 Le quorum de serveur � effacer doit avoir �t� supprim� du cluster. Effacer un serveur de quorum valide pourrait compromettre le quorum du cluster.
 Le r�pertoire de quorum par d�faut � la ligne "%s" est d�j� utilis�.
 Le nom ou l'ID du cluster n'est pas configur� pour le serveur de quorum sur le port "%s".
 Le serveur de quorum sur le port "%s" est arr�t�.
 Le serveur de quorum sur le port "%s" est d�marr�.
 Le serveur de quorum sur le port "%s" n'est pas en cours d'ex�cution.
 Le serveur de quorum sur le port "%s" n'est configur� dans aucun cluster.
 Le serveur de quorum sur le port "%s" est effac�.
 Le serveur de quorum n'est pas encore d�marr� sur le port "%s".

 Le serveur de quorum "%s" n'est pas configur� dans le fichier "%s".
 Le r�pertoire de quorum "%s" � la ligne "%s" n'est pas unique.
 L'option "%s" requiert un argument.
 M�moire insuffisante.
 Port "%s" non valide.
 Port non valide "%s" dans le fichier "%s".
 ID de cluster non valide "%s".
 Erreur interne.
 �chec du d�marrage du serveur de quorum sur le port "%s". Consultez le journal du syst�me pour plus de d�tails.
 Impossible de d�sactiver le serveur de quorum sur le port "%s".
 Voulez-vous continuer~? L'ID de cluster "%s" n'est pas hexad�cimal.
 Impossible d'envoyer des messages sur le port "%s".
 Impossible de recevoir des messages sur le port "%s".
 Impossible d'ouvrir le fichier "%s".
 Impossible d'obtenir le noeud IP pour l'h�te "%s".
 Impossible d'obtenir la longueur des donn�es re�ues.
 Impossible d'ex�cuter la ligne "%s" dans le fichier "%s".
 Cr�ation de socket impossible sur le port "%s".
 === Serveur de quorum sur le port %s ===
 %s %s
     Cl� de r�servation : 		0x%llx
     Cl� d'enregistrement : 		0x%llx
        %s <subcommand> -? | --aide
        %s -V | --version
 
stop	Arr�ter les serveurs de quorum 
start	D�marrer les serveurs de quorum 
show	Afficher la configuration des serveurs de quorum 
clear	Nettoyer la configuration du cluster d'un serveur de quorum

 
Arr�ter les serveurs de quorum
 
D�marrer les serveurs de quorum
 
Afficher la configuration des serveurs de quorum
 
OPTIONS :
 
G�rer les serveurs de quorum

SOUS-COMMANDES :
 
Disactiv�			Vrai

 
Disactiv�			Faux

 
Effacer la configuration du cluster d'un serveur de quorum
 
  ID de noeud : 			%d
 
  -y	R�pondre automatiquement "oui" � la question de confirmation
 
  -v	Sortie d�taill�e


 
  -d	D�sactiver le serveur de quorum
 
  -c <clustername>
	Sp�cifier le nom du cluster
 
  -I <clusterID>
	Sp�cifier l'ID de cluster comme �tant hexad�cimal
 
  -?	Afficher le message d'aide
 
  ---  R�servation du cluster %s (id 0x%8.8X)  ---
 
  ---  Enregistrements du cluster %s (id 0x%8.8X)  ---
 