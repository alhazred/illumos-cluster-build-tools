!   D   �
    @  ��������               .   8   ��������i            �   �   ���������   �           ;  ��������?  w        p  �  ���������  �     
   �    ���������    	      �  4  ���������  Q          l  ����   �    ���������  P          �  ��������G  �        o    ���������  _        �  �  ��������  �        2  #  ��������f  i        �  �  ���������  �        �  �  ���������  �          +  ��������  K        0  \  ����    ~  �  ���������       2   �  +  ���������  Y  "   $     �  ��������7  �  #   '   O  �  ��������v     &   (   �  X  ���������  �  %   -   �  �  ��������  �  *   ,     �  ��������2  	  +   /   Q  B	  ��������u  f	  .   0   �  �	  ����1   �  �	  ���������  �	  )   ;      
  ��������?  J
  3   5   Y  l
  ��������t  �
  4   8   �  �
  ���������  �
  7   9   �     ����:   �    ���������  *  6   ?   1	  g  ��������C	    <   >   �	  �  ���������	  �  =   A   �	  	  ���������	  <  @   B   #
  �  ����C   ?
  �  ��������p
  �  You must specify the quorum server to clear.
 You must specify either "%s" or a list of quorum servers.
 You must specify both the cluster name and the cluster ID.
 You cannot specify both "%s" and quorum server at the same time.
 You can only specify the "-c" option once.
 You can only specify the "-I" option once.
 You can only clear one quorum server at a time.
 Usage: %s <subcommand> [<quorumserver> ...]
 Usage error.
 Usage Unrecognized subcommand - "%s".

 Unrecognized option - "%s".
 Unexpected option - "%s".
 The quorum server to be cleared must have been removed from the cluster. Clearing a valid quorum server could compromise the cluster quorum.
 The default quorum directory in line "%s" is already used.
 The cluster name or ID is not configured for quorum server on port "%s".
 Quorum server on port "%s" is stopped.
 Quorum server on port "%s" is started.
 Quorum server on port "%s" is not running.
 Quorum server on port "%s" is not configured in any cluster.
 Quorum server on port "%s" is cleared.
 Quorum server is not yet started on port "%s".

 Quorum server "%s" is not configured in file "%s".
 Quorum directory "%s" in line "%s" is not unique.
 Option "%s" requires an argument.
 Not enough memory.
 Invalid port "%s".
 Invalid port "%s" in file "%s".
 Invalid cluster ID "%s".
 Internal error.
 Failed to start quorum server on port "%s". Refer to system log for details.
 Failed to disable quorum server on port "%s".
 Do you want to continue Cluster ID "%s" is not a hexadecimal.
 Cannot send messages on port "%s".
 Cannot receive messages on port "%s".
 Cannot open file "%s".
 Cannot get the IP node for host "%s".
 Cannot get length of received data.
 Cannot execute the "%s" line in file "%s".
 Cannot create socket on port "%s".
 === Quorum Server on port %s ===
 %s %s
     Reservation key:		0x%llx
     Registration key:		0x%llx
        %s <subcommand> -? | --help
        %s -V | --version
 
stop	Stop the quorum servers 
start	Start the quorum servers 
show	Show the configuration of the quorum servers 
clear	Cleanup the cluster configuration for a quorum server

 
Stop the quorum servers
 
Start the quorum servers
 
Show the configuration of the quorum servers
 
OPTIONS:
 
Manage quorum servers

SUBCOMMANDS:
 
Disabled			True

 
Disabled			False

 
Clear the cluster configuration for a quorum server
 
  Node ID:			%d
 
  -y	Automatically respond "yes" to the confirmation question
 
  -v	Verbose output


 
  -d	Disable quorum server
 
  -c <clustername>
	Specify the cluster name
 
  -I <clusterID>
	Specify the cluster ID as a hexadecimal
 
  -?	Display help message
 
  ---  Cluster %s (id 0x%8.8X) Reservation ---
 
  ---  Cluster %s (id 0x%8.8X) Registrations ---
 Vous devez spécifier le serveur de quorum à effacer.
 Vous devez spécifier soit "%s" soit une liste de serveurs de quorum.
 Vous devez spécifier le nom du cluster et son ID.
 Vous ne pouvez pas spécifier "%s" et le serveur de quorum en même temps.
 Vous ne pouvez spécifier l'option "-c" qu'une seule fois.
 Vous ne pouvez spécifier l'option "-I" qu'une seule fois.
 Vous ne pouvez effacer qu'un seul serveur de quorum à la fois.
 Utilisation : %s <subcommand> [<quorumserver> ...]
 Erreur d'utilisation.
 Utilisation Sous-commande non reconnue - "%s".

 Option non reconnue - "%s".
 Option inattendue - "%s".
 Le quorum de serveur à effacer doit avoir été supprimé du cluster. Effacer un serveur de quorum valide pourrait compromettre le quorum du cluster.
 Le répertoire de quorum par défaut à la ligne "%s" est déjà utilisé.
 Le nom ou l'ID du cluster n'est pas configuré pour le serveur de quorum sur le port "%s".
 Le serveur de quorum sur le port "%s" est arrêté.
 Le serveur de quorum sur le port "%s" est démarré.
 Le serveur de quorum sur le port "%s" n'est pas en cours d'exécution.
 Le serveur de quorum sur le port "%s" n'est configuré dans aucun cluster.
 Le serveur de quorum sur le port "%s" est effacé.
 Le serveur de quorum n'est pas encore démarré sur le port "%s".

 Le serveur de quorum "%s" n'est pas configuré dans le fichier "%s".
 Le répertoire de quorum "%s" à la ligne "%s" n'est pas unique.
 L'option "%s" requiert un argument.
 Mémoire insuffisante.
 Port "%s" non valide.
 Port non valide "%s" dans le fichier "%s".
 ID de cluster non valide "%s".
 Erreur interne.
 Échec du démarrage du serveur de quorum sur le port "%s". Consultez le journal du système pour plus de détails.
 Impossible de désactiver le serveur de quorum sur le port "%s".
 Voulez-vous continuer~? L'ID de cluster "%s" n'est pas hexadécimal.
 Impossible d'envoyer des messages sur le port "%s".
 Impossible de recevoir des messages sur le port "%s".
 Impossible d'ouvrir le fichier "%s".
 Impossible d'obtenir le noeud IP pour l'hôte "%s".
 Impossible d'obtenir la longueur des données reçues.
 Impossible d'exécuter la ligne "%s" dans le fichier "%s".
 Création de socket impossible sur le port "%s".
 === Serveur de quorum sur le port %s ===
 %s %s
     Clé de réservation : 		0x%llx
     Clé d'enregistrement : 		0x%llx
        %s <subcommand> -? | --aide
        %s -V | --version
 
stop	Arrêter les serveurs de quorum 
start	Démarrer les serveurs de quorum 
show	Afficher la configuration des serveurs de quorum 
clear	Nettoyer la configuration du cluster d'un serveur de quorum

 
Arrêter les serveurs de quorum
 
Démarrer les serveurs de quorum
 
Afficher la configuration des serveurs de quorum
 
OPTIONS :
 
Gérer les serveurs de quorum

SOUS-COMMANDES :
 
Disactivé			Vrai

 
Disactivé			Faux

 
Effacer la configuration du cluster d'un serveur de quorum
 
  ID de noeud : 			%d
 
  -y	Répondre automatiquement "oui" à la question de confirmation
 
  -v	Sortie détaillée


 
  -d	Désactiver le serveur de quorum
 
  -c <clustername>
	Spécifier le nom du cluster
 
  -I <clusterID>
	Spécifier l'ID de cluster comme étant hexadécimal
 
  -?	Afficher le message d'aide
 
  ---  Réservation du cluster %s (id 0x%8.8X)  ---
 
  ---  Enregistrements du cluster %s (id 0x%8.8X)  ---
 