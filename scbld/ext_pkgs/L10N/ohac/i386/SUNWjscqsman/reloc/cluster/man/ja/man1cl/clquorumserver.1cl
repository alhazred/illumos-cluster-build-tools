'\" te
.\" Copyright 2005 Sun Microsystems, Inc. All rights reserved. Use is subject to license terms.
.TH clquorumserver 1CL "04 Oct 2005" "Sun Cluster 3.2" "Sun Cluster 保守コマンド"
.SH NAME
clquorumserver, clqs \- 定足数サーバーの管理
.SH SYNOPSIS
.LP
.nf
\fB/usr/cluster/bin/clquorumserver\fR \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorumserver \fIsubcommand\fR\fR 
\fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorumserver \fIsubcommand\fR\fR 
[\fB-v\fR] [\fIquorumserver\fR]
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorumserver clear\fR  \fB-c\fR \fIclustername\fR
\fB-I\fR \fIclusterID\fR [\fB-y\fR] \fIquorumserver\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorumserver show\fR  [+ |  \fIquorumserver\fR...]
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorumserver start\fR  + |  \fIquorumserver\fR...
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorumserver stop\fR  + |  \fIquorumserver\fR...
.fi

.SH DESCRIPTION
.sp
.LP
 \fBclquorumserver\fRコマンドは、次の作業に使用します。
.RS +4
.TP
.ie t \(bu
.el o
1 つまたは複数の定足数サーバーの無効な構成情報をクリーンアップする
.RE
.RS +4
.TP
.ie t \(bu
.el o
1 つまたは複数の定足数サーバーの構成を表示する
.RE
.RS +4
.TP
.ie t \(bu
.el o
1 つまたは複数の定足数サーバーを起動する
.RE
.RS +4
.TP
.ie t \(bu
.el o
1 つまたは複数の定足数サーバーを停止する
.RE
.sp
.LP
\fBclqs\fR コマンドは、\fBclquorumserver\fR コマンドの短縮形式です。どちらの形式のコマンドも使用できます。  
.sp
.LP
このコマンドの書式は次のとおりです。
.sp
.LP
 \fBclquorumserver\fR [\fIsubcommand\fR][\fIoptions\fR]
.sp
.LP
 \fIoptions\fR に \fB-?\fR、\fB-v\fR、または \fB-V\fR オプションを指定する場合だけは、\fIsubcommand\fR を省略できます。
.sp
.LP
定足数サーバーは、クラスタの定足数デバイスとして構成してください。定足数サーバーの構成については、\fBscqsd.conf\fR(4) と \fBscqsd\fR(1M) を参照してください。\fBquorum_server\fR タイプの定足数デバイスのクラスタへの追加については、\fBclquorum\fR(1CL) を参照してください。
.SH サブコマンド
.sp
.LP
サポートされるサブコマンドには次のものがあります。
.sp
.ne 2
.mk
.na
\fB\fBclear\fR\fR
.ad
.sp .6
.RS 4n
期限切れのクラスタ情報を定足数サーバーから削除します。定足数サーバーは、定足数デバイスとしてサービスを提供しているクラスタについての情報を保持しています。次の状況では、この情報が無効になる可能性があります。
.RS +4
.TP
.ie t \(bu
.el o
\fBclquorum remove\fR コマンドを使用してクラスタ定足数デバイスを削除せずに、クラスタの運用を停止した場合。
.RE
.RS +4
.TP
.ie t \(bu
.el o
定足数サーバーホストが停止している間に、\fBquorum_server\fR タイプの定足数デバイスをクラスタから削除した場合。
.RE
.LP
Caution - 
.sp
.RS 2
定足数サーバーがクラスタから削除されていない場合に、このサブコマンドを使用して有効な定足数サーバーをクリーンアップすると、クラスタ定足数が損なわれる可能性があります。
.RE
特定のクラスタサーバーのクラスタ名とクラスタ ID を指定してください。詳細は、\fB-c\fR オプションと \fB-I\fR オプションを参照してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.admin\fR RBAC の承認が必要です。詳細は、\fBrbac\fR(5) を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBshow\fR\fR
.ad
.sp .6
.RS 4n
定足数サーバーについての構成情報を表示します。このサブコマンドは、定足数サーバーを定足数デバイスとして構成しているすべてのクラスタごとに、対応するクラスタ名、クラスタ ID、予約鍵のリスト、および登録鍵のリストを表示します。
.sp
プラス記号 (\fB+\fR) を使用すると、複数の定足数サーバーを指定できます。
.sp
オペランドをまったく指定しない場合、あるいは、プラス記号 (\fB+\fR) をオペランドに指定した場合、このコマンドは、動作しているすべての定足数サーバーの構成を表示します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。詳細は、\fBrbac\fR(5) を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBstart\fR\fR
.ad
.sp .6
.RS 4n
定足数サーバーを起動します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBstop\fR\fR
.ad
.sp .6
.RS 4n
定足数サーバーを起動します。
.RE

.SH OPTIONS
.sp
.LP
次のオプションがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB-\fB-help\fR\fR
.ad
.sp .6
.RS 4n
ヘルプ情報を出力します。
.sp
このオプションは単独でもサブコマンド付きでも使用できます。
.RS +4
.TP
.ie t \(bu
.el o
このオプションを単独で使用する場合、利用可能なサブコマンドのリストが出力されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
このオプションをサブコマンドを付けて使用する場合、そのサブコマンドの使用法オプションが出力されます。
.RE
このオプションを使用する場合、ほかの処理は実行されません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-c\fR \fIclustername\fR\fR
.ad
.br
.na
\fB\fB--clustername\fR \fIclustername\fR\fR
.ad
.sp .6
.RS 4n
定足数サーバーを定足数デバイスとして使用するクラスタの名前を指定します。クラスタ名を取得するには、\fBcluster show\fR などの Sun Cluster コマンドをクラスタノード上で実行します。
.sp
\fBclear\fR サブコマンドを使用する場合は、このオプションの指定は必須です。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-I\fR \fIclusterID\fR\fR
.ad
.br
.na
\fB\fB--clusterID\fR \fIclusterID\fR\fR
.ad
.sp .6
.RS 4n
クラスタ ID を指定します。クラスタ ID は 8 桁の 16 進数です。クラスタ ID を取得するには、\fBcluster show\fR などの Sun Cluster コマンドをクラスタノード上で実行します。
.sp
\fBclear\fR サブコマンドを使用する場合は、このオプションの指定は必須です。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB\fB--version\fR\fR
.ad
.sp .6
.RS 4n
コマンドのバージョンを出力します。
.sp
このオプションには、サブコマンドやオペランドなどのオプションは指定しないでください。サブコマンド、オペランド、またはほかのオプションは無視されます。\fB-V\fR オプションは、コマンドのバージョンだけを出力します。その他の処理は行いません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR\fR
.ad
.br
.na
\fB-\fB--verbose\fR\fR
.ad
.sp .6
.RS 4n
詳細な情報を標準出力 \fBstdout\fR に出力します。
.sp
このオプションは、このコマンドの任意の形式に指定できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-y\fR\fR
.ad
.br
.na
\fB-\fB-yes\fR\fR
.ad
.sp .6
.RS 4n
このオプションは、\fBclear\fR サブコマンドを指定するときだけに使用します。このオプションは、\fBclear\fR サブコマンドがたずねる確認の質問にあらかじめ答えます。このオプションを使用しない場合、\fBclear\fR サブコマンドはクラスタ情報をクリーンアップするかどうかを確認する質問を行い、ユーザーは \fByes\fR または \fBno\fR で答える必要があります。このサブコマンドは、ユーザーが \fByes\fR と答えた場合に処理を継続します。このオプションを使用すると、\fBclear\fR サブコマンドは、確認の質問を行わず、指定された定足数サーバーのクラスタ情報をそのまま削除します。
.RE

.SH OPERANDS
.sp
.LP
次のオペランドを指定できます。
.sp
.ne 2
.mk
.na
\fB\fIquorumserver\fR\fR
.ad
.sp .6
.RS 4n
 1 つまたは複数の定足数サーバーの識別子を指定します。定足数サーバーは、ポート番号かインスタンス名で識別できます。ポート番号は、クラスタノードが定足数サーバーと通信するために使用されます。インスタンス名は、定足数サーバー構成ファイル \fB/etc/scqsd/scqsd.conf\fR で指定できます。\fBscqsd.conf\fR(4)を参照してください。
.RE

.SH EXIT STATUS
.sp
.LP
指定したすべてのオペランドでコマンドが成功すると、コマンドはゼロ\fBCL_NOERR\fR) を返します。あるオペランドでエラーが発生すると、コマンドはオペランドリストの次のオペランドを処理します。戻り値は常に、最初に発生したエラーを反映します。
.sp
.LP
次の終了値が返されます。
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fBCL_NOERR\fR\fR
.ad
.sp .6
.RS 4n
エラーなし
.sp
実行したコマンドは正常に終了しました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR \fBCL_ENOMEM\fR\fR
.ad
.sp .6
.RS 4n
十分なスワップ空間がありません。
.sp
クラスタノードがスワップメモリーまたはその他のオペレーティングシステムリソースを使い果たしました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB3\fR \fBCL_EINVAL\fR\fR
.ad
.sp .6
.RS 4n
無効な引数
.sp
コマンドを間違って入力したか、\fB-i\fR オプションで指定したクラスタ構成情報の構文が間違っていました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB6\fR \fBCL_EACCESS\fR\fR
.ad
.sp .6
.RS 4n
アクセス権がありません
.sp
指定したオブジェクトにアクセスできません。このコマンドを実行するには、スーパーユーザーまたは RBAC アクセスが必要である可能性があります。詳細は、\fBsu\fR(1M)、および \fBrbac\fR(5) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB18\fR \fBCL_EINTERNAL\fR\fR
.ad
.sp .6
.RS 4n
内部エラーが発生しました
.sp
内部エラーは、ソフトウェアの欠陥またはその他の欠陥を示しています。
.RE

.sp
.ne 2
.mk
.na
\fB\fB35\fR \fBCL_EIO\fR\fR
.ad
.sp .6
.RS 4n
I/O error
.sp
物理的な入出力エラーが発生しました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB36\fR \fBCL_ENOENT\fR\fR
.ad
.sp .6
.RS 4n
そのようなオブジェクトはありません。
.sp
次のいずれかの理由のために、指定したオブジェクトを見つけることができません。
.RS +4
.TP
.ie t \(bu
.el o
オブジェクトが存在しません。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-o\fR オプションで作成しようとした構成ファイルへのパスのディレクトリが存在しません。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-i\fR オプションでアクセスしようとした構成ファイルにエラーが含まれています。
.RE
.RE

.SH EXAMPLES
.LP
\fBExample 1 \fR1 つの定足数サーバーの構成の表示
.sp
.LP
次のコマンドは、ポート 9000 を使用する定足数サーバーの構成情報を表示します。

.sp
.in +2
.nf
# \fBclquorumserver show 9000\fR
.fi
.in -2
.sp

.LP
\fBExample 2 \fR複数の定足数サーバーの構成の表示
.sp
.LP
次のコマンドは、インスタンス名のリストに列挙された定足数サーバーの構成情報を表示します。 

.sp
.in +2
.nf
# \fBclquorumserver show qs1 qs2 qs3\fR
.fi
.in -2
.sp

.LP
\fBExample 3 \fR動作しているすべての定足数サーバーの構成の表示
.sp
.LP
次のコマンドは、動作しているすべての定足数サーバーの構成情報を表示します。 

.sp
.in +2
.nf
# \fBclquorumserver show +\fR
.fi
.in -2
.sp

.LP
\fBExample 4 \fR定足数サーバーの起動
.sp
.LP
次のコマンドは、構成されているすべての定足数サーバーを起動します。

.sp
.in +2
.nf
# \fBclquorumserver start +\fR
.fi
.in -2
.sp

.sp
.LP
次のコマンドは、ポート 9000 で待機している定足数サーバーを起動します。

.sp
.in +2
.nf
# \fBclquorumserver start 9000\fR
.fi
.in -2
.sp

.sp
.LP
次のコマンドは、インスタンス名が \fBqs1\fR の定足数サーバーを起動します。 

.sp
.in +2
.nf
# \fBclquorumserver start qs1\fR
.fi
.in -2
.sp

.LP
\fBExample 5 \fRポート番号による定足数サーバーの停止
.sp
.LP
次のコマンドは、ポート 9000 で待機している定足数サーバーを起動します。

.sp
.in +2
.nf
# \fBclquorumserver stop 9000\fR
.fi
.in -2
.sp

.LP
\fBExample 6 \fR定足数サーバーからの期限切れのクラスタ情報のクリーンアップ
.sp
.LP
次の例は、\fBsc-cluster\fR という名前のクラスタについての情報を定足数サーバーから削除します。

.sp
.LP
この方法で定足数サーバーの構成を解除するときには、十分に注意してください。一般的には、\fBclquorum remove\fR を使用して定足数サーバーデバイスをクラスタ構成から削除し、単一セットの定足数サーバー上で構成情報をクリーンアップするべきです。このコマンドを使用するのは、\fBclquorum remove\fR 動作中にクラスタと定足数サーバーホスト間の通信が失われた場合だけです。 

.sp
.in +2
.nf
# \fBclquorumserver clear -c sc-cluster -I 0x4308D2CF 9000\fR
The quorum server to be unconfigured must have been removed from
the cluster. Unconfiguring a valid quorum server could compromise
the cluster quorum. Do you want to continue? (yes or no)
.fi
.in -2
.sp

.SH ATTRIBUTES
.sp
.LP
次の属性の詳細は、\fBattributes\fR(5) のマニュアルページを参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscu
_
インタフェースの安定性発展中
.TE

.SH SEE ALSO
.sp
.LP
\fBIntro\fR(1CL), \fBclquorum\fR(1CL), \fBcluster\fR(1CL), \fBscqsd\fR(1M), \fBscqsd.conf\fR(4).
.SH NOTES
.sp
.LP
スーパーユーザーはこのコマンドのすべての形式を実行できます。
.sp
.LP
任意のユーザーは次のオプションを指定してこのコマンドを実行できます。
.RS +4
.TP
.ie t \(bu
.el o
\fB-?\fR (ヘルプ) オプション
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-V\fR (バージョン) オプション
.RE
.sp
.LP
スーパーユーザー以外のユーザーがほかのサブコマンドを指定してこのコマンドを実行するには、RBAC の承認が必要です。次の表を参照してください。
.sp

.sp
.TS
tab();
cw(0i) |cw(5.5i) 
lw(0i) |lw(5.5i) 
.
サブコマンドRBAC の承認
_
\fBclear\fR\fBsolaris.cluster.admin\fR
_
\fBshow\fR\fBsolaris.cluster.read\fR
_
\fBstart\fR\fBsolaris.cluster.admin\fR
_
\fBstop\fR\fBsolaris.cluster.admin\fR
.TE

