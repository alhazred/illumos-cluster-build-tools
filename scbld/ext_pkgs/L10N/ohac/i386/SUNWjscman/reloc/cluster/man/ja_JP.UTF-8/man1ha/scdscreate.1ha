'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scdscreate 1HA "2008 年 9 月 11 日" "Sun Cluster 3.2" "Sun Cluster コマンド"
.SH 名前
scdscreate \- Sun Cluster リソースタイプテンプレートを作成する
.SH 形式
.LP
.nf
\fBscdscreate\fR \fB-V\fR \fI vendor-id\fR \fB-T\fR \fIresource-type-name \fR \fB-a\fR [\fB-s\fR] [\fB-n\fR \fIRT-version\fR] [\fB -d\fR \fIworking-directory\fR] [\fB-k\fR | \fB -g\fR]
.fi

.SH 機能説明
.sp
.LP
\fBscdscreate\fR コマンドは、アプリケーションを高可用性(\fBHA\fR) またはスケーラブルにするため、テンプレートを作成します。このコマンドは、ネットワーク対応 (クライアントサーバーモデル) アプリケーションと非ネットワーク対応 (クライアントを持たない) アプリケーションの両方に対して、C、GDS (Generic Data Service)、または Korn シェルベースのテンプレートを作成できます。
.sp
.LP
テンプレートを作成するには、根本的に異なる方法が 2 つあります。
.sp
.ne 2
.mk
.na
\fBGDS\fR
.ad
.RS 28n
.rt  
\fBscdscreate\fR は、事前にクラスタにインストールされている単一のリソースタイプ \fBSUNW.gds\fR から動作する 3 つの駆動スクリプトを作成します。これらのスクリプトの名前は start \fI RT-Name\fR、stop \fIRT-Name\fR、および remove\fIRT-Name\fR で、それぞれのアプリケーションのインスタンスを起動、停止、および削除します。このモデルでは、事前にクラスタにインストールされているリソースタイプ \fBSUNW.gds\fR は不変です。
.RE

.sp
.ne 2
.mk
.na
\fB生成されたソースコード\fR
.ad
.RS 28n
.rt  
\fBscdscreate\fR は、Sun Cluster リソースタイプのテンプレートを作成します。指定されたアプリケーションを高可用性またはスケーラブルにするため、このテンプレートのインスタンス化は Resource Group Manager(\fBRGM\fR) の制御下で実行されます。
.RE

.sp
.LP
どちらのモデルでも、ネットワーク対応 (クライアントサーバーモデル) のアプリケーションと非ネットワーク対応 (クライアントレス) のアプリケーションのテンプレートを作成できます。
.sp
.LP
\fBscdscreate\fR は、\fIworking-directory\fR の下に $\fIvendor-id\fR$\fIresource-type-name\fR という形式のサブディレクトリを作成します。このサブディレクトリには、駆動スクリプトか、そのリソースタイプのソース、バイナリ、パッケージファイルが格納されます。\fBscdscreate\fR はまた、構成ファイル \fBrtconfig\fR を作成します。この構成ファイルには、リソースタイプの構成情報を格納できます。\fBscdscreate\fR では、1 つのディレクトリにリソースタイプを 1 つだけ作成できます。異なるリソースタイプは異なるディレクトリに作成する必要があります。
.SH オプション
.sp
.LP
次のオプションがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fB-a\fR\fR
.ad
.RS 28n
.rt  
ネットワーク対応でないリソースタイプの作成を指定します。\fB\fR作成されたテンプレート内のネットワーク関連コードはすべて無効になります。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-n\fR\ \fIRT-version\fR\fR
.ad
.RS 28n
.rt  
生成されたリソースタイプのバージョンを指定します。このパラメータを省略した場合で、かつ、C または Korn シェルベースのアプリケーションを作成する場合、テキスト文字列 \fB1.0\fR がデフォルトで使用されます。このパラメータを省略した場合で、かつ、GDS ベースのアプリケーションを作成する場合、GDS の \fBRT_version\fR 文字列がデフォルトで使用されます。\fIRT-version\fR は、同じベースリソースタイプの複数の登録バージョン (つまり、アップグレード) 間を区別します。
.sp
次の文字は \fIRT-version\fR に使用できません。空白、タブ、スラッシュ (\fB/\fR)、バックスラッシュ (\fB\e\fR)、アスタリスク (\fB*\fR)、疑問符 (\fB?\fR)、コンマ (\fB,\fR)、セミコロン (\fB;\fR)、左大括弧 (\fB[\fR)、または右大括弧 (\fB]\fR)
.RE

.sp
.ne 2
.mk
.na
\fB\fB-d\fR\ \fIworking-directory \fR\fR
.ad
.RS 28n
.rt  
現在のディレクトリ以外のディレクトリにリソースタイプのテンプレートを作成します。\fB\fRこのオプションを省略した場合、テンプレートは現在のディレクトリに作成されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-g\fR\fR
.ad
.RS 28n
.rt  
アプリケーションを高可用性またはスケーラブルにするため、テンプレートを GDS ベースの形式で作成します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-k\fR\fR
.ad
.RS 28n
.rt  
C シェルではなく、Korn シェルココマンド構文でリソースコードを作成します。\fBksh\fR(1)を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-s\fR\fR
.ad
.RS 28n
.rt  
リソースタイプがスケーラブルであることを示します。スケーラブルなリソースタイプのインスタンス (リソース) をフェイルオーバーリソースグループ内に構成して、スケーラブル機能を無効にすることができます。\fB\fRこのオプションを省略した場合、フェイルオーバーリソースタイプのテンプレートが作成されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-T\fR\ \fIresource-type-name \fR\fR
.ad
.RS 28n
.rt  
リソースタイプの名前とバージョン。これらをベンダー ID と結合すると、作成されるリソースを一意に識別できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\ \fIvendor-id\fR\fR
.ad
.RS 28n
.rt  
通常、ベンダー ID には、リソースタイプの作成元ベンダーのストックシンボルかその他の識別子を指定します。\fBscdscreate\fR は、リソースタイプの名前の始まりに、ベンダー ID とピリオド (\fB .\fR) を付けます。この構文によって、複数のベンダーが同じリソースタイプの名前を使用している場合でも、リソースタイプの名前は一意のままになります。
.RE

.SH 終了ステータス
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
コマンドは正常に完了しました。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
エラーが発生しました。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB\fIworking-directory\fR/rtconfig \fR\fR
.ad
.RS 36n
.rt  
以前のセッションからの情報を保持して、\fBscdscreate\fR の終了と再起動機能を利用します。
.RE

.SH 属性
.sp
.LP
\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能\fBSUNWscdev\fR
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
\fBksh\fR(1)、scdsbuilder(1HA)、scdsconfig(1HA)、\fBattributes\fR(5)、rt_properties(5)
.sp
.LP
\fI『Sun Cluster Data Services Developer\&'s Guide for Solaris OS 』\fR
