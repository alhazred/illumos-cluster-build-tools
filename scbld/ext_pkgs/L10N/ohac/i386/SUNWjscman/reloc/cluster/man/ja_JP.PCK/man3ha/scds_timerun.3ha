'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_timerun 3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_timerun \- 指定されたコマンドを、指定された時間だけ実行する
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fI file\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_timerun\fR(\fBscds_handle_t \fR \fIhandle\fR, \fBconst char *\fR\fIcommand\fR, \fBtime_t\fR \fItimeout\fR, \fBint\fR \fIsignal\fR, \fBint *\fR\fIcmd_exit_code\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_timerun()\fR 関数は、hatimerun(1M) を使用して指定されたコマンドを実行します。\fBscds_timerun()\fR は、\fItimeout\fR 引数で指定された時間内にコマンドが完了しない場合、\fIsignal\fR 引数で指定されたシグナルを送信してコマンドを強制終了します。
.sp
.LP
\fIcommand\fR 引数は入出力のリダイレクトをサポートしません。だたし、リダイレクトを実行するスクリプトを作成し、\fIcommand\fR 引数を使って、このスクリプトを \fBscds_timerun()\fR が実行するコマンドに指定することは可能です。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIcommand\fR\fR
.ad
.RS 20n
.rt  
実行するコマンドを含む文字列です。
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
コマンドの実行のために割り当てられた時間 (秒) です。
.RE

.sp
.ne 2
.mk
.na
\fB\fIsignal\fR\fR
.ad
.RS 20n
.rt  
タイムアウトに達しても完了しないコマンドを強制終了するシグナルです。\fBsignal = -1\fR の場合、\fBSIGKILL\fR が使用されます。\fBsignal\fR(3HEAD)を参照してください。 
.RE

.sp
.ne 2
.mk
.na
\fB\fIcmd_exit_code\fR\fR
.ad
.RS 20n
.rt  
コマンドの実行によって返されるコードです。
.RE

.SH 戻り値
.sp
.LP
\fBscds_timerun()\fR 関数の戻り値は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 36n
.rt  
コマンドが実行され、\fBcmd_exit_code\fR に子プログラムの終了ステータスを格納。 
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_INTERNAL\fR\fR
.ad
.RS 36n
.rt  
\fBscds_timerun()\fR が、子プログラムでは検出されなかったタイムアウト以外のエラーを検出。あるいは、hatimerun(1M) がシグナル \fBSIGTERM\fR をキャッチします。 
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_INVAL\fR\fR
.ad
.RS 36n
.rt  
無効な入力引数を検出。 
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_TIMEOUT\fR\fR
.ad
.RS 36n
.rt  
\fIcommand\fR 引数によって指定されたコマンドの実行が完了する前にタイムアウト。 
.RE

.sp
.LP
その他のエラーコードについては、scha_calls(3HA)を参照してください。
.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
hatimerun(1M)、scds_initialize(3HA)、scha_calls(3HA)、\fBsignal\fR(3HEAD)、\fBattributes\fR(5)
