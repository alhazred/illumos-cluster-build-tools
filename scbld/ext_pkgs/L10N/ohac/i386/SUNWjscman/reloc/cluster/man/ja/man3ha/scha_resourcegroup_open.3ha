'\" te
.\"  Copyright 2007 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH  scha_resourcegroup_open   3HA  "2007 年 9 月 7 日" " Sun Cluster 3.2 " " Sun Cluster HA およびデータサービス "
.SH 名前
scha_resourcegroup_open, scha_resourcegroup_close, scha_resourcegroup_get \- リソース情報アクセス関数
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR scha #include <scha.h> \fBscha_err_t\fR \fBscha_resourcegroup_open\fR(\fBconst char *\fR\fIrgname\fR, \fBscha_resourcegroup_t *\fR\fIhandle\fR);
.fi

.LP
.nf
\fBscha_err_t\fR \fBscha_resourcegroup_close\fR(\fBscha_resourcegroup_t\fR \fIhandle\fR);
.fi

.LP
.nf
\fBscha_err_t\fR \fBscha_resourcegroup_get\fR(\fBscha_resourcegroup_t\fR \fIhandle\fR, \fBconst char *\fR\fItag\fR...);
.fi

.SH 機能説明
.sp
.LP
\fBscha_resourcegroup_open()\fR、\fBscha_resourcegroup_get()\fR、\fBscha_resourcegroup_close()\fR の 3 つの関数を同時に使用することで Resource Group Manager (RGM) クラスタ機能の管理するリソースグループに関する情報を入手できます。
.sp
.LP
\fBscha_resourcegroup_open()\fR は、リソースグループへのアクセスを初期し、\fBscha_resourcegroup_get()\fR が使用するためのアクセスハンドルを返します。 
.sp
.LP
\fIrgname\fR 引数には、アクセスするリソースグループの名前を指定します。
.sp
.LP
\fIhandle\fR 引数は、関数が返す値を格納する変数のアドレスです。
.sp
.LP
\fBscha_resourcegroup_get()\fR 関数は、\fItag\fR 引数に指定されるリソースグループの情報にアクセスします。\fItag\fR 引数には、\fBscha_tags.h\fR ヘッダーファイルのマクロで定義される文字列値が入ります。タグ以降の引数は、\fItag\fR の値に依存します。タグ以降に追加する引数は、情報を取り出すクラスタノードを指定する際に必要です。
.sp
.LP
引数リストの最後の引数は、\fItag\fR で指定される情報の格納に適した変数型にする必要があります。このパラメータは出力引数で、取得したリソースグループの情報を格納します。関数の実行に失敗した場合、出力引数に値は返されません。\fBscha_resourcegroup_get()\fR で使用されたハンドルで \fBscha_resourcegroup_close()\fR が呼び出されるまで、\fBscha_resourcegroup_get()\fR の返す情報用に割り当てられたメモリーは維持されます。
.sp
.LP
\fBscha_resourcegroup_close()\fR には、事前に \fBscha_resourcegroup_open()\fR 関数を使って得た \fIhandle\fR 引数の値を指定します。この関数は、該当するハンドルを使用して得た \fBscha_resourcegroup_get()\fR の戻り値用割り当てメモリーを解放するとともに、このハンドルを無効化します。値を返す必要が生じるごとに、個々の \fBget\fR 呼び出しでメモリーが割り当てられる点に注意してください。ある呼び出しで値を返すために割り当てられたメモリーが、以降の呼び出しによって上書きされたり、再利用されたりすることはありません。
.SS "\fItag\fR 引数に使用できるマクロ"
.sp
.LP
 \fBscha_tags.h\fR 内で定義されている、\fBscha_resourcegroup_get()\fR 関数に対する \fItag\fR 引数として、次のマクロを使用できます。これらのマクロはリソースグループプロパティーに名前を付けます。リソースグループのプロパティー値が生成されます。\fBRG_STATE\fR プロパティーは、関数を呼び出したノードまたはゾーン上の値を示します。
.sp
.LP
ここでは出力引数および追加引数の型を説明します。構造と \fBenum\fR 型は scha_calls(3HA) のマニュアルページで説明されています。
.sp
.ne 2
.mk
.na
\fB\fBSCHA_DESIRED_PRIMARIES\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBint*\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_FAILBACK\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t*\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_GLOBAL_RESOURCES_USED\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_IMPL_NET_DEPEND\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t*\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_MAXIMUM_PRIMARIES\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBint*\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_NODELIST\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_PATHPREFIX\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_PINGPONG_INTERVAL\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBint*\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RESOURCE_LIST\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_AFFINITIES\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_AUTO_START\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t*\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_DEPENDENCIES\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t**\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_DESCRIPTION\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_IS_FROZEN\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t*\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_MODE\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_rgmode_t*\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_PROJECT_NAME\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_SLM_CPU\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_SLM_CPU_MIN\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_SLM_PSET_TYPE\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_SLM_TYPE\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar**\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_STATE\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_rgstate_t*\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_STATE_NODE\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_rgstate_t*\fR です。追加引数の型は \fBchar*\fR です。追加引数はクラスタノードを指定し、そのノード上のリソースグループの状態を返します。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_SUSP_AUTO_RECOVERY\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t*\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RG_SYSTEM\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t*\fR です。
.RE

.SH 戻り値
.sp
.LP
これらの関数は、次の戻り値を返します。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR \fR
.ad
.RS 36n
.rt  
関数の実行に成功。
.RE

.sp
.LP
その他のエラーコードについては、scha_calls(3HA)を参照してください。
.SH 使用例
.LP
\fB例 1 \fR\fBscha_resourcegroup_get()\fR 関数の使用例
.sp
.LP
次の例では、\fBscha_resourcegroup_get()\fR を用いて、\fBexample_RG\fR 内のリソースリストを取得します。

.sp
.in +2
.nf
main() {
  #include <scha.h>

  scha_err_t err;
  scha_str_array_t *resource_list;
  scha_resourcegroup_t handle;
  int ix;

  char * rgname = "example_RG";	

  err = scha_resourcegroup_open(rgname, &handle);

  err = scha_resourcegroup_get(handle, SCHA_RESOURCE_LIST, \e
        &resource_list);

  if (err == SCHA_ERR_NOERR) {
   for (ix = 0; ix < resource_list->array_cnt; ix++) {
       printf("Group: %s contains resource %s\
", rgname,
               resource_list->str_array[ix]);
       }
   }

/* resource_list memory freed */
err = scha_resourcegroup_close(handle);	
}
.fi
.in -2

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/scha.h\fR\fR
.ad
.RS 36n
.rt  
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libscha.so\fR\fR
.ad
.RS 36n
.rt  
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scha_resourcegroup_get(1HA)、scha_calls(3HA)、\fBattributes\fR(5)
