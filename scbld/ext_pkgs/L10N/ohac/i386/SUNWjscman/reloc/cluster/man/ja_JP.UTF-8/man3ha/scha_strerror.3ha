'\" te
.\"  Copyright 2007 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH  scha_strerror   3HA  "2007 年 9 月 7 日" " Sun Cluster 3.2 " " Sun Cluster HA およびデータサービス "
.SH 名前
scha_strerror, scha_strerror_i18n \-  エラーコードからエラーメッセージの作成
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR scha #include <scha.h> \fBchar *\fR\fBscha_strerror\fR(\fBscha_err_t\fR \fIerror_code\fR);
.fi

.LP
.nf
\fBchar *\fR\fBscha_strerror_i18n\fR(\fBscha_err_t\fR \fIerror_code\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscha_strerror()\fR および \fBscha_strerror_i18n()\fR 関数は、与えられたエラーコード \fBscha_err_t\fR のエラーを説明する簡単な文字列を生成します。\fBscha_strerror()\fR から返された文字列は、英語で表示されます。\fBscha_strerror_i18n()\fR から返された文字列は、\fBLC_MESSAGES\fRロケールカテゴリで指定されているその国および地域の言語で表示されます。\fBsetlocale\fR(3C)を参照してください。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIerror_code\fR\fR
.ad
.RS 20n
.rt  
エラーを説明する簡単な文字列の生成元となるエラーコード。
.RE

.SH 使用例
.LP
\fB例 1 \fR\fBscha_strerror_i18n()\fR 関数の使用
.sp
.in +2
.nf
sample()
{
     scha_err_t  err;

     /* resource group containing example_R */
     char * resource_group = "example_RG";

     /* a configured resource */
     char * resource_name = "example_R";

     err = scha_control(SCHA_GIVEOVER, resource_group, resource_name);

     if (err != SCHA_ERR_NOERR) {
         syslog(LOG_ERR, "scha_control GIVEOVER failed: %s", 
            scha_strerror_i18n(err));
     }
}
.fi
.in -2

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/scha.h\fR\fR
.ad
.RS 36n
.rt  
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libscha.so\fR\fR
.ad
.RS 36n
.rt  
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scha_calls(3HA)、\fBsetlocale\fR(3C)、\fBsyslog\fR(3C)、\fBattributes\fR(5)
