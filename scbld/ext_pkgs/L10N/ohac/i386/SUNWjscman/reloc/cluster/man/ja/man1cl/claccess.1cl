'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH claccess 1CL "2008 年 9 月 11 日" "Sun Cluster 3.2" "Sun Cluster 保守コマンド"
.SH 名前
claccess \- ノード用の Sun Cluster アクセスポリシーの管理
.SH 形式
.LP
.nf
\fB/usr/cluster/bin/claccess\fR \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess [\fIsubcommand\fR] \fR \fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess \fIsubcommand\fR\fR [\fIoptions\fR] \fB-v\fR [\fI hostname\fR[,\&.\|.\|.]]
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess allow\fR \fB-h \fR \fIhostname\fR[,\&.\|.\|.]
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess allow-all\fR 
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess deny\fR \fB-h\fR \fI hostname\fR[,\&.\|.\|.]
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess deny-all\fR 
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess list\fR 
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess set\fR \fB-p\fR protocol=\fI authprotocol\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/claccess show\fR 
.fi

.SH 機能説明
.sp
.LP
\fBclaccess\fR コマンドは、クラスタ構成にアクセスしようとするマシンのネットワークアクセスポリシーを制御します。\fBclaccess\fR コマンドに短形式はありません。
.sp
.LP
クラスタは、クラスタ構成にアクセスできるマシンのリストを管理します。クラスタはまた、これらのノードがクラスタ構成にアクセスするのに使用する承認プロトコルの名前を格納します。 
.sp
.LP
あるマシンがクラスタ構成にアクセスしようとするとき、たとえば、クラスタ構成への追加を求めるとき (clnode(1CL) を参照)、クラスタはこのリストをチェックして、そのノードがアクセス権を持っているかどうかを判定します。そのノードがアクセス権を持っている場合、そのノードは、クラスタ構成にアクセスすることが承認および許可されます。
.sp
.LP
\fBclaccess\fR コマンドは、次の作業に使用できます。
.RS +4
.TP
.ie t \(bu
.el o
任意の新しいマシンが自分自身をクラスタ構成に追加したり、自分自身をクラスタ構成から削除したりすることを許可する
.RE
.RS +4
.TP
.ie t \(bu
.el o
任意のノードが自分自身をクラスタ構成に追加したり、自分自身をクラスタ構成から削除したりすることを禁止する
.RE
.RS +4
.TP
.ie t \(bu
.el o
チェックする承認タイプを制御する
.RE
.sp
.LP
このコマンドは、大域ゾーンだけで使用できます。
.sp
.LP
\fBclaccess\fR コマンドの一般的な形式は次のとおりです。
.sp
.LP
 \fBclaccess\fR [\fIsubcommand\fR] [\fIoptions\fR]
.sp
.LP
\fIoptions\fR に \fB-?\fR または \fB-V\fR オプションを指定する場合だけは、\fIsubcommand\fR を省略できます。
.sp
.LP
このコマンドの各オプションには、長い形式と短い形式があります。各オプションの両方の形式は、このマニュアルページの「オプション」セクションのオプションの説明で紹介されています。
.SH サブコマンド
.sp
.LP
サポートされるサブコマンドには次のものがあります。
.sp
.ne 2
.mk
.na
\fB\fBallow\fR\fR
.ad
.sp .6
.RS 4n
指定されたマシン (1 つまたは複数) がクラスタ構成にアクセスすることを許可します。 
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR 役割に基づくアクセス制御 (RBAC) の承認が必要です。\fBrbac\fR(5) のマニュアルページを参照してください。
.sp
\fBdeny\fR および \fBallow-all\fR サブコマンドも参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBallow-all\fR\fR
.ad
.sp .6
.RS 4n
すべてのマシンが自分自身をクラスタ構成に追加して、クラスタ構成にアクセスすることを許可します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5) のマニュアルページを参照してください。
.sp
\fBdeny-all\fR および \fBallow\fR サブコマンドも参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBdeny\fR\fR
.ad
.sp .6
.RS 4n
指定されたマシン (1 つまたは複数) がクラスタ構成にアクセスすることを禁止します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5) のマニュアルページを参照してください。
.sp
\fBallow\fR および \fBdeny-all\fR サブコマンドも参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBdeny-all\fR\fR
.ad
.sp .6
.RS 4n
すべてのマシンがクラスタ構成にアクセスすることを禁止します。
.sp
クラスタを初めて構成したあと、デフォルトの設定では、どのノードにもクラスタ構成へのアクセス権はありません。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5) のマニュアルページを参照してください。
.sp
\fBallow-all\fR および \fBdeny\fR サブコマンドも参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBlist\fR\fR
.ad
.sp .6
.RS 4n
クラスタ構成にアクセスする承認を持っているマシンの名前を表示します。承認プロトコルも表示するには、\fBshow\fR サブコマンドを使用します。 
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBset\fR\fR
.ad
.sp .6
.RS 4n
承認プロトコルを \fB-p\fR オプションで指定した値に設定します。デフォルトでは、システムは \fBsys\fR を承認プロトコルとして使用します。「オプション」の \fB- p\fR オプションを参照してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBshow\fR\fR
.ad
.sp .6
.RS 4n
クラスタ構成にアクセスするアクセス権を持っているマシンの名前を表示します。承認プロトコルも表示します。 
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5) のマニュアルページを参照してください。
.RE

.SH オプション
.sp
.LP
次のオプションがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB-\fB-help\fR\fR
.ad
.sp .6
.RS 4n
ヘルプ情報を表示します。このオプションを使用すると、ほかの処理は実行されません。
.sp
このオプションを指定するとき、サブコマンドは指定してもしなくてもかまいません。このオプションをサブコマンドなしで指定すると、このコマンドのサブコマンドのリストが表示されます。サブコマンド付きでこのオプションを指定すると、サブコマンドの使用方法が表示されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-h\fR \fIhostname\fR\fR
.ad
.br
.na
\fB-\fB-host\fR=\fIhostname\fR\fR
.ad
.br
.na
\fB-\fB-host\fR \fIhostname\fR\fR
.ad
.sp .6
.RS 4n
アクセスを付与または拒否するノードの名前を指定します。 
.RE

.sp
.ne 2
.mk
.na
\fB\fB-p\fR protocol=\fIauthentication-protocol \fR\fR
.ad
.br
.na
\fB-\fB-authprotocol=\fR\fIauthentication-protocol \fR\fR
.ad
.br
.na
\fB-\fB-authprotocol \fR\fIauthentication-protocol \fR\fR
.ad
.sp .6
.RS 4n
マシンがクラスタ構成へのアクセス権を持っているかどうかをチェックするのに使用する承認プロトコルを指定します。
.sp
サポートされるプロトコルは、\fBdes\fR と \fBsys\fR (または \fBunix\fR) です。デフォルトの認証型は \fBsys\fR ですが、これは最低限のセキュリティー保護しか実行しません。ノードの追加と削除についての詳細は、\fI『Sun Cluster System Administration Guide for Solaris OS 』\fR?の\fI「Adding a Node」\fRを参照してください。これらの承認タイプについての詳細は、\fI『System Administration Guide: Security Services 』\fRの第 16 章\fI「Using Authentication Services (Tasks)」\fRを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB-\fB-version\fR\fR
.ad
.sp .6
.RS 4n
コマンドのバージョンを表示します。
.sp
このオプションには、サブコマンドやオペランドなどのオプションは指定しないでください。サブコマンドやオペランドなどのオプションは無視されます。\fB-V\fR オプションは、コマンドのバージョンだけを表示します。ほかの処理は実行されません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR\fR
.ad
.br
.na
\fB-\fB-verbose\fR\fR
.ad
.sp .6
.RS 4n
詳細な情報を標準出力 \fBstdout\fR に表示します。
.RE

.SH 終了ステータス
.sp
.LP
指定したすべてのオペランドでコマンドが成功すると、コマンドはゼロ\fBCL_NOERR\fR) を返します。あるオペランドでエラーが発生すると、コマンドはオペランドリストの次のオペランドを処理します。戻り値は常に、最初に発生したエラーを反映します。
.sp
.LP
次の終了コードが返されます。
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fBCL_NOERR\fR\fR
.ad
.sp .6
.RS 4n
エラーなし
.sp
実行したコマンドは正常に終了しました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR \fBCL_ENOMEM\fR\fR
.ad
.sp .6
.RS 4n
十分なスワップ空間がありません。
.sp
クラスタノードがスワップメモリーまたはその他のオペレーティングシステムリソースを使い果たしました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB3\fR \fBCL_EINVAL\fR\fR
.ad
.sp .6
.RS 4n
無効な引数
.sp
コマンドを間違って入力したか、\fB-i\fR オプションで指定したクラスタ構成情報の構文が間違っていました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB6\fR \fBCL_EACCESS\fR\fR
.ad
.sp .6
.RS 4n
アクセス権がありません
.sp
指定したオブジェクトにアクセスできません。このコマンドを実行するには、スーパーユーザーまたは RBAC アクセスが必要である可能性があります。詳細は、\fBsu\fR(1M)、および \fBrbac\fR(5) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB18\fR \fBCL_EINTERNAL \fR\fR
.ad
.sp .6
.RS 4n
内部エラーが発生しました
.sp
内部エラーは、ソフトウェアの欠陥またはその他の欠陥を示しています。
.RE

.sp
.ne 2
.mk
.na
\fB\fB39\fR \fBCL_EEXIST\fR\fR
.ad
.sp .6
.RS 4n
オブジェクトは存在します。
.sp
指定したデバイス、デバイスグループ、クラスタインターコネクトコンポーネント、ノード、クラスタ、リソース、リソースタイプ、またはリソースグループはすでに存在します。
.RE

.SH 使用例
.LP
\fB例 1 \fR新しいホストのアクセスの許可
.sp
.LP
次の \fBclaccess\fR コマンドは、新しいホストがクラスタ構成にアクセスすることを許可します。

.sp
.in +2
.nf
# \fBclaccess allow -h \fIphys-schost-1\fR\fR
.fi
.in -2
.sp

.LP
\fB例 2 \fR承認タイプの設定
.sp
.LP
次の \fBclaccess\fR コマンドは、現在の承認タイプを \fBdes\fR に設定します。

.sp
.in +2
.nf
# \fBclaccess set -p protocol=des\fR
.fi
.in -2
.sp

.LP
\fB例 3 \fRすべてのホストのアクセスの拒否
.sp
.LP
次の \fBclaccess\fR コマンドは、すべてのホストがクラスタ構成にアクセスすることを拒否します。

.sp
.in +2
.nf
# \fBclaccess deny-all\fR
.fi
.in -2
.sp

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWsczu
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
Intro(1CL)、clnode(1CL)、cluster(1CL)
.SH 注意事項
.sp
.LP
スーパーユーザーはこのコマンドのすべての形式を実行できます。
.sp
.LP
任意のユーザーは次のサブコマンドとオプションを指定してこのコマンドを実行できます。
.RS +4
.TP
.ie t \(bu
.el o
\fB-?\fR オプション
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-V\fR オプション
.RE
.sp
.LP
スーパーユーザー以外のユーザーが他のサブコマンドを指定してこのコマンドを実行するには、\fBRBAC\fR の承認が必要です。次の表を参照してください。
.sp

.sp
.TS
tab() box;
cw(0i) |cw(5.5i) 
lw(0i) |lw(5.5i) 
.
サブコマンド\fBRBAC の承認\fR
_
\fBallow\fR\fBsolaris.cluster.modify\fR
_
\fBallow-all\fR\fBsolaris.cluster.modify\fR
_
\fBdeny\fR\fBsolaris.cluster.modify\fR
_
\fBdeny-all\fR\fBsolaris.cluster.modify\fR
_
\fBlist\fR\fBsolaris.cluster.read\fR
_
\fBset\fR\fBsolaris.cluster.modify\fR
_
\fBshow\fR\fBsolaris.cluster.read\fR
.TE

