'\" te
.\"  Copyright 2007 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH  scha_resourcetype_open   3HA  "2007 年 9 月 7 日" " Sun Cluster 3.2 " " Sun Cluster HA およびデータサービス "
.SH 名前
scha_resourcetype_open, scha_resourcetype_close, scha_resourcetype_get \-  リソースタイプ情報アクセス関数 
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR scha #include <scha.h> \fBscha_err_t\fR \fBscha_resourcetype_open\fR(\fBconst char *\fR\fIrtname\fR, \fBscha_resourcetype_t *\fR\fIhandle\fR);
.fi

.LP
.nf
\fBscha_err_t\fR \fBscha_resourcetype_close\fR(\fBscha_resourcetype_t\fR \fIhandle\fR);
.fi

.LP
.nf
\fBscha_err_t\fR \fBscha_resourcetype_get\fR(\fBscha_resourcetype_t\fR \fIhandle\fR, \fBconst char *\fR\fItag\fR...);
.fi

.SH 機能説明
.sp
.LP
\fBscha_resourcetype_open()\fR、\fBscha_resourcetype_get ()\fR、および\fBscha_resourcetype_close()\fR 関数を使用すると、Resource Group Manager (RGM) クラスタ機能が利用するリソースタイプの情報を入手できます。
.sp
.LP
\fBscha_resourcetype_open()\fR は、リソースタイプへのアクセスを初期化し、\fBscha_resourcetype_get()\fR が使用するアクセスハンドルを返します。
.sp
.LP
\fBscha_resourcetype_open()\fR の \fIrtname\fR 引数には、アクセスするリソースタイプの名前を指定します。
.sp
.LP
\fIhandle\fR 引数の値は、関数の戻り値を格納する変数のアドレスとなります。
.sp
.LP
\fBscha_resourcetype_get()\fR 関数は、\fItag\fR 引数に指定されるリソースタイプの情報にアクセスします。\fItag\fR 引数には、\fBscha_tags.h\fR ヘッダーファイルのマクロで定義される文字列値が入ります。タグ以降の引数は、\fItag\fR の値に依存します。
.sp
.LP
タグ以降に追加する引数は、情報を取り出すクラスタノードや、タグ固有の他の情報を指定する際に必要となることがあります。引数リストの最後の引数は、\fItag\fR で指定される情報の格納に適した型にする必要があります。これは out 引数で、リソースタイプの情報を格納します。関数の実行に失敗した場合、出力引数に値は返されません。\fBscha_resourcetype_get()\fR で使用されたハンドルで \fBscha_resourcetype_close()\fR が呼び出されるまで、\fBscha_resourcetype_get()\fR の返す情報用に割り当てられたメモリーは維持されます。
.sp
.LP
\fBscha_resourcetype_close()\fR には、事前に \fIscha_resourcetype_open\fR 関数を使って得た \fBhandle()\fR 引数の値を指定します。この関数は、該当するハンドルを使用して得た \fBscha_resourcegroup_get()\fR の戻り値用割り当てメモリーを解放するとともに、このハンドルを無効化します。値を返す必要が生じると、get 呼び出しごとにメモリー割り当てが発生する点に注意してください。ある呼び出しで値を返すために割り当てられたメモリーが、以降の呼び出しによって上書きされたり、再利用されることはありません。
.sp
.LP
\fBscha_tags.h\fR に定義されるマクロには、\fIscha_resourcetype_get\fR の \fBtag()\fR 引数に使用されるものがあります。ここでは出力引数および追加引数の型を説明します。構造と \fBenum\fR 型は scha_calls(3HA) で説明されています。
.SS "\fBoptag\fR 引数"
.sp
.LP
次のマクロによって、リソースタイプのプロパティーを指定します。出力は、リソースタイプの名前付きプロパティーの値です。
.LP
注 - 
.sp
.RS 2
\fIoptag\fR 引数 (\fBSCHA_API_VERSION\fR や \fBSCHA_BOOT\fR など) には大文字と小文字の区別は\fBありません\fR。\fIoptag\fR 引数を指定するときには、大文字と小文字の任意の組み合わせを使用できます。
.RE
.sp
.ne 2
.mk
.na
\fB\fBSCHA_API_VERSION\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBint*\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_BOOT\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_FAILOVER\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t *\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_FINI\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_GLOBALZONE\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t *\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_INIT\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_INIT_NODES\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_initnodes_flag_t *\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_INSTALLED_NODES\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_IS_LOGICAL_HOSTNAME\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t *\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_IS_SHARED_ADDRESS\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t *\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_MONITOR_CHECK\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_MONITOR_START\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_MONITOR_STOP\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_PER_NODE\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t *\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_PKGLIST\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_POSTNET_STOP\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_PRENET_START\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_PROXY\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t *\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RESOURCE_LIST\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBscha_str_array_t **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RT_BASEDIR\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RT_DESCRIPTION\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RT_SYSTEM\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t *\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_RT_VERSION\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_SINGLE_INSTANCE\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBboolean_t *\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_START\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_STOP\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_UPDATE\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_VALIDATE\fR\fR
.ad
.sp .6
.RS 4n
出力引数の型は \fBchar **\fR です。
.RE

.SH 戻り値
.sp
.LP
これらの関数は、次の戻り値を返します。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR \fR
.ad
.RS 36n
.rt  
関数の実行に成功。
.RE

.sp
.LP
その他のエラーコードについては、scha_calls(3HA) のマニュアルページを参照してください。
.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/scha.h\fR\fR
.ad
.RS 36n
.rt  
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libscha.so\fR\fR
.ad
.RS 36n
.rt  
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scha_resource_get(1HA)、scha_calls(3HA), scha_strerror(3HA)、scha_strerror_i18n(3HA)、\fBattributes\fR(5)、rt_properties(5)
