'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_fm_tcp_write 3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_fm_tcp_write \- アプリケーションとの TCP 接続を使用したデータの書き込み
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fI file\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_fm_tcp_write\fR (\fBscds_handle_t \fR \fIhandle\fR, \fBint\fR \fIsock\fR, \fB char *\fR\fIbuffer\fR, \fBsize_t *\fR\fIsize\fR, \fBtime_t\fR \fItimeout\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_fm_tcp_write()\fR 関数は、監視対象のプロセスへの TCP 接続を使ってデータを書き込みます。
.sp
.LP
\fIsize\fR 引数は入力および出力引数です。入力時には書き込まれるバイト数を指定します。出力時には実際に書き込まれたバイト数を返します。\fIsize\fR の入力値と出力値が一致しない場合、エラーが発生します。要求されたすべてのデータが書き込まれる前にタイムアウトになった場合、\fBSCHA_ERR_TIMEOUT\fR が返されます。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIsock\fR\fR
.ad
.RS 20n
.rt  
以前の scds_fm_tcp_connect(3HA) 呼び出しから返されるソケット番号です。
.RE

.sp
.ne 2
.mk
.na
\fB\fIbuffer\fR\fR
.ad
.RS 20n
.rt  
データバッファーです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIsize\fR\fR
.ad
.RS 20n
.rt  
データバッファーサイズです。入力時には書き込まれるバイト数を指定します。出力時には実際に書き込まれたバイト数を返します。
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
タイムアウト値 (秒) です。
.RE

.SH 戻り値
.sp
.LP
\fBscds_fm_tcp_write()\fR 関数の戻り値は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 28n
.rt  
関数が正常に終了。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_TIMEOUT\fR\fR
.ad
.RS 28n
.rt  
関数がタイムアウト。
.RE

.sp
.ne 2
.mk
.na
\fBその他の値\fR
.ad
.RS 28n
.rt  
関数の実行に失敗。障害コードの意味については、scha_calls(3HA)を参照してください。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scds_fm_tcp_connect(3HA)、scds_fm_tcp_read(3HA)、scds_initialize(3HA)、scha_calls(3HA)、\fBattributes\fR(5)
