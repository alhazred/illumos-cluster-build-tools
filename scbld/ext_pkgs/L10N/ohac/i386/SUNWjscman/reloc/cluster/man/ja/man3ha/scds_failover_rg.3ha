'\" te
.\" Copyright 2007 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_failover_rg 3HA "2007 年 9 月 7 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_failover_rg \- リソースグループのフェイルオーバー
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h> \fBscha_err_t scds_failover_rg\fR (\fBscds_handle_t\fR \fIhandle\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_failover_rg()\fR 関数は、呼び出し元プログラムに渡されるリソースを含むリソースグループ上で scha_control (3HA) \fBSCHA_GIVEOVER\fR 操作を実行します。
.sp
.LP
この関数は、正常に実行されると戻りません。したがって、この関数を、呼び出し元プログラムで最後に実行されるコードにしてください。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.SH 戻り値
.sp
.LP
次の戻り値がサポートされています。
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 20n
.rt  
関数が正常に終了。
.RE

.sp
.ne 2
.mk
.na
\fBその他の値\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。その他のエラーコードについては、scha_calls(3HA)を参照してください。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scha_calls(3HA)、scha_control(3HA)、\fBattributes\fR(5)
