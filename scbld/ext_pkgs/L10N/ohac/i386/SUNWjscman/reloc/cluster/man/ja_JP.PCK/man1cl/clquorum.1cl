'\" te
.\"  Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH  clquorum   1CL  " 2008 年 9 月 11 日" " Sun Cluster 3.2 " " Sun Cluster 保守コマンド "
.SH 名前
clquorum, clq \- Sun Cluster 定足数デバイスおよびプロパティーの管理
.SH 形式
.LP
.nf
\fB/usr/cluster/bin/clquorum\fR \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum \fIsubcommand\fR\fR \fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum \fIsubcommand\fR\fR [\fIoptions\fR] \fB-v\fR \fIdevicename\fR[ \&.\|.\|.]
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum add\fR [\fB-a\fR] [\fB-t\fR \fItype\fR] [\fB-p\fR \fIname\fR=\fIvalue\fR[,\&.\|.\|.]] \fIdevicename\fR[ \&.\|.\|.]
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum add\fR \fB-i\fR {- | \fIclconfigfile\fR} [\fB-t\fR \fItype\fR] [\fB-p\fR \fIname\fR=\fIvalue\fR[,\&.\|.\|.]] {+ | \fIdevicename\fR[ \&.\|.\|.]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum disable\fR [\fB-t\fR \fItype\fR[,\&.\|.\|.]] {+ | \fIdevicename\fR...}
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum enable\fR [\fB-t\fR \fItype\fR[,\&.\|.\|.]] {+ | \fIdevicename\fR[ \&.\|.\|.]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum export\fR [\fB-o\fR {- | \fIclconfigfile\fR}] [\fB-t\fR \fItype\fR[,\&.\|.\|.]] {+ | \fIdevicename\fR[ \&.\|.\|.]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum list\fR [\fB-t\fR \fItype\fR[,\&.\|.\|.]] [\fB-n\fR \fInode\fR[,\&.\|.\|.]] [+ | \fIdevicename\fR[ \&.\|.\|.]]
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum remove\fR [\fB-t\fR \fItype\fR[,\&.\|.\|.]] {+ | \fIdevicename\fR[ \&.\|.\|.]}
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum reset\fR 
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum show\fR [\fB-t\fR \fItype\fR[,\&.\|.\|.]] [\fB-n\fR \fInode\fR[,\&.\|.\|.]] [+ | \fIdevicename\fR[ \&.\|.\|.]]
.fi

.LP
.nf
\fB/usr/cluster/bin/clquorum status\fR [\fB-t\fR \fItype\fR[,\&.\|.\|.]] [\fB-n\fR \fInode\fR[,\&.\|.\|.]] [+ | \fIdevicename\fR[ \&.\|.\|.]]
.fi

.SH 機能説明
.sp
.LP
\fBclquorum\fR コマンドは、クラスタ定足数デバイスとクラスタ定足数プロパティーを管理します。\fBclq\fR コマンドは、\fBclquorum\fR コマンドの短縮形式です。\fBclquorum\fR コマンドと \fBclq\fR コマンドは同じものです。どちらの形式のコマンドも使用できます。
.sp
.LP
このコマンドの形式は非大域ゾーンで使用できます。このコマンドの有効な使用法の詳細は、個々のサブコマンドの説明を参照してください。管理を容易にするため、このコマンドは大域ゾーンで使用します。
.sp
.LP
このコマンドの書式は次のとおりです。
.sp
.LP
\fBclquorum\fR [\fIsubcommand\fR] [\fIoptions\fR] [\fIoperands\fR]
.sp
.LP
\fIoptions\fR に \fB-?\fR または \fB-V\fR オプションを指定する場合だけは、\fIsubcommand\fR を省略できます。
.sp
.LP
このコマンドの各オプションには、長い形式と短い形式があります。各オプションの両方の形式は、このマニュアルページの「オプション」セクションのオプションの説明で紹介されています。
.sp
.LP
定足数デバイスは、スプリットブレーンおよび記憶喪失状態からクラスタを保護するために必要です。スプリットブレーンおよび記憶喪失状態については、『\fISun Cluster Concepts Guide for Solaris OS\fR』の定足数および定足数デバイスに関するセクションを参照してください。各定足数デバイスは、SCSI ケーブルまたは IP ネットワークによって、2 つ以上のノードに接続する必要があります。 
.sp
.LP
定足数デバイスは、共有 SCSI ストレージデバイス、共有 NAS ストレージデバイス、または定足数サーバーの場合があります。定足数デバイスがユーザーデータを格納する場合に、そのデバイスを定足数デバイスとして追加または削除しても、格納されているデータは影響を受けません。ただし、レプリケーションストレージデバイスを使用する場合、定足数デバイスは複製されないボリューム上に置きます。 
.sp
.LP
ノードと定足数デバイスが保守状態にある場合を除き、ノードと定足数デバイスは両方、クラスタ定足数構成に参加します。ノードまたは定足数デバイスが保守状態の場合、その投票数は常に 0 で、このノードまたは定足数デバイスは、定足数構成に参加しません。
.sp
.LP
\fBclquorum\fR コマンドを使用すると、次に示す作業を実行できます。
.RS +4
.TP
.ie t \(bu
.el o
定足数デバイスを Sun Cluster 構成に追加する
.RE
.RS +4
.TP
.ie t \(bu
.el o
Sun Cluster 構成から定足数デバイスを削除する
.RE
.RS +4
.TP
.ie t \(bu
.el o
定足数プロパティーを管理する
.RE
.SH サブコマンド
.sp
.LP
サポートされるサブコマンドには次のものがあります。
.sp
.ne 2
.mk
.na
\fB\fBadd\fR\fR
.ad
.sp .6
.RS 4n
指定した共有デバイスを定足数デバイスとして追加します。 
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
個々の定足数デバイスは、クラスタ内の最低 2 つのノードに接続します。定足数デバイスは、クラスタ構成内の接続パスを使用して、デバイスが接続されているすべてのノードに追加されます。その後、定足数デバイス〓クラスタノード間の接続が変更された場合は、ユーザーがパスを更新します。パスは、定足数デバイスを削除し、構成に追加し直すことによって更新できます。この状態は、定足数デバイスに接続されているノードをさらに追加する場合や、1 つ以上のノードから定足数デバイスを切断する場合に発生することがあります。定足数の管理の詳細は、Sun Cluster 管理ガイドを参照してください。
.sp
定足数デバイスには、タイプがいくつかあります。詳細は、「オプション」セクションの \fB-t\fR オプションを参照してください。デフォルトのタイプは \fBshared_disk\fR です。
.sp
スーパーユーザー以外のユーザーは、\fBsolaris.cluster.modify\fR 役割に基づくアクセス制御 (\fBRBAC\fR) の承認が必要です。\fBrbac\fR(5)を参照してください。
.sp
\fBremove\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBdisable\fR\fR
.ad
.sp .6
.RS 4n
定足数デバイスまたはノードを定足数保守状態に置きます。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
保守状態では、共有デバイスまたはノードの投票数は 0 です。そのような共有デバイスまたはノードは、定足数構成にもはや参加しません。さらに、保守状態のノードの場合、そのノードに接続されているすべての定足数デバイスの投票数は、1 減分されます。
.sp
この機能は、保守のために長期間ノードまたはデバイスを停止する必要がある場合に便利です。\fBinstallmode\fR が設定されていない場合、ノードをクラスタにブートし直すと、ノードは、保守モードを解除します。
.sp
ノードを保守状態にする前に、ノードを停止する必要があります。
.sp
スーパーユーザー以外のユーザーは、\fBsolaris.cluster.modify\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5)を参照してください。
.sp
\fBenable\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBenable\fR\fR
.ad
.sp .6
.RS 4n
定足数デバイスまたはノードを定足数保守状態から解除します。 
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
\fBenable\fR サブコマンドは、定足数デバイスまたはノードの保守モードを解除します。このサブコマンドは、定足数デバイスまたはノードの設定済み定足数投票数をデフォルトにリセットします。これにより、共有デバイスまたはノードは、定足数構成に参加できるようになります。 
.sp
定足数デバイスをリセットすると、定足数デバイスの投票数は \fIN\fR-1 に変更されます。この計算では、\fIN\fR は、デバイスに接続されているゼロ以外の投票数を持つノードの数です。ノードのリセット後、投票数はそのデフォルト値にリセットされます。次に、ノードに接続されている定足数デバイスの投票数が、1 増分されます。
.sp
インストールモード設定 \fBinstallmode\fR が有効でない場合は、各ノードの定足数構成が、ブート時に自動的に有効になります。
.sp
スーパーユーザー以外のユーザーは、\fBsolaris.cluster.modify\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5)を参照してください。
.sp
\fBdisable\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBexport\fR\fR
.ad
.sp .6
.RS 4n
クラスタ定足数の構成情報をエクスポートします。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
\fB-o\fR オプションを使用してファイルを指定する場合、構成情報は、そのファイルに書き込まれます。ファイルを指定しない場合、この情報は、標準出力 \fBstdout\fR に書き込まれます。
.sp
\fBexport\fR サブコマンドは、クラスタ構成データをまったく変更しません。
.sp
スーパーユーザー以外のユーザーは、\fBsolaris.cluster.read\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5)を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBlist\fR\fR
.ad
.sp .6
.RS 4n
クラスタ内で設定されている定足数デバイスの名前を表示します。
.sp
このサブコマンドは、大域ゾーンまたは非大域ゾーンで使用できます。管理を容易にするため、この書式のコマンドは大域ゾーンで使用します。
.sp
オプションを指定しない場合、\fBlist\fR サブコマンドは、クラスタ内で構成されているすべての定足数デバイスを表示します。\fB-t\fR オプションを指定すると、このサブコマンドは、指定されているタイプの定足数デバイスだけを表示します。\fB-n\fR オプションを指定すると、このサブコマンドは、指定されているいずれかのノードに接続されているすべての定足数デバイスの名前を表示します。 
.sp
スーパーユーザー以外のユーザーは、\fBsolaris.cluster.read\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5)を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBremove\fR\fR
.ad
.sp .6
.RS 4n
指定された定足数デバイス (1 つまたは複数) を、Sun Cluster 定足数構成から削除します。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
\fBremove\fR サブコマンドは、物理デバイスを切断したり、削除したりするわけではありません。また、何らかのデータが存在する場合も、デバイス上のユーザーデータに影響を与えることはありません。\fBinstallmode\fR が有効でないかぎり、2 ノードクラスタの最後の定足数デバイスは削除できません。 
.sp
削除できるのは、定足数デバイスだけです。このサブコマンドを使用して、クラスタノードを削除できません。
.sp
スーパーユーザー以外のユーザーは、\fBsolaris.cluster.modify\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5)を参照してください。
.sp
\fBadd\fR サブコマンドの説明も参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBreset\fR\fR
.ad
.sp .6
.RS 4n
定足数構成全体をリセットし、デフォルトの投票数にします。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
\fBinstallmode\fR が有効な場合、このノードはリセットによってクリアーされます。1 つ以上の定足数デバイスが正常に構成されている場合を除き、2 ノードクラスタでは \fBinstallmode\fR をリセットできません。
.sp
スーパーユーザー以外のユーザーは、\fBsolaris.cluster.modify\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5)を参照してください。
.sp
\fBinstallmode\fR プロパティーについての詳細は、cluster(1CL) の \fB-p\fR オプションも参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBshow\fR\fR
.ad
.sp .6
.RS 4n
定足数デバイスのプロパティーを表示します。 
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
 オプションを指定しない場合、\fBshow\fR サブコマンドは、クラスタ内のすべての定足数デバイスのプロパティーを表示します。 
.sp
\fB-t\fR オプションを使用してタイプを指定すると、このサブコマンドは、そのタイプのデバイスのプロパティーだけを表示します。「オプション」セクションの \fB-t\fR を参照してください。
.sp
\fB-n\fR オプションを使用してノードを指定すると、このサブコマンドは、指定されるずれかのノードに接続されている定足数デバイスのプロパティーを表示します。 
.sp
スーパーユーザー以外のユーザーは、\fBsolaris.cluster.read\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5)を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBstatus\fR\fR
.ad
.sp .6
.RS 4n
定足数デバイスの状態と投票数を表示します。 
.sp
このサブコマンドは、大域ゾーンまたは非大域ゾーンで使用できます。管理を容易にするため、この書式のコマンドは大域ゾーンで使用します。
.sp
オプションを指定しない場合、\fBstatus\fR サブコマンドは、クラスタ内のすべての定足数デバイスに関する情報を表示します。 
.sp
\fB-t\fR オプションを使用してタイプを指定すると、このサブコマンドは、そのタイプのデバイスの情報だけを表示します。「オプション」セクションの \fB-t\fR を参照してください。
.sp
\fB-n\fR オプションを使用してノードを指定すると、このサブコマンドは、指定されるずれかのノードに接続されている定足数デバイスのプロパティーを表示します。 
.sp
スーパーユーザー以外のユーザーは、\fBsolaris.cluster.read\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5)を参照してください。
.RE

.SH オプション
.sp
.LP
次のオプションがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB-\fB-help\fR\fR
.ad
.sp .6
.RS 4n
ヘルプ情報を表示します。このオプションを使用する場合、ほかの処理は実行されません。
.sp
このオプションを指定するとき、サブコマンドは指定してもしなくてもかまいません。
.sp
このオプションをサブコマンドなしで指定すると、このコマンドのサブコマンドのリストが表示されます。
.sp
サブコマンド付きでこのオプションを指定すると、サブコマンドの使用方法が表示されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-a\fR\fR
.ad
.br
.na
\fB\fB--autoconfig\fR\fR
.ad
.sp .6
.RS 4n
共有ディスクを使用する 2 ノードクラスタの場合、定足数デバイスが構成されていないときは、1 つの定足数デバイスを自動的に選択して構成します。
.sp
クラスタにあるすべての共有ディスクは、定足数デバイスとしての資格を備えていなければなりません。\fBautoconfig\fR サブコマンドは、使用できるデバイスが定足数デバイスになる資格があるかどうかをチェックしません。\fBautoconfig\fR サブコマンドは、共有ディスクだけをチェックします。
.sp
スーパーユーザー以外のユーザーは、\fBsolaris.cluster.modify\fR \fBRBAC\fR の承認が必要です。\fBrbac\fR(5)を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-i\fR \fIclconfigfile\fR\fR
.ad
.br
.na
\fB-\fB-input=\fR\fIclconfigfile\fR\fR
.ad
.br
.na
\fB-\fB-input \fR\fIclconfigfile\fR\fR
.ad
.sp .6
.RS 4n
定足数デバイスの管理に使用する構成情報を指定します。この情報は clconfiguration(5CL) のマニュアルページに定義されている書式に準拠させてください。
.sp
ほかのコマンド行オプションでサブコマンドとともに \fB-i\fR を使用すると、コマンド行オプションの引数が、構成ファイル内の設定を上書きします。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-n\fR \fInode-name\fR\fR
.ad
.br
.na
\fB-\fB-node=\fR\fInode-name\fR\fR
.ad
.br
.na
\fB-\fB-node \fR\fInode-name\fR\fR
.ad
.sp .6
.RS 4n
定足数デバイスの接続先ノード名を指定します。このオプションは、指定されているノードに接続されている定足数デバイスに対して表示される情報を制限するために、\fBlist\fR、\fBstatus\fR、および \fBshow\fR サブコマンドで使用されます。
.sp
ノード名または \fInode-name\fR に対応するノード ID を指定できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-o\fR {- | \fIclconfigfile\fR}\fR
.ad
.br
.na
\fB-\fB-output={- | \fR\fIclconfigfile\fR\fB-}\fR\fR
.ad
.br
.na
\fB-\fB-output {- | \fR\fIclconfigfile\fR\fB-}\fR\fR
.ad
.sp .6
.RS 4n
ファイルまたは標準出力 \fBstdout\fR に定足数デバイスの構成情報を書き込みます。この構成情報の形式は、clconfiguration(5CL) のマニュアルページで説明されている形式に準拠します。標準出力を指定するには、ファイル名の代わりに \fB-\fR を指定します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-p\fR \fIname\fR=\fIvalue\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB-\fB-property \fR\fIname\fR\fB-=\fR\fIvalue\fR\fB-[,...]\fR\fR
.ad
.br
.na
\fB-\fB-property \fR\fIname\fR\fB- \fR\fIvalue\fR\fB-[,...]\fR\fR
.ad
.sp .6
.RS 4n
デバイスタイプ固有の定足数デバイスのプロパティーを指定します。このオプションは、\fBadd\fR サブコマンドとともに使用します。これらプロパティーのリストと説明については、\fB-t\fR オプションの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-t\fR \fIdevice-type\fR\fR
.ad
.br
.na
\fB-\fB-type=\fR\fIdevice-type\fR\fR
.ad
.br
.na
\fB-\fB-type \fR\fIdevice-type\fR\fR
.ad
.sp .6
.RS 4n
定足数デバイスタイプを指定します。このオプションが指定されている場合、オペランドは、指定されているタイプである必要があります。
.sp
\fBadd\fR、\fBexport\fR、および \fBremove\fR サブコマンドの現在サポートされている定足数デバイスタイプは、次のとおりです。
.RS +4
.TP
.ie t \(bu
.el o
共有ローカルディスク - \fBshared_disk\fR によって指定
.RE
.RS +4
.TP
.ie t \(bu
.el o
Network Appliance 社の Network Attached Storage デバイス - \fBnetapp_nas\fR によって指定
.RE
.RS +4
.TP
.ie t \(bu
.el o
Sun Cluster Quorum Server マシンで実行する定足数サーバー処理 - \fBquorum_server\fR によって指定
.RE
デフォルトのタイプは \fBshared_disk\fR です。
.sp
\fBadd\fR サブコマンドは、\fB-t\fR\fBnode\fR を定足数タイプとして受け付けません。
.sp
\fBenable\fR、\fBdisable\fR、\fBlist\fR、\fBshow\fR、および \fBstatus\fR サブコマンドの場合、タイプ \fBnode\fR、\fBshared_disk\fR、\fBnetapp_nas\fR、または \fBquorum_server\fR を指定できます。これらの異なるタイプの定足数デバイスは、次に示すプロパティーを持っています。
.sp
.ne 2
.mk
.na
\fB\fBnetapp_nas\fR\fR
.ad
.sp .6
.RS 4n
 \fBnetapp_nas\fR タイプの定足数デバイスは、次のプロパティーを持ちます。
.sp
\fBfiler=\fIfiler-name\fR\fR: ネットワーク上のデバイスの名前を指定します。クラスタは、この名前を使用して、NAS デバイスにアクセスします。 
.sp
\fBlun_id=\fIlun-id\fR\fR: NAS 定足数デバイスとなる NAS デバイス上の LUN を指定します。\fBlun_id\fR プロパティーのデフォルトは 0 です。定足数デバイスに対してデバイス上で LUN 0 を設定した場合は、このプロパティーを指定する必要はありません。
.sp
これらのプロパティーは、\fBadd\fR サブコマンドを使用し NAS デバイスを定足数デバイスとして追加する際に必要です。 
.sp
定足数デバイスを追加する前に、NAS デバイスが設定され、動作状態になっている必要があります。NAS デバイスが、すでにブートされて動作しており、NAS 定足数として使用される LUN が、すでに作成されている必要があります。
.sp
定足数デバイスとして Network Appliance 社の NAS デバイスにサポートを提供するために、クラスタ管理者は、Network Appliance 社が提供する定足数デバイスサポートモジュールをインストールする必要があります。クラスタノードにモジュールがインストールされていない場合、\fBadd\fR サブコマンドは失敗します。サポートモジュールの入手方法については、『\fISun Cluster With Network-Attached Storage Devices Manual for Solaris OS\fR』を参照してください。さらに、Network Appliance 社の NAS デバイスの iSCSI ライセンスが、有効である必要があります。
.sp
クラスタ管理者が必要な手順を実行したあとは、\fBclquorum add\fR サブコマンドを使用して、NAS デバイスを定足数として追加できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fBnode\fR\fR
.ad
.sp .6
.RS 4n
定足数構成に参加するノードに対して、固有のプロパティーは設定されていません。
.sp
このタイプは、\fBenable\fR、\fBdisable\fR、\fBlist\fR、\fBstatus\fR、および \fBshow\fR サブコマンドでのみ使用します。タイプ \fBnode\fR の定足数デバイスを追加する際に使用できません。
.RE

.sp
.ne 2
.mk
.na
\fB\fBquorum_server\fR\fR
.ad
.sp .6
.RS 4n
 \fBquorum_server\fR タイプの定足数デバイスは、次に示すプロパティーを持っています。
.sp
\fBqshost=\fIquorum-server-host\fR\fR: 定足数サーバーが動作するマシンの名前を指定します。このホストは、ネットワーク上のマシンまたはホスト名の IP アドレスとすることができます。ホスト名を指定する場合は、\fB/etc/hosts\fR ファイル、\fB/etc/inet/ipnodes\fR ファイル、またはこの両方に、マシンの IP アドレスを指定する必要があります。
.sp
\fBport=\fIport\fR\fR: 定足数サーバーがクラスタノードと通信するために使用するポート番号を指定します。
.sp
定足数サーバーを追加する前に、定足数サーバーソフトウェアをホストマシンにインストールし、定足数サーバーを起動して実行しておく必要があります。詳細は『\fBSun Cluster Quorum Server ユーザーズガイド\fR』を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBshared_disk\fR\fR
.ad
.sp .6
.RS 4n
\fBshared_disk\fR 定足数デバイスに対して、固有のプロパティーは設定されていません。\fBautoconfig\fR サブコマンドは、このタイプの定足数デバイスだけを受け付けます。
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB-\fB-version\fR\fR
.ad
.sp .6
.RS 4n
コマンドのバージョンを表示します。
.sp
このオプションは、ほかのサブコマンド、オプション、またはオペランドと一緒に使用しないでください。サブコマンド、オプション、またはオペランドは、無視されます。\fB-V\fR オプションは、コマンドのバージョンだけを表示します。その他の処理は行いません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR\fR
.ad
.br
.na
\fB-\fB-verbose\fR\fR
.ad
.sp .6
.RS 4n
詳細な情報を標準出力 \fBstdout\fR に表示します。
.RE

.SH オペランド
.sp
.LP
次のオペランドがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fIdevicename\fR\fR
.ad
.sp .6
.RS 4n
\fBadd\fR、\fBexport\fR、および \fBremove\fR サブコマンドの場合のみ、オペランドは共有ディスク (SCSI、定足数サーバー、または NAS 定足数デバイス) の名前です。\fBadd\fR サブコマンドの場合、\fB-i\fR を使用して \fBclconfigurationfile\fR を指定しないときは、1 つ以上のデバイスをオペランドとして指定する必要があります。
.sp
\fBdisable\fR、\fBenable\fR、\fBlist\fR、\fBstatus\fR、および \fBshow\fR サブコマンドの場合のみ、オペランドをノードの名前または共有ディスク (SCSI、定足数サーバー、または NAS 定足数デバイス) の名前とすることができます。 
.sp
どのような場合でも、オペランドのタイプは \fB-t\fR オプション (指定する場合) の値と一致します。 
.sp
\fIdevicename\fR オペランドとして、次の値を使用します。
.RS +4
.TP
.ie t \(bu
.el o
ノードの場合、オペランドはノード名またはノード ID です。
.RE
.RS +4
.TP
.ie t \(bu
.el o
SCSI 定足数デバイスの場合、オペランドは、\fBd1\fR または \fB/dev/did/rdsk/d1\fR のように、デバイス識別子またはフル DID パス名である必要があります。 
.RE
.RS +4
.TP
.ie t \(bu
.el o
NAS 定足数デバイスの場合、オペランドは、クラスタ構成にデバイスを追加したときに定義したデバイス名である必要があります。 
.RE
.RS +4
.TP
.ie t \(bu
.el o
定足数サーバーの定足数デバイスの場合、オペランドは、単数または複数の定足数サーバーの識別子を指定する必要があります。これは定足数サーバーのインスタンス名でもよく、すべての定足数デバイスで一意の必要があります。 
.RE
.RE

.sp
.ne 2
.mk
.na
\fB+\fR
.ad
.sp .6
.RS 4n
\fBdisable\fR、\fBenable\fR、\fBlist\fR、\fBstatus\fR、および \fBshow\fR サブコマンドの場合のみ、クラスタに対して構成されているすべての定足数デバイスを指定します。\fB-t\fR オプションを使用する場合、プラス記号 (\fB+\fR) オペランドでは該当するタイプを持つすべてのデバイスを指定します。 
.RE

.SH 終了ステータス
.sp
.LP
指定したすべてのオペランドでコマンドが成功すると、コマンドはゼロ (\fBCL_NOERR\fR) を返します。あるオペランドでエラーが発生すると、コマンドはオペランドリストの次のオペランドを処理します。戻り値は常に、最初に発生したエラーを反映します。
.sp
.LP
次の終了値が返される可能性があります。
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fBCL_NOERR\fR\fR
.ad
.sp .6
.RS 4n
エラーなし
.sp
実行したコマンドは正常に終了しました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR \fBCL_ENOMEM\fR\fR
.ad
.sp .6
.RS 4n
十分なスワップ空間がありません。
.sp
クラスタノードがスワップメモリーまたはその他のオペレーティングシステムリソースを使い果たしました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB3\fR \fBCL_EINVAL\fR\fR
.ad
.sp .6
.RS 4n
無効な引数
.sp
コマンドを間違って入力したか、\fB-i\fR オプションで指定したクラスタ構成情報の構文が間違っていました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB6\fR \fBCL_EACCESS\fR\fR
.ad
.sp .6
.RS 4n
アクセス権がありません
.sp
指定したオブジェクトにアクセスできません。このコマンドを実行するには、スーパーユーザーまたは RBAC アクセスが必要である可能性があります。詳細は、\fBsu\fR(1M)、および \fBrbac\fR(5) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB18\fR \fBCL_EINTERNAL\fR\fR
.ad
.sp .6
.RS 4n
内部エラーが発生しました。
.sp
内部エラーは、ソフトウェアの欠陥またはその他の欠陥を示しています。
.RE

.sp
.ne 2
.mk
.na
\fB\fB35\fR \fBCL_EIO\fR\fR
.ad
.sp .6
.RS 4n
I/O error
.sp
物理的な入出力エラーが発生しました。
.RE

.sp
.ne 2
.mk
.na
\fB\fB36\fR \fBCL_ENOENT\fR\fR
.ad
.sp .6
.RS 4n
そのようなオブジェクトはありません。
.sp
次のいずれかの理由のために、指定したオブジェクトを見つけることができません。
.RS +4
.TP
.ie t \(bu
.el o
オブジェクトが存在しません。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-o\fR オプションで作成しようとした構成ファイルへのパスのディレクトリが存在しません。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-i\fR オプションでアクセスしようとした構成ファイルにエラーが含まれています。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fB39\fR \fBCL_EEXIST\fR\fR
.ad
.sp .6
.RS 4n
オブジェクトは存在します。
.sp
指定したデバイス、デバイスグループ、クラスタインターコネクトコンポーネント、ノード、クラスタ、リソース、リソースタイプ、またはリソースグループはすでに存在します。
.RE

.sp
.ne 2
.mk
.na
\fB\fB41\fR \fBCL_ETYPE\fR\fR
.ad
.sp .6
.RS 4n
無効なタイプです。
.sp
\fB-t\fR または \fB-p\fR オプションで指定したタイプは存在しません。
.RE

.SH 使用例
.LP
\fB例 1 \fRSCSI 定足数デバイスの追加
.sp
.LP
\fBclquorum\fR コマンドは、すべてのクラスタノードに接続されている SCSI 定足数デバイスを構成します。 

.sp
.in +2
.nf
# \fBclquorum add /dev/did/rdsk/d4s2\fR
.fi
.in -2
.sp

.sp
.LP
\fBadd\fR サブコマンドの使用時は、\fBshared_disk\fR タイプがデフォルトです。\fBshared_disk\fR 定足数デバイスを追加するには、\fB-t\ shared_disk\fR を指定する必要はありません。

.LP
\fB例 2 \fRNetwork Appliance NAS 定足数デバイスの追加
.sp
.LP
次の \fBclquorum\fR コマンドは、すべてのクラスタノードに接続されている Network Appliance NAS 定足数デバイス \fBqd1\fR を追加します。

.sp
.in +2
.nf
# \fBclquorum -t netapp_nas -p filer=nas1.sun.com -p lun_id=0 qd1\fR
.fi
.in -2
.sp

.sp
.LP
NAS 定足数デバイスの名前は、すべてのクラスタ定足数デバイスで一意である必要があります。

.LP
\fB例 3 \fR定足数サーバーの追加
.sp
.LP
次の \fBclquorum\fR コマンドは、定足数サーバー \fBqs1\fR を構成します。

.sp
.in +2
.nf
# \fBclquorum add -t quorum_server -p qshost=10.11.114.81 -p port=9000 qs1\fR
.fi
.in -2
.sp

.LP
\fB例 4 \fR定足数デバイスの削除
.sp
.LP
次の \fBclquorum\fR コマンドは、定足数デバイス \fBd4\fR を削除します。

.sp
.in +2
.nf
# \fBclquorum remove d4\fR
.fi
.in -2
.sp

.sp
.LP
定足数デバイスを削除するために使用するコマンドは、デバイスタイプが \fBshared_disk\fR、\fBnas\fR、または \fBquorum_server\fR のいずれでも同じです。 

.LP
\fB例 5 \fR定足数デバイスの保守状態への移行
.sp
.LP
次の \fBclquorum\fR コマンドは、定足数デバイス \fBqs1\fR を保守状態に移行し、デバイスが保守状態であることを確認します。

.sp
.in +2
.nf
# \fBclquorum disable qs1\fR
# \fBclquorum status qs1\fR
\ 
=== Cluster Quorum ===

--- Quorum Votes by Device ---

Device Name       Present      Possible      Status
-----------       -------      --------      ------
qs1                1            1             Offline
.fi
.in -2
.sp

.LP
\fB例 6 \fR定足数デバイスの定足数投票数のリセット
.sp
.LP
次の \fBclquorum\fR コマンドは、定足数デバイス \fBd4\fR の設定済みの定足数投票数をデフォルトにリセットします。

.sp
.in +2
.nf
# \fBclquorum enable d4\fR
.fi
.in -2
.sp

.LP
\fB例 7 \fRクラスタ内で構成されている定足数デバイスの表示
.sp
.LP
次の \fBclquorum\fR コマンドは、簡潔な形式と詳細形式で、定足数デバイスを表示します。

.sp
.in +2
.nf
# \fBclquorum list\fR
d4
qd1
pcow1
pcow2
.fi
.in -2
.sp

.sp
.in +2
.nf
# \fBclquorum list -v\fR
Quorums               Type
-------               ----
d4                    shared_disk
qd1                   netapp_nas
pcow1                 node
pcow2                 node
.fi
.in -2
.sp

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWsczu
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
Intro(1CL)、cluster(1CL)、clconfiguration(5CL)
.SH 注意事項
.sp
.LP
スーパーユーザーはこのコマンドのすべての形式を実行できます。
.sp
.LP
任意のユーザーは次のオプションを指定してこのコマンドを実行できます。
.RS +4
.TP
.ie t \(bu
.el o
\fB-?\fR オプション
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-V\fR オプション
.RE
.sp
.LP
スーパーユーザー以外のユーザーがほかのサブコマンドを指定してこのコマンドを実行するには、\fBRBAC\fR の承認が必要です。次の表を参照してください。
.sp

.sp
.TS
tab() box;
cw(0i) |cw(5.5i) 
lw(0i) |lw(5.5i) 
.
サブコマンド\fBRBAC\fR の承認
_
\fBadd\fR\fBsolaris.cluster.modify\fR
_
\fBdisable\fR\fBsolaris.cluster.modify\fR
_
\fBenable\fR\fBsolaris.cluster.modify\fR
_
\fBexport\fR\fBsolaris.cluster.read\fR
_
\fBlist\fR\fBsolaris.cluster.read\fR
_
\fBremove\fR\fBsolaris.cluster.modify\fR
_
\fBreset\fR\fBsolaris.cluster.modify\fR
_
\fBshow\fR\fBsolaris.cluster.read\fR
_
\fBstatus\fR\fBsolaris.cluster.read\fR
.TE

