'\" te
.\" Copyright 2007 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH clconfiguration 5CL "2007 年 9 月 10 日" "Sun Cluster 3.2" "Sun Cluster 標準、環境、およびマクロ"
.SH 名前
clconfiguration \- Sun Cluster 構成用 XML ファイルのクラスタ DTD について記述します。
.SH 機能説明
.sp
.LP
\fBclconfiguration\fR では、Sun Cluster 構成用 XML (eXtensible Markup Language) ファイルの文書型定義 (Documentation Type Definition、DTD) を規定します。Sun Cluster 構成ファイルには、XML 要素付きの Sun Cluster 構成情報が含まれています。このファイルには、1 つまたは複数のクラスタ、あるいはクラスタの一部の構成情報が含まれます。この Sun Cluster 構成情報は、クラスタ構成のバックアップやクラスタの複製など、多数のクラスタ機能で使用できます。
.sp
.LP
DTD は、各要素、それらの関係、およびそれらの属性を定義します。要素名は、要素の内容を反映したものになります。たとえば、\fB<devicegroup>\fR という要素では、クラスタデバイスグループを定義します。要素の属性が、プロパティーまたは特性の変更や詳細設定に使用される場合もあります。オブジェクト指向の多数の Sun Cluster コマンドに含まれる \fBexport\fR サブコマンドは、DTD で規定されているフォーマットでクラスタオブジェクト情報をエクスポートします。多くの Sun Cluster コマンドでは、クラスタ構成用 XML データの使用により、Sun Cluster オブジェクトの追加、作成、および変更が行えます。 
.SH 要素の階層
.sp
.LP
DTD に必要な要素の階層を、次のリストに示します。このリストでは、子および属性プロパティーのデフォルト値が次のようになっています。
.sp
.ne 2
.mk
.na
\fB必須\fR
.ad
.RS 28n
.rt  
特に断らないかぎり、1 つ以上必要です。
.RE

.sp
.ne 2
.mk
.na
\fBオプション\fR
.ad
.RS 28n
.rt  
特に断らないかぎり、ゼロまたは 1 つ存在できます。
.RE

.sp
.in +2
.nf
<propertyList>
 <property>
<state>
<allNodes>

<-- Cluster -->

<cluster>

 <-- Cluster Nodes -->

 <nodelist>
   <node>
   
 <-- Cluster Transport -->

 <clusterTransport>
   <transportNodeList>
     <transportNode>
       <transportAdapter>
         <transportType>
   <transportSwitchList>
     <transportSwitch>
   <transportCableList>
     <transportCable>
       <endpoint>

 <-- Cluster Global Devices -->

 <deviceList>
   <device>
     <devicePath>

 <-- Cluster Quorum -->

 <clusterQuorum>
   <quorumNodeList>
     <quorumNode>
   <quorumDeviceList>
     <quorumDevice>
       <quorumDevicePathList>
         <quorumDevicePath>

 <-- Cluster Device Groups -->

 <devicegroupList>
   <devicegroup>
     <memberDeviceList>
       <memberDevice>
     <devicegroupNodeList>
       <devicegroupNode>

 <-- Cluster Resource Types -->

 <resourcetypeList>
   <resourcetype>
      <resourcetypeRTRFile>
      <resourcetypeNodeList>
        <resourcetypeNode>
        <methodList>
        <method>
        <parameterList>
          <parameter>

 <-- Cluster Resources -->

 <resourceList>
   <resource>
     <resourceNodeList>
       <resourceNode)
     <monitoredState>
 
 <-- Cluster Resource Groups -->

 <resourcegroupList>
   <resourcegroup>
     <failoverMode>
     <managedState>
     <resourcegroupNodeList>
       <resourcegroupNode>
     <resourcegroupResourceList>
       <resourcegroupResource>

 <-- Cluster NAS Devices -->

 <nasdeviceList>
   <nasdevice>
      <nasdir>

 <-- Cluster SNMP -->

 <snmpmibList>
   <snmpmib>
 <snmphostList>
   <snmphost>
 <snmpuserList>
   <snmpuser> 

 <-- Cluster Telemetrics -->

 <telemetrics>
   <telemetryObjectType>
     <telemetryAttribute>
.fi
.in -2
.sp

.SH 要素
.sp
.LP
このセクションでは、クラスタ DTD で定義されているすべての要素の一覧を示し、それらについて解説します。要素に子または属性が必要な場合、必須のデフォルト値は 1 です。オプションの要素のデフォルト数は、ゼロまたは 1 です。
.sp
.ne 2
.mk
.na
\fB\fB<allNodes>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ内の全メンバーノードのリストです。\fB<allNodes>\fR 要素は、汎用要素です。
.sp
\fB<allNodes>\fR 要素は、クラスタの全ノードを表すために使用されます。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcetypeNodeList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<cluster>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ構成用 XML ファイル全体のルート要素。どのクラスタ構成用 XML ファイルも、ルートとしてこの要素で始めます。DTD では、\fB<cluster>\fR 要素を 1 つだけ受け入れることができます。クラスタ構成用 XML ファイル内の後続の \fB<cluster>\fR 要素は無視されます。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能) 
.RS +4
.TP
.ie t \(bu
.el o
\fB<propertyList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<nodeList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<clusterTransport>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<deviceList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<clusterQuorum>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<deviceGroupList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourcetypeList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourcegroupList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourceList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<nasdeviceList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<snmpmibList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<snmphostList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<snmpuserList>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
クラスタの名前。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<clusterQuorum>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ定足数構成のルート要素。クラスタ定足数情報はすべて、\fB<clusterQuorum>\fR 要素の子要素内で定義されます。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
 \fB<cluster>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<quorumDeviceList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<quorumNodeList>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<clusterTransport>\fR\fR
.ad
.sp .6
.RS 4n
クラスタトランスポート構成のルート要素。クラスタトランスポート情報はすべて、\fB<clusterTransport>\fR 要素のサブレベル内で表示されます。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR\&.
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<transportNodeList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<transportSwitchList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<transportCableList>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<device>\fR\fR
.ad
.sp .6
.RS 4n
クラスタデバイス ID 擬似ドライバ (DID) デバイス。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<deviceList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
 \fB<devicePath> (ゼロ個以上)\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須: 
.RS +4
.TP
.ie t \(bu
.el o
\fBctd\fR
.sp
UNIX ディスク名。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
デバイスのインスタンス番号。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<devicegroup>\fR\fR
.ad
.sp .6
.RS 4n
クラスタデバイスグループインスタンスのルート要素。各デバイスグループの特性はすべて、\fB<devicegroup>\fR 要素の子要素内で定義します。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<devicegroupList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<devicegroupNodeList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<memberDeviceList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<propertyList>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
デバイスグループの名前。\fBname\fR 属性は、有効な任意の文字列とすることができます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBtype\fR
.sp
デバイスグループのタイプ。\fBtype\fR 属性は、\fBrawdisk \fR、\fBvxvm\fR、\fBsvm\fR、または \fBsds\fR という値を持つことができます。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<devicegroupList>\fR\fR
.ad
.sp .6
.RS 4n
すべてのクラスタデバイスグループのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<devicegroup>\fR
.sp
クラスタ内の各デバイスグループでは、\fB<devicegroup>\fR 要素を 1 つ使用することができます。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<devicegroupNode>\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループが存在するノード。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<devicegroupNodeList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBnodeRef\fR
.sp
クラスタノードの名前を指定します。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<devicegroupNodeList>\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループが存在するノードのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<devicegroup>\fR (1 つか多数)
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fB<devicegroupNode> (1 つか多数)\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<deviceList>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ DID デバイスのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<device>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
固定:
.RS +4
.TP
.ie t \(bu
.el o
\fBreadonly\fR
.sp
\fBreadonly\fR 属性は、\fBtrue\fR の固定値を持ちます。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<deviceNode>\fR\fR
.ad
.sp .6
.RS 4n
特定の \fB<device>\fR が存在するノードおよびディスクデバイス。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<device>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBnodeRef\fR
.sp
インスタンスが存在するノードの名前。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<endpoint>\fR\fR
.ad
.sp .6
.RS 4n
トランスポート終端の 1 つ。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<transportCable>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
アダプタまたはスイッチの名前。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBnodeRef\fR
.sp
指定したアダプタがホストされているノードの名前。\fBnodeRef\fR 属性が必要なのは、\fBtype\fR 属性に \fBadapter\fR が設定されている場合だけです。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBtype\fR
.sp
\fBtype\fR 属性には、\fBadapter\fR または \fBswitch\fR を設定できます。
.sp
\fBtype\fR 属性に \fBadapter\fR を設定する場合は、\fBnodeRef\fR 属性を指定します。
.sp
\fBtype\fR 属性を \fBswitch\fR に設定した場合、\fBport\fR 属性を指定できます。ただし、\fBport\fR 属性は必要ではありません。
.RE
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fBport\fR
.sp
スイッチ上のポートの番号。\fBport\fR 属性は、\fBtype\fR 属性に \fBswitch\fR を設定する場合だけ指定します。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<failoverMode>\fR\fR
.ad
.sp .6
.RS 4n
リソースグループのフェイルオーバーモード。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcegroup>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBvalue\fR
.sp
\fBvalue\fR 属性には、\fBfailover\fR または \fBscalable\fR を設定できます。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<managedState>\fR\fR
.ad
.sp .6
.RS 4n
リソースグループが管理されているのかどうかを示します。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcegroup>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBvalue\fR
.sp
\fBvalue\fR 属性は、\fBtrue\fR または \fBfalse\fR とすることができます。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<memberDevice>\fR\fR
.ad
.sp .6
.RS 4n
特定のデバイスグループのメンバー名。\fB<devicegroup>\fR のタイプが \fBrawdisk\fR の場合は、raw ディスクのパス名をそれぞれ指定して、\fB<member>\fR 要素を 1 つまたは複数指定します。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<memberDeviceList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
メンバーの名前。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<memberDeviceList>\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループメンバーのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<devicegroup>\fR(一つか多数)
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fB<memberDevice>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<method>\fR\fR
.ad
.sp .6
.RS 4n
汎用のメソッドタイプと特定のリソースタイプの実際のメソッド名の間のマッピング。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<methodList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
リソースタイプのメソッドの実際の名前。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBtype\fR
.sp
リソースタイプのメソッドのタイプ。次のタイプを指定できます。
.RS +4
.TP
.ie t \(bu
.el o
\fBMONITOR_CHECK\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBMONITOR_START\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBMONITOR_STOP\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBPRENET_START\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBSTART\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBSTOP\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBVALIDATE\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBUPDATE\fR
.RE
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<methodList>\fR\fR
.ad
.sp .6
.RS 4n
特定の \fB<resourcetype>\fR で利用可能なすべての \fB<method>\fR 要素のリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcetype>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<method>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
固定:
.RS +4
.TP
.ie t \(bu
.el o
\fBreadonly\fR
.sp
\fBreadonly\fR 属性は、\fBtrue\fR の固定値を持ちます。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<monitoredState>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ内の要素の状態の一部を示すブール値。たとえば、リソースの \fB<monitoredState>\fR はそのリソースが監視対象であるかどうかを示しますが、そのリソースが使用可能かどうかは示しません。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resource>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBvalue\fR
.sp
\fBvalue\fR 属性には、\fBtrue\fR または \fBfalse\fR を設定できます。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<nasdevice>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ上の NAS デバイスの単一インスタンス。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<nasdeviceList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<nasdir>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
NAS デバイスのホスト名。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBtype\fR
.sp
NAS デバイスのタイプ。\fBsun\fR または \fBnetapp\fR を指定します。
.RE
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fBuserid\fR
.sp
NAS デバイスへのアクセスに必要なユーザー名。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<nasdeviceList>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ上のすべての NAS デバイスのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<nasdevice>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<nasdir>\fR\fR
.ad
.sp .6
.RS 4n
NAS デバイス上のディレクトリの 1 つ。各 NAS デバイスは、複数の NAS ディレクトリを持つことができます。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<nasdevice>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBpath\fR
.sp
NAS ディレクトリへのパス。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<node>\fR\fR
.ad
.sp .6
.RS 4n
1 つのクラスタノード。クラスタ内のノードごとに、\fB<node>\fR 要素を 1 つ指定します。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<nodeList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<propertyList>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
ノードの名前に等しくします。
.RE
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fBglobaldevfs\fR
.sp
グローバルマウントディレクトリへのパス。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBid\fR
.sp
クラスタノード ID。指定しなかった場合、クラスタノード ID 属性のデフォルト値は空の文字列になります。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<nodeList>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ内の全ノードのリスト。 
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<node>\fR
.sp
クラスタ上のノードごとに、\fBnode\fR 属性を 1 つ以上指定します。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<parameter>\fR\fR
.ad
.sp .6
.RS 4n
\fB<method>\fR 要素のタイムアウト値とクラスタリソースタイプの他のパラメータを規定する一連の属性。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<parameterList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBextension\fR
.sp
\fBextension\fR 属性には、\fBtrue\fR または \fBfalse\fR を設定できます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
パラメータの名前
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBtunability\fR
.sp
パラメータの調整時期の値。\fBtunability\fR 属性には、次の値のいずれか 1 つを設定できます。\fBatCreation\fR\fB、anyTime\fR、または \fBwhenDisabled\fR。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBtype\fR
.sp
パラメータのタイプ。\fBtype\fR 属性には、次の値のいずれか 1 つを設定できます。\fBboolean\fR、\fBenum\fR、\fBint\fR、\fBstring \fR、または \fBstringArray\fR。
.RE
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fBdefault\fR
.sp
値が明示的に指定されなかった場合のこのパラメータのデフォルト値。たとえば、\fBmethod\fR 要素のタイムアウトのデフォルト値は \fBSTART\fR です。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBdescription\fR
.sp
パラメータの説明。未定義の場合、この属性のデフォルト値は空の文字列です。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBenumList\fR
.sp
オブジェクトの列挙型リスト。たとえば、優先度に従ったフェイルオーバーモードのリストが属性になる場合もあります。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBmaxLength\fR
.sp
\fBstring\fR または \fBstringArray\fR 型のパラメータの最大長。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBminArrayLength\fR
.sp
\fBstringArray\fR 型パラメータの最小サイズ。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBminLength\fR
.sp
\fBstring\fR または \fBstringArray\fR 型のパラメータの最小長。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<parameterList>\fR\fR
.ad
.sp .6
.RS 4n
リソースタイプを記述する \fB<parameter>\fR 要素のリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcetype>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<parameter>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
固定:
.RS +4
.TP
.ie t \(bu
.el o
\fBreadonly\fR
.sp
\fBreadonly\fR 属性は、\fBtrue\fR の固定値を持ちます。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<property>\fR\fR
.ad
.sp .6
.RS 4n
1 つのプロパティーを規定する汎用要素。このプロパティーは、クラスタ関連構成のサブセットに固有なものではありません。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<propertyList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
プロパティーの名前です。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBvalue\fR
.sp
プロパティーの値です。
.RE
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fBreadonly\fR
.sp
\fBreadonly\fR 属性には、\fBtrue\fR または \fBfalse\fR を設定できます。この値を指定しないと、この属性のデフォルト値は \fBfalse\fR になります。
.RE
.RS +4
.TP
.ie t \(bu
.el o
タイプ
.sp
プロパティータイプ。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<propertyList>\fR\fR
.ad
.sp .6
.RS 4n
\fB<property>\fR 要素のリスト。\fB <propertyList>\fR 要素は、汎用要素です。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR、\fB<deviceGroup>\fR、 \fB <node>\fR、\fB<quorumDevice>\fR、\fB<quorumNode>\fR、\fB<resource>\fR、\fB<resourceNode>\fR、\fB <resourcegroup>\fR、\fB<resourceType>\fR、\fB<transportAdapter>\fR、\fB<transportType>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
\fBOptional:\fR
.RS +4
.TP
.ie t \(bu
.el o
\fB<property>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fBextension\fR
.sp
この属性は次のいずれかの値を取ることができます。\fBtrue\fR、\fBfalse\fR、\fBmixed\fR、または \fBdoesNotApply\fR。値を指定しないと、\fBextension\fR 属性はデフォルト値 \fBdoesNotApply\fR になります。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBreadonly\fR
.sp
この属性は、\fBtrue\fR または \fBfalse\fR の値を持つことができます。値を指定しないと、\fBreadonly\fR 属性はデフォルト値 \fBfalse\fR を持ちます。 
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<quorumDevice>\fR\fR
.ad
.sp .6
.RS 4n
それぞれのクラスタ定足数デバイス。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<quorumDeviceList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<propertyList>\fR
.sp
\fB<quorumDevice>\fR 要素は、\fB<propertyList>\fR の子を 1 つだけ持つことができます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<quorumDevicePathList>\fR
.sp
\fB<quorumDevice>\fR 要素は、\fB<quorumDevicePathList>\fR の子を 1 つだけ持つことができます。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
定足数デバイスの名前。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBtype\fR
.sp
この要素によって参照される定足数デバイスのタイプ。\fBtype\fR 属性には、\fBnetapp_nas\fR、\fBscsi\fR、または \fBquorum_server\fR を設定できます。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<quorumDeviceList>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ内の定足数デバイスのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<clusterQuorum>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<quorumDevice>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<quorumDevicePath>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ定足数デバイスのパス。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<quorumDevicePathList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<state>\fR
.sp
\fB<quorumDevicePath>\fR 要素は、\fB<state>\fR の子を 1 つだけ持つことができます。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBnodeRef\fR
.sp
定足数デバイスが存在するノードの名前。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<quorumDevicePathList>\fR\fR
.ad
.sp .6
.RS 4n
特定の \fB<quorumDevice>\fR のすべてのパスのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<quorumDevice>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fB<quorumDevicePath>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
固定:
.RS +4
.TP
.ie t \(bu
.el o
\fBreadonly\fR
.sp
\fBreadonly\fR 属性には、\fBtrue\fR が設定されます。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<quorumNode>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ定足数に参加するクラスタ内の 1 つのノード。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<quorumNodeList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<propertyList>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fB<nodeRef>\fR
.sp
ノードの名前。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<quorumNodeList>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ定足数に参加するすべてのノードのリスト。\fBinstallmode\fR にない有効なクラスタの場合、このリストには通常、クラスタ内のすべてのノードが含まれます。まだ \fBinstallmode\fR にあるクラスタの場合、このリストにはクラスタノードの 1 つだけが含まれる場合があります。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<clusterQuorum>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fB<quorumNode>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
固定:
.RS +4
.TP
.ie t \(bu
.el o
\fBreadonly\fR
.sp
\fBreadonly\fR 属性には、\fBtrue\fR が設定されます。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resource>\fR\fR
.ad
.sp .6
.RS 4n
1 つのクラスタリソース。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourceList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourceNodeList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<propertyList>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
リソースの名前
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBresourcegroupRef\fR
.sp
リソースが属するリソースグループ。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBresourcetypeRef\fR
.sp
この要素によって規定されるリソースの型。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourceList>\fR\fR
.ad
.sp .6
.RS 4n
構成内で定義されているクラスタリソースのルート ノードのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<resource>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourcegroup>\fR\fR
.ad
.sp .6
.RS 4n
クラスタリソースグループ。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcegroupList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fB<failoverMode>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<managedState>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourcegroupNodeList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourcegroupResourceList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<propertyList>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
リソースの名前
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourcegroupList>\fR\fR
.ad
.sp .6
.RS 4n
構成内で定義されているクラスタリソースグループのルートノード。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourcegroup>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourcegroupNode>\fR\fR
.ad
.sp .6
.RS 4n
リソースグループが定義されているノード。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcegroupNodeList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBnodeRef\fR
.sp
クラスタノードの名前。
.RE
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
ゾーン
.sp
ゾーンの名前。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourcegroupNodeList>\fR\fR
.ad
.sp .6
.RS 4n
特定のリソースグループが動作するクラスタノード。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcegroup>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourcegroupNode>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourcegroupResource>\fR\fR
.ad
.sp .6
.RS 4n
特定のリソースグループに属するクラスタリソース。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcegroupResourceList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBresourceRef\fR
.sp
リソースの名前
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourcegroupResourceList>\fR\fR
.ad
.sp .6
.RS 4n
リソースグループで定義されているリソースのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcegroup>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourcegroupResource>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourceNode>\fR\fR
.ad
.sp .6
.RS 4n
リソースが定義されているノード。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourceNodeList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fB<state>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<monitoredState>\fR
.RE
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<propertyList>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBnodeRef\fR
.sp
リソースタイプの名前。
.RE
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fBzone\fR
.sp
ゾーンの名前。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourcetype>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ内で利用可能な 1 つのクラスタリソースタイプ。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcetypeList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourcetypeRTRFile>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourcetypeNodeList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<methodList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<parameterList>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<propertyList>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
リソースタイプの名前。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourcetypeList>\fR\fR
.ad
.sp .6
.RS 4n
構成内で定義されているクラスタリソースタイプのルートノード。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourcetype>\fR 
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourcetypeNode>\fR\fR
.ad
.sp .6
.RS 4n
リソースタイプが定義されている 1 つのノード。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcetypeNodeList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBnodeRef\fR
.sp
クラスタノードの名前。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourcetypeNodeList>\fR\fR
.ad
.sp .6
.RS 4n
特定のリソースタイプが存在するクラスタノードのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcetype>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
必須: \fB<resourcetypeNodeList>\fR 要素には、1 つ以上の \fB<resourcetypeNode>\fR 要素か、\fB<allNodes>\fR 要素を 1 つだけ含めます。
.RS +4
.TP
.ie t \(bu
.el o
\fB<resourcetypeNode>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<allNodes>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<resourcetypeRTRFile>\fR\fR
.ad
.sp .6
.RS 4n
特定のリソースタイプを規定するリソースタイプ登録 (RTR) ファイルの名前。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<resourcetype>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
RTR ファイルの名前。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<snmphost>\fR\fR
.ad
.sp .6
.RS 4n
クラスタノード上で構成される SNMP ホストおよびコミュニティー。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<snmphostList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBcommunity\fR
.sp
SNMP コミュニティー名。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
インスタンスの名前。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBnodeRef\fR
.sp
SNMP ホストおよびコミュニティーが存在するノード。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<snmphostList>\fR\fR
.ad
.sp .6
.RS 4n
クラスタノード上で構成される SNMP ホストおよびコミュニティーのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<snmphost>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<snmpmib>\fR\fR
.ad
.sp .6
.RS 4n
クラスタノード上の SNMP MIB。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<snmpmibList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fBstate\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
MIB の名前。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBnodeRef\fR
.sp
SNMP MIB が存在するノード。
.RE
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fBprotocol\fR
.sp
MIB が使用する SNMP プロトコル。この属性のデフォルト値は、\fBSNMPv2\fR です。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBvalue\fR
.sp
SNMPv3 または SNMPv2
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fBsnmpmibList\fR\fR
.ad
.sp .6
.RS 4n
クラスタノード上の SNMP MIB のリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<snmpmib>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<snmpuser>\fR\fR
.ad
.sp .6
.RS 4n
クラスタノード上で構成される SNMPv3 ユーザー。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<snmpuserList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
ユーザー名。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBnodeRef\fR
.sp
SNMPv3 ユーザーが存在するノード。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBauth\fR
.sp
\fBauth\fR 属性には、\fBMD5\fR または \fBSHA\fR を設定できます。
.RE
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fBdefaultUser\fR
.sp
\fBdefaultUser\fR 属性には、\fByes\fR または \fBno\fR を設定できます。値を指定しないと、ノードの構成に基づいて適切な値がこの属性のデフォルト値になります。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBdefaultSecurityLevel\fR
.sp
ユーザーのセキュリティーレベル。\fBsecurity\fR 属性には、次のいずれか 1 つの値を設定できます。
.RS +4
.TP
.ie t \(bu
.el o
\fBauthPriv\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBauthNoPriv\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBnoAuthNoPriv\fR
.RE
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<snmpuserList>\fR\fR
.ad
.sp .6
.RS 4n
クラスタノード上で構成される SNMPv3 ユーザーのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
\fB<snmpuser>\fR
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<state>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ構成内のさまざまなオブジェクトの状態。\fB<state>\fR 要素は、汎用要素です。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<quorumDevicePath>\fR、\fB<resourceNode>\fR、\fB<snmpmib>\fR、\fB<telemetryAttribute>\fR、\fB<transportAdapter>\fR、\fB<transportCable>\fR、\fB<transportSwitch>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
なし
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBvalue\fR
.sp
\fBvalue\fR 属性には、\fBenabled\fR または \fBdisabled\fR を設定できます。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<telemetrics>\fR\fR
.ad
.sp .6
.RS 4n
クラスタ監視しきい値
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<cluster>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<telemetryObjectType>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<telemetryAttribute>\fR\fR
.ad
.sp .6
.RS 4n
ユーザーが監視できるシステムリソースの属性。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<telemetryObjectType>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fB<state>\fR (1 個以上)
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
属性の名前
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<telemetryObjectType>\fR\fR
.ad
.sp .6
.RS 4n
ユーザーが監視できるオブジェクトのタイプ。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<telemetrics>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fB<telemetryAttibute>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
名前
.sp
属性の名前
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<transportAdapter>\fR\fR
.ad
.sp .6
.RS 4n
プライベートクラスタトランスポートで使用するネットワークアダプタ。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<transportNode>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能) 
.RS +4
.TP
.ie t \(bu
.el o
\fB<state>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<transportType>\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB<propertyList>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
ネットワークアダプタの名前。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<transportCable>\fR\fR
.ad
.sp .6
.RS 4n
プライベートクラスタトランスポートで使用するネットワークケーブル。このケーブルは、物理ケーブルを指さない場合もあり、2 つの \fB<endpoint>\fR 要素間のパスとする方が適切です。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<transportCableList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fB<endpoint>\fR
.sp
\fB<transportCable>\fR 要素は、2 つの \fB<endpoint>\fR 要素を持ちます。それぞれの \fBendpont\fR 要素では、ケーブル終端の一方を規定します。
.RE
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<state>\fR
.sp
\fB<transportCable>\fR 要素は、\fB<state>\fR 要素を 1 つだけ持つことができます。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<transportCableList>\fR\fR
.ad
.sp .6
.RS 4n
2 つのクラスタ \fB<endpoint>\fR 要素を接続するのに使用するネットワークケーブルのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<clusterTransport>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
\fBオプション:\fR
.RS +4
.TP
.ie t \(bu
.el o
\fB<transportCable>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<transportNode>\fR\fR
.ad
.sp .6
.RS 4n
プライベートクラスタトランスポートで使用されるクラスタノードの 1 つ。クラスタのノードごとに、\fB<transportNode>\fR 要素を 1 つ指定します。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<transportNodeList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<nodeRef>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBtransportAdapterList\fR
.sp
クラスタノードの名前。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<transportNodeList>\fR\fR
.ad
.sp .6
.RS 4n
プライベートクラスタトランスポートで使用するノードのリスト。このノードリストには常に、クラスタのメンバーと同じ一連のノードが含まれます。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<clusterTransport>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<transportNode>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<transportSwitch>\fR\fR
.ad
.sp .6
.RS 4n
1 つのクラスタトランスポートスイッチ。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<transportSwitchList>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<state>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBname\fR
.sp
トランスポートスイッチの名前。
.RE
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fBport\fR
.sp
スイッチ上のポートの番号。
.RE
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<transportSwitchList>\fR\fR
.ad
.sp .6
.RS 4n
プライベートクラスタトランスポートシステムによって使用されるネットワークスイッチのリスト。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<clusterTransport>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<transportSwitch>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
なし
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB<transportType>\fR\fR
.ad
.sp .6
.RS 4n
\fB<transportAdapter>\fR 要素で使用されるネットワークトランスポートのタイプ。
.sp
.ne 2
.mk
.na
\fB親:\fR
.ad
.RS 28n
.rt  
\fB<transportAdapter>\fR
.RE

.sp
.ne 2
.mk
.na
\fB子:\fR
.ad
.RS 28n
.rt  
(省略可能)
.RS +4
.TP
.ie t \(bu
.el o
\fB<propertyList>\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB属性:\fR
.ad
.RS 28n
.rt  
必須:
.RS +4
.TP
.ie t \(bu
.el o
\fBvalue\fR
.sp
\fBvalue\fR 属性には、\fBdlpi\fR または \fBrsm\fR を設定できます。
.RE
.RE

.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/xml/cluster.dtd\fR\fR
.ad
.sp .6
.RS 4n
Sun Cluster 構成用 XMLファイルの構造を定義する文書型定義 (DTD) ファイル。
.RE

.SH 関連項目
.sp
.LP
Intro(1CL)、cluster(1CL)
.sp
.LP
http://www.w3.org/XML/
