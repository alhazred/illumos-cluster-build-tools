'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_free_ext_property  3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_free_ext_property \- リソース拡張プロパティーメモリーの解放
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBvoid\fR \fBscds_free_ext_property\fR (\fBscha_ext_prop_value_t *\fR\fIproperty_value\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_free_ext_property()\fR 関数は、scds_get_ext_property(3HA) への呼び出し時に割り当てられたメモリーを再利用します。 
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIproperty_value\fR\fR
.ad
.RS 20n
.rt  
プロパティー値のポインタです。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scds_get_ext_property(3HA)、\fBattributes\fR(5)
