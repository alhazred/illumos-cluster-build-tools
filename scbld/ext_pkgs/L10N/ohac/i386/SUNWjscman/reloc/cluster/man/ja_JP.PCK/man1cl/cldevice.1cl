'\" te
.\"  Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH  cldevice   1CL  " 2008 年 8 月 20 日 " " Sun Cluster 3.2 " " Sun Cluster 保守コマンド "
.SH 名前
cldevice, cldev \- Sun Cluster デバイスの管理
.SH 形式
.LP
.nf
\fB/usr/cluster/bin/cldevice\fR \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice\fR [\fIsubcommand\fR] \fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice\fR \fIsubcommand\fR [\fIoptions\fR] \fB-v\fR [+ | \fIdevice\fR ...]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice check\fR [\fB-n\fR \fInode\fR[,\&.\|.\|.]] [+]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice clear\fR [\fB-n\fR \fInode\fR[,\&.\|.\|.]] [+]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice combine\fR \fB-t\fR \fIreplication-type\fR \fB-g\fR \fIreplication-device-group\fR \fB-d\fR \fIdestination-device\fR \fIdevice\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice export\fR [\fB-o\fR {- | \fIconfigfile\fR}] [\fB-n\fR \fInode\fR[,\&.\|.\|.]] [+ | \fIdevice \fR...]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice list\fR [\fB-n\fR \fInode\fR[,\&.\|.\|.]] [+ | \fIdevice\fR ...]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice monitor\fR [\fB-i\fR {- | \fIclconfigfile\fR} ] [\fB-n\fR \fInode\fR[,\&.\|.\|.] ] {+ | \fIdisk-device \fR...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice populate\fR 
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice refresh\fR [\fB-n\fR \fInode\fR[,\&.\|.\|.]] [+]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice rename\fR \fB-d\fR \fIdestination-device\fR \fIdevice\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice repair\fR [\fB-n\fR \fInode\fR[,\&.\|.\|.]] {+ | \fIdevice\fR ...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice replicate\fR \fB-t\fR \fIreplication-type\fR [\fB-S\fR \fIsource-node\fR] \fB-D\fR \fIdestination-node\fR [+]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice set\fR \fB-p\fR default_fencing={global | pathcount | scsi3 | nofencing | nofencing-noscrub} [\fB-n\fR \fInode\fR[,\&.\|.\|.]] \fIdevice\fR ...
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice show\fR [\fB-n\fR \fInode\fR[,\&.\|.\|.]] [+ | \fIdevice \fR...]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice status\fR [\fB-s\fR \fIstate\fR] [\fB-n \fInode\fR\fR[,\&.\|.\|.]] [+ | [\fIdisk-device \fR]]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevice unmonitor\fR [\fB-i\fR {- | \fIclconfigfile\fR} ] [\fB-n \fInode\fR[,\&.\|.\|.]\fR] {+ | \fIdisk-device\fR ...}
.fi

.SH 機能説明
.sp
.LP
\fBcldevice\fR コマンドは、Sun Cluster 環境のデバイスを管理します。このコマンドは、Sun Cluster デバイス識別子 (DID) 疑似デバイスドライバを管理し、ディスクデバイスパスを監視するのに使用します。
.RS +4
.TP
.ie t \(bu
.el o
DID ドライバは、あるデバイスへの複数のパスが利用可能である場合でも一意のデバイス ID をそのデバイスに提供します。詳細は、did(7) のマニュアルページを参照してください。
.RE
.RS +4
.TP
.ie t \(bu
.el o
ディスクパスとは、クラスタノードと物理ディスクまたは LUN ストレージデバイス間の接続のことです。ディスクパスには、Solaris カーネルドライバスタック、Host Bus Adapter、および介在する任意のケーブル、スイッチ、またはネットワーク接続形態が含まれます。
.RE
.sp
.LP
\fBcldev\fR コマンドは、\fBcldevice\fR コマンドの短縮形式です。どちらの形式のコマンドも使用できます。
.sp
.LP
\fBlist\fR および \fBshow\fR サブコマンドを除いて、\fBcldevice\fR コマンドは、オンラインかつクラスタモードであるクラスタノードから実行してください。
.sp
.LP
このコマンドの書式は次のとおりです。
.sp
.LP
\fBcldevice\fR [\fIsubcommand\fR] [\fIoptions\fR] [\fIoperands\fR]
.sp
.LP
\fIoptions\fR に \fB-?\fR または \fB-V\fR オプションを指定する場合だけは、\fIsubcommand\fR を省略できます。
.sp
.LP
このコマンドの各オプションには、長い形式と短い形式があります。各オプションの両方の形式については、このマニュアルページの「オプション」セクションを参照してください。
.sp
.LP
詳細は、Intro(1CL) のマニュアルページを参照してください。
.sp
.LP
このコマンドは、大域だけで使用できます。
.SH サブコマンド
.sp
.LP
サポートされるサブコマンドには次のものがあります。
.sp
.ne 2
.mk
.na
\fB\fBcheck\fR\fR
.ad
.sp .6
.RS 4n
デバイスの物理デバイスに対する整合性検査を、カーネル表現と比較して実行します。この整合性検査で問題が発見されると、エラーメッセージが表示されます。この処理は、すべてのデバイスが検査されるまで継続されます。
.sp
デフォルトでは、このサブコマンドは現在のノードだけに影響します。\fB-n\fR オプションは、別のノードに接続されているデバイスの検査処理を実行するのに使用します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBclear\fR\fR
.ad
.sp .6
.RS 4n
現在のノードから切断された配下のデバイスへの DID 参照をすべて削除します。
.sp
デフォルトでは、このサブコマンドは現在のノードだけに影響します。\fB-n\fR オプションは、クリア処理を実行する別のクラスタノードを指定するのに使用します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBcombine\fR\fR
.ad
.sp .6
.RS 4n
指定されたデバイスを指定された対象先に結合します。
.sp
\fBcombine\fR サブコマンドは、ソースデバイスのパスを対象先のパスに結合します。このようにパスを結合すると、DID インスタンス番号が 1 つになり、対象先の DID インスタンス番号と同じになります。DID インスタンスを SRDF と結合するには、このコマンドを使用します。
.sp
\fBcombine\fR サブコマンドは、ストレージベースの複製用に DID デバイスを手動で構成するのに使用できます。ただし、TrueCopy の複製デバイスの場合、\fBreplicate\fR サブコマンドを使用して、複製デバイスを自動的に構成します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBexport\fR\fR
.ad
.sp .6
.RS 4n
クラスタデバイスの構成情報をエクスポートします。
.sp
\fB-o\fR オプションでファイル名を指定する場合、構成情報はその新しいファイルに書き込まれます。\fB-o\fR オプションを指定しない場合、構成情報は標準出力に書き込まれます。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBlist\fR\fR
.ad
.sp .6
.RS 4n
すべてのデバイスパスを表示します。
.sp
オペランドをまったく指定しない場合、あるいは、プラス記号 (\fB+\fR) をオペランドに指定する場合、すべてのデバイスが報告されます。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBmonitor\fR\fR
.ad
.sp .6
.RS 4n
指定したディスクパスの監視をオンにします。
.sp
\fBmonitor\fR サブコマンドは、ディスクデバイスだけに機能します。このサブコマンドは、テープなどのデバイスには影響しません。
.sp
デフォルトでは、このサブコマンドはすべてのノードからのパスの監視をオンにします。
.sp
\fB-i\fR オプションは、ディスクパスの監視プロパティーを設定するクラスタ構成ファイルを指定するのに使用します。\fB-i\fR オプションは、指定されたファイルで監視対象のマークが付いているディスクパス上でディスクパスの監視を開始します。ほかのディスクパスに変更は行われません。クラスタ構成ファイルについての詳細は、clconfiguration(5CL) のマニュアルページを参照してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBpopulate\fR\fR
.ad
.sp .6
.RS 4n
広域デバイス名前空間を生成します。
.sp
広域デバイス名前空間は \fB/global\fR ディレクトリの下にマウントされます。名前空間は、物理デバイスへの論理リンクの集合から構成されます。\fB/dev/global\fR ディレクトリはクラスタ内の各ノードから見えるので、どのノードからでも個々の物理デバイスが確認できます。したがって、クラスタ内のどのノードからでも、広域デバイスの名前空間に追加されたディスク、テープ、または CD-ROM にアクセスできます。
.sp
\fBpopulate\fR サブコマンドを使用すると、管理者は、システムをリブートせずに、新しい広域デバイスを広域デバイス名前空間に接続できます。これらのデバイスは、テープドライブ、CD-ROM ドライブ、またはディスクドライブでもかまいません。
.sp
\fBpopulate\fR サブコマンドを実行する前に、\fBdevfsadm\fR(1M) コマンドを実行してください。代わりに、再構成再起動を実行して、広域デバイス名前空間を再構築し、新しい広域デバイスを接続してもかまいません。再構成再起動についての詳細は、\fBboot\fR(1M) のマニュアルページを参照してください。
.sp
\fBpopulate\fR サブコマンドは、現在のクラスタメンバーであるノードから実行してください。
.sp
\fBpopulate\fR サブコマンドはその作業をリモートノード上で非同期的に実行します。したがって、このコマンドを実行したノード上でコマンドが完了しても、すべてのクラスタノード上でこのコマンドが完了しているわけではありません。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBrefresh\fR\fR
.ad
.sp .6
.RS 4n
クラスタノード上にある現在のデバイスツリーに対してデバイス構成情報を更新します。このコマンドにより、\fBrdsk\fR および \fBrmt\fR のデバイスツリーの完全な検索が行われます。このコマンドにより、新たに認識されたデバイス識別子ごとに、新しい DID インスタンス番号が割り当てられます。また、新たに認識されたデバイスごとに、新しいパスが追加されます。
.sp
デフォルトでは、このサブコマンドは現在のノードだけに影響します。\fB-n\fR オプションは、\fBrefresh\fR サブコマンドと一緒に使用して、更新処理を実行するクラスタノードを指定します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBrename\fR\fR
.ad
.sp .6
.RS 4n
指定されたデバイスに新しい DID インスタンス番号をつけます。
.sp
このコマンドは、ソースデバイスの DID インスタンス番号に対応する DID デバイスパスを削除して、指定された宛先 DID インスタンス番号を使ってその DID デバイスパスを作成し直します。このサブコマンドは、間違って変更された DID インスタンス番号を復元するのにも使用できます。
.sp
共有ストレージに接続されているすべてのクラスタノードで \fBrename\fR サブコマンドを実行したあとに、\fBdevfsadm\fR および \fBcldevice populate\fR コマンドを実行し、広域デバイス名前空間を更新して、設定の変更を反映させます。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBrepair\fR\fR
.ad
.sp .6
.RS 4n
指定されたデバイスに対して修復手順を実行します。
.sp
デフォルトでは、このサブコマンドは現在のノードだけに影響します。\fB-n\fR オプションは、修復処理を実行するクラスタノードを指定するのに使用します。
.sp
オペランドをまったく指定しない場合、あるいは、プラス記号 (\fB+\fR) をオペランドに指定する場合、このコマンドは、現在のノードに接続されているすべてのデバイスについての構成情報を更新します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBreplicate\fR\fR
.ad
.sp .6
.RS 4n
ストレージベースの複製機能で使用する DID デバイスを構成します。
.LP
注 - 
.sp
.RS 2
\fBreplicate\fR サブコマンドは、Hitachi TrueCopy との使用に限られます。DID インスタンスと EMC SRDF を結合する方法はサポートされていません。DID インスタンスと SRDF を結合するには、\fBcldevice combine\fR を使用します。
.RE
\fBreplicate\fR サブコマンドは、ソースノード上の各 DID インスタンス番号を宛先ノード上の対応する DID インスタンス番号に結合します。複製デバイスの各ペアは、単一の論理 DID デバイスにマージされます。 
.sp
デフォルトでは、現在のノードがソースノードです。\fB-S\fR オプションは、別のソースノードを指定するのに使用します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBset\fR\fR
.ad
.sp .6
.RS 4n
指定されたデバイスのプロパティーを変更します。
.sp
\fB-p\fR オプションは、変更するプロパティーを指定するのに使用します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBshow\fR\fR
.ad
.sp .6
.RS 4n
指定されたすべてのデバイスパスの構成レポートを表示します。
.sp
このレポートには、デバイスへのパスと、そのパスが監視状態または監視解除状態のどちらにあるかが表示されます。
.sp
デフォルトでは、このサブコマンドはすべてのデバイスの構成情報を表示します。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBstatus\fR\fR
.ad
.sp .6
.RS 4n
指定されたすべてのディスクデバイスパスのステータスを表示します。
.sp
デフォルトでは、このサブコマンドはすべてのノードからのすべてのディスクパスのステータスを表示します。
.sp
\fBstatus\fR サブコマンドはディスクデバイスだけに機能します。このレポートには、テープなどのデバイスは報告されません。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBunmonitor\fR\fR
.ad
.sp .6
.RS 4n
コマンドのオペランドとして指定されたディスクパスの監視をオフにします。
.sp
デフォルトでは、このサブコマンドはすべてのノードからのパスの監視をオフにします。
.sp
\fBunmonitor\fR サブコマンドはディスクデバイスだけに機能します。このサブコマンドは、テープなどのデバイスには影響しません。
.sp
\fB-i\fR オプションは、ディスクパスの監視をオフにするクラスタ構成ファイルを指定するのに使用します。ディスクパスの監視がオフになるのは、指定されたファイルで監視解除のマークが付いているディスクパスです。ほかのディスクパスに変更は行われません。詳細は、clconfiguration(5CL) のマニュアルページを参照してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.SH オプション
.sp
.LP
次のオプションがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB\fB--help\fR\fR
.ad
.sp .6
.RS 4n
ヘルプ情報を表示します。
.sp
このオプションは単独でもサブコマンド付きでも使用できます。
.RS +4
.TP
.ie t \(bu
.el o
このオプションを単独で使用する場合、利用可能なサブコマンドのリストが出力されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
このオプションをサブコマンドを付けて使用する場合、そのサブコマンドの使用法オプションが出力されます。
.RE
このオプションを使用する場合、ほかの処理は実行されません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-D\fR \fIdestination-node\fR\fR
.ad
.br
.na
\fB\fB--destinationnode=\fR\fIdestination-node\fR\fR
.ad
.br
.na
\fB\fB--destinationnode \fR\fIdestination-node\fR\fR
.ad
.sp .6
.RS 4n
デバイスを複製する複製先ノードを指定します。ノードは、ノード名またはノード ID のどちらででも指定できます。
.sp
\fB-D\fR オプションは、\fBreplicate\fR サブコマンドだけで有効です。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-d\fR \fIdestination-device\fR\fR
.ad
.br
.na
\fB\fB--device=\fR\fIdestination-device\fR\fR
.ad
.br
.na
\fB\fB--device \fR\fIdestination-device\fR\fR
.ad
.sp .6
.RS 4n
ストレージベースの複製を行うために、複製先デバイスの DID インスタンス番号を指定します。
.sp
DID インスタンス番号は \fB-d\fR オプションだけで使用します。ほかの形式の DID 名や UNIX のフルパス名は、対象先を指定するのには使用しません。
.sp
\fB-d\fR オプションは、\fBrename\fR および \fBcombine\fR サブコマンドだけで有効です。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-g\fR \fIreplication-device-group\fR\fR
.ad
.sp .6
.RS 4n
複製デバイスグループを指定します。このオプションは、\fBcombine\fR サブコマンドとのみ使用できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-i\fR {- | \fIclconfigfile\fR}\fR
.ad
.br
.na
\fB\fB--input={- | \fR\fIclconfigfile\fR\fB-}\fR\fR
.ad
.br
.na
\fB\fB--input {- | \fR\fIclconfigfile\fR\fB-}\fR\fR
.ad
.sp .6
.RS 4n
監視するディスクパスまたは監視しないディスクパスで使用される構成情報を指定します。この情報は clconfiguration(5CL) のマニュアルページに定義されている書式に準拠させてください。この情報は、ファイルに含めることも、標準入力を介して指定することもできます。標準入力を指定するには、ファイル名の代わりに、マイナス記号 (-) を指定します。
.sp
\fB-i\fR オプションは、\fBmonitor\fR および \fBunmonitor\fR サブコマンドだけで有効です。
.sp
コマンドで指定するオプションは、構成ファイルで設定されている任意のオプションより優先されます。構成パラメータがクラスタ構成ファイルに存在しない場合、これらのパラメータをコマンド行で指定してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-n\fR \fInode\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--node=\fR\fInode\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--node \fR\fInode\fR[,\&.\|.\|.]\fR
.ad
.sp .6
.RS 4n
サブコマンドが、\fB-n\fR オプションで指定されるノードからのディスクパスだけを含むことを指定します。ノードは、ノード名またはノード ID のどちらででも指定できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-o\fR {- | \fIconfigfile\fR}\fR
.ad
.br
.na
\fB\fB--output={- | \fR\fIconfigfile\fR\fB-}\fR\fR
.ad
.br
.na
\fB\fB--output {- | \fR\fIconfigfile\fR\fB-}\fR\fR
.ad
.sp .6
.RS 4n
clconfiguration(5CL) のマニュアルページで定義されている形式で、ディスクパス構成情報を記述します。この情報は、ファイルまたは標準出力のどちらにでも書き込むことができます。
.sp
\fB-o\fR オプションは、\fBexport\fR サブコマンドだけで有効です。
.sp
このオプションの引数としてファイル名を指定する場合、このコマンドは新しいファイルを作成して、そのファイルに構成情報を出力します。同じ名前のファイルがすでにある場合、このコマンドはエラーで終了します。既存のファイルに変更は行われません。
.sp
このオプションの引数としてマイナス記号 (-) を指定する場合、このコマンドは標準出力に構成情報を表示します。このコマンドのほかの標準出力はすべて抑制されます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-p default_fencing={global | pathcount | scsi3 | nofencing | nofencing-noscrub}\fR\fR
.ad
.br
.na
\fB-\fB-property=default_fencing={global|pathcount|scsi3|nofencing|nofencing-noscrub}\fR\fR
.ad
.br
.na
\fB-\fB-property\ default_fencing={global|pathcount|scsi3|nofencing|nofencing-noscrub}\fR\fR
.ad
.sp .6
.RS 4n
変更するプロパティーを指定します。
.sp
このオプションは \fBset\fR サブコマンドと一緒に使用して、次のプロパティーを変更します。
.sp
.ne 2
.mk
.na
\fB\fBdefault_fencing\fR\fR
.ad
.sp .6
.RS 4n
指定したデバイスについて、グローバルなデフォルトのフェンシングアルゴリズムを変更します。定足数デバイスとして構成されているデバイスのデフォルトのフェンシングアルゴリズムは変更できません。
.sp
デバイスのデフォルトのフェンシングアルゴリズムは、次の値のいずれかに設定できます。
.sp
.ne 2
.mk
.na
\fB\fBglobal\fR\fR
.ad
.sp .6
.RS 4n
グローバルなデフォルトのフェンシング設定を使用します。フェンシングのグローバルデフォルトの設定については、cluster(1CL) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBnofencing\fR\fR
.ad
.sp .6
.RS 4n
Persistent Group Reservation (PGR) キーをチェックし、いずれかのキーを削除したあとで、指定されたデバイスのフェンシングをオフにします。
.LP
注意 - 
.sp
.RS 2
Serial Advanced Technology Attachment (SATA) ディスクのような、SCSI をサポートしていないディスクを使用している場合は、フェンシングをオフにします。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fBnofencing-noscrub\fR\fR
.ad
.sp .6
.RS 4n
最初に PGR キーをチェックまたは削除せずに、指定されたデバイスのフェンシングをオフにします。\fB\fR
.LP
注意 - 
.sp
.RS 2
Serial Advanced Technology Attachment (SATA) ディスクのような、SCSI をサポートしていないディスクを使用している場合は、フェンシングをオフにします。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fBpathcount\fR\fR
.ad
.sp .6
.RS 4n
共有デバイスに接続されている DID パスの数でフェンシングプロトコルを決定します。
.RS +4
.TP
.ie t \(bu
.el o
使用する DID パスが 3 未満のデバイスには、このコマンドは SCSI-2 プロトコルを設定します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
使用する DID パスが 3 以上のデバイスには、このコマンドは SCSI-3 プロトコルを設定します。
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fBscsi3\fR\fR
.ad
.sp .6
.RS 4n
SCSI-3 プロトコルを設定します。そのデバイスが SCSI-3 プロトコルをサポートしない場合、フェンシングプロトコルの設定は変更されません。
.RE

.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-S\fR \fIsource-node\fR\fR
.ad
.br
.na
\fB\fB--sourcenode=\fR\fIsource-node\fR\fR
.ad
.br
.na
\fB\fB--sourcenode \fR\fIsource-node\fR\fR
.ad
.sp .6
.RS 4n
デバイスを複製先ノードに複製する複製元ノードを指定します。ノードは、ノード名またはノード ID のどちらででも指定できます。
.sp
\fB-S\fR オプションは、\fBreplicate\fR サブコマンドだけで有効です。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-s\fR \fIstate\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--state=\fR\fIstate\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--state \fR\fIstate\fR[,\&.\|.\|.]\fR
.ad
.sp .6
.RS 4n
指定した状態のディスクパスの状態情報を表示します。
.sp
\fB-s\fR オプションは、\fBstatus\fR サブコマンドだけで有効です。\fB-s\fR オプションを指定する場合、出力されるステータスは指定した \fIstate\fR にあるディスクパスだけに制限されます。次に、\fIstate\fR に可能な値を示します。
.RS +4
.TP
.ie t \(bu
.el o
\fBfail\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBok\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBunknown\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBunmonitored\fR
.RE
.RE

.sp
.ne 2
.mk
.na
\fB\fB-t\fR\fR
.ad
.sp .6
.RS 4n
複製デバイスタイプを指定します。このオプションは、\fBreplicate\fR および \fBcombine\fR サブコマンドとともに使用できます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB\fB--version\fR\fR
.ad
.sp .6
.RS 4n
コマンドのバージョンを表示します。
.sp
このオプションには、サブコマンドやオペランドなどのオプションは指定しないでください。サブコマンド、オペランド、またはほかのオプションは無視されます。\fB-V\fR オプションは、コマンドのバージョンを表示するだけです。その他の処理は行いません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR\fR
.ad
.br
.na
\fB\fB--verbose\fR\fR
.ad
.sp .6
.RS 4n
詳細な情報を標準出力に表示します。
.sp
このオプションは、このコマンドの任意の形式に指定できます。
.RE

.SH オペランド
.sp
.LP
次のオペランドがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fIdevice\fR\fR
.ad
.sp .6
.RS 4n
デバイスの名前を指定します。指定できるデバイスは、ディスク、テープ、および CD-ROM ですが、これらだけに制限されるわけではありません。
.sp
サブコマンドが複数のデバイスを受け入れる場合、プラス記号 (\fB+\fR) を使用すると、すべてのデバイスを指定できます。
.sp
\fBcldevice\fR コマンドのサブコマンドはすべて、\fBrepair\fR サブコマンドを除き、デバイスパスをオペランドとして受け付けます。\fBrepair\fR サブコマンドは、オペランドとしてデバイス名だけを受け付けます。\fIdevice\fR 名には、フルグローバルパス名、デバイス名、または DID インスタンス番号のいずれかを指定できます。これらの形式のデバイス名の例は、それぞれ、\fB/dev/did/dsk/d3\fR、\fBd3\fR、および \fB3\fR です。詳細は、did(7) のマニュアルページを参照してください。
.sp
デバイス名はまた、\fB/dev/rdsk/c0t0d0s0\fR のような UNIX のフルパス名でもかまいません。
.sp
指定されたデバイスには、複数のノードからそのデバイスへのパスが複数存在する可能性もあります。\fB-n\fR オプションを使用しない場合、すべてのノードから指定されたデバイスへのパスがすべて選択されます。
.sp
\fBmonitor\fR、\fBunmonitor\fR、および \fBstatus\fR サブコマンドは、ディスクデバイスをオペランドとして受け付けます。
.RE

.SH 終了ステータス
.sp
.LP
このコマンドセットにあるすべてのコマンドの終了ステータスコードの完全なリストについては、Intro(1CL) のマニュアルページを参照してください。
.sp
.LP
指定したすべてのオペランドでコマンドが成功すると、コマンドはゼロ (\fBCL_NOERR\fR) を返します。あるオペランドでエラーが発生すると、コマンドはオペランドリストの次のオペランドを処理します。戻り値は常に、最初に発生したエラーを反映します。
.sp
.LP
このコマンドは、次の終了ステータスコードを返します。
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fBCL_NOERR\fR\fR
.ad
.sp .6
.RS 4n
エラーなし
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR \fBCL_ENOMEM\fR\fR
.ad
.sp .6
.RS 4n
十分なスワップ空間がありません
.RE

.sp
.ne 2
.mk
.na
\fB\fB3\fR \fBCL_EINVAL\fR\fR
.ad
.sp .6
.RS 4n
無効な引数
.RE

.sp
.ne 2
.mk
.na
\fB\fB6\fR \fBCL_EACCESS\fR\fR
.ad
.sp .6
.RS 4n
アクセス権がありません
.RE

.sp
.ne 2
.mk
.na
\fB\fB9\fR \fBCL_ESTATE\fR\fR
.ad
.sp .6
.RS 4n
オブジェクトの状態が不正です
.RE

.sp
.ne 2
.mk
.na
\fB\fB15\fR\fBCL_EPROP\fR\fR
.ad
.sp .6
.RS 4n
不正なプロパティーです
.RE

.sp
.ne 2
.mk
.na
\fB\fB35\fR \fBCL_EIO\fR\fR
.ad
.sp .6
.RS 4n
I/O error
.RE

.sp
.ne 2
.mk
.na
\fB\fB36\fR \fBCL_ENOENT\fR\fR
.ad
.sp .6
.RS 4n
そのようなオブジェクトはありません
.RE

.sp
.ne 2
.mk
.na
\fB\fB37\fR \fBCL_EOP\fR\fR
.ad
.sp .6
.RS 4n
許可されていない処理
.RE

.SH 使用例
.LP
\fB例 1 \fRクラスタにあるすべてのディスクパスの監視
.sp
.LP
次の例では、クラスタインフラストラクチャーにあるすべてのディスクパスの監視を有効にする方法を示します。

.sp
.in +2
.nf
# \fBcldevice monitor +\fR
.fi
.in -2
.sp

.LP
\fB例 2 \fR単一のディスクパスの監視
.sp
.LP
次の例では、ディスク \fB/dev/did/dsk/d3\fR へのパスが有効であるすべてのノードで、このパスの監視を有効にする方法を示します。

.sp
.in +2
.nf
# \fBcldevice monitor /dev/did/dsk/d3\fR
.fi
.in -2
.sp

.LP
\fB例 3 \fR単一ノードのディスクパスを監視
.sp
.LP
次の例では、ノード \fBphys-schost-2\fR でディスク \fB/dev/did/dsk/d4\fR と \fB/dev/did/dsk/d5\fR へのパスの監視を有効にする方法を示します。

.sp
.LP
最初の例では、\fB-n\fR オプションを使用して、監視するディスクパスをノード \fBphys-schost-2\fR に接続されているディスクパスに制限し、さらに、監視するデバイスを指定したデバイス \fBd4\fR と \fBd5\fR に制限しています。

.sp
.in +2
.nf
# \fBcldevice monitor -n phys-schost-2 d4 d5\fR
.fi
.in -2
.sp

.sp
.LP
2 番目の例では、監視するディスクパスを \fInode\fR:\fIdevice\fR 名、\fBphys-schost-2:d4\fR、および \fBphys-schost-2:d5\fR で指定しています。

.sp
.in +2
.nf
# \fBcldevice monitor phys-schost-2:d4 phys-schost-2:d5\fR
.fi
.in -2
.sp

.LP
\fB例 4 \fRすべてのディスクパスとそのステータスの出力
.sp
.LP
次の例では、クラスタのすべてのディスクパスとそのステータスを出力する方法を示します。

.sp
.in +2
.nf
# \fBcldevice status\fR
Device Instance             Node                Status
---------------             ----                ------
/dev/did/rdsk/d1            phys-schost-2       Unmonitored

/dev/did/rdsk/d2            phys-schost-2       Unmonitored

/dev/did/rdsk/d3            phys-schost-1       Ok
                           phys-schost-2       Ok

/dev/did/rdsk/d4            phys-schost-1       Ok
                           phys-schost-2       Ok

/dev/did/rdsk/d5            phys-schost-1       Unmonitored
.fi
.in -2
.sp

.LP
\fB例 5 \fRステータスが \fBfail\fR であるすべてのディスクパスの出力
.sp
.LP
次の例では、ノード \fBphys-schost-2\fR 上で監視されており、ステータスが \fBfail\fR であるすべてのディスクパスを出力する方法を示します。

.sp
.in +2
.nf
# \fBcldevice status -s fail -n phys-schost-1\fR
Device Instance             Node                Status
---------------             ----                ------
/dev/did/rdsk/d3            phys-schost-1       Fail

/dev/did/rdsk/d4            phys-schost-1       Fail
.fi
.in -2
.sp

.LP
\fB例 6 \fR単一ノードからのすべてのディスクパスのステータスの出力
.sp
.LP
次の例では、ノード \fBphys-schost-2\fR 上でオンラインであるすべてのディスクパスのパスとステータスを出力する方法を示します。

.sp
.in +2
.nf
# \fBcldevice status -n phys-schost-1\fR
Device Instance             Node                Status
---------------             ----                ------
/dev/did/rdsk/d3            phys-schost-1       Ok

/dev/did/rdsk/d4            phys-schost-1       Ok

/dev/did/rdsk/d5            phys-schost-1       Unmonitored
.fi
.in -2
.sp

.LP
\fB例 7 \fR新しいデバイスのデバイス構成データベースへの追加
.sp
.LP
次の例では、このコマンドを実行したノード \fBphys-schost-2\fR の現在のデバイス構成で、CCR データベースを更新する方法を示します。このコマンドは、クラスタのほかのノードに接続されているデバイスのデータベースは更新しません。

.sp
.in +2
.nf
phys-schost-2# \fBcldevice refresh\fR
.fi
.in -2
.sp

.LP
\fB例 8 \fR単一 DID でのデバイスの結合
.sp
.LP
次の例では、あるデバイスのパスを別のデバイスのパスと結合する方法を示します。このようにパスを結合すると、DID インスタンス番号が 1 つになり、対象先の DID インスタンス番号と同じになります。

.sp
.in +2
.nf
# \fBcldevice combine -t srdf -g devgrp1 -d 20 30\fR
.fi
.in -2
.sp

.LP
\fB例 9 \fRデバイスインスタンスのデバイスパスのリストの表示
.sp
.LP
次の例では、DID ドライバのインスタンス \fB3\fR に対応するすべてのデバイスのパスのリストを表示する方法を示します。

.sp
.in +2
.nf
# \fBcldevice list 3\fR
d3 
.fi
.in -2
.sp

.LP
\fB例 10 \fRクラスタのすべてのデバイスパスのリストの表示
.sp
.LP
次の例では、任意のクラスタノードに接続されているすべてのデバイスのすべてのデバイスパスのリストを表示する方法を示します。

.sp
.in +2
.nf
# \fBcldevice list -v\fR
DID Device          Full Device Path
----------          ----------------
d1                  phys-schost-1:/dev/rdsk/c0t0d0
d2                  phys-schost-1:/dev/rdsk/c0t1d0
d3                  phys-schost-1:/dev/rdsk/c1t8d0
d3                  phys-schost-2:/dev/rdsk/c1t8d0
d4                  phys-schost-1:/dev/rdsk/c1t9d0
d4                  phys-schost-2:/dev/rdsk/c1t9d0
d5                  phys-schost-1:/dev/rdsk/c1t10d0
d5                  phys-schost-2:/dev/rdsk/c1t10d0
d6                  phys-schost-1:/dev/rdsk/c1t11d0
d6                  phys-schost-2:/dev/rdsk/c1t11d0
d7                  phys-schost-2:/dev/rdsk/c0t0d0
d8                  phys-schost-2:/dev/rdsk/c0t1d0
.fi
.in -2
.sp

.LP
\fB例 11 \fRデバイスに関する構成情報の表示
.sp
.LP
次の例では、デバイス \fBc4t8d0\fR に関する構成情報を表示する方法を示します。

.sp
.in +2
.nf
# \fBcldevice show /dev/rdsk/c4t8d0\fR

=== DID Device Instances ===

DID Device Name:                                /dev/did/rdsk/d3
 Full Device Path:                               phys-schost1:/dev/rdsk/c4t8d0
 Full Device Path:                               phys-schost2:/dev/rdsk/c4t8d0
 Replication:                                    none
 default_fencing:                                nofencing
.fi
.in -2
.sp

.LP
\fB例 12 \fRストレージベースの複製機能で使用するためのデバイスの構成
.sp
.LP
次の例では、ストレージベースの複製機能で使用するために、DID デバイスを構成します。このコマンドは、複製されたデバイスが構成されているソースノードから実行します。複製元ノードの各 DID インスタンス番号が、複製先ノード \fBphys-schost-1\fR 上の対応する DID インスタンス番号と結合されます。

.sp
.in +2
.nf
# \fBcldevice replicate -t truecopy -D phys-schost-1\fR
.fi
.in -2
.sp

.LP
\fB例 13 \fR単一デバイスの SCSI プロトコルの設定
.sp
.LP
次の例では、デバイス \fB11\fR (インスタンス番号で指定) を SCSI-3 プロトコルに設定します。このデバイスは、構成された定足数デバイスではありません。

.sp
.in +2
.nf
# \fBcldevice set -p default_fencing=scsi3 11\fR
.fi
.in -2
.sp

.LP
\fB例 14 \fR最初に PGR キーをチェックしないでデバイスのフェンシングをオフにする
.sp
.LP
次の例では、ノード \fBphys-schost-2\fR 上のディスク \fB/dev/did/dsk/d5\fR のフェンシングをオフにします。このコマンドは、最初に Persistent Group Reservation (PGR) キーをチェックまたは削除せずに、フェンシングをオフにします。\fB\fR

.sp
.in +2
.nf
# \fBcldevice set -p default_fencing=nofencing-noscrub -n phys-schost-2 d5\fR
.fi
.in -2
.sp

.sp
.LP
Serial Advanced Technology Attachment (SATA) ディスクのような、SCSI をサポートしていないディスクを使用している場合は、フェンシングをオフにします。

.LP
\fB例 15 \fR2 ノードクラスタ \fBphys-schost\fR ですべてのデバイスのフェンシングをオフにする
.sp
.LP
次の例では、\fBphys-schost\fR という名前の 2 ノードクラスタですべてのデバイスのフェンシングをオフにします。

.sp
.in +2
.nf
# \fBcluster set -p global_fencing=nofencing\fR
# \fBcldevice set -p default_fencing=global -n phys-schost-1,phys-schost-2 d5\fR
.fi
.in -2
.sp

.sp
.LP
\fBcluster\fR コマンドと \fBglobal_fencing\fR プロパティ?についての詳細は、cluster(1CL) のマニュアルページを参照してください。

.sp
.LP
Serial Advanced Technology Attachment(SATA) ディスクのような、SCSI をサポートしていないディスクを使用している場合は、フェンシングをオフにします。

.LP
\fB例 16 \fRデバイス名による修復手順の実行
.sp
.LP
次の例では、デバイス \fB/dev/dsk/c1t4d0\fR に関連付けられていたデバイス識別子で修復手順を実行する方法を示します。このデバイスは新しいデバイスに置き換えられ、今では新しいデバイス識別子が関連付けられています。データベース中では、\fBrepair\fR サブコマンドは、このインスタンス番号が現在新しいデバイス識別子に対応していることを記録しています。

.sp
.in +2
.nf
# \fBcldevice repair c1t4d0\fR
.fi
.in -2
.sp

.LP
\fB例 17 \fRインスタンス番号による修復手順の実行
.sp
.LP
次の例では、デバイス識別子で修復手順を実行する代替方法を示します。この例では、置き換えられるデバイスへのデバイスパスに関連付けられているインスタンス番号を指定しています。置き換えられたデバイスのインスタンス番号は \fB2\fR です。

.sp
.in +2
.nf
# \fBcldevice repair 2\fR
.fi
.in -2
.sp

.LP
\fB例 18 \fR広域デバイス名前空間の生成
.sp
.LP
次の例では、新しい広域デバイスを追加したあと、 または DID デバイスを新しいインスタンス番号に移動したあとに、広域デバイスの名前空間を生成する方法を示します。

.sp
.in +2
.nf
# \fBdevfsadm\fR
# \fBcldevice populate\fR
.fi
.in -2
.sp

.LP
\fB例 19 \fRDID デバイスの移動
.sp
.LP
次の例では、移動元インスタンス \fB15\fR の DID インスタンスを新しい DID インスタンス \fB10\fR に移動し、広域デバイス名前空間を更新して、構成の変更を反映させます。

.sp
.in +2
.nf
# \fBcldevice rename 15:10\fR
# \fBdevfsadm\fR
# \fBcldevice populate\fR
.fi
.in -2
.sp

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWsczu
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
Intro(1CL)、cluster(1CL)、\fBboot\fR(1M)、\fBdevfsadm\fR(1M)、clconfiguration(5CL)、\fBrbac\fR(5)、did(7)
.SH 注意事項
.sp
.LP
スーパーユーザーはこのコマンドのすべての形式を実行できます。
.sp
.LP
任意のユーザーは次のオプションを指定してこのコマンドを実行できます。
.RS +4
.TP
.ie t \(bu
.el o
\fB-?\fR (ヘルプ) オプション
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-V\fR (バージョン) オプション
.RE
.sp
.LP
スーパーユーザー以外のユーザーがほかのサブコマンドを指定してこのコマンドを実行するには、RBAC の承認が必要です。次の表を参照してください。
.sp

.sp
.TS
tab() box;
cw(0i) |cw(5.5i) 
lw(0i) |lw(5.5i) 
.
サブコマンドRBAC の承認
_
\fBcheck\fR\fBsolaris.cluster.read\fR
_
\fBclear\fR\fBsolaris.cluster.modify\fR
_
\fBcombine\fR\fBsolaris.cluster.modify\fR
_
\fBexport\fR\fBsolaris.cluster.read\fR
_
\fBlist\fR\fBsolaris.cluster.read\fR
_
\fBmonitor\fR\fBsolaris.cluster.modify\fR
_
\fBpopulate\fR\fBsolaris.cluster.modify\fR
_
\fBrefresh\fR\fBsolaris.cluster.modify\fR
_
\fBrename\fR\fBsolaris.cluster.modify\fR
_
\fBrepair\fR\fBsolaris.cluster.modify\fR
_
\fBreplicate\fR\fBsolaris.cluster.modify\fR
_
\fBset\fR\fBsolaris.cluster.modify\fR
_
\fBshow\fR\fBsolaris.cluster.read\fR
_
\fBstatus\fR\fBsolaris.cluster.read\fR
_
\fBunmonitor\fR\fBsolaris.cluster.modify\fR
.TE

.sp
.LP
ディスクパスのステータスの変化をログに記録するには、\fBsyslogd\fR コマンドを使用します。
.sp
.LP
マルチポート式のテープドライブまたは CD-ROM ドライブはそれぞれ、個々の物理接続をするごとにネームスペースに表示されます。
