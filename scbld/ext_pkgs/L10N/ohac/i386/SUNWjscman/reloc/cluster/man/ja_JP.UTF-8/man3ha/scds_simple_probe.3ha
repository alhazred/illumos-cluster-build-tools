'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_simple_probe 3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_simple_probe \- アプリケーションとの TCP 接続を確立し、終了することによる検証
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fIfile\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_simple_probe\fR(\fBscds_handle_t\fR \fIhandle\fR, \fBconst char *\fR\fIhostname\fR, \fBint\fR \fIport\fR, \fBtime_t\fR \fItimeout\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_simple_probe()\fR 関数は、\fBconnect\fR(3SOCKET) および \fB close\fR(2) のラッパー関数です。タイムアウト時間内に実行されます。
.sp
.LP
scds_get_rg_hostnames(3HA) または scds_get_rs_hostnames(3HA) のいずれかを使用して \fIhostname\fR を取得します。
.sp
.LP
この関数の代わりに scds_simple_net_probe(3HA) の使用を検討します。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIhostname\fR\fR
.ad
.RS 20n
.rt  
接続先のマシンのインターネットホスト名です。
.RE

.sp
.ne 2
.mk
.na
\fB\fIport\fR\fR
.ad
.RS 20n
.rt  
接続を行うポート番号です。
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
正常に接続が完了するまで待機するタイムアウト値 (秒) です。
.RE

.SH 戻り値
.sp
.LP
\fBscds_simple_probe()\fR 関数の戻り値は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 28n
.rt  
関数が正常に終了。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_TIMEOUT\fR\fR
.ad
.RS 28n
.rt  
関数がタイムアウト。
.RE

.sp
.LP
その他のエラーコードについては、scha_calls(3HA)を参照してください。
.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能\fBSUNWscdev\fR
_
インタフェースの安定性非推奨
.TE

.SH 関連項目
.sp
.LP
\fB close\fR(2)、\fB connect\fR(3SOCKET)、scds_fm_net_connect(3HA)、scds_fm_net_disconnect(3HA)、scds_get_rg_hostnames(3HA)、scds_get_rs_hostnames(3HA)、scds_initialize(3HA)、scds_simple_net_probe(3HA)、scha_calls(3HA)、\fB attributes\fR(5)
