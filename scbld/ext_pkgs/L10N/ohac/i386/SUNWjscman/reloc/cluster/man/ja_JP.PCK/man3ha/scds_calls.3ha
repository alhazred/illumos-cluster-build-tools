'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_calls 3HA "2008 年 8 月 15 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_calls \- Sun Cluster Data Services Development Library (DSDL) 関数
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fI file\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>
.fi

.SH 機能説明
.sp
.LP
Data Services Development Library (DSDL) は、\fBscha\fR ライブラリ関数の機能をカプセル化および拡張する、上位のライブラリ関数セットです。\fBscha\fR ライブラリ関数については、scha_calls(3HA) のマニュアルページを参照してください。
.sp
.LP
DSDL 関数は \fBlibdsdev.so\fR ライブラリで実装されています。
.sp
.LP
DSDL 関数は、一般に次のカテゴリに分類されます。
.RS +4
.TP
.ie t \(bu
.el o
汎用関数
.sp
.LP
汎用関数には、初期化関数、取得関数、フェイルオーバーおよび再起動関数、そして実行関数が含まれます。これらの関数では、次の操作を行うことができます。
.RS +4
.TP
.ie t \(bu
.el o
DSDL 環境を初期化します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
リソースタイプ、リソース、リソースグループの名前と、拡張プロパティーの値を取得します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
リソースグループをフェイルオーバーおよび再起動し、リソースを再起動します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
エラー文字列をエラーメッセージに変換します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
タイムアウトを適用してコマンドを実行します。
.RE
.RE
.RS +4
.TP
.ie t \(bu
.el o
プロパティー関数
.sp
これらの関数は、関連するリソース、リソースタイプ、およびリソースグループに固有のプロパティー (よく使用される一部の拡張プロパティーも含む) にアクセスするために使用する API を提供します。DSDL は、\fBscds_initialize()\fR 関数を使用してコマンド行引数を解析します。関連するリソース、リソースタイプ、およびリソースグループのさまざまなプロパティーをキャッシュに書き込みます。\fB\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
ネットワークリソースアクセス関数
.sp
これらの関数は、リソースおよびリソースグループによって使用されるネットワーク資源を管理します。これらの関数は、ホスト名、ポートリスト、およびネットワークアドレスを処理し、TCP ベースの監視を有効にします。
.RE
.RS +4
.TP
.ie t \(bu
.el o
Process Monitor Facility (PMF) 関数
.sp
これらの関数は、Process Monitor Facility (PMF) の機能をカプセル化しています。
.RE
.RS +4
.TP
.ie t \(bu
.el o
障害監視関数
.sp
これらの関数は、障害履歴を保持し、その履歴を \fBRetry_count\fR および \fBRetry_interval\fR プロパティーと関連付けて評価することにより、障害監視の事前定義モデルを提供します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
ユーティリティー関数
.sp
これらの関数は、メッセージやデバッグ用メッセージをシステムログに書き込みます。
.RE
.SS "初期化関数"
.sp
.LP
次の関数は、呼び出しメソッドを初期化します。
.RS +4
.TP
.ie t \(bu
.el o
scds_initialize(3HA) - リソースを割り当て、DSDL 環境を初期化します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_close(3HA) - \fBscds_initialize()\fR 関数によって割り当てられたリソースを解放します。
.RE
.SS "取得関数"
.sp
.LP
次の関数は、ゾーン、リソースタイプ、リソース、リソースグループ、および拡張プロパティーについての情報を取得します。
.RS +4
.TP
.ie t \(bu
.el o
scds_get_zone_name(3HA) - 自身の代わりにメソッドが実行されているゾーンの名前を取得します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_get_resource_type_name(3HA) - 呼び出しプログラム用のリソースタイプ名を取得します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_get_resource_name(3HA) - 呼び出しプログラム用のリソース名を取得します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_get_resource_group_name(3HA) - 呼び出しプログラム用のリソースグループ名を取得します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_get_ext_property(3HA) - 指定した拡張プロパティーの値を取得します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_get_current_method_name(3HA) - 呼び出されたデータサービスメソッドによるパス名の最終要素を取得します。\fBbasename\fR(3C) のマニュアルページを参照してください。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_free_ext_property(3HA) - \fBscds_get_ext_property()\fR によって割り当てられたメモリーを解放します。
.RE
.sp
.LP
次の関数は、リソースによって使用される \fBSUNW.HAStoragePlus\fR リソースの状態情報を取得します。
.sp
.LP
scds_hasp_check(3HA) - リソースにより使用される \fBSUNW.HAStoragePlus\fR リソースに関する状態情報を取得します。当該リソース用に定義されている \fBResource_dependencies\fR または \fBResource_dependencies_weak\fR のシステム属性を使用することによって、当該リソースが依存しているすべての \fBSUNW.HAStoragePlus\fR リソース状態 (オンラインであるか、オンラインでないか) についての情報が得られます。詳細は、SUNW.HAStoragePlus(5) のマニュアルページを参照してください。
.SS "フェイルオーバー関数と再起動関数"
.sp
.LP
次の関数は、リソースまたはリソースグループをフェイルオーバーまたは再起動します。
.RS +4
.TP
.ie t \(bu
.el o
scds_failover_rg(3HA) - リソースグループをフェイルオーバーします。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_restart_rg(3HA) - リソースグループを再起動します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_restart_resource(3HA) - リソースを再起動します。
.RE
.SS "実行関数"
.sp
.LP
次の関数は、タイムアウトを適用してコマンドを実行し、エラーコードをエラーメッセージに変換します。
.RS +4
.TP
.ie t \(bu
.el o
scds_timerun(3HA) - タイムアウト値を適用してコマンドを実行します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_error_string(3HA) および scds_error_string_i18n(3HA) - エラーコードをエラー文字列に変換します。\fBscds_error_string()\fR から返された文字列は、英語で表示されます。\fBscds_error_string_i18n()\fR から返された文字列は、\fBLC_MESSAGES\fR ロケールカテゴリで指定されているその国および地域の言語で表示されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_svc_wait(3HA) - 指定のタイムアウト時間が経過するまで、監視対象のプロセスグループの終了を待機します。
.RE
.SS "プロパティー関数"
.sp
.LP
このカテゴリの関数は、関連するリソースタイプ、リソース、およびリソースグループ (よく使用される一部の拡張プロパティーも含む) にアクセスするために使用する API を提供します。DSDL は、\fBscds_initialize()\fR 関数を使用してコマンド行引数を解析します。関連するリソース、リソースタイプ、およびリソースグループのさまざまなプロパティーをキャッシュに書き込みます。\fB\fR
.sp
.LP
次の関数を始めとするこれらの関数の説明は、scds_property_functions(3HA) のマニュアルページにあります。
.RS +4
.TP
.ie t \(bu
.el o
\fBscds_get_ext_\fR\fIproperty-name\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBscds_get_rg_\fR\fIproperty-name\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBscds_get_rs_\fR\fIproperty-name\fR
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBscds_get_rt_\fR\fIproperty-name\fR
.RE
.SS "ネットワークリソースアクセス関数"
.sp
.LP
これらの関数を使用して、ネットワーク資源を管理します。
.sp
.LP
次の関数はホスト名を扱います。
.RS +4
.TP
.ie t \(bu
.el o
scds_get_rs_hostnames(3HA) - リソースによって使用されているホスト名のリストを取得します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_get_rg_hostnames(3HA) - リソースグループ内のネットワーク資源によって使用されているホスト名のリストを取得します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_print_net_list(3HA) - ホスト名リストの内容を \fBsyslog\fR(3C) に書き込みます。通常、この関数はデバッグに使用します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_free_net_list(3HA) - \fBscds_get_rs_hostnames()\fR または \fBscds_get_rg_hostnames()\fR によって割り当てられたメモリーを解放します。
.RE
.sp
.LP
次の関数はポートリストを扱います。
.RS +4
.TP
.ie t \(bu
.el o
scds_get_port_list(3HA) - リソースによって使用されているポート/プロトコルのペアのリストを取得します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_print_port_list(3HA) - ポート/プロトコルのリストの内容を \fBsyslog\fR(3C) に書き込みます。通常、この関数はデバッグに使用します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_free_port_list(3HA) - \fBscds_get_port_list()\fR によって割り当てられたメモリーを解放します。
.RE
.sp
.LP
次の関数はネットワークアドレスを扱います。
.RS +4
.TP
.ie t \(bu
.el o
scds_get_netaddr_list(3HA) - リソースによって使用されているネットワークアドレスのリストを取得します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_print_netaddr_list(3HA) - ネットワークアドレスのリストの内容を \fBsyslog\fR(3C) に書き込みます。通常、この関数はデバッグに使用します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_free_netaddr_list(3HA) - \fBscds_get_netaddr_list()\fR によって割り当てられたメモリーを解放します。
.RE
.sp
.LP
次のカテゴリの関数は、TCP ベースの監視を有効にします。通常、障害モニターはこれらの関数を使用して、サービスとの単純ソケット接続を確立し、サービスのデータを読み書きしてサービスの状態を確認したあと、サービスとの接続を切断します。
.sp
.LP
この関数セットには、次の関数が含まれます。
.RS +4
.TP
.ie t \(bu
.el o
scds_fm_tcp_connect(3HA) - IPv4 アドレッシングだけを使用するプロセスへの TCP 接続を確立します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_fm_net_connect(3HA) - IPv4 か IPv6 アドレッシングのどちらかを使用するプロセスへの TCP 接続を確立します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_fm_tcp_read(3HA) - TCP 接続を使って、監視されているプロセスからデータを読み取ります。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_fm_tcp_write(3HA) - TCP 接続を使って、監視されているプロセスにデータを書き込みます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_simple_probe(3HA) - プロセスへの TCP 接続を確立し、切断することによってプロセスを検証します。この関数は IPv4 アドレスだけを扱います。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_simple_net_probe(3HA) - プロセスへの TCP 接続を確立し、切断することによってプロセスを検証します。この関数は、IPv4 または IPv6 アドレスを扱います。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_fm_tcp_disconnect(3HA) - 監視されているプロセスへの接続を切断します。この関数は IPv4 アドレスだけを扱います。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_fm_net_disconnect(3HA) - 監視されているプロセスへの接続を切断します。この関数は、IPv4 または IPv6 アドレスを扱います。
.RE
.SS "PMF 関数"
.sp
.LP
次のカテゴリの関数は、Process Monitor Facility (PMF) の機能をカプセル化しています。PMF 経由の監視の DSDL モデルは、\fBpmfadm\fR に対して暗黙の \fItag\fR 値を作成し、使用します。詳細は、pmfadm(1M) のマニュアルページを参照してください。
.sp
.LP
また、PMF 機能は、\fBRestart_interval \fR、\fBRetry_count\fR、および \fBaction_script\fR 用の暗黙値も使用します (\fBpmfadm \fR の \fB-t\fR、\fB-n\fR、および \fB-a\fR オプション)。もっとも重要な点は、DSDL が PMF によって検出されたプロセス障害履歴を、障害モニターによって検出されたアプリケーション障害履歴に結びつけ、再起動またはフェイルオーバーのどちらを行うかを決定することです。
.sp
.LP
このカテゴリには次のような関数があります。
.RS +4
.TP
.ie t \(bu
.el o
scds_pmf_get_status(3HA) - 指定するインスタンスが PMF 制御のもとで監視されているかどうかを判別します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_pmf_restart_fm(3HA) - PMF を使用して障害モニターを再起動します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_pmf_signal(3HA) - PMF 制御のもとで動作するプロセスツリーに、指定するシグナルを送信します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_pmf_start(3HA) および scds_pmf_start_env(3HA) - PMF 制御のもとで指定したプログラム (障害モニターを含む) を実行します。\fBscds_pmf_start_env()\fR 関数は、\fBscds_pmf_start()\fR 関数の処理内容に加えて、指定された環境を実行プログラムに渡す処理を実行します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_pmf_stop(3HA) - PMF 制御のもとで動作しているプロセスの監視を停止します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_pmf_stop_monitoring(3HA) - PMF 制御のもとで動作しているプロセスの監視を停止します。
.RE
.SS "障害監視関数"
.sp
.LP
これらの関数は、障害履歴を保持し、その履歴を \fBRetry_count\fR および \fBRetry_interval\fR プロパティーと関連付けて評価することにより、障害監視の事前定義モデルを提供します。
.sp
.LP
このカテゴリには次のような関数があります。
.RS +4
.TP
.ie t \(bu
.el o
scds_fm_sleep(3HA) - 障害モニター制御ソケットでメッセージを待機します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_fm_action(3HA) - プローブ完了のあとでアクションを実行します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_fm_print_probes(3HA) - システムログに検証状態情報を書き込みます。
.RE
.SS "ユーティリティー関数"
.sp
.LP
次の関数は、メッセージやデバッグ用メッセージをシステムログに書き込むために使用します。
.RS +4
.TP
.ie t \(bu
.el o
scds_syslog(3HA) - システムログにメッセージを書き込みます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
scds_syslog_debug(3HA) - システムログにデバッグ用メッセージを書き込みます。
.RE
.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/scds.h\fR\fR
.ad
.RS 36n
.rt  
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.RS 36n
.rt  
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能\fBSUNWscdev\fR
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
pmfadm(1M)、scds_close(3HA)、scds_error_string(3HA)、scds_error_string_i18n(3HA)、scds_failover_rg(3HA)、scds_fm_action(3HA)、scds_fm_net_connect(3HA)、scds_fm_net_disconnect(3HA)、scds_fm_print_probes(3HA)、scds_fm_sleep(3HA)、scds_fm_tcp_connect(3HA)、scds_fm_tcp_disconnect(3HA)、scds_fm_tcp_read(3HA)、scds_fm_tcp_write(3HA)、scds_free_ext_property(3HA)、scds_free_net_list(3HA)、scds_free_netaddr_list(3HA)、scds_free_port_list(3HA)、scds_get_ext_property(3HA)、scds_get_netaddr_list(3HA)、scds_get_port_list(3HA)、scds_get_resource_group_name(3HA)、scds_get_resource_name(3HA)、scds_get_resource_type_name(3HA)、scds_get_rg_hostnames(3HA)、scds_get_rs_hostnames(3HA)、scds_get_zone_name(3HA)、scds_hasp_check(3HA)、scds_initialize(3HA)、scds_pmf_get_status(3HA)、scds_pmf_restart_fm(3HA), scds_pmf_signal(3HA)、scds_pmf_start(3HA)、scds_pmf_stop(3HA)、scds_pmf_stop_monitoring(3HA)、scds_print_net_list(3HA)、scds_print_netaddr_list(3HA)、scds_print_port_list(3HA)、scds_property_functions(3HA)、scds_restart_resource(3HA)、scds_restart_rg(3HA)、scds_simple_net_probe(3HA)、scds_simple_probe(3HA)、scds_svc_wait(3HA)、scds_syslog(3HA)、scds_syslog_debug(3HA)、bscds_timerun(3HA)、scha_calls(3HA)、SUNW.HAStoragePlus(5)、\fBattributes\fR(5)
