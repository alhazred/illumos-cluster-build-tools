'\" te
.\" Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH scds_fm_tcp_read 3HA "2008 年 8 月 13 日" "Sun Cluster 3.2" "Sun Cluster HA およびデータサービス"
.SH 名前
scds_fm_tcp_read \- アプリケーションとの TCP 接続を使用したデータの読み取り
.SH 形式
.LP
.nf
cc [\fIflags\fR\&.\|.\|.] \fB-I\fR /usr/cluster/include \fI file\fR \fB-L\fR /usr/cluster/lib \fB -l \fR dsdev #include <rgm/libdsdev.h>

\fBscha_err_t\fR \fBscds_fm_tcp_read\fR (\fBscds_handle_t \fR \fI handle\fR, \fBint\fR \fIsock \fR, \fBchar *\fR\fIbuffer\fR, \fBsize_t *\fR\fIsize\fR, \fBtime_t\fR \fItimeout\fR);
.fi

.SH 機能説明
.sp
.LP
\fBscds_fm_tcp_read()\fR 関数は、監視対象のプロセスと TCP 接続からデータを読み取ります。
.sp
.LP
\fIsize\fR 引数は入力および引数です。入力時にバッファーのサイズをバイト数で指定します。完了時に、この関数は、\fIbuffer\fR 内にデータを配置し、実際に読み取られたバイト数を \fIsize\fR に指定します。バッファーのサイズが読み取られるバイト数より小さい場合、\fIsize\fR バイトの完全なバッファーを返します。この関数は、さらにデータを取得するために再度呼び出すことができます。
.sp
.LP
タイムアウトになった場合は、\fBSCHA_ERR_TIMEOUT\fR を返します。この場合、関数は、要求されたバイト数より少ないバイト数 (\fIsize\fR で表される) を返します。
.SH パラメータ
.sp
.LP
次のパラメータがサポートされます。
.sp
.ne 2
.mk
.na
\fB\fIhandle\fR\fR
.ad
.RS 20n
.rt  
scds_initialize(3HA) から返されるハンドルです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIsock\fR\fR
.ad
.RS 20n
.rt  
以前の scds_fm_tcp_connect(3HA) 呼び出しから返されるソケット番号です。
.RE

.sp
.ne 2
.mk
.na
\fB\fIbuffer\fR\fR
.ad
.RS 20n
.rt  
データバッファーです。
.RE

.sp
.ne 2
.mk
.na
\fB\fIsize\fR\fR
.ad
.RS 20n
.rt  
データバッファーサイズです。入力時にバッファーのサイズをバイト数で指定します。出力時、関数は実際に読み取られたバイト数を返します。
.RE

.sp
.ne 2
.mk
.na
\fB\fItimeout\fR\fR
.ad
.RS 20n
.rt  
タイムアウト値 (秒) です。
.RE

.SH 戻り値
.sp
.LP
\fBscds_fm_tcp_read()\fR 関数の戻り値は次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fB0\fR\fR
.ad
.RS 20n
.rt  
関数の実行に成功。
.RE

.sp
.ne 2
.mk
.na
\fB0 以外\fR
.ad
.RS 20n
.rt  
関数の実行に失敗。
.RE

.SH エラー
.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_NOERR\fR\fR
.ad
.RS 28n
.rt  
関数が正常に終了。
.RE

.sp
.ne 2
.mk
.na
\fB\fBSCHA_ERR_TIMEOUT\fR\fR
.ad
.RS 28n
.rt  
関数がタイムアウト。
.RE

.sp
.ne 2
.mk
.na
\fBその他の値\fR
.ad
.RS 28n
.rt  
関数の実行に失敗。障害コードの意味については、scha_calls(3HA)を参照してください。
.RE

.SH ファイル
.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/include/rgm/libdsdev.h\fR\fR
.ad
.sp .6
.RS 4n
インクルードファイル
.RE

.sp
.ne 2
.mk
.na
\fB\fB/usr/cluster/lib/libdsdev.so\fR\fR
.ad
.sp .6
.RS 4n
ライブラリ
.RE

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWscdev
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
scds_fm_tcp_disconnect(3HA)、scds_fm_tcp_write(3HA)、scds_initialize(3HA)、scha_calls(3HA)、\fBattributes\fR(5)
