'\" te
.\"  Copyright 2008 Sun Microsystems, Inc. All rights reserved. ライセンス契約の条件に応じて使用できます。
.TH  cldevicegroup   1CL  " 2008 年 8 月 11 日" " Sun Cluster 3.2 " " Sun Cluster 保守コマンド "
.SH 名前
cldevicegroup, cldg \- Sun Cluster デバイスグループの管理
.SH 形式
.LP
.nf
\fB/usr/cluster/bin/cldevicegroup\fR \fB-V\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup\fR [\fIsubcommand\fR] \fB-?\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup\fR \fIsubcommand\fR [\fIoptions\fR] \fB-v\fR [\fIdevicegroup\fR \&.\|.\|.]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup add-device\fR \fB-d\fR \fIdevice\fR[,\&.\|.\|.] \fIdevicegroup\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup add-node\fR \fB-n\fR \fInode\fR[,\&.\|.\|.] [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] {+ | \fIdevicegroup\fR ...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup create\fR \fB-n\fR \fInode\fR[,\&.\|.\|.] \fB-t\fR \fIdevicegroup-type\fR [\fB-d\fR \fIdevice\fR[,\&.\|.\|.] ] [\fB-p\fR \fIname\fR=\fIvalue\fR] \fIdevicegroup\fR ...
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup create\fR \fB-i\fR {- | \fIclconfigfile\fR} [\fB-d\fR \fIdevice\fR[,\&.\|.\|.] ] [\fB-n\fR \fInode\fR[,\&.\|.\|.] ] [\fB-p\fR \fIname\fR=\fIvalue\fR] [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.]] {+ | \fIdevicegroup\fR ...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup delete\fR [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] {+ | \fIdevicegroup\fR ...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup disable\fR [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] {+ | \fIdevicegroup\fR ...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup enable\fR [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] {+ | \fIdevicegroup\fR ...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup export\fR [\fB-n\fR \fInode\fR[,\&.\|.\|.] ] [\fB-o\fR {- | \fIclconfigfile\fR}] [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] {+ | \fIdevicegroup\fR ...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup list\fR [\fB-n\fR \fInode\fR[,\&.\|.\|.] ] [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] [+ | \fIdevicegroup\fR ...]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup offline\fR [ \fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] {+ | \fIdevicegroup\fR ...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup online\fR [\fB-e\fR] [\fB-n\fR \fInode\fR ] [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] {+ | \fIdevicegroup\fR ...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup remove-device\fR \fB-d\fR \fIdevice\fR[,\&.\|.\|.] \fIdevicegroup\fR
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup remove-node\fR \fB-n\fR \fInode\fR[,\&.\|.\|.] [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] {+ | \fIdevicegroup\fR ...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup set\fR \fB-p\fR \fIname\fR=\fIvalue\fR [\fB-p\fR \fIname\fR=\fIvalue\fR]\&.\|.\|. [\fB-d\fR \fIdevice\fR[,\&.\|.\|.] ] [\fB-n \fInode\fR\fR[,\&.\|.\|.] ] [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] {+ | \fIdevicegroup\fR ...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup show\fR [\fB-n\fR \fInode\fR[,\&.\|.\|.] ] [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] [+ | \fIdevicegroup\fR ...]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup status\fR [\fB-n\fR \fInode\fR[,\&.\|.\|.] ] [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] [+ | \fIdevicegroup\fR ...]
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup switch\fR \fB-n \fInode\fR\fR [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] {+ | \fIdevicegroup\fR ...}
.fi

.LP
.nf
\fB/usr/cluster/bin/cldevicegroup sync\fR [\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.] ] {+ | \fIdevicegroup\fR ...}
.fi

.SH 機能説明
.sp
.LP
\fBcldevicegroup\fR コマンドは、Sun Cluster デバイスグループを管理します。\fBcldg\fR コマンドは、\fBcldevicegroup\fR コマンドの短縮形式です。次の 2 つのコマンドは同一です。どちらの形式のコマンドも使用できます。
.sp
.LP
このコマンドの書式は次のとおりです。
.sp
.LP
\fBcldevicegroup\fR [\fIsubcommand\fR] [\fIoptions\fR] [\fIoperands\fR]
.sp
.LP
\fIoptions\fR に \fB-?\fR または \fB-V\fR オプションを指定する場合だけは、\fIsubcommand\fR を省略できます。
.sp
.LP
このコマンドの各オプションには、長い形式と短い形式があります。各オプションの両方の形式については、このマニュアルページの「オプション」セクションを参照してください。
.sp
.LP
\fBlist\fR、\fBshow\fR、および \fBstatus\fR を除いて、ほとんどのサブコマンドには少なくとも 1 つのオペランドが必要です。多くのサブコマンドは、適用できる\fBすべての\fRオブジェクトを示すオペランドとしてプラス記号 (\fB+\fR) を受け入れます。詳細は、このマニュアルページの SYNOPSIS およびほかのセクションを参照してください。
.sp
.LP
それぞれのサブコマンドはすべてのデバイスグループタイプに使用できますが、次のサブコマンドを除きます。
.RS +4
.TP
.ie t \(bu
.el o
\fBadd-device\fR および \fBremove-device\fR サブコマンドは、\fBrawdisk\fR タイプだけに有効です。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBadd-node\fR、\fBcreate\fR、\fBdelete\fR、および \fBremove-node\fR サブコマンドは、\fBrawdisk\fR タイプと \fBvxvm\fR タイプだけに有効です。
.RE
.sp
.LP
このコマンドの形式は非大域ゾーンで使用できます。このコマンドの有効な使用法の詳細は、個々のサブコマンドの説明を参照してください。管理を容易にするため、このコマンドは大域ゾーンで使用します。
.SH サブコマンド
.sp
.LP
サポートされるサブコマンドには次のものがあります。
.sp
.ne 2
.mk
.na
\fB\fBadd-device\fR\fR
.ad
.sp .6
.RS 4n
新しいメンバーディスクデバイスを既存の raw ディスクデバイスグループに追加します。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
\fBadd-device\fR サブコマンドは、\fBrawdisk\fR タイプの既存のデバイスグループだけで使用できます。デバイスグループタイプについての詳細は、\fB-t\fR オプションの説明を参照してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.sp
ディスクデバイスを raw ディスクデバイスグループから削除する方法については、\fBremove-device\fR サブコマンドの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBadd-node\fR\fR
.ad
.sp .6
.RS 4n
新しいノードを既存のデバイスグループに追加します。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
このサブコマンドは、\fBrawdisk\fR および \fBvxvm\fR デバイスグループタイプだけをサポートします。Sun Cluster コマンドでは、\fBsvm\fR または \fBsds\fR デバイスグループにノードを追加できません。代わりに、Solaris Volume Manager コマンドを使用して、Solaris Volume Manager ディスクセットにノードを追加してください。ディスクセットは \fBsvm\fR または \fBsds\fR デバイスグループとして、Sun Cluster ソフトウェアに自動的に登録されます。デバイスグループタイプについての詳細は、\fB-t\fR オプションの説明を参照してください。
.sp
デバイスグループの \fBpreferenced\fR プロパティーが \fBtrue\fR に設定されている場合、このサブコマンドはそのデバイスグループに使用できません。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.sp
ノードをデバイスグループから削除する方法については、\fBremove-node\fR サブコマンドの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBcreate\fR\fR
.ad
.sp .6
.RS 4n
新しいデバイスグループを作成します。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
このサブコマンドは、\fBrawdisk\fR および \fBvxvm\fR デバイスグループタイプだけをサポートします。Sun Cluser コマンドでは、\fBsvm\fR または \fBsds\fR デバイスグループは作成できません。その代わりに、Solaris ボリュームマネージャーコマンドを使用して、Solaris ボリュームマネージャーディスクセットを作成します。ディスクセットは、\fBsvm\fR または \fBsds\fR デバイスグループとして、Sun Cluster ソフトウェアに自動的に登録されます。デバイスグループタイプについての詳細は、\fB-t\fR オプションの説明を参照してください。
.sp
\fB-i\fR オプションで構成ファイルを指定する場合、プラス記号 (\fB+\fR) をオペランドとして指定できます。このオペランドを使用するとき、このコマンドは、構成ファイルで指定された、まだ存在していないすべてのデバイスグループを作成します。
.sp
\fBrawdisk\fR デバイスグループタイプの場合、\fB-d\fR オプションを \fBcreate\fR サブコマンドと一緒に使用して、1 つまたは複数のデバイスをデバイスグループに指定します。デバイスを指定するとき、コマンドの呼び出しごとに 1 つの \fB-d\fR オプションを使用します。\fB-i\fR オプションを使用しないかぎり、1 つのコマンド呼び出しで複数の raw ディスクデバイスグループは作成できません。
.sp
\fBvxvm\fR デバイスグループタイプの場合、\fB-p localonly={true|false}\fR オプションを \fBcreate\fR サブコマンドと一緒に使用して、ローカル専用 VxVM ディスクグループを VxVM デバイスグループに変更したり、VxVM デバイスグループをローカル専用 VxVM ディスクグループに変更したりします。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.sp
デバイスグループを削除する方法については、\fBdelete\fR サブコマンドの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBdelete\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループを削除します。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
このサブコマンドは、\fBrawdisk\fR および \fBvxvm\fR デバイスグループタイプだけをサポートします。
.sp
Sun Cluster コマンドでは、\fBsvm\fR または \fBsds\fR デバイスグループは削除できません。その代わりに、\fBsvm\fR または \fBsds\fR デバイスグループを削除するには、Solaris Volume Manager コマンドを使用して、配下の Solaris Volume Manager ディスクセットを削除します。
.sp
削除する前に、デバイスグループはオフラインになっている必要があります。
.sp
\fB+\fR オペランドを指定する場合、\fBautogen\fR プロパティーが \fBfalse\fR に設定されたデバイスグループだけが影響を受けます。起動時にシステムによって自動的に作成される、\fBautogen\fR プロパティーが \fBtrue\fR に設定されたデバイスグループにこのコマンドを適用するには、デバイスグループごとに明示的に指定してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.sp
デバイスグループを作成する方法については、\fBcreate\fR サブコマンドの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBdisable\fR\fR
.ad
.sp .6
.RS 4n
オフラインのデバイスグループを無効にします。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
デバイスグループの無効な状態は、再起動しても変わりません。
.sp
有効なデバイスグループをオフラインにする前には、まず、\fBdisable\fR サブコマンドを使用して、デバイスグループの有効な状態をクリアしてください。
.sp
デバイスグループがオンラインである場合、\fBdisable\fR アクションは失敗して、指定されたデバイスグループを無効にできません。
.sp
\fBswitch\fR サブコマンドまたは \fBonline\fR サブコマンドでは、無効なデバイスグループはオンラインにできません。まず、\fBenable\fR サブコマンドを使用して、デバイスグループの無効な状態をクリアしてください。
.sp
\fB+\fR オペランドを指定する場合、\fBautogen\fR プロパティーが \fBfalse\fR に設定されたデバイスグループだけが影響を受けます。起動時にシステムによって自動的に作成される、\fBautogen\fR プロパティーが \fBtrue\fR に設定されたデバイスグループにこのコマンドを適用するには、デバイスグループごとに明示的に指定してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.sp
デバイスグループを有効にする方法については、\fBenable\fR サブコマンドの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBenable\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループを有効にします。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
デバイスグループの無効な状態は、再起動しても変わりません。
.sp
無効なデバイスグループをオンラインにする前には、まず、\fBenable\fR サブコマンドを使用して、デバイスグループの無効な状態をクリアしてください。
.sp
\fB+\fR オペランドを指定する場合、\fBautogen\fR プロパティーが \fBfalse\fR に設定されたデバイスグループだけが影響を受けます。起動時にシステムによって自動的に作成される、\fBautogen\fR プロパティーが \fBtrue\fR に設定されたデバイスグループにこのコマンドを適用するには、デバイスグループごとに明示的に指定してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.sp
デバイスグループを無効にする方法については、\fBdisable\fR サブコマンドの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBexport\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループ構成情報をエクスポートします。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
\fB-o\fR オプションでファイル名を指定する場合、構成情報はその新しいファイルに書き込まれます。\fB-o\fR オプションを指定しない場合、出力は標準出力に書き込まれます。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBlist\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループのリストを表示します。
.sp
このサブコマンドは、大域ゾーンまたは非大域ゾーンで使用できます。管理を容易にするため、この書式のコマンドは大域ゾーンで使用します。
.sp
デフォルトでは、このサブコマンドは、\fBautogen\fR プロパティーが \fBfalse\fR に設定されているクラスタにあるすべてのデバイスグループのリストを表示します。クラスタ内のすべてのデバイスグループを表示するには、\fB-v\fR オプションも指定します。
.sp
\fB+\fR オペランドを指定する場合、\fBautogen\fR プロパティーが \fBfalse\fR に設定されたデバイスグループだけが影響を受けます。起動時にシステムによって自動的に作成される、\fBautogen\fR プロパティーが \fBtrue\fR に設定されたデバイスグループにこのコマンドを適用するには、デバイスグループごとに明示的に指定してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBoffline\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループをオフラインにします。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
デバイスグループが有効な場合、\fBoffline\fR サブコマンドを実行する前に、\fBdisable\fR サブコマンドを実行して、そのデバイスグループを無効にしてください。
.sp
オフラインのデバイスグループを起動するには、次のアクションのいずれかを実行します。
.RS +4
.TP
.ie t \(bu
.el o
明示的な \fBonline\fR サブコマンドまたは \fBswitch\fR サブコマンドを実行します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
そのデバイスグループ内のデバイスにアクセスします。
.RE
.RS +4
.TP
.ie t \(bu
.el o
そのデバイスグループに依存するファイルシステムをマウントします。
.RE
\fB+\fR オペランドを指定する場合、\fBautogen\fR プロパティーが \fBfalse\fR に設定されたデバイスグループだけが影響を受けます。起動時にシステムによって自動的に作成される、\fBautogen\fR プロパティーが \fBtrue\fR に設定されたデバイスグループにこのコマンドを適用するには、デバイスグループごとに明示的に指定してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.admin\fR RBAC の承認が必要です。
.sp
デバイスグループをオンラインにする方法については、\fBonline\fR サブコマンドの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBonline\fR\fR
.ad
.sp .6
.RS 4n
指定されたノードでデバイスグループをオンラインにします。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
デバイスグループが無効な場合、そのデバイスグループをオンラインにする前に、次のいずれかの方法により、そのデバイスグループを有効にしてください。
.RS +4
.TP
.ie t \(bu
.el o
\fB-e\fR オプションを \fBonline\fR サブコマンドと一緒に使用します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBonline\fR サブコマンドを実行する前に、\fBenable\fR サブコマンドを実行します。
.RE
\fB+\fR オペランドを指定する場合、\fBautogen\fR プロパティーが \fBfalse\fR に設定されたデバイスグループだけが影響を受けます。起動時にシステムによって自動的に作成される、\fBautogen\fR プロパティーが \fBtrue\fR に設定されたデバイスグループにこのコマンドを適用するには、デバイスグループごとに明示的に指定してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.admin\fR RBAC の承認が必要です。
.sp
デバイスグループをオフラインにする方法については、\fBoffline\fR サブコマンドの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBremove-device\fR\fR
.ad
.sp .6
.RS 4n
メンバーディスクデバイスを raw ディスクデバイスグループから削除します。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
\fBremove-device\fR サブコマンドは、\fBrawdisk\fR デバイスグループタイプだけに有効です。このサブコマンドは、\fBsvm\fR、\fBsds\fR、および \fBvxvm\fR デバイスグループタイプに対しては有効ではありません。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.sp
ディスクデバイスを raw ディスクデバイスグループに追加する方法については、\fBadd-device\fR サブコマンドの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBremove-node\fR\fR
.ad
.sp .6
.RS 4n
既存のデバイスグループからノードを削除します。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
このサブコマンドは、\fBrawdisk\fR および \fBvxvm\fR デバイスグループタイプだけをサポートします。Sun Cluster コマンドでは、\fBsvm\fR または \fBsds\fR デバイスグループからノードは削除できません。代わりに、Solaris Volume Manager コマンドを使用して、Solaris Volume Manager ディスクセットからノードを削除してください。ディスクセットは \fBsvm\fR または \fBsds\fR デバイスグループとして、Sun Cluster ソフトウェアに自動的に登録されます。デバイスグループタイプについての詳細は、\fB-t\fR オプションの説明を参照してください。
.sp
\fBpreferenced\fR プロパティーが \fBtrue\fR に設定されている場合、\fBremove-node\fR サブコマンドはそのデバイスグループに使用できません。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.sp
ノードをデバイスグループに追加する方法については、\fBadd-node\fR サブコマンドの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBset\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループに関連付けられている属性を変更します。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
\fBrawdisk\fR デバイスグループタイプの場合、\fB-d\fR オプションを \fBset\fR サブコマンドと一緒に使用して、指定したデバイスグループのメンバーディスクデバイスの新しいリストを指定します。
.sp
\fB+\fR オペランドを指定する場合、\fBautogen\fR プロパティーが \fBfalse\fR に設定されたデバイスグループだけが影響を受けます。起動時にシステムによって自動的に作成される、\fBautogen\fR プロパティーが \fBtrue\fR に設定されたデバイスグループにこのコマンドを適用するには、デバイスグループごとに明示的に指定してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBshow\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループの構成レポートを作成します。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
デフォルトでは、このサブコマンドは、\fBautogen\fR プロパティーが \fBfalse\fR に設定されているクラスタにあるすべてのデバイスグループについて報告します。クラスタ内のすべてのデバイスグループを表示するには、\fB-v\fR オプションも指定します。
.sp
\fB+\fR オペランドを指定する場合、\fBautogen\fR プロパティーが \fBfalse\fR に設定されたデバイスグループだけが影響を受けます。起動時にシステムによって自動的に作成される、\fBautogen\fR プロパティーが \fBtrue\fR に設定されたデバイスグループにこのコマンドを適用するには、デバイスグループごとに明示的に指定してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBstatus\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループのステータスレポートを作成します。
.sp
このサブコマンドは、大域ゾーンまたは非大域ゾーンで使用できます。管理を容易にするため、この書式のコマンドは大域ゾーンで使用します。
.sp
デフォルトでは、このサブコマンドは、\fBautogen\fR プロパティーが \fBfalse\fR に設定されているクラスタにあるすべてのデバイスグループについて報告します。クラスタ内のすべてのデバイスグループを表示するには、\fB-v\fR オプションも指定します。
.sp
\fB+\fR オペランドを指定する場合、\fBautogen\fR プロパティーが \fBfalse\fR に設定されたデバイスグループだけが影響を受けます。起動時にシステムによって自動的に作成される、\fBautogen\fR プロパティーが \fBtrue\fR に設定されたデバイスグループにこのコマンドを適用するには、デバイスグループごとに明示的に指定してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.read\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBswitch\fR\fR
.ad
.sp .6
.RS 4n
Sun Cluster 構成内の、あるプライマリノードから別のノードにデバイスグループを転送します。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
\fB+\fR オペランドを指定する場合、\fBautogen\fR プロパティーが \fBfalse\fR に設定されたデバイスグループだけが影響を受けます。起動時にシステムによって自動的に作成される、\fBautogen\fR プロパティーが \fBtrue\fR に設定されたデバイスグループにこのコマンドを適用するには、デバイスグループごとに明示的に指定してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.modify\fR RBAC の承認が必要です。
.RE

.sp
.ne 2
.mk
.na
\fB\fBsync\fR\fR
.ad
.sp .6
.RS 4n
クラスタリングソフトウェアとデバイスグループ情報の同期を取ります。
.sp
このサブコマンドは、大域ゾーンだけで使用できます。
.sp
VxVM デバイスグループの VxVM ボリュームを追加または削除する場合、または所有者、グループ、アクセス権などのいずれかのボリューム属性を変更する場合は、必ずこのサブコマンドを使用します。
.sp
また、\fBsync\fR サブコマンドを使用して、デバイスグループ構成をレプリケーション構成または非レプリケーション構成に変更します。
.sp
Hitachi TrueCopy データレプリケーションを使用するディスクを含むデバイスグループの場合、このサブコマンドは、デバイスグループ構成とレプリケーション構成の同期を取ります。この同期は、データレプリケーション用に構成されたディスクを Sun Cluster ソフトウェアに認識させ、Sun Cluster ソフトウェアが必要に応じてフェイルオーバーやスイッチオーバーを処理できるようにします。
.sp
複製用に構成されたディスクを含む Solaris Volume Manager ディスクセットを作成したあとは、対応する \fBsvm\fR または \fBsds\fR デバイスグループに \fBsync\fR サブコマンドを実行してください。Solaris Volume Manager ディスクセットは、\fBsvm\fR または \fBsds\fR デバイスグループとして Sun Cluster ソフトウェアに自動的に登録されますが、その時点で複製情報の同期は取られません。
.sp
新たに作成した \fBvxvm\fR および \fBrawdisk\fR デバイスグループタイプの場合、そのディスクの複製情報の同期を手動で取る必要はありません。VxVM ディスクグループまたは raw ディスクデバイスグループを Sun Cluster ソフトウェアに登録すると、ディスク上のすべてのレプリケーション情報を Sun Cluster ソフトウェアが自動的に検出します。
.sp
\fB+\fR オペランドを指定する場合、\fBautogen\fR プロパティーが \fBfalse\fR に設定されたデバイスグループだけが影響を受けます。起動時にシステムによって自動的に作成される、\fBautogen\fR プロパティーが \fBtrue\fR に設定されたデバイスグループにこのコマンドを適用するには、デバイスグループごとに明示的に指定してください。
.sp
スーパーユーザー以外のユーザーがこのサブコマンドを使用するには、\fBsolaris.cluster.admin\fR RBAC の承認が必要です。
.RE

.SH オプション
.sp
.LP
次のオプションがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fB-?\fR\fR
.ad
.br
.na
\fB\fB--help\fR\fR
.ad
.sp .6
.RS 4n
ヘルプ情報を表示します。
.sp
このオプションは、単独でもサブコマンド付きでも使用できます。
.RS +4
.TP
.ie t \(bu
.el o
このオプションを単独で使用する場合、利用可能なサブコマンドのリストが出力されます。
.RE
.RS +4
.TP
.ie t \(bu
.el o
このオプションをサブコマンドを付けて使用する場合、そのサブコマンドの使用法オプションが出力されます。
.RE
このオプションを使用すると、ほかの処理は実行されません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-d\fR \fIdevice\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--device\fR=\fIdevice\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--device\fR \fIdevice\fR[,\&.\|.\|.]\fR
.ad
.sp .6
.RS 4n
指定されている raw ディスクデバイスグループのメンバーになるディスクデバイスのリストを指定します。
.sp
\fB-d\fR オプションが有効になるのは、デバイスグループタイプ \fBrawdisk\fR 用に \fBcreate\fR および \fBset\fR サブコマンドと組み合わせて使用する場合だけです。常に、ノードリスト全体を指定してください。このオプションを使用して、メンバーディスクリストに個々のディスクを追加したり、メンバーディスクリストから個々のディスクを削除したりすることはできません。
.sp
ディスクは、DID 広域デバイス名 (たとえば、\fBd3\fR) のみで指定します。詳細は、did(7) のマニュアルページを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-e\fR\fR
.ad
.br
.na
\fB\fB--enable\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループを有効にします。このオプションは、\fBonline\fR サブコマンドと一緒に使用するときだけに有効です。
.sp
指定したデバイスグループがすでに有効である場合、\fB-e\fR オプションは無視され、このコマンドはデバイスグループをオンラインにする処理に進みます。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-i\fR {- | \fIclconfigfile\fR}\fR
.ad
.br
.na
\fB\fB--input\fR={- | \fIclconfigfile\fR}\fR
.ad
.br
.na
\fB\fB--input\fR {- | \fIclconfigfile\fR}\fR
.ad
.sp .6
.RS 4n
デバイスグループの作成に使用する構成情報を指定します。この情報は clconfiguration(5CL) のマニュアルページに定義されている書式に準拠させてください。この情報は、ファイルに含めることも、標準入力を介して指定することもできます。標準入力を指定するには、ファイル名の代わりにマイナス記号 (\fB-\fR) を指定します。
.sp
\fB-i\fR オプションは、完全修飾したデバイスグループリストに含まれるデバイスグループだけに影響します。
.sp
コマンドで指定するオプションは、構成ファイルで設定されている任意のオプションより優先されます。構成パラメータがクラスタ構成ファイルに存在しない場合、これらのパラメータをコマンド行で指定してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-n\fR \fInode\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--node\fR=\fInode\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--node\fR \fInode\fR[,\&.\|.\|.]\fR
.ad
.sp .6
.RS 4n
ノードまたはノードリストを指定します。
.sp
デフォルトでは、ノードリストの順番は、デバイスグループの主ノードとして引き継がれるべき優先順位を示します。例外は、Sun Cluster の制御範囲の外にあるローカル専用ディスクグループの場合で、主ノードと二次ノードの概念は当てはまりません。
.sp
デバイスグループの \fBpreferenced\fR プロパティーが \fBfalse\fR に設定されている場合、ノードリストの順番は無視されます。その代わりに、グループ内で最初にデバイスにアクセスしたノードが自動的に、そのグループの主ノードになります。デバイスグループノードリストの \fBpreferenced\fR プロパティーの設定については、\fB-p\fR オプションを参照してください。
.sp
\fB-n\fR オプションは、\fBsvm\fR または \fBsds\fR デバイスグループのノードリストを指定するのには使用できません。その代わりに、Solaris ボリュームマネージャーのコマンドまたはユーティリティーを使用して、配下のディスクセットのノードリストを指定してください。
.sp
\fBcreate\fR および \fBset\fR サブコマンドは \fB-n\fR オプションを使用して、デバイスグループタイプ \fBrawdisk\fR および \fBvxvm\fR 専用の潜在的な主ノードのリストを指定します。デバイスグループの完全なノードリストを指定してください。\fB-n\fR オプションを使用して、ノードリストに個別ノードを追加したり、ノードリストから個別ノードを削除したりすることはできません。
.sp
\fBswitch\fR サブコマンドは \fB-n\fR オプションを使用して、新しいデバイスグループの主ノードとして単一のノードを指定します。
.sp
\fBexport\fR、\fBlist\fR、\fBshow\fR、および \fBstatus\fR サブコマンドは \fB-n\fR オプションを使用して、指定されたノード上でオンラインでないデバイスグループを出力から除外します。
.sp
主ノードと二次ノードの概念は、Sun Cluster の制御範囲の外にある \fBlocalonly\fR ディスクグループには当てはまりません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-o\fR {- | \fIclconfigfile\fR}\fR
.ad
.br
.na
\fB\fB--output\fR={- | \fIclconfigfile\fR}\fR
.ad
.br
.na
\fB\fB--output\fR {- | \fIclconfigfile\fR}\fR
.ad
.sp .6
.RS 4n
clconfiguration(5CL) のマニュアルページで説明されている形式で、デバイスグループ構成を表示します。この情報は、ファイルまたは標準出力のどちらにでも書き込むことができます。
.sp
このオプションの引数としてファイル名を指定する場合、このコマンドは新しいファイルを作成して、そのファイルに構成情報を出力します。同じ名前のファイルがすでにある場合、このコマンドはエラーで終了します。既存のファイルに変更は行われません。
.sp
このオプションの引数としてマイナス記号 (\fB-\fR) を指定すると、このコマンドは標準出力に構成情報を表示します。このコマンドのほかの標準出力はすべて抑制されます。
.sp
\fB-o\fR オプションは、\fBexport\fR サブコマンドだけで有効です。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-p\fR \fIname\fR=\fIvalue\fR\fR
.ad
.br
.na
\fB\fB--property\fR=\fIname\fR=\fIvalue\fR\fR
.ad
.br
.na
\fB\fB--property\fR \fIname\fR=\fIvalue\fR\fR
.ad
.sp .6
.RS 4n
デバイスグループプロパティーに値を設定します。
.sp
\fB-p\fR オプションは、\fBcreate\fR サブコマンドと \fBset\fR サブコマンドだけに有効です。\fB-p \fR\fIname\fR\fB-=\fR\fIvalue\fR は複数回指定できます。
.sp
次のプロパティーがサポートされています。
.sp
.ne 2
.mk
.na
\fB\fBautogen\fR\fR
.ad
.sp .6
.RS 4n
\fBautogen\fR プロパティーは \fBtrue\fR または \fBfalse\fR の値を持つことができます。手動で作成したデバイスグループの場合、デフォルトは \fBfalse\fR です。システム作成デバイスグループの場合、デフォルトは \fBtrue\fR です。
.sp
\fBautogen\fR プロパティーは、\fBlist\fR、\fBshow\fR、および \fBstatus\fR サブコマンドのインジケータです。これらのサブコマンドは、\fB-v\fR オプションを使用しないかぎり、\fBautogen\fR プロパティーが \fBtrue\fR に設定されているデバイスをリストに載せません。
.sp
このプロパティーは、デバイスグループタイプ \fBrawdisk\fR だけに有効です。デバイスグループタイプについての詳細は、\fB-t\fR オプションを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBfailback\fR\fR
.ad
.sp .6
.RS 4n
\fBfailback\fR プロパティーは \fBtrue\fR または \fBfalse\fR の値を持つことができます。デフォルトは \fBfalse\fR です。
.sp
\fBfailback\fR プロパティーは、デバイスグループの主ノードがクラスタメンバーシップから切り離され、あとで再結合された場合のシステムの動作を指定します。
.sp
デバイスグループの主ノードがクラスタメンバーシップから切り離された段階で、デバイスグループは二次ノードに処理を継続します。そして障害の発生したノードがクラスタメンバーシップに再結合すると、デバイスグループは、そのまま二次ノードにマスターされ続けるか、あるいは、オリジナルの主ノードにフェイルバックするか、のいずれかの動作を取ります。
.RS +4
.TP
.ie t \(bu
.el o
\fBfailback\fR プロパティーが \fBtrue\fR に設定されている場合、デバイスグループは、オリジナルの主ノードにマスターされるようになります。
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fBfailback\fR プロパティーが \fBfalse\fR に設定されている場合、デバイスグループは、二次ノードにマスターされ続けます。
.RE
デバイスグループ作成中、\fBfailback\fR プロパティーはデフォルトでは無効にされます。\fBset\fR 処理中、\fBfailback\fR プロパティーは変更されません。
.RE

.sp
.ne 2
.mk
.na
\fB\fBlocalonly\fR\fR
.ad
.sp .6
.RS 4n
\fBlocalonly\fR プロパティーは \fBtrue\fR または \fBfalse\fR の値を持つことができます。デフォルトは \fBfalse\fR です。
.sp
\fBlocalonly\fR プロパティーは、\fBrawdisk\fR および \fBvxvm\fR タイプのディスクグループに対してのみ有効です。
.sp
ディスクグループが特定のノードだけでマスターされるようにする場合、\fBlocalonly=true\fR というプロパティーの設定で、ディスクグループを構成します。ローカル専用ディスクグループは、Sun Cluster ソフトウェアの制御範囲の外にあります。ローカル専用ディスクグループの ノードリストには、1 つのノードだけ指定できます。ディスクグループの \fBlocalonly\fR プロパティーを \fBtrue\fR に設定するとき、そのディスクグループのノードリストに指定できるのは 1 つのノードだけです。
.RE

.sp
.ne 2
.mk
.na
\fB\fBnumsecondaries\fR\fR
.ad
.sp .6
.RS 4n
\fBnumsecondaries\fR プロパティーには、\fB0\fR より大きく、ノードリスト内のノードの合計数より小さい整数値を指定してください。デフォルトは \fB1\fR です。
.sp
このプロパティーの設定は、デバイスグループの二次ノードの希望数を動的に変更する場合に使用できます。デバイスグループの二次ノードは、現在の主ノードに問題が生じた場合に、新たな主ノードとしての機能を引き継ぐことができます。
.sp
\fBnumsecondaries\fR プロパティーを使用することで、可用性のレベルを維持しながら、デバイスグループの二次ノードの指定数を変更できます。デバイスグループの二次ノードリストからノードを削除すると、そのノードは主ノードとして機能を引き継ぐことはできなくなります。
.sp
\fBnumsecondaries\fR プロパティーは、現在クラスタモードであるデバイスグループのノードだけに適用されます。このノードは、デバイスグループの \fBpreferenced\fR プロパティーと一緒に使用できる必要があります。デバイスグループの \fBpreferenced\fR プロパティーが \fBtrue\fR に設定されている場合、まず、優先順位のもっとも低いノードが二次ノードリストから削除されます。優先フラグがついているノードがデバイスグループの中に 1 つもないと、クラスタはノードをランダムに選択し、削除します。
.sp
デバイスグループにおける二次ノードの実際の数が目標のレベル未満になると、二次ノードリストから削除された資格のある各ノードがリストに戻されます。二次ノードリストに戻される資格のある各ノードは、次の条件のすべてに適合する必要があります。
.RS +4
.TP
.ie t \(bu
.el o
そのノードは現在クラスタにあります。
.RE
.RS +4
.TP
.ie t \(bu
.el o
そのノードはそのデバイスグループに属しています。
.RE
.RS +4
.TP
.ie t \(bu
.el o
そのノードは現在、主ノードまたは二次ノードではありません。
.RE
二次ノードリストに戻されるのは、デバイスグループの優先順位がもっとも高いノードからです。二次ノードの目標数に到達するまで、優先順位が高いノードから順に二次ノードリストに戻されます。
.sp
あるノードがクラスタに参加したとき、そのノードの優先順位がそのデバイスグループの既存の二次ノードよりも高い場合、優先順位が低いノードが二次ノードリストから削除されます。削除されるノードは新たに追加されたノードにより置換されます。この置換は、クラスタ内に存在する二次ノードの実際の数が目標数を超えたときだけ発生します。
.sp
デバイスグループのノードリストの \fBpreferenced\fR プロパティーを設定する方法についての詳細は、\fBpreferenced\fR プロパティーを参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBpreferenced\fR\fR
.ad
.sp .6
.RS 4n
\fBpreferenced\fR プロパティーは \fBtrue\fR または \fBfalse\fR の値を持つことができます。デフォルトは \fBtrue\fR です。
.sp
デバイスグループの作成中、\fBpreferenced\fR プロパティーが \fBtrue\fR に設定されている場合、ノードリストはノードの優先順位も示します。ノードの優先順位は、各ノードが、デバイスグループの一次ノードとしてテイクオーバーしようとする順序を決定します。
.sp
デバイスグループの作成中、このプロパティーが \fBfalse\fR に設定されている場合、デバイスグループ内のデバイスに最初にアクセスしたノードが自動的に主ノードになります。指定されたノードリスト内のノードの順番は意味を持ちません。ノードリストを再指定せずにこのプロパティーに \fBtrue\fR を再設定しても、ノードの順番は再度有効にはなりません。
.sp
\fBpreferenced=true\fR プロパティーを指定し、\fB-n\fR オプションを使用して、デバイスグループのノードリスト全体を優先順位どおりに指定しない場合、\fBset\fR 処理中に、ノードの優先順位は変更されません。
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-t\fR \fIdevicegroup-type\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--type\fR=\fIdevicegroup-type\fR[,\&.\|.\|.]\fR
.ad
.br
.na
\fB\fB--type\fR \fIdevicegroup-type\fR[,\&.\|.\|.]\fR
.ad
.sp .6
.RS 4n
デバイスグループタイプまたデバイスグループタイプのリストを指定します。
.sp
\fBcreate\fR サブコマンドの場合、1 つのデバイスグループタイプだけを指定できます。これにより、このオプションで指定したタイプのデバイスグループが作成されます。
.sp
これ以外で \fB-t\fR オプションを受け付けることができるサブコマンドの場合、このオプションにより、コマンドに指定したデバイスグループのリストが指定したデバイスグループタイプだけに限定されます。
.sp
必ずしもすべてのサブコマンドとオプションがすべてのデバイスグループタイプに対して有効であるわけではありません。たとえば、\fBcreate\fR サブコマンドは \fBrawdisk\fR および \fBvxvm\fR デバイスグループタイプのみには有効ですが、\fBsvm\fR または \fBsds\fR デバイスグループタイプには有効でありません。
.sp
\fB-t\fR オプションでサポートされるデバイスグループタイプは次のとおりです。
.sp
.ne 2
.mk
.na
\fB\fBrawdisk\fR\fR
.ad
.sp .6
.RS 4n
raw ディスクデバイスグループを指定します。
.sp
raw ディスクとは、ボリュームマネージャーのボリュームやメタデバイスの一部ではないディスクのことです。raw ディスクデバイスグループを使用すると、デバイスグループ内にディスクセットを定義できます。デフォルトでは、システムブート時に、raw ディスクデバイスグループが、構成内のデバイス ID (DID) 擬似ドライバデバイスごとに作成されます。慣例により、初期化時には、raw ディスクデバイスグループの名前が割り当てられます。これらの名前は、DID デバイス名から派生されます。raw ディスクデバイスグループに追加されるノードごとに、\fBcldevicegroup\fR コマンドは、グループ内のすべてのデバイスがそのノードに物理的に接続されていることを確認します。
.sp
\fBcreate\fR サブコマンドは、raw ディスクデバイスグループを作成して、複数のディスクデバイスをデバイスグループに追加します。新しい raw ディスクデバイスグループを作成する前に、新しいデバイスグループに追加する各デバイスを、ブート時にそのデバイスが作成されたデバイスグループから削除してください。これにより、これらのデバイスが含まれる新しい raw ディスクデバイスグループを作成できます。\fB-n\fR オプションで潜在的な主ノード優先順位リストを指定することに加えて、これらのデバイスのリストを \fB-d\fR オプションで指定します。
.sp
指定した単一のノード上でデバイスグループをマスターするには、\fB-p\fR オプションを使用して、\fBlocalonly=true\fR というプロパティーの設定で、デバイスグループを構成します。ローカル専用デバイスグループを作成するとき、ノードリストには 1 つのノードだけを指定できます。
.sp
\fBdelete\fR サブコマンドは、デバイスグループ名をクラスタデバイスグループ構成から削除します。
.sp
\fBset\fR サブコマンドは、次の変更を raw ディスクデバイスグループに行います。
.RS +4
.TP
.ie t \(bu
.el o
潜在的な主ノードの優先順位を変更します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
新しいノードリストを指定します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
フェイルバックを有効または無効にします。
.RE
.RS +4
.TP
.ie t \(bu
.el o
二次ノードの目標数を設定します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
複数の広域デバイスをデバイスグループに追加します。
.RE
raw ディスクデバイス名が raw ディスクデバイスグループに登録されている場合、その raw ディスクデバイス名は Solaris ボリュームマネージャーデバイスグループまたは VxVM デバイスグループには登録できません。
.sp
\fBsync\fR サブコマンドは、新しいレプリケーション構成と、Hitachi TrueCopy データ複製を使用するディスクを含む raw ディスクデバイスグループ構成の同期を取ります。レプリケーション構成を変更したあとは、\fBsync\fR サブコマンドを使用します。ディスクグループのディスクはすべて、複製されるか複製されないかのどちらかであり、両方を組み合わせることはできません。
.RE

.sp
.ne 2
.mk
.na
\fB\fBsds\fR\fR
.ad
.sp .6
.RS 4n
もともと Solstice DiskSuite\u\s-2TM\s+2\d ソフトウェアで作成されていたデバイスグループを指定します。複数所有者ディスクセットを除いて、このデバイスグループタイプは Solaris Volume Manager デバイスグループタイプ \fBsvm\fR と同等です。詳細は、\fBsvm\fR デバイスグループタイプの説明を参照してください。
.RE

.sp
.ne 2
.mk
.na
\fB\fBsvm\fR\fR
.ad
.sp .6
.RS 4n
Solaris ボリュームマネージャーデバイスグループを指定します。
.sp
Solaris ボリュームマネージャーデバイスグループは、次のコンポーネントで定義されます。
.RS +4
.TP
.ie t \(bu
.el o
名前
.RE
.RS +4
.TP
.ie t \(bu
.el o
グループにアクセスできるノード
.RE
.RS +4
.TP
.ie t \(bu
.el o
ディスクセット内のデバイスのグローバルなリスト
.RE
.RS +4
.TP
.ie t \(bu
.el o
潜在的な主ノードの優先順位やフェイルバックの動作などのアクションを制御するプロパティーの集合
.RE
Solaris ボリュームマネージャーは、多重ホストまたは共有ディスクセットの概念を持っています。共有ディスクセットとは、2 つ以上のホストとディスクドライブからなるグループのことです。このディスクドライブはすべてのホストからアクセス可能であり、すべてのホスト上でデバイス名が同じです。こうしたデバイス名の統一は、raw ディスクデバイスを用いてディスクセットを構築することにより行います。デバイス ID 疑似ドライバ (DID) を使用することで、多重ホストに使用するディスクの名前はクラスタ内で整合性が保たれます。Solaris ボリュームマネージャーデバイスグループのノードリストに構成できるのは、すでにディスクセットの一部として構成されているホストだけです。共有ディスクセットにドライブを追加する場合、そのドライブが、ほかの何らかの共有ディスクセットに属していてはいけません。
.sp
Solaris Volume Manager の \fBmetaset\fR コマンドはディスクセットを作成して、そのディスクセットを Solaris Volume Manager デバイスグループとして、Sun Cluster ソフトウェアに自動的に登録します。デバイスグループを作成したあとは、\fBcldevicegroup\fR コマンドの \fBset\fR サブコマンドを使用して、ノード優先順位リストを設定し、\fBpreferenced\fR、\fBfailback\fR、および \fBnumsecondaries\fR プロパティーを設定します。
.sp
1 つのデバイスグループには、1 つの Solaris ボリュームマネージャーディスクセットだけを割り当てることができます。デバイスグループ名は常に、ディスクセットの名前と一致する必要があります。
.sp
Solaris Volume Manager デバイスグループにノードを追加または削除するのに、\fBadd-node\fR または \fBremove-node\fR サブコマンドは使用できません。その代わりに、Solaris Volume Manager の \fBmetaset\fR コマンドを使用して、配下の Solaris Volume Manager ディスクセットで、ノードを追加または削除します。
.sp
Solaris Volume Manager のデバイスグループをクラスタ構成から削除するのに、\fBdelete\fR サブコマンドは使用できません。その代わりに、Solaris Volume Manager の \fBmetaset\fR コマンドを使用して、配下の Solaris Volume Manager ディスクセットを削除します。
.sp
\fBexport\fR、\fBlist\fR、\fBshow\fR、\fBstatus\fR、および \fBsync\fR サブコマンドだけが、Solaris Volume Manager 複数所有者ディスクセットで機能します。Solaris ボリュームマネージャーデバイスグループの配下のディスクセットを追加または削除するには、Solaris ボリュームマネージャーのコマンドまたはユーティリティーを使用してください。
.sp
\fBsync\fR サブコマンドは、新しいレプリケーション構成と、Hitachi TrueCopy データレプリケーションを使用するディスクを含むデバイスグループ構成の同期を取ります。レプリケーション構成を変更したあとは、\fBsync\fR サブコマンドを使用します。ディスクセットのディスクはすべて、複製されるか複製されないかのどちらかであり、両方を組み合わせることはできません。
.RE

.sp
.ne 2
.mk
.na
\fB\fBvxvm\fR\fR
.ad
.sp .6
.RS 4n
VERITAS Volume Manager (VxVM) デバイスグループを指定します。
.sp
\fBcreate\fR サブコマンドは、新しい VxVM ディスクグループを Sun Cluster デバイスグループ構成に追加します。この作業では、新しいデバイスグループの名前を定義し、このデバイスグループにアクセスできるノードを指定し、動作を制御するために使用されるプロパティーのセットを指定する必要があります。
.sp
指定した単一のノードからディスクグループにアクセスするには、\fB-p\fR オプションを使用して、\fBlocalonly=true\fR というプロパティーの設定で、ディスクグループを構成します。ローカル専用 \fBvxvm\fR ディスクグループを作成するとき、ノードリストには 1 つのノードだけを指定できます。
.sp
1 つのデバイスグループには、1 つの VxVM ディスクグループだけを割り当てることができます。デバイスグループ名は常に、VxVM ディスクグループの名前と一致します。デバイスグループのノードリストにあるノードの 1 つで、対応する VxVM ディスクグループをインポートするまでは、VxVM デバイスグループを作成できません。
.sp
ディスクグループ内のすべての物理ディスクを実際にノードに接続するまで、このノードを VxVM デバイスグループに追加しないでください。
.sp
\fBdelete\fR サブコマンドは、VxVM デバイスグループを Sun Cluster デバイスグループ構成から削除します。
.sp
\fBcreate\fR サブコマンドは、次の変更を \fBvxvm\fR デバイスグループに行います。
.RS +4
.TP
.ie t \(bu
.el o
潜在的な主ノード設定の順番を変更します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
フェイルバックを有効または無効にします。
.RE
.RS +4
.TP
.ie t \(bu
.el o
二次ノードの目標数を変更します。
.RE
.RS +4
.TP
.ie t \(bu
.el o
ローカル専用 VxVM ディスクグループを通常の VxVM ディスクグループに変更します。
.RE
\fBsync\fR サブコマンドは、クラスタリングソフトウェアと VxVM ディスクグループボリューム情報の同期を取ります。このサブコマンドは、デバイスグループにボリュームを追加または削除した場合、あるいは、任意のボリューム属性 (たとえば、所有者、グループ、またはアクセス権) を変更したときに必ず使用してください。
.sp
Hitachi TrueCopy データレプリケーションを使用するディスクがディスクグループに含まれているときに、レプリケーション構成を変更した場合、\fBsync\fR サブコマンドを使用して、新しいレプリケーション構成とデバイスグループ構成の同期をとってください。ディスクグループのディスクはすべて、複製されるか複製されないかのどちらかであり、両方を組み合わせることはできません。
.sp
VxVM 共有ディスクグループには、\fBcldevicegroup\fR コマンドは使用できません。
.RE

.RE

.sp
.ne 2
.mk
.na
\fB\fB-V\fR\fR
.ad
.br
.na
\fB\fB--version\fR\fR
.ad
.sp .6
.RS 4n
コマンドのバージョンを表示します。
.sp
このオプションには、サブコマンドやオペランドなどのオプションは指定しないでください。サブコマンドやオペランドなどのオプションは無視されます。\fB-V\fR オプションは、コマンドのバージョンを表示するだけです。その他の処理は行いません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB-v\fR\fR
.ad
.br
.na
\fB\fB--verbose\fR\fR
.ad
.sp .6
.RS 4n
詳細なメッセージを標準出力に表示します。
.sp
このオプションは、このコマンドの任意の形式に使用できます。
.RE

.SH オペランド
.sp
.LP
次のオペランドを指定できます。
.sp
.ne 2
.mk
.na
\fB\fIdevicegroup\fR\fR
.ad
.RS 20n
.rt  
デバイスグループを指定します。
.sp
\fBcldevicegroup\fR コマンドは、Sun Cluster デバイスグループ名だけをオペランドとして受け付けます。複数のデバイスグループ名を受け付けるほとんどの形式のコマンドの場合、プラス記号 (\fB+\fR) を使用するとすべての可能なデバイスグループを指定できます。
.LP
注 - 
.sp
.RS 2
\fB+\fR オペランドは、手動で作成されたデバイスグループだけを含み、\fBautogen\fR プロパティーに \fBtrue\fR が設定されている自動的に作成されたデバイスグループをすべて無視します。Sun Cluster ソフトウェアは、システムが起動されるたびに、このようなデバイスグループを自動的に作成します。これらの「非表示の」デバイスグループにコマンドを適用するには、各デバイスグループを明示的に指定する必要があります。
.RE
.RE

.SH 終了ステータス
.sp
.LP
このコマンドセットにあるすべてのコマンドの終了ステータスコードの完全なリストについては、Intro(1CL) のマニュアルページを参照してください。
.sp
.LP
指定したすべてのオペランドでコマンドが成功すると、コマンドはゼロ (\fBCL_NOERR\fR) を返します。あるオペランドでエラーが発生すると、コマンドはオペランドリストの次のオペランドを処理します。戻り値は常に、最初に発生したエラーを反映します。
.sp
.LP
このコマンドは、次の終了ステータスコードを返します。
.sp
.ne 2
.mk
.na
\fB\fB0\fR \fBCL_NOERR\fR\fR
.ad
.sp .6
.RS 4n
エラーなし
.RE

.sp
.ne 2
.mk
.na
\fB\fB1\fR \fBCL_ENOMEM\fR\fR
.ad
.sp .6
.RS 4n
十分なスワップ空間がありません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB3\fR \fBCL_EINVAL\fR\fR
.ad
.sp .6
.RS 4n
無効な引数
.RE

.sp
.ne 2
.mk
.na
\fB\fB6\fR \fBCL_EACCESS\fR\fR
.ad
.sp .6
.RS 4n
アクセス権がありません
.RE

.sp
.ne 2
.mk
.na
\fB\fB35\fR \fBCL_EIO\fR\fR
.ad
.sp .6
.RS 4n
I/O error
.RE

.sp
.ne 2
.mk
.na
\fB\fB36\fR \fBCL_ENOENT\fR\fR
.ad
.sp .6
.RS 4n
そのようなオブジェクトはありません。
.RE

.sp
.ne 2
.mk
.na
\fB\fB39\fR \fBCL_EEXIST\fR\fR
.ad
.sp .6
.RS 4n
オブジェクトは存在します。
.RE

.SH 使用例
.LP
\fB例 1 \fRVxVM デバイスグループの作成
.sp
.LP
次の例では、VxVM デバイスグループ \fBvxdevgrp1\fR を希望ノードリスト \fBphys-schost-1,phys-schost-2,phys-schost-3\fR とともに作成して、\fBfailback\fR プロパティーを \fBtrue\fR に設定する方法を示します。

.sp
.in +2
.nf
# \fBcldevicegroup create -t vxvm -n phys-schost-1,phys-schost-2,phys-schost-3\fR \\
\fB-p failback=true vxdevgrp1\fR
.fi
.in -2
.sp

.LP
\fB例 2 \fRデバイスグループの変更
.sp
.LP
次の例では、デバイスグループ \fBdevgrp1\fR の \fBpreference\fR プロパティーを \fBtrue\fR に設定して、\fBnumsecondaries\fR プロパティーを \fB2\fR に設定する方法を示します。このコマンドはまた、希望ノードリスト \fBphys-schost-1,phys-schost-2,phys-schost-3\fR も指定しています。

.sp
.in +2
.nf
# \fBcldevicegroup set -p preferenced=true -p numsecondaries=2\fR \\
\fB-n phys-schost-1,phys-schost-2,phys-schost-3 devgrp1\fR
.fi
.in -2
.sp

.LP
\fB例 3 \fRraw ディスクデバイスグループの変更
.sp
.LP
次の例では、既存の raw ディスクデバイスグループ \fBrawdevgrp1\fR を変更する方法を示します。このコマンドは、デバイス \fBd3\fR および \fBd4\fR を新しいメンバーデバイスリストに指定して、\fBlocalonly\fR 属性を \fBtrue\fR に設定します。ノード \fBphys-schost-1\fR は、ローカル専用 raw ディスクデバイスグループで許可される唯一の一次ノードです。

.sp
.in +2
.nf
# \fBcldevicegroup set -d d3,d4 \\
-p localonly=true -n phys-schost-1 rawdevgrp1\fR
.fi
.in -2
.sp

.LP
\fB例 4 \fRデバイスグループの \fBnumsecondaries\fR 属性のリセット
.sp
.LP
次の例では、デバイスグループ \fBdevgrp1\fR の \fBnumsecondaries\fR 属性に何も値を指定しないことによって、適切なシステムデフォルト値にリセットする方法を示します。

.sp
.in +2
.nf
# \fBcldevicegroup set -p numsecondaries= devgrp1\fR
.fi
.in -2
.sp

.LP
\fB例 5 \fRデバイスグループのスイッチオーバー
.sp
.LP
次の例では、デバイスグループ \fBdevgrp1\fR を新しいマスターノード \fBphys-schost-2\fR に切り替える方法を示します。

.sp
.in +2
.nf
# \fBcldevicegroup switch -n phys-schost-2 devgrp1\fR
.fi
.in -2
.sp

.LP
\fB例 6 \fRデバイスグループの無効化
.sp
.LP
次の例では、デバイスグループ \fBdevgrp1\fR を無効にする方法を示します。

.sp
.in +2
.nf
# \fBcldevicegroup disable devgrp1\fR
.fi
.in -2
.sp

.LP
\fB例 7 \fRデバイスグループのオフライン
.sp
.LP
次の例では、デバイスグループ \fBdevgrp1\fR を無効にして、オフラインにする方法を示します。

.sp
.in +2
.nf
# \fBcldevicegroup disable devgrp1\fR
# \fBcldevicegroup offline devgrp1\fR
.fi
.in -2
.sp

.LP
\fB例 8 \fRデバイスグループのその主ノードでのオンライン化
.sp
.LP
次の例では、デバイスグループ \fBdevgrp1\fR をそのデフォルトの主ノードでオンラインにする方法を示します。このコマンドはまず、デバイスグループを有効にします。

.sp
.in +2
.nf
# \fBcldevicegroup online -e devgrp1\fR
.fi
.in -2
.sp

.LP
\fB例 9 \fRデバイスグループの指定されたノードでのオンライン化
.sp
.LP
次の例では、デバイスグループ \fBdevgrp1\fR を新しい主ノードとして指定された \fBphys-schost-2\fR でオンラインにする方法を示します。

.sp
.in +2
.nf
# \fBcldevicegroup switch -n phys-schost-2 devgrp1\fR
.fi
.in -2
.sp

.LP
\fB例 10 \fR新しいノードのデバイスグループへの追加
.sp
.LP
次の例では、新しいノード \fBphys-schost-3\fR をデバイスグループ \fBdevgrp1\fR に追加する方法を示します。このデバイスグループは、デバイスグループタイプ \fBsvm\fR ではありません。

.sp
.in +2
.nf
# \fBcldevicegroup add-node -n phys-schost-3 devgrp1\fR
.fi
.in -2
.sp

.LP
\fB例 11 \fRデバイスグループの削除
.sp
.LP
次の例では、デバイスグループ \fBdevgrp1\fR を Sun Cluster 構成から削除する方法を示します。このデバイスグループは、デバイスグループタイプ \fBsvm\fR ではありません。

.sp
.in +2
.nf
# \fBcldevicegroup delete devgrp1\fR
.fi
.in -2
.sp

.LP
\fB例 12 \fR複製情報とデバイスグループ構成の同期化
.sp
.LP
次の例では、デバイスグループ \fBdevgrp1\fR のディスクが使用するレプリケーション構成を Sun Cluster ソフトウェアに認識させる方法を示します。

.sp
.in +2
.nf
# \fBcldevicegroup sync devgrp1\fR
.fi
.in -2
.sp

.SH 属性
.sp
.LP
次の属性については、\fBattributes\fR(5)を参照してください。
.sp

.sp
.TS
tab() box;
cw(2.75i) |cw(2.75i) 
lw(2.75i) |lw(2.75i) 
.
属性タイプ属性値
_
使用可能SUNWsczu
_
インタフェースの安定性発展中
.TE

.SH 関連項目
.sp
.LP
Intro(1CL)、cldevice(1CL)、cluster(1CL)、\fBmetaset\fR(1M)、clconfiguration(5CL)、\fBrbac\fR(5)、did(7)
.sp
.LP
\fI『Sun Cluster System Administration Guide for Solaris OS 』\fR
.SH 注意事項
.sp
.LP
スーパーユーザーはこのコマンドのすべての形式を実行できます。
.sp
.LP
また、任意のユーザーは次のオプションを指定してこのコマンドを実行できます。
.RS +4
.TP
.ie t \(bu
.el o
\fB-?\fR (ヘルプ) オプション
.RE
.RS +4
.TP
.ie t \(bu
.el o
\fB-V\fR (バージョン) オプション
.RE
.sp
.LP
スーパーユーザー以外のユーザーがほかのサブコマンドを指定してこのコマンドを実行するには、RBAC の承認が必要です。次の表を参照してください。
.sp

.sp
.TS
tab() box;
cw(0i) |cw(5.5i) 
lw(0i) |lw(5.5i) 
.
サブコマンドRBAC の承認
_
\fBadd-device\fR\fBsolaris.cluster.modify\fR
_
\fBadd-node\fR\fBsolaris.cluster.modify\fR
_
\fBcreate\fR\fBsolaris.cluster.modify\fR
_
\fBdelete\fR\fBsolaris.cluster.modify\fR
_
\fBdisable\fR\fBsolaris.cluster.modify\fR
_
\fBenable\fR\fBsolaris.cluster.modify\fR
_
\fBexport\fR\fBsolaris.cluster.read\fR
_
\fBlist\fR\fBsolaris.cluster.read\fR
_
\fBoffline\fR\fBsolaris.cluster.admin\fR
_
\fBonline\fR\fBsolaris.cluster.admin\fR
_
\fBremove-device\fR\fBsolaris.cluster.modify\fR
_
\fBremove-node\fR\fBsolaris.cluster.modify\fR
_
\fBset\fR\fBsolaris.cluster.modify\fR
_
\fBshow\fR\fBsolaris.cluster.read\fR
_
\fBstatus\fR\fBsolaris.cluster.read\fR
_
\fBswitch\fR\fBsolaris.cluster.modify\fR
_
\fBsync\fR\fBsolaris.cluster.admin\fR
.TE

