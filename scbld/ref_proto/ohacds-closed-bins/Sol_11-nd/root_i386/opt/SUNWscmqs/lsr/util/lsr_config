#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident   "@(#)lsr_config 1.3     07/08/07 SMI"
# 
# This file will be sourced in by lsr_register and the parameters
# listed below will be used.
#
# These parameters can be customized in (key=value) form
#
#	+++ Required parameters +++
#
#      RS - name of the SC resource for the Listener
#      RG - name of the SC resource group to contain RS
#    QMGR - name of the Queue Manager
#    PORT - name of the Queue Manager port number
#           For multiple ports entries, enter port numbers
#	    separated by a "/", e.g. PORT=1414/1415/1416 
#  IPADDR - name of the IP address for the Listener
#	    (WebSphere MQ v6 onwards, refer to the runmqlsr(1)
#	    man page for additional information)
# BACKLOG - tcp/ip backlog number (100)
#      LH - name of the SC LogicalHostname resource
# QMGR_RS - name of the SC Queue Manager resource
#	    (Leave blank if the Listener should not 
#           be dependent on the Queue Manager)
#  USERID - name of the WebSphere MQ userid 
#           (Default USERID=mqm)
#
#       +++ Non-global (failover) zone parameters +++
#       +++ Only required if WebSphere MQ should  +++
#       +++ run within a Solaris 10 failover zone +++
#
# RS_ZONE - name of the non-global (failover) zone managed by the
#           Sun Cluster Data Service for Solaris Containers
# PROJECT - name for the Solaris Project to be used for this resource
#           (Default PROJECT=default)
#
# Example 1 - Configuration parameters for deployment of WebSphere MQ
#               within Solaris 10 global zone nodes or pre-Solaris 10 nodes.
#
#       +++ Required parameters +++
#       RS=wmq1-lsr
#       RG=wmq1-rg
#       QMGR=qmgr1
#       PORT=1414/1415/1416
#       IPADDR=192.168.1.100
#       BACKLOG=100
#       LH=wmq1-lh
#	QMGR_RS=
#       USERID=mqm
#
#       +++ Failover zone parameters +++
#       RS_ZONE=
#       PROJECT=default
#
# Example 2 - Configuration parameters for deployment of WebSphere MQ
#               within a Solaris 10 non-global (failover) zone.
#
#       +++ Required parameters +++
#       RS=zone1-lsr
#       RG=zone1-rg
#       QMGR=qmgr1
#       PORT=1414/1415/1416
#       IPADDR=192.168.1.100
#       BACKLOG=100
#       LH=zone1-lh
#	QMGR_RS=zone1-qmgr1
#       USERID=mqm
#
#       +++ Failover zone parameters +++
#       RS_ZONE=zone1
#       PROJECT=default

# +++ Required parameters +++
RS=
RG=
QMGR=
PORT=
IPADDR=
BACKLOG=100
LH=
QMGR_RS=
USERID=mqm

# +++ Failover zone parameters +++
# These parameters are only required when WebSphere MQ should run
#  within a failover zone managed by the Sun Cluster Data Service
# for Solaris Containers.
RS_ZONE=
PROJECT=default
