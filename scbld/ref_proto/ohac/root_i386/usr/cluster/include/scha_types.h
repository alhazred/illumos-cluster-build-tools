/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCHA_TYPES_H_
#define	_SCHA_TYPES_H_

#pragma ident	"@(#)scha_types.h	1.21	08/05/20 SMI"

#ifdef linux
#include <sys/os_compat.h>
#endif
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 *	Possible return values for operation tags:
 *	SCHA_ON_OFF_SWITCH, SCHA_MONITORED_SWITCH
 */
typedef enum scha_switch {
	SCHA_SWITCH_DISABLED = 0,
	SCHA_SWITCH_ENABLED
} scha_switch_t;

/*
 *	Possible return values for operation tags:
 *	SCHA_STATUS, SCHA_STATUS_NODE
 */
typedef enum scha_rsstatus {
	SCHA_RSSTATUS_OK = 0,
	SCHA_RSSTATUS_OFFLINE,
	SCHA_RSSTATUS_FAULTED,
	SCHA_RSSTATUS_DEGRADED,
	SCHA_RSSTATUS_UNKNOWN
} scha_rsstatus_t;

/*
 *	Possible return values for operation tags:
 *	SCHA_RESOURCE_STATE, SCHA_RESOURCE_STATE_NODE
 */
typedef enum scha_rsstate {
	SCHA_RSSTATE_ONLINE = 0,
	SCHA_RSSTATE_OFFLINE,
	SCHA_RSSTATE_START_FAILED,
	SCHA_RSSTATE_STOP_FAILED,
	SCHA_RSSTATE_MONITOR_FAILED,
	SCHA_RSSTATE_ONLINE_NOT_MONITORED,
	SCHA_RSSTATE_STARTING,
	SCHA_RSSTATE_STOPPING,
	SCHA_RSSTATE_DETACHED	/* reserved for future use */
} scha_rsstate_t;

/*
 *	Possible return values for operation tags:
 *	SCHA_RG_STATE, SCHA_RG_STATE_NODE
 */
typedef enum scha_rgstate {
	SCHA_RGSTATE_UNMANAGED = 0,
	SCHA_RGSTATE_ONLINE,
	SCHA_RGSTATE_OFFLINE,
	SCHA_RGSTATE_PENDING_ONLINE,
	SCHA_RGSTATE_PENDING_OFFLINE,
	SCHA_RGSTATE_ERROR_STOP_FAILED,
	SCHA_RGSTATE_ONLINE_FAULTED,
	SCHA_RGSTATE_PENDING_ONLINE_BLOCKED
} scha_rgstate_t;

/*
 *	Possible return values for operation tags:
 *	SCHA_RG_MODE
 */
typedef enum scha_rgmode {
	RGMODE_NONE = 0,	/* NOT intended for public use */
	RGMODE_FAILOVER,
	RGMODE_SCALABLE
} scha_rgmode_t;

/*
 *	Possible return values for operation tags:
 *	SCHA_FAILOVER_MODE
 */
typedef enum scha_failover_mode {
	SCHA_FOMODE_NONE = 0,
	SCHA_FOMODE_HARD,
	SCHA_FOMODE_SOFT,
	SCHA_FOMODE_RESTART_ONLY,
	SCHA_FOMODE_LOG_ONLY
} scha_failover_mode_t;

/*
 *	Possible return values for operation tags:
 *	SCHA_INIT_NODES
 */
typedef enum scha_initnodes_flag {
	SCHA_INFLAG_RG_PRIMARIES = 0,
	SCHA_INFLAG_RT_INSTALLED_NODES
} scha_initnodes_flag_t;

/*
 *	Possible return values for operation tags:
 *	SCHA_NODESTATE_NODE, SCHA_NODESTATE_LOCAL
 */
typedef enum scha_node_state {
	SCHA_NODE_UP = 0,
	SCHA_NODE_DOWN
} scha_node_state_t;

/*
 *	Possible return values for the 'prop_type' field in
 *	'scha_extprop_values' structure defined below
 */
typedef enum scha_prop_type {
	SCHA_PTYPE_STRING = 0,
	SCHA_PTYPE_INT,
	SCHA_PTYPE_BOOLEAN,
	SCHA_PTYPE_ENUM,
	SCHA_PTYPE_STRINGARRAY,
	SCHA_PTYPE_UINTARRAY,
	SCHA_PTYPE_UINT
} scha_prop_type_t;

/*
 * scha_str_array_t
 *	This structure is used for those function tags where the type of the
 *	return value is either a string or an array of strings.
 */
typedef struct scha_str_array {
	uint_t		array_cnt;	/* number of elements in the array */
	boolean_t	is_ALL_value;	/* true if ALL(*) value is specified */
	char		**str_array;	/* pointer to an array of */
					/* NULL terminated strings */
} scha_str_array_t;

/*
 * scha_uint_array_t
 *	This structure is used for those function tags where the type of the
 *	return value is an array of integers.
 */
typedef struct scha_uint_array {
	uint_t	array_cnt;		/* number of elements in the array */
	uint_t	*uint_array;		/* pointer to an array of integers */
} scha_uint_array_t;

/*
 * scha_status_value_t
 *
 *	This structure is used for storing the return value(s) of the resource
 *	status.
 */
typedef struct scha_status_value {
	scha_rsstatus_t		status;		/* resource status */
	char			*status_msg;	/* resource status message */
} scha_status_value_t;

/*
 * scha_extprop_value_t
 *	This structure is used for storing the return values of an
 *	extension property.
 */
typedef struct scha_extprop_value {
	scha_prop_type_t	prop_type;	/* property type */
	union {
		char		*val_str;	/* for SCHA_STRING type */
		int		val_int;	/* for SCHA_INT type */
		char		*val_enum; 	/* for SCHA_ENUM type */
		boolean_t	val_boolean;	/* for SCHA_BOOLEAN type */
		scha_str_array_t	*val_strarray;
						/* for SCHA_STRINGARRAY type */
	} val;
} scha_extprop_value_t;


typedef void * scha_resource_t;
typedef void * scha_resourcetype_t;
typedef void * scha_resourcegroup_t;
typedef void * scha_cluster_t;


#ifdef __cplusplus
}
#endif

#endif	/* _SCHA_TYPES_H_ */
