<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)DiskTablePagelet.jsp	1.3	08/05/20 SMI"
 */
--%>

<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<jato:pagelet>
<script type="text/javascript" src="/com_sun_web_ui/js/browserVersion.js">

</script>

<SCRIPT LANGUAGE="JavaScript">
    
function getHiddenValue(){

    var prefix, diskTableComp, ej;
    // Element name (prefix) of selection checkbox.
    var elementName = "<cc:text name='SelectionNameText'/>";

    // Document form.
    var form = document.scForm;

    for (i = 0; i < form.elements.length; i++) {
        var e = form.elements[i];
        if (e.name.indexOf(elementName) != -1) {
	    if (e.checked) {
                prefix =  elementName.substring(0,elementName.indexOf("StatusTable.SelectionRadiobutton"));
                diskTableComp = prefix + "TableTiledView[" + e.value + "].HiddenDiskStatus";
                ej = form.elements[diskTableComp];
                return ej.value;
            }
        }
     }
}
   
// This function will toggle the disabled state of action buttons
// depending on single or multiple selections.
function toggleNodeTblBtnsDisabledState() {

    // Action button.
    var disableButton = "<cc:text name='DisableButtonNameText'/>";
    var enableButton = "<cc:text name='EnableButtonNameText'/>";
    
    var values = getHiddenValue().split("#");
    var operation = values[0];
    
    if (operation == 'enable'){
        // Toggle action button disabled state.
        ccSetButtonDisabled(disableButton, "scForm", true);
        ccSetButtonDisabled(enableButton, "scForm", false);
    }
    if (operation == 'disable'){
        // Toggle action button disabled state.
        ccSetButtonDisabled(disableButton, "scForm", false);
        ccSetButtonDisabled(enableButton, "scForm", true);
    }
}
    
 function displayDiskConfirmation() {
 
  var msg = ''; 

  var values = getHiddenValue().split("#");
  operation = values[0];
  var node = values[1];
  var name = values[2];
  
  var enableMonitoring = '<cc:text name="StaticText" defaultValue="diskStatus.enable.confirmation" bundleID="scBundle"/>';

  var disableMonitoring = '<cc:text name="StaticText" defaultValue="diskStatus.disable.confirmation" bundleID="scBundle"/>';

  var header;
  if (operation == 'enable') {
    header = enableMonitoring;
  } else if (operation == 'disable') {
    header = disableMonitoring;
  }

  msg = header.replace('{0}', name);
  msg = msg.replace('{1}', node);

   return confirm(msg + '\n <cc:text name="StaticText" defaultValue="selectionConfirmation" bundleID="scBundle"/>');
 }
</SCRIPT>

<!-- Action Table -->
<cc:actiontable
name="StatusTable"
bundleID="scBundle"
title="diskStatus.title"
summary="diskStatus.information"
empty="diskStatus.emptyTable"
selectionJavascript="setTimeout('toggleNodeTblBtnsDisabledState()', 0)"
selectionType="single"
showAdvancedSortingIcon="false"
showLowerActions="false"
showPaginationControls="true"
showPaginationIcon="true"
showSelectionIcons="true"
showSortingRow="true"
maxRows="10"
page="1"/> 

</jato:pagelet>
