<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Banner.jsp	1.7	08/05/20 SMI"
 */
--%>

<%@page info="Banner" language="java"%>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<jato:useViewBean className="com.sun.cluster.spm.banner.BannerViewBean">

<!-- Define the resource bundle, html, head, meta, stylesheet and body tags -->
<cc:header pageTitle="banner.title" copyrightYear="2002"
 baseName="com.sun.cluster.spm.resources.Resources" bundleID="testBundle"
 onLoad="clearCookies(); checkJava();" >

<SCRIPT LANGUAGE="JavaScript">
  function refresh() {
     if (parent && parent.content && parent.content.reloadPage) {
       parent.content.reloadPage();
     }
  }

  function refreshTree() {
     if (parent && parent.menu && parent.menu.reloadPage) {
       parent.menu.reloadPage();
     }
  }

  function checkJava() {
     if (!navigator.javaEnabled() && parent.menu && parent.content) {
       parent.menu.location="/SunClusterManager/tree/Tree" ;
       parent.content.location="/SunClusterManager/cluster/ClusterStatus?FromTree=yes" ;
     }
  }
  
  // This function will clear any unwanted cookies which are set in lockhart
  function clearCookies() {
    
    var date = new Date();
    
    date.setTime(date.getTime()+(-1*24*60*60*1000));    
    document.cookie = "cctree_Tree_Tree=; expires=" + date.toGMTString() + "; path=/SunClusterManager/tree/";
  }
  
</SCRIPT>

<!-- Masthead -->
<cc:primarymasthead name="Masthead" bundleID="testBundle" />

<jato:content name="clusterStatusApplet" />

</cc:header>
</jato:useViewBean>
