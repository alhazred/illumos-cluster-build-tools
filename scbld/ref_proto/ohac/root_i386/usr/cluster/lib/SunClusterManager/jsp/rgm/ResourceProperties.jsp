<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/**
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceProperties.jsp	1.10	08/07/14 SMI"
 */
--%>
<%@ page language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.rgm.ResourcePropertiesViewBean">


    <!-- Page header component -->
    <spm:header pageTitle="rgm.resource.title" copyrightYear="2003"
        baseName="com.sun.cluster.spm.resources.Resources"
        bundleID="scBundle"
        event="" 
	tableTitle="rgm.resource.props.table.system.summary,rgm.resource.props.table.timeout.summary,rgm.resource.props.table.ext.summary,rgm.resource.props.table.perNodeExt.summary" >

	<jato:containerView name="EditableScript">
	  <jsp:include page="../edit/EditablePagelet.jsp"/>
	</jato:containerView>

        <jato:form name="scForm" method="post">

            <!-- Bread Crumb component -->
            <cc:breadcrumbs name="BreadCrumb" bundleID="scBundle" />

            <!-- Navigation Tabs -->
            <cc:tabs name="GenericTabs" bundleID="scBundle" />

            <!-- Alert Inline for command result -->
            <spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

            <!-- Page Title -->
            <cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
                pageTitleText="rgm.resource.props.pageTitle.title" 
                showPageTitleSeparator="true"
                pageTitleHelpMessage="rgm.resource.props.pageTitle.help">

                <cc:hidden name="Hidden"/>

                <SCRIPT LANGUAGE="JavaScript">
/*
                selectionJavascript="setTimeout('printPernodeValues()', 0)"                
                
                    //Print per-node properties value. 
                    function printPernodeValues() {
                    var pernodeValue = getHiddenFieldValue("HiddenValName");
                    var values;
                    var valArray = new Array();    
                    var propArray = pernodeValue.split('%');
                    document.write('<table>');
                    for (var i =0; i < propArray.length; i++) {
                    document.write('<tr>');
                    valArray = propArray[i].split("@");
                    document.write('<td>' + valArray[0] + '</td>');
                    document.write('<td>');
                    values = valArray[1].split('$');
                    for (var j=0; j < values.length; j++) {
                    document.write('<tr>');
                    document.write('<td>' + values[j] + '</td>');
                    document.write('</tr>');
                    }
                    document.write('</td>');
                    document.write('<tr>');
                    }
                    document.write('</table>');
                    }
                    */
                </SCRIPT>

                <!-- Action Buttons -->
                <spm:rbac auths="solaris.cluster.modify">
                    <BR>
                    <div class="ConMgn"> 
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <cc:wizardwindow name="ResourceWizard" bundleID="scBundle"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                </spm:rbac>

                <div class="ConMgn"> 
                    <table border="0" cellpadding="0" cellspacing="0">
                        <!-- Resource Name  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="NameLabel" bundleID="scBundle"
                                    defaultValue="rgm.resource.label.name"
                                    styleLevel="2"
                                    elementName="NameValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:text name="NameValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>

                        <!-- Resource Enabled flag  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="EnabledLabel" bundleID="scBundle"
                                    defaultValue="rgm.resource.label.enabled"
                                    styleLevel="2"
                                    elementName="EnabledValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:text name="EnabledValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>

                        <!-- Resource Status  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="StatusLabel" bundleID="scBundle"
                                    defaultValue="rgm.resource.label.status"
                                    styleLevel="2"
                                    elementName="StatusValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:alarm name="StatusAlarm" bundleID="scBundle"/>
                                        <cc:text name="StatusValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>

                        <!-- Resource Monitored flag  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="MonitoredLabel" bundleID="scBundle"
                                    defaultValue="rgm.resource.label.monitored"
                                    styleLevel="2"
                                    elementName="MonitoredValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:text name="MonitoredValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div><img src="/com_sun_web_ui/images/other/dot.gif"
                width="1" height="10" alt=""></div>

                <!-- Action Table for system properties -->
                <cc:actiontable name="SystemTable"
                bundleID="scBundle"
                title="rgm.resource.props.table.system.title"
                summary="rgm.resource.props.table.system.summary"
                empty="rgm.resource.props.table.system.empty"
                rowSelectionType="none"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="false"
                showSortingRow="true"
                page="1"/>

                <BR>
  
                <!-- Action Table for time-out properties -->
                <cc:actiontable name="TimeoutTable"
                bundleID="scBundle"
                title="rgm.resource.props.table.timeout.title"
                summary="rgm.resource.props.table.timeout.summary"
                empty="rgm.resource.props.table.timeout.empty"
                rowSelectionType="none"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="false"
                showSortingRow="false"
                page="1"/>

                <BR>
  
                <!-- Action Table for extended properties -->
                <cc:actiontable name="ExtTable"
                bundleID="scBundle"
                title="rgm.resource.props.table.ext.title"
                summary="rgm.resource.props.table.ext.summary"
                empty="rgm.resource.props.table.ext.empty"
                rowSelectionType="none"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="false"
                showSortingRow="false"
                page="1"/>

                <BR>

                <!-- Action Table for Per-node extended properties -->
                <cc:actiontable name="PernodeExtTable"
                bundleID="scBundle"
                title="rgm.resource.props.table.perNodeExt.title"
                summary="rgm.resource.props.table.perNodeExt.summary"
                empty="rgm.resource.props.table.perNodeExt.empty"                
                rowSelectionType="none"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="false"
                showSortingRow="false"
                page="1"/>

            </cc:pagetitle>
        </jato:form>
    </spm:header>
</jato:useViewBean>
