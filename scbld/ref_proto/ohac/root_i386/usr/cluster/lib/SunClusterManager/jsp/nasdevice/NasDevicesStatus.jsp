<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)NasDevicesStatus.jsp	1.4	08/05/20 SMI"
 */
--%>

<%@page info="NasDevicesStatus" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className=
                  "com.sun.cluster.spm.nasdevice.NasDevicesStatusViewBean">

<spm:header pageTitle="NASDevices.pagetitle" 
           copyrightYear="2005" 
           baseName="com.sun.cluster.spm.resources.Resources" 
           bundleID="scBundle" onLoad="reloadTree()"
           event="ESC_cluster_dcs_primaries_changing,ESC_cluster_dcs_config_change,ESC_cluster_dcs_state_change,ESC_cluster_cofig_change,ESC_cluster_dpm_">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<!-- Status Alert</div>t -->
<spm:alertinline name="StatusAlert" bundleID="testBundle" />
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle" 
 pageTitleText="NASDevices.pagetitle" 
 showPageTitleSeparator="true" 
 pageTitleHelpMessage="NASDevices.help"/>

<BR>

<!-- Action Table -->
<cc:actiontable name="NasDevicesTable"
bundleID="scBundle"
title="NASDevices.table.title"
summary="NASDevices.table.summary"
empty="NASDevices.table.empty"
rowSelectionType="none"
showAdvancedSortingIcon="false"
showLowerActions="false"
showPaginationControls="false"
showPaginationIcon="false"
showSelectionIcons="false"
showSortingRow="true"
maxRows="10"
page="1"/>

</jato:form>
</spm:header>
</jato:useViewBean>


