<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/**
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceStatus.jsp	1.8	08/07/14 SMI"
 */
--%>
<%@ page language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.rgm.ResourceStatusViewBean">

    <!-- Page header component -->
    <spm:header pageTitle="rgm.resource.title" copyrightYear="2003"
        baseName="com.sun.cluster.spm.resources.Resources"
        bundleID="scBundle"
        onLoad="reloadTree()"
        event="ESC_cluster_r_,ESC_cluster_fm">

        <jato:form name="scForm" method="post">

            <!-- Hidden Field for the tree -->
            <cc:hidden name="TreeNodeHiddenField"/>

            <!-- Bread Crumb component -->
            <cc:breadcrumbs name="BreadCrumb" bundleID="scBundle" />

            <!-- Navigation Tabs -->
            <cc:tabs name="GenericTabs" bundleID="scBundle" />

            <!-- Alert Inline for command result -->
            <spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

            <!-- Page Title -->
            <cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
                pageTitleText="rgm.resource.status.pageTitle.title" 
                showPageTitleSeparator="true"
                pageTitleHelpMessage="rgm.resource.status.pageTitle.help">
              
                <SCRIPT LANGUAGE="JavaScript">

                    function getHiddenFieldValue(hiddenTextFieldName) {
     
                    // Element name (prefix) of selection checkbox.
                    var elementName = "<cc:text name='SelectionNameText'/>";

                    // Document form.
                    var form = document.scForm;
                    
                    var nameItem, ItemNumIndex, selectedRow, prefix, resComp, ej = null, retValue = null;
                    var sValue, s11, s12;
                    var countChecked = 0;
                    var enabled = "<%=com.sun.cluster.common.ClusterState.CLSTATE_ENABLED%>";
                    var disabled = "<%=com.sun.cluster.common.ClusterState.CLSTATE_DISABLED%>";
                    var monitored = "<%=com.sun.cluster.common.ClusterState.CLSTATE_MONITORED%>";
                    var nMonitored = "<%=com.sun.cluster.common.ClusterState.CLSTATE_NOTMONITORED%>";
                    
                    var sEnabled = false, sDisabled = false, sMonitored = false, sNMonitored = false;
    
                    for (i = 0; i < form.elements.length; i++) {
                        var e = form.elements[i];
                        if ((e.name.indexOf(elementName) != -1)&&(e.type=="checkbox")) {
                            if (e.checked) {
                                countChecked = countChecked + 1;
                                nameItem = e.name;
                                ItemNumIndex = nameItem.lastIndexOf("Checkbox")+8;
                                selectedRow = nameItem.substring(ItemNumIndex, nameItem.length);
                                prefix =  elementName.substring(0,elementName.indexOf("NodesTable.SelectionCheckbox"));
                                resComp = prefix + "TableTiledView[" + selectedRow + "]." + hiddenTextFieldName;
                                ej = form.elements[resComp];
                                if (hiddenTextFieldName == 'HiddenNodeName'){
                                    if (retValue == null)
                                        retValue = ej.value;
                                    else 
                                        retValue = retValue + ", " + ej.value;
                                } else if (hiddenTextFieldName == 'HiddenNodeBtnState') {
                                    sValue =  ej.value; 
                                    s11 = sValue.substring(0, 1);
                                    if (s11 == enabled) sEnabled = true;
                                    if (s11 == disabled) sDisabled = true;                        
                                    s12 = sValue.substring(1, 2);
                                    if (s12 == monitored) sMonitored = true;
                                    if (s12 == nMonitored) sNMonitored = true;

				    if (retValue == null)
                                        retValue = ej.value;
                                    else
                                        retValue = retValue + ", " + ej.value;
                                }
                            }
                        }
                    }
                    
                    if (countChecked==0) {
			return '*';
		    }

                    if (hiddenTextFieldName == 'HiddenNodeBtnState'){
                        if ((sEnabled) && (!sDisabled))
                            retValue = "1";
                        else if ((!sEnabled) && (sDisabled))                            
                            retValue = "0";
                        else 
                            retValue = "2";

                        if ((sMonitored) && (!sNMonitored))
                            retValue = retValue + "1";
                        else if ((!sMonitored) && (sNMonitored))                            
                            retValue = retValue + "0";
                        else 
                            retValue = retValue + "2";
                        
                        if (countChecked > 1) 
                            retValue = retValue + "0";
                        else 
                            retValue = retValue + "1";
	
                    }                 
                    
                    return retValue;  
                    }

                    function displayConfirmation(operation) {
   
                        var msg = '';
			if (operation == 'pernodeEnable') {
				msg = '<cc:text name="StaticText" 
                        	defaultValue="rgm.resource.operation.pernodeEnable.confirm"
                        	bundleID="scBundle"/>';
  
                        } else if (operation == 'pernodeDisable') {
                            msg = '<cc:text name="StaticText" 
                            defaultValue="rgm.resource.operation.pernodeDisable.confirm"
                            bundleID="scBundle"/>';
  
                        } else if (operation == 'pernodeEnableMonitor') {
                            msg = '<cc:text name="StaticText" 
                            defaultValue="rgm.resource.operation.pernodeEnableMonitor.confirm"
                            bundleID="scBundle"/>';
  
                        } else if (operation == 'pernodeDisableMonitor') {
                            msg = '<cc:text name="StaticText" 
                            defaultValue="rgm.resource.operation.pernodeDisableMonitor.confirm"
                            bundleID="scBundle"/>';
                        }

                        msg = msg.replace('{0}', getHiddenFieldValue("HiddenNodeName"));
                        var result = confirm(msg + '\n <cc:text name="StaticText"
                        defaultValue="selectionConfirmation"
                        bundleID="scBundle"/>');
                        if (result) {
                            unRegisterEvents();
                        }  
                        return result;
                    }

                    // This function will toggle the disabled state of action buttons
                    // depending on single or multiple selections.
                    function toggleDisabledState() {
                    // Action button.
                        var pernodeDisableActionButton = "<cc:text name='PernodeDisableActionButtonNameText'/>";
                        var pernodeEnableActionButton = "<cc:text name='PernodeEnableActionButtonNameText'/>";
                        var pernodeDisableMonitorActionButton = "<cc:text name='PernodeDisableMonitorActionButtonNameText'/>";
                        var pernodeEnableMonitorActionButton = "<cc:text name='PernodeEnableMonitorActionButtonNameText'/>";
    
                        var btnValue = getHiddenFieldValue("HiddenNodeBtnState");
                        
                        if (btnValue != '*'){
                            if (btnValue.charAt(0)=='1'){
                                ccSetButtonDisabled(pernodeDisableActionButton, "scForm", false);
                                ccSetButtonDisabled(pernodeEnableActionButton, "scForm", true); 
                            } else if (btnValue.charAt(0)=='0'){
                                ccSetButtonDisabled(pernodeDisableActionButton, "scForm", true);
                                ccSetButtonDisabled(pernodeEnableActionButton, "scForm", false);
                            } else {
                               ccSetButtonDisabled(pernodeEnableActionButton, "scForm", true);
                               ccSetButtonDisabled(pernodeDisableActionButton, "scForm", true);                          
                            }
                            
                            if (btnValue.charAt(1)=='1'){
                                ccSetButtonDisabled(pernodeDisableMonitorActionButton, "scForm", false);
                                ccSetButtonDisabled(pernodeEnableMonitorActionButton, "scForm", true);
                            } else if (btnValue.charAt(1)=='0'){
                                ccSetButtonDisabled(pernodeDisableMonitorActionButton, "scForm", true);
                                ccSetButtonDisabled(pernodeEnableMonitorActionButton, "scForm", false);
                            } else {
                                ccSetButtonDisabled(pernodeEnableMonitorActionButton, "scForm", true);                            
                                ccSetButtonDisabled(pernodeDisableMonitorActionButton, "scForm", true);                            
                            }
                            return;
                        }     
    
                        // Toggle action button disabled state.
                        ccSetButtonDisabled(pernodeDisableActionButton, "scForm", true);
                        ccSetButtonDisabled(pernodeEnableActionButton, "scForm", true);
                        ccSetButtonDisabled(pernodeDisableMonitorActionButton, "scForm", true);
                        ccSetButtonDisabled(pernodeEnableMonitorActionButton, "scForm", true);
                    }	
                </SCRIPT>
              
                <div class="ConMgn"> 
                    <table border="0" cellpadding="0" cellspacing="0">
                        <!-- Resource Name  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="NameLabel" bundleID="scBundle"
                                    defaultValue="rgm.resource.label.name"
                                    styleLevel="2"
                                    elementName="NameValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:text name="NameValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>

                        <!-- Resource Enabled flag  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="EnabledLabel" bundleID="scBundle"
                                    defaultValue="rgm.resource.label.enabled"
                                    styleLevel="2"
                                    elementName="EnabledValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:text name="EnabledValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>

                        <!-- Resource Status  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="StatusLabel" bundleID="scBundle"
                                    defaultValue="rgm.resource.label.status"
                                    styleLevel="2"
                                    elementName="StatusValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:alarm name="StatusAlarm" bundleID="scBundle"/>
                                        <cc:text name="StatusValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>

                        <!-- Resource Monitored flag  -->
                        <tr>
                            <td valign="top">
                                <div class="ConTblCl1Div">&nbsp;
                                    <cc:label name="MonitoredLabel" bundleID="scBundle"
                                    defaultValue="rgm.resource.label.monitored"
                                    styleLevel="2"
                                    elementName="MonitoredValue" />
                                </div>
                            </td>
                            <td valign="top">
                                <div class="ConTblCl2Div">
                                    <span id="psLbl2" class="ConDefTxt">
                                        <cc:text name="MonitoredValue" bundleID="scBundle"/>
                                    </span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div><img src="/com_sun_web_ui/images/other/dot.gif"
                width="1" height="10" alt=""></div>

                <!-- Action Table for state per node -->
                <cc:actiontable name="NodesTable"
                bundleID="scBundle"
                title="rgm.resource.status.table.nodes.title"
                summary="rgm.resource.status.table.nodes.summary"
                empty="rgm.resource.status.table.nodes.empty"
                selectionJavascript="setTimeout('toggleDisabledState()', 0)"                
                selectionType="multiple"                
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="false"
                showSortingRow="true"
                page="1"/>

                <BR>
                <!-- Action Table for load balancing -->
                <jato:content name="LoadBalancingTable">
                    <cc:actiontable name="LoadBalancingTable"
                    bundleID="scBundle"
                    title="rgm.resource.status.table.loadBalancing.title"
                    summary="rgm.resource.status.table.loadBalancing.summary"
                    empty="rgm.resource.status.table.loadBalancing.empty"
                    rowSelectionType="none"
                    showAdvancedSortingIcon="false"
                    showLowerActions="false"
                    showPaginationControls="false"
                    showPaginationIcon="false"
                    showSelectionIcons="false"
                    showSortingRow="true"
                    page="1"/>

                    <BR>
                </jato:content>
  
                <!-- Action Table for resource dependencies -->
                <cc:actiontable name="DependenciesTable"
                bundleID="scBundle"
                title="rgm.resource.status.table.dependencies.title"
                summary="rgm.resource.status.table.dependencies.summary"
                empty="rgm.resource.status.table.dependencies.empty"
                rowSelectionType="none"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="false"
                showSortingRow="false"
                page="1"/>

            </cc:pagetitle>
        </jato:form>
    </spm:header>
</jato:useViewBean>
