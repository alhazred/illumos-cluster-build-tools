<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)PublicInterfacePagelet.jsp	1.9	08/05/20 SMI"
 */
--%>

<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>

<jato:pagelet>

<!-- Status Alert -->
<spm:alertinline name="StatusAlert" bundleID="scBundle" />

<!-- Command Alert -->
<spm:alertinline name="../CommandResultAlert" bundleID="scBundle" />

<!-- Selection Error Alert -->
<spm:alertinline name="../SelectionErrorAlert" bundleID="scBundle" />

<!-- PageTitle -->
<cc:pagetitle name="../GenericPageTitle" bundleID="scBundle"
  pageTitleText="title"
  showPageTitleSeparator="true"
  pageTitleHelpMessage="help">

 
<div><img src="/com_sun_web_ui/images/other/dot.gif" width="1" height="5" alt=""></div>

<!-- Summary  -->

<div class="ConMgn">
<table  border="0" cellpadding="0" cellspacing="0">
<tr>
  <td valign="top">
	<div class="ConTblCl1Div">&nbsp;
	<cc:label name="PublicInterfaceLabel" bundleID="scBundle"
         defaultValue="netif.publicInterface.label"/>
	</div>
   </td>
   <td valign="top">
	<div class="ConTblCl2Div">
        <span id="psLbl2" class="ConDefTxt">
	<cc:text name="PublicInterfaceValue" bundleID="scBundle"/>
	</span>
        </div>
   </td>
 </tr>
 <tr>
   <td valign="top">
	<div class="ConTblCl1Div">&nbsp;
	 <cc:label name="StatusLabel" bundleID="scBundle"
          defaultValue="netif.publicInterface.status.label"/>
	</div>
   </td>
   <td valign="top">
	<div class="ConTblCl2Div">
	<span id="psLbl2" class="ConDefTxt">
	<cc:alarm name="StatusAlarm"/>
	<cc:text name="StatusValue" bundleID="scBundle"/>
	</span>
        </div>
   </td>
 </tr>
</table>
</div>

<!-- in current implementation, if no node rights user see nothing -->
<spm:rbac auths="solaris.cluster.read">

<div><img src="/com_sun_web_ui/images/other/dot.gif" width="1" height="10" alt=""></div>

<jato:containerView name="TableView">

<!-- Action Table -->
<cc:actiontable
name="Table"
bundleID="scBundle"
title="netif.publicInterface.TableTitle"
summary="netif.publicInterface.TableSummary"
empty="netif.publicInterface.emptyTable"
selectionType="none"
showAdvancedSortingIcon="false"
showLowerActions="false"
showPaginationControls="false"
showPaginationIcon="false"
showSelectionIcons="false"
showSelectionSortIcon="false"
showSortingRow="false"
page="1"/>

</jato:containerView>
</spm:rbac>
</cc:pagetitle>

</jato:pagelet>
