<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)EditablePagelet.jsp	1.5	08/07/14 SMI"
 */
--%>

<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<jato:pagelet>

<script type="text/javascript" src="/com_sun_web_ui/js/browserVersion.js">
</script>

<SCRIPT LANGUAGE="JavaScript">
var editedElem;
var curTableName;
var cmdExecUrl = '/SunClusterManager/common/CommandExecutionServlet?';
var info_large_img_url = '/SunClusterManager/images/edit/info_large.gif';
var err_large_img_url = '/SunClusterManager/images/edit/error_large.gif';
var request;
var infoDivClick;
var tableNameToPropsMap = new Map();
var tableNameToInfoPopUpMap = new Map();
var tableNameToEditPopUpMap = new Map();
var hiddenStringToEditPropsMap = new Map();
var windowWidthSize = 0;
var windowHeightSize = 0;
var agent = navigator.userAgent.toLowerCase();
var loaded = false;

// Sets the global variables windowHeightSize and windowWidthSize 
function setWindowSizeVars() {
	if (typeof(window.innerWidth) == 'number') {
		//Non-IE
		windowWidthSize = window.innerWidth;
		windowHeightSize = window.innerHeight;
	} else if (document.documentElement && 
		(document.documentElement.clientWidth || document.documentElement.clientHeight)) {
		// IE 6+
		windowWidthSize = document.documentElement.clientWidth;
		windowHeightSize = document.documentElement.clientHeight;
	} else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
		// IE 4
		windowWidthSize = document.body.clientWidth;
		windowHeightSize = document.body.clientHeight;
	}

}

// Javascript object to be used by the Map object
function KeyValue() {
	this.key = "";
	this.value = "";
}

// Javascript implementation of Map Object
function Map() {
	this.keyValueArr = new Array();
	this.put = function(keyStr, valueObj) {

			if( ( typeof keyStr != "undefined" ) &&
				( typeof valueObj != "undefined" ) ) {
				var keyVal = new KeyValue();
				keyVal.key = keyStr;
				keyVal.value = valueObj;
				this.keyValueArr[this.keyValueArr.length] = keyVal;
			}
		    }

	this.get = function(keyStr) {

			for( var i = 0 ; i < this.keyValueArr.length ; i++ ) {
				if( this.keyValueArr[i].key == keyStr ) {
					return this.keyValueArr[i].value;
				}
			}
			return "";
		    }
}

// Javascript object to contain the properties of each editable field
function EditableProperty() {
	this.dataType = "text";
	this.valueList = "";
	this.preValueCommand = "";
	this.postValueCommand = "";
	this.tableName = "";
}

function initEditor(tableNames) {

	if (loaded == false) {
		loaded = true;
	} else {
		return;
	}

	if (tableNames == "") {
		return;
	}

	// Initialize global variables
	request = "";
	curTableName = "";
	infoDivClick = false;
	// Initialize the document for events
	document.onclick = checkMouseClick;

	// Now make the tables editable
	tableNames = tableNames.split(",");
	for (count = 0; count < tableNames.length; count++) {
		makeTableEditable(tableNames[count]);
	}
}

function makeTableEditable(tableName) {

	var tableElem, tableElems;
	var tabRows, tabCols;
	var paraElem, textElem, tmpElem;
	var isTableEdit = false;
	var info_div, divElem;
	var inputElem, radioElem, miscElem;
	var childDivElem;
        var count, msgStr;

	if (tableName == "") {
		return;
	}

	// Find the Table to be made editable.
        tableElem = "";
        tableElems = document.getElementsByTagName('table');

        for (count = 0; count < tableElems.length;  count++) {
                var tmpTable;
                var sumValue = "";

                tmpTable  = tableElems[count];
                sumValue = tmpTable.getAttribute('summary');

                if (sumValue == tableName) {
                        tableElem = tmpTable;
                        break;
                }
        }

        if (tableElem == "") {
		// Table not found
                return;
        }

        // Table was found. Add listeners to all the rows
        tabRows = tableElem.getElementsByTagName('tr');

        for (count = 0; count < tabRows.length;  count++) {
                var curRow = tabRows[count];
                var paraElem, textElem, index;

                tabCols = curRow.getElementsByTagName('td');

                if (tabCols.length < 2) {
                        continue;
                }

                // Fix the table columns and rows so that they dont move
                tabCols[0].style.width = tabCols[0].offsetWidth + "px";
                tabCols[1].style.width = tabCols[1].offsetWidth + "px";
                curRow.style.height = curRow.offsetHeight + "px";
		index = addEditablePropertyToCache(tabCols[0], tableName);
		if (index == -1) {
			// If -1 was returned then this field is not editable
			continue;
		}

		isTableEdit = true;
		// If we are here then this field is editable
		// First set the index in the hidden field to be picked up later
		// Set the handler for the click event
		if (tabCols[1].attachEvent) {
                        // IE Specific handler
                        tabCols[1].attachEvent('onclick', function() { handleEditRequest(this, event)});
                } else {
                        tabCols[1].setAttribute('onclick', 'handleEditRequest(this, event)');
                }               
                        
		tabCols[1].style.paddingLeft = "1px";
		tabCols[1].style.paddingRight = "3px";
		tabCols[1].style.paddingTop = "1px";
		tabCols[1].style.paddingBottom = "1px";
                // Create the paragraph element for the border feel
                paraElem = document.createElement('p');
                paraElem.style.borderWidth = "thin";
                paraElem.style.borderStyle = "solid";
                paraElem.style.borderColor = "#BEC7CC #BEC7CC #BEC7CC #BEC7CC";
                paraElem.style.left = "0px";
                paraElem.style.top = "0px";
                paraElem.style.height = "20px";//25px for firefox
                paraElem.style.width = "100%";
                textElem = tabCols[1].childNodes[0];

		// If the value was not set, then there will be no text element
		if (!textElem) {
			textElem = document.createTextNode('');
			tabCols[1].appendChild(textElem);
		}

                tabCols[1].replaceChild(paraElem, textElem);
                paraElem.appendChild(textElem);
        }

	if (isTableEdit == false) {
		return;
	}

	// If we are here then the table has editable fields.
	// We have to add the Editable PopUp and the Info PopUp
	// to the table. We need to add the pop-ups for each
	// table otherwise there will be a misalignment each time
	// we dynamically add the pop-ups as children to a table.

	// Create the Information Pop-Up
	info_div = document.createElement('div');
	info_div.setAttribute('id', 'info-popup');
         
        info_div.style.position = "absolute";
	info_div.style.left = "664";
	info_div.style.top = "76";
	info_div.style.zIndex = "200";
                 
	createPopUpImages(500, 100, info_div, false, "handleInfoPopUpClose()");
	createAlertTable(500, 100, info_div);
	info_div.style.visibility = "hidden";
	// Add the info-popup as child to this table and keep it in the cache
	// for later retrieval
	tableElem.appendChild(info_div);
	tableNameToInfoPopUpMap.put(tableName, info_div);

	// Now create the Editable Fields PopUp

	// Initialize the pop-up
	divElem = document.createElement('div');
	divElem.setAttribute('id', 'pseudo-popup');     

        divElem.style.position = "absolute";
	divElem.style.left = "664";
	divElem.style.top = "76";
	divElem.style.zIndex = "180";
                 
	// Build the pop-up
	createPopUpImages(0, 0, divElem, true, 'javascript:return(handleCancelRequest());');

	childDivElem = document.createElement('div');

        childDivElem.setAttribute('style', 'position: absolute; left: 0px; top: 0px; z-index: 182;');
                         
	// Set the header
	tmpElem = document.createElement('div');
         
	if (tmpElem.attachEvent && tmpElem.className != null) {
		tmpElem.setAttribute('className', 'AlrtInfTxt');
	} else {
		tmpElem.setAttribute('class', 'AlrtInfTxt');
	}
        
	msgStr = '<cc:text name="EditableBundleText" 
                    defaultValue="editable.edit.popup.header"
                    bundleID="scBundle"/>';
	headerElem = document.createTextNode(msgStr);
	tmpElem.appendChild(headerElem);

        tmpElem.style.position = "absolute";
	tmpElem.style.left = "0";
	tmpElem.style.top = "5";
	tmpElem.style.width = "280";
                 
	childDivElem.appendChild(tmpElem);

	// Set the Label
	tmpElem = document.createElement('div');
	if (tmpElem.attachEvent && tmpElem.className != null) {
		tmpElem.setAttribute('className', 'LblLev2Txt');
	} else {
		tmpElem.setAttribute('class', 'LblLev2Txt');
	}

	msgStr = '<cc:text name="EditableBundleText" 
                    defaultValue="editable.edit.popup.label"
                    bundleID="scBundle"/>';
	headerElem = document.createTextNode(msgStr);
	tmpElem.appendChild(headerElem);

        tmpElem.style.position = "absolute";
	tmpElem.style.left = "30";
	tmpElem.style.top = "45";
	tmpElem.style.width = "1000";
                 
	childDivElem.appendChild(tmpElem);

	// Set the TextBox
	textElem = document.createElement('input');
	textElem.setAttribute('id', 'TextElem');
	textElem.setAttribute('type', 'text');

	if (textElem.attachEvent) {
		textElem.attachEvent('onkeypress', function() { return (handleTextKeyPress(event)); });
	} else {
		textElem.setAttribute('onkeypress', 'return (handleTextKeyPress(event));');
	}

        textElem.style.position = "absolute";
	textElem.style.left = "130";
	textElem.style.top = "45";
                 
	textElem.style.visibility = "hidden";
	childDivElem.appendChild(textElem);

	// Set the Radio Button
	radioElem = document.createElement('div');
	if (radioElem.attachEvent && radioElem.className != null) {
		radioElem.setAttribute('className', 'ConTblCl2Div');
	} else { 
		radioElem.setAttribute('class', 'ConTblCl2Div');
	}
		
	radioElem.setAttribute('id', 'RadioElem');

        radioElem.style.position = "absolute";
	radioElem.style.left = "130";
	radioElem.style.top = "37";
	radioElem.style.width = "200";

	// True radio
	if (is_ie) {
		inputElem = document.createElement('<input type="radio" name="RadioElem" id="RadioElemTrue"/>');
	} else {
		inputElem = document.createElement('input');
                inputElem.setAttribute('type', 'radio');
		inputElem.setAttribute('id', 'RadioElemTrue');
		inputElem.setAttribute('name', 'RadioElem');
	}
        
	if (inputElem.attachEvent && inputElem.className != null) {
		inputElem.setAttribute('className', 'Rb');
		inputElem.attachEvent('onclick', function() { handleRadioClick(this); });
	} else {
		inputElem.setAttribute('class', 'Rb');
		inputElem.setAttribute('onclick', 'handleRadioClick(this);');
	}
	radioElem.appendChild(inputElem);
	radioElem.appendChild(document.createTextNode(''));

	// False radio
	if (is_ie) {
                inputElem = document.createElement('<input type="radio" name="RadioElem" id="RadioElemFalse"/>');
	} else {
		inputElem = document.createElement('input');
		inputElem.setAttribute('id', 'RadioElemFalse');
		inputElem.setAttribute('name', 'RadioElem');
		inputElem.setAttribute('type', 'radio');
	}

	if (inputElem.attachEvent && inputElem.className != null) {
		inputElem.setAttribute('className', 'Rb');
		inputElem.attachEvent('onclick', function() { handleRadioClick(this); });
	} else {
		inputElem.setAttribute('class', 'Rb');
		inputElem.setAttribute('onclick', 'handleRadioClick(this)');
	}
	radioElem.appendChild(inputElem);
	radioElem.appendChild(document.createTextNode(''));

	if (radioElem.attachEvent) {
		radioElem.attachEvent('onkeypress', function() { return (handleTextKeyPress(event)); });
	} else {
		radioElem.setAttribute('onkeypress', 'return (handleTextKeyPress(event));');
	}

	childDivElem.appendChild(radioElem);

	// Set the OK button
	inputElem = document.createElement('input');
	inputElem.setAttribute('id', 'SubmitBut');
	inputElem.setAttribute('type', 'button');
	if (inputElem.attachEvent && inputElem.className != null) {
		inputElem.setAttribute('className', 'Btn1Dis');
	} else {
		inputElem.setAttribute('class', 'Btn1Dis');
	}
	
        inputElem.style.position = "absolute";
	inputElem.style.left = "75";
	inputElem.style.top = "85";

	msgStr = '<cc:text name="EditableBundleText" 
                    defaultValue="editable.edit.popup.ok.button"
                    bundleID="scBundle"/>';
	inputElem.value = msgStr;
         
	if (inputElem.attachEvent) {
		inputElem.attachEvent('onclick', function() { return(handleOKRequest()); });
	} else {
		inputElem.setAttribute('onclick', 'javascript:return(handleOKRequest());');
	}
	childDivElem.appendChild(inputElem);
	inputElem.disabled = true;

	// Set the Cancel button
	inputElem = document.createElement('input');
	inputElem.setAttribute('type', 'button');
	if (inputElem.attachEvent && inputElem.className != null) {
		inputElem.setAttribute('className', 'Btn1');
	} else {
		inputElem.setAttribute('class', 'Btn1');
	}

        inputElem.style.position = "absolute";
	inputElem.style.left = "165";
	inputElem.style.top = "85";
                 
	msgStr = '<cc:text name="EditableBundleText" 
                    defaultValue="editable.edit.popup.cancel.button"
                    bundleID="scBundle"/>';
	inputElem.value = msgStr;
	if (inputElem.attachEvent) {
		inputElem.attachEvent('onclick', function() { return(handleCancelRequest()); });	
	} else {
		inputElem.setAttribute('onclick', 'javascript:return(handleCancelRequest());');
	}
	childDivElem.appendChild(inputElem);

	divElem.appendChild(childDivElem);

	divElem.style.visibility = "hidden";

	// Add the edit-popup as child to this table and keep it in the cache
	// for later retrieval
	tableElem.appendChild(divElem);
	tableNameToEditPopUpMap.put(tableName, divElem);
}

function handleTextKeyPress(event) {
	var okElem, keyChar;
	var divElem;

	keyChar = event.keyCode;
	divElem = tableNameToEditPopUpMap.get(curTableName);
	okElem = getElemWithAttr(divElem, 'input', 'id', 'SubmitBut');

	if (keyChar == 13) {
		if (okElem.disabled == false) {
			handleOKRequest();
		}
		return false;
	} 

	// Enable the textBox
	if (okElem.attachEvent && okElem.className != null) {
		okElem.setAttribute('className', 'Btn1');	
	} else {
		okElem.setAttribute('class', 'Btn1');	
	}

	okElem.disabled = false;
	return true;
}

function createPopUpImages(width, height, parDivElem, showArrow, closeHandler) {

	var childDivElem, imgElem;
	var tmpVal, miscVal;

	if (!parDivElem) {
		return;
	}

	if (width < 50) {
		width = 318;//300
	}

	if (height < 50) {
		height = 130;
	}

	// Create the child Div elem
	childDivElem = document.createElement('div');

        childDivElem.style.position = "absolute";
	childDivElem.style.left = "0";
	childDivElem.style.top = "0";
                 
	// Set the images
	imgElem = document.createElement('img');
	imgElem.setAttribute('width', '25');
	imgElem.setAttribute('height', '25');
	imgElem.setAttribute('src', '/SunClusterManager/images/edit/top-left.gif');

        imgElem.style.position = "absolute";
	imgElem.style.left = "0";
	imgElem.style.top = "0"; 
                 
	childDivElem.appendChild(imgElem);

	imgElem = document.createElement('img'); 
	tmpVal = width - 50;
	imgElem.setAttribute('width', tmpVal); //300
	imgElem.setAttribute('height', '24'); 
	imgElem.setAttribute('src', '/SunClusterManager/images/edit/top-480.gif');
	imgElem.style.position = "absolute";

        imgElem.style.left = "23";
	tmpVal = width - 50;
	imgElem.style.width = tmpVal + 3;
	imgElem.style.height = "24"; 
                 
	imgElem.style.top = "0";
	childDivElem.appendChild(imgElem);
	
	imgElem = document.createElement('img');
	imgElem.setAttribute('width', '25');
	imgElem.setAttribute('height', '25');
	imgElem.setAttribute('src', '/SunClusterManager/images/edit/top-right.gif'); tmpVal = width - 25;

        imgElem.style.position = "absolute";
	imgElem.style.left = tmpVal;
	imgElem.style.top = "0";
                 
	childDivElem.appendChild(imgElem);

	imgElem = document.createElement('img');
	imgElem.setAttribute('width', '24');tmpVal = height - 50
	imgElem.setAttribute('height', tmpVal);//200
	imgElem.setAttribute('src', '/SunClusterManager/images/edit/left-360.gif');

        imgElem.style.position = "absolute";
	imgElem.style.left = "0";
	imgElem.style.top = "25";
	imgElem.style.height = tmpVal;

	childDivElem.appendChild(imgElem);

	imgElem = document.createElement('img');tmpVal = width - 50 + 2;
	imgElem.setAttribute('width', tmpVal);tmpVal = height - 30;//290
	imgElem.setAttribute('height', tmpVal);//200
	imgElem.setAttribute('src', '/SunClusterManager/images/edit/centre.png');

        imgElem.style.position = "absolute";
	imgElem.style.left = "24";
	imgElem.style.top = "25";
	imgElem.style.height = tmpVal;
	tmpVal =  width - 50 + 2;
	imgElem.style.width = tmpVal; 
                 
	childDivElem.appendChild(imgElem);

	imgElem = document.createElement('img');
	imgElem.setAttribute('width', '25');tmpVal = height - 50;
	imgElem.setAttribute('height', tmpVal);//91
	imgElem.setAttribute('src', '/SunClusterManager/images/edit/right-360.gif');tmpVal = width - 25;

	imgElem.style.position = "absolute";
         imgElem.style.left = tmpVal;
         imgElem.style.top = "25";

        tmpVal = height - 50;
	imgElem.style.height = tmpVal;
                 
	childDivElem.appendChild(imgElem);

	imgElem = document.createElement('img');
	imgElem.setAttribute('width', '25');
	imgElem.setAttribute('height', '25'); 
	imgElem.setAttribute('src', '/SunClusterManager/images/edit/bottom-left.gif');tmpVal = height - 25;

        imgElem.style.position = "absolute";
	imgElem.style.left = "0";
	imgElem.style.top = tmpVal;
                 
	childDivElem.appendChild(imgElem);

	imgElem = document.createElement('img');tmpVal = width - 50 + 12;
	imgElem.setAttribute('width', tmpVal);//300
	imgElem.setAttribute('height', '25');
	imgElem.setAttribute('src', '/SunClusterManager/images/edit/bottom-480.gif');tmpVal = height - 25;

	imgElem.style.position = "absolute";
         imgElem.style.top = tmpVal;

        imgElem.style.left = "23";
	tmpVal = width - 50 + 12;
	imgElem.style.width = tmpVal;
                 
	childDivElem.appendChild(imgElem);

	if (showArrow) {
		imgElem = document.createElement('img');

                imgElem.style.width = "45";
		imgElem.style.height = "91";
                         
		imgElem.setAttribute('src', '/SunClusterManager/images/edit/arrow2.gif');tmpVal = width - 25;

                imgElem.style.position = "absolute";
		imgElem.style.left = tmpVal;
		imgElem.style.top = "25";
                         
		childDivElem.appendChild(imgElem);
	}

	imgElem = document.createElement('img');

        imgElem.style.width = "25";
	imgElem.style.height = "25";
                 
	imgElem.setAttribute('src', '/SunClusterManager/images/edit/bottom-right.gif');tmpVal = width - 25; miscVal = height - 25;

        imgElem.style.position = "absolute";
	imgElem.style.left = tmpVal;
	imgElem.style.top = miscVal;
                 
	childDivElem.appendChild(imgElem);

	imgElem = document.createElement('img');
         
        imgElem.style.width = "15";
	imgElem.style.height = "15";

	imgElem.setAttribute('src', '/SunClusterManager/images/edit/close.gif');tmpVal = width -25;

	if (imgElem.attachEvent) {
		imgElem.attachEvent('onclick', function() { handleCancelRequest(); });
	} else {
		imgElem.setAttribute('onclick', 'handleCancelRequest();');
	}
        
        imgElem.style.position = "absolute";
	imgElem.style.left = tmpVal;
	imgElem.style.top = "5";
                 
	childDivElem.appendChild(imgElem);

	parDivElem.appendChild(childDivElem);

}

function createAlertTable(width, height, parDivElem) {

	var childDivElem, imgElem, tableElem;
	var rowElem, colElem, tmpElem;
	var childElem;
	var tmpVal;
        var tbody;
        
	// Create the child Div elem
	childDivElem = document.createElement('div');
	childDivElem.setAttribute('id', 'InfoPopUpMessageDiv');
	if (childDivElem.attachEvent) {
		childDivElem.attachEvent('onclick', function() { handleInfoPopUpClick(); });
                childDivElem.style.position = "absolute";
		childDivElem.style.left = "0px";
		childDivElem.style.top = "0px";
		childDivElem.style.width = width;
		childDivElem.style.zIndex = "181";
                 
                tbody = document.createElement("tbody");
		tableElem = document.createElement("table");
                 
                tableElem.setAttribute("className", "AlrtTbl");
		tableElem.style.position = "absolute";
		tableElem.style.left = "0px";
		tableElem.style.top = "25px";
		tableElem.style.zIndex = "182";
                 
                tableElem.style.cellspacing = 0;
                tableElem.style.cellpadding= 0;
                tableElem.style.border = 0;
                tableElem.style.align= "center";
                tableElem.style.width = width;
                tableElem.style.height = height - 25;
                
                rowElem = document.createElement('tr');
                tbody.appendChild(rowElem);
                tableElem.appendChild(tbody);
	} else {
		childDivElem.setAttribute('onclick', 'handleInfoPopUpClick()');
                childDivElem.setAttribute('style', 'position: absolute; left: 0px; top: 0px; z-index: 181; width:' + width + ';' );
                
                tableElem = document.createElement('table');
		tableElem.setAttribute('class', 'AlrtTbl');
		tableElem.setAttribute('style', 'position: absolute; left: 0px; top: 25px; z-index: 182;');
		tableElem.setAttribute('cellspacing', '0');
		tableElem.setAttribute('cellpadding', '0');
		tableElem.setAttribute('border', '0');
		tableElem.setAttribute('align', 'center');
		tableElem.setAttribute('width', width);
		tableElem.setAttribute('height', height - 25);
		
                rowElem = document.createElement('tr');
                tableElem.appendChild(rowElem);    
	}    
	
        colElem = document.createElement("td");
        colElem.setAttribute('valign', 'middle');
        colElem.style.backgroundColor = "#FFFFCC";
        colElem.style.border = "0px none #FFFFCC";
        rowElem.appendChild(colElem);

	tmpElem = document.createElement('div');
	if (tmpElem.attachEvent && tmpElem.className != null) {
		tmpElem.setAttribute('className', 'AlrtInfTxt');
	} else {
		tmpElem.setAttribute('class', 'AlrtInfTxt');
	}

	imgElem = document.createElement('img');
	imgElem.setAttribute("id", "InfoPopUpInfoImage");
	// The src will be set according the result of the command executed
        imgElem.style.src = "";
	imgElem.style.width = "21";
	imgElem.style.height = "21";
	imgElem.style.border = "0";
                 
	imgElem.style.visibility = "hidden";
	tmpElem.appendChild(imgElem);

	var txtNode = document.createTextNode("Modified Successfully");
	tmpElem.appendChild(txtNode);	
	
	colElem.appendChild(tmpElem);

	tmpElem = document.createElement('div');
	if (tmpElem.attachEvent && tmpElem.className != null) {
		tmpElem.setAttribute("className", "AlrtMsgTxt");
	} else {
		tmpElem.setAttribute('class', 'AlrtMsgTxt');
	}

	childElem = document.createElement("span");
	if (childElem.attachEvent && childElem.className != null) {
		childElem.setAttribute("className", "LblLev2Txt");
	} else {
		childElem.setAttribute('class', 'LblLev2Txt');
	}

	var tmpNode = document.createTextNode("Command executed: ");
	childElem.appendChild(tmpNode);
	tmpElem.appendChild(childElem);

	childElem = document.createElement('span');

        childElem.style.color = "rgb(0, 0, 0)";
	childElem.style.fontfamily = "monospace";
	childElem.style.fontweight = "normal";
                 
	txtNode = document.createTextNode('');
	childElem.appendChild(txtNode);
	tmpElem.appendChild(childElem);

	colElem.appendChild(tmpElem);
	childDivElem.appendChild(tableElem);
	parDivElem.appendChild(childDivElem);
}

function addEditablePropertyToCache(tdElem, tableName) {
	var hiddenElem;
	var editProps, editPropsArr;
	var index, propVal, propObj;
	var tmpInt;

	if (!tdElem || tableName =="") {
		return -1;
	}

	hiddenElem = tdElem.childNodes[1];

	if (!hiddenElem) {
		return -1;
	}

	editProps = trim(hiddenElem.value);

	if (editProps == "") {
		return -1;
	}

	index = editProps.indexOf('Editable');

	if (index == -1) {
		// This happens when the frame is reloaded
		return -1; 
	}

	propVal = editProps.substring(index + 8);
	index = propVal.indexOf('&');

	if (index != -1) {
		propVal = propVal.substring(0, index);
	}

	if (propVal != "true") {
		return -1;
	}

	// If we are here then this is an editable field

	// First trim the hidden property
	index = editProps.indexOf('&');
	editProps = editProps.substring(index + 1);
	propObj = new EditableProperty();

	// Set the type
	index = editProps.indexOf('type');
	if (index == -1) {
		propVal = "text";
	} else {
		tmpInt = editProps.indexOf('&');
		propVal = editProps.substring(index + 4, tmpInt);
		editProps = editProps.substring(tmpInt + 1);
	}
	propObj.dataType = propVal;

	// Set the ValueList
	index = editProps.indexOf('valuelist');
	if (index == -1) {
		propVal = "";
	} else {
		tmpInt = editProps.indexOf('&');
		propVal = editProps.substring(index + 9, tmpInt);
		editProps = editProps.substring(tmpInt + 1);
	}
	propObj.valueList = propVal;

	// Set the PreValueCommand
	index = editProps.indexOf('PreValueCommand');
	if (index == -1) {
		propVal = "";
	} else {
		tmpInt = editProps.indexOf('&');
		propVal = editProps.substring(index + 15, tmpInt);
		editProps = editProps.substring(tmpInt + 1);
	}
	propObj.preValueCommand = propVal;

	// Set the PostValueCommand
	index = editProps.indexOf('PostValueCommand');
	if (index == -1) {
		propVal = "";
	} else {
		tmpInt = editProps.indexOf('&');
		if (tmpInt == -1) {
			tmpInt = editProps.length - 1;
		}
		propVal = editProps.substring(index + 16, tmpInt + 1);
	}
	propObj.postValueCommand = propVal;

	editPropsArr = tableNameToPropsMap.get(tableName);

	if (editPropsArr == "") {
		editPropsArr = new Array();
		tableNameToPropsMap.put(tableName, editPropsArr);
	}

	// Set the Table Name
	propObj.tableName = tableName;
        
	hiddenStringToEditPropsMap.put(trim(hiddenElem.value),propObj);
	index = editPropsArr.push(propObj);
	return (index - 1);
}

function handleEditRequest(eventObj, event) {
	var inputElem;
	var editPropsVal, editPropsArr;
	var hiddenElem, editPropsVal, editPropsObj;
	var value, tValue, fValue;
	var divElem;

	// Close the infoPopUp if it was visible
	handleInfoPopUpClose();
	// Close the EditPopUp if it was visible
	// We have to close the EditPopUp if it
	// is visible from another table

	if (eventObj.event) {
		eventObj = eventObj.event.srcElement.parentElement;
	}

	if (curTableName !=
		eventObj.parentNode.parentNode.parentNode.getAttribute("summary")) {
		handleCancelRequest();
	}

	var summary;
	if (eventObj.parentElement && eventObj.parentElement.parentElement.parentElement != null) {
                summary = eventObj.parentElement.parentElement.parentElement.getAttribute("summary");
        } else {
                summary = eventObj.parentNode.parentNode.parentNode.getAttribute("summary");
        }

        if (curTableName != summary) {
                handleCancelRequest();
        }	

        editedElem = eventObj.childNodes[0];//eventObj is the 'td' elem
	if (eventObj.parentElement && editedElem.parentElement.parentElement.childNodes[0].childNodes[1] != null) {
                curTableName = eventObj.parentElement.parentElement.parentElement.getAttribute("summary");
                hiddenElem = editedElem.parentElement.parentElement.childNodes[0].childNodes[1];
        } else {
                curTableName = eventObj.parentNode.parentNode.parentNode.getAttribute("summary");
                hiddenElem = editedElem.parentNode.parentNode.childNodes[1].childNodes[1];
        }

        if (!hiddenElem.value) {
            curTableName = "";
            return false;
        }
            
	editPropsVal = hiddenElem.value;
	editPropsObj = hiddenStringToEditPropsMap.get(trim(editPropsVal));

	setWindowSizeVars();
	// Set the pop-up at the correct position
        divElem = tableNameToEditPopUpMap.get(curTableName);
            
	var top = getTopMousePoint(editedElem);
        top -= 40;
	divElem.style.top  = top;

	var left = getLeftMousePoint(editedElem);
	left -= 337;
	divElem.style.left = left;

	divElem.style.visibility = "visible";

	var newHeight = windowHeightSize + document.body.scrollTop;	
	if ((newHeight - top) < 0 || (newHeight - top) < 141) {
		window.scrollBy(0, (newHeight - top) - 10);
	} else if (top < document.body.scrollTop) {
		window.scrollBy(0, - (document.body.scrollTop - top + 10));
	}

	// Decide to show textbox or radio based on the datatype
	// If it boolean type show the radio, else show textbox
	if (editPropsObj.dataType == "bool") {
		inputElem = getElemWithAttr(divElem, 'div', 'id', 'RadioElem');
		inputElem.style.visibility = "visible";
		value = eventObj.childNodes[0].childNodes[0].data;
		value = trim(value);
		// Get the true value
		editPropsVal = editPropsObj.valueList.split(",");
		tValue = editPropsVal[0];
		fValue = editPropsVal[1];

		if (tValue == value) {
			getElemWithAttr(divElem, 'input', 'id', 'RadioElemTrue').checked = true;
			getElemWithAttr(divElem, 'input', 'id', 'RadioElemFalse').checked = false;
		} else {
			getElemWithAttr(divElem, 'input', 'id', 'RadioElemTrue').checked = false;
			getElemWithAttr(divElem, 'input', 'id', 'RadioElemFalse').checked = true;
		}

		// Set the apprpriate Radio values
		getElemWithAttr(divElem, 'input', 'id', 'RadioElemTrue').nextSibling.data = tValue;
		getElemWithAttr(divElem, 'input', 'id', 'RadioElemFalse').nextSibling.data = fValue;

		// Hide the other element objects
		inputElem = getElemWithAttr(divElem, 'input', 'id', 'TextElem');
		inputElem.style.visibility = "hidden";
	} else {
		inputElem = getElemWithAttr(divElem, 'input', 'id', 'TextElem');
		inputElem.style.visibility = "visible";
		inputElem.value = eventObj.childNodes[0].childNodes[0].data;
		inputElem.focus();
		// Hide the other element objects
		inputElem = getElemWithAttr(divElem, 'div', 'id', 'RadioElem');
		inputElem.style.visibility = "hidden";
	}

	okElem = getElemWithAttr(divElem, 'input', 'id', 'SubmitBut');
	if (okElem.attachEvent && okElem.className != null) {
		okElem.setAttribute('className', 'Btn1Dis');	
	} else {
		okElem.setAttribute('class', 'Btn1Dis');	
	}
	okElem.disabled = true;

	return true;
}

function handleRadioClick(radioObj) {
	var value, okElem;
	var divElem;

        if (radioObj.event) {
                radioObj = radioObj.event.srcElement.parentElement;
        }

	value = editedElem.childNodes[0].data;
	value = trim(value);
	divElem = tableNameToEditPopUpMap.get(curTableName);
	okElem = getElemWithAttr(divElem, 'input', 'id', 'SubmitBut');

	if (value == radioObj.nextSibling.data) {
		if (okElem.attachEvent && okElem.className != null) {
			okElem.setAttribute('className', 'Btn1Dis');
		} else {
			okElem.setAttribute('class', 'Btn1Dis');
		}
		okElem.disabled = true;
	} else {
		if (okElem.attachEvent && okElem.className != null) {
			okElem.setAttribute('className', 'Btn1');	
		} else {
			okElem.setAttribute('class', 'Btn1');	
		}
		okElem.disabled = false;
	}
}

function checkMouseClick(e) {
	if (infoDivClick == true) {
		infoDivClick = false;
		return;
	}

	handleInfoPopUpClose();
	infoDivClick = false;
}

function getTopMousePoint(elem) {

	var yVal = 0;
	while( elem != null ) {
		yVal += elem.offsetTop;
		elem = elem.offsetParent;
	}

	return yVal;
}

function getLeftMousePoint(elem) {

	var xVal = 0;
	while( elem != null ) {
		xVal += elem.offsetLeft;
		elem = elem.offsetParent;
	}

	return xVal;
}

function getElemWithAttr(parentElem, elemTagName, attrKey, attrVal) {

	var childNodes, childElem;
	var count;

	if (!parentElem) {
		return;
	}

	if (elemTagName == "" || attrKey == "" || attrVal == "") {
		return;
	}

	childNodes = parentElem.getElementsByTagName(elemTagName);
	for (count = 0; count < childNodes.length; count++) {
		childElem = childNodes[count];

		if (childElem.getAttribute(attrKey) == attrVal) {
			return childElem;
		}
	}

	// If we are here then the child element was not found
	return null;
}

function handleOKRequest() {
	var value, elem;
	var okElem, divElem;

	// We have to pick the value from the
	// object which was visible
	divElem = tableNameToEditPopUpMap.get(curTableName);
	elem = getElemWithAttr(divElem, 'input', 'id', 'TextElem');

	if (elem.style.visibility != "hidden") {
		value = elem.value;
	} else {
		// If we are here then it means the radio was visible
		elem = getElemWithAttr(divElem, 'input', 'id', 'RadioElemTrue');
		if (elem.checked == true) {
			value = "true";
		} else {
			value = "false";
		}
	}

	okElem = getElemWithAttr(divElem, 'input', 'id', 'SubmitBut');
	if (okElem.attachEvent && okElem.className != null) {
		okElem.setAttribute('className', 'Btn1Dis');	
	} else {
		okElem.setAttribute('class', 'Btn1Dis');	
	}
	okElem.disabled = true;
	// Just execute the command and return
	executeCommand(value);
	return true;
}

function handleCancelRequest() {
	var valElem, divElem;

	if (curTableName == "") {
		return;
	}

	divElem = tableNameToEditPopUpMap.get(curTableName);
	valElem = getElemWithAttr(divElem, 'input', 'id', 'TextElem');
	valElem.style.visibility = "hidden";
	valElem.blur();
	valElem = getElemWithAttr(divElem, 'div', 'id', 'RadioElem');
	valElem.style.visibility = "hidden";
	divElem.style.visibility = "hidden";
	return true;
}

function executeCommand(value) {

        var url, cmdToExec;
	var hiddenElem;
	var editPropsVal, editPropsObj, editPropsArr;

	if (!editedElem) {
		return;
	}

	if (editedElem.parentElement && editedElem.parentElement.parentElement.childNodes[0].childNodes[1] != null) {
                hiddenElem = editedElem.parentElement.parentElement.childNodes[0].childNodes[1];
        } else {
                hiddenElem = editedElem.parentNode.parentNode.childNodes[1].childNodes[1];
        }

	editPropsVal = hiddenElem.value;
	editPropsObj = hiddenStringToEditPropsMap.get(trim(editPropsVal));
	cmdToExec = getCommandUrlParams(editPropsObj.preValueCommand, value,
				editPropsObj.postValueCommand);

        if (window.XMLHttpRequest) {
                request = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
                request = new ActiveXObject("Microsoft.XMLHTTP");
        } else {
                return;
        }

        // Form the URL
        url = cmdExecUrl + cmdToExec;
        request.onreadystatechange = handleResponse;
        request.open("POST", url, true);
        request.send(null);
}

function getCommandUrlParams(preCmdStr, propValue, postCmdStr) {
	var cmdUrlParam;
	var count;
	var cmdStrArr;
	var cmdArgCount = 0;

	preCmdStr = trim(preCmdStr);
	cmdStrArr = preCmdStr.split(' ');
	cmdUrlParam = '';

	for (count = 0; count < cmdStrArr.length; count++) {
		cmdUrlParam += 'CommandArg' + count;
		cmdUrlParam += '=' + cmdStrArr[count] + '&'; 
	}

	cmdUrlParam = cmdUrlParam.substring(0, cmdUrlParam.length - 1);
	cmdUrlParam += encodeURIComponent(propValue) + '&';
	cmdArgCount = cmdStrArr.length;
	postCmdStr = trim(postCmdStr);
	cmdStrArr = postCmdStr.split(' ');

	for (count = 0; count < cmdStrArr.length; count++) {
		cmdUrlParam += 'CommandArg' + cmdArgCount;
		cmdUrlParam += '=' + cmdStrArr[count] + '&'; 
                cmdArgCount++;
	}

	cmdUrlParam += 'CommandArgsCount=' + cmdArgCount;
	return (cmdUrlParam);
}

function handleResponse() {

	var resCode, index;
	var value, elem, value;
	var editPropsVal, editPropsObj, editPropsArr;
	var divElem, infoImg;
	var msgStr;

        if (request.readyState != 4) {
                return;
        }

	index = request.responseText.indexOf('&');
	resCode = request.responseText.substring(0, index);
	value = request.responseText.substring(index + 1);

	// Close the pop-up
	handleCancelRequest();
	divElem = tableNameToInfoPopUpMap.get(curTableName);
	infoImg = getElemWithAttr(divElem, 'img', 'id', 'InfoPopUpInfoImage');

	infoImg.style.visibility = "visible";
	if (resCode == 0) {
		msgStr = '<cc:text name="EditableBundleText" 
                    defaultValue="editable.command.result.success"
                    bundleID="scBundle"/>';
	} else {
		msgStr = '<cc:text name="EditableBundleText" 
                    defaultValue="editable.command.result.error"
                    bundleID="scBundle"/>';
	}

	infoImg.nextSibling.data = msgStr;
       
	if (infoImg.parentElement) {
		// CHANGED
                // Get rid of the line breaks <br>
                newvalue = value.replace(/<br>/g, '\n');
		infoImg.parentElement.nextSibling.childNodes[1].childNodes[0].data = unescape(newvalue);
        } else {
                infoImg.parentNode.nextSibling.childNodes[1].innerHTML = value;
        }

	displayInfoPopUp(resCode);

	if (resCode != 0) {
		// Nothing more to do
		return;
	}

	// If we are here then the command execution was a success
	// We have to update the value in the cell
	if (editedElem.parentElement && editedElem.parentElement.parentElement.childNodes[0].childNodes[1] != null) {
		// CHANGED
                editPropsVal = editedElem.parentElement.parentElement.childNodes[0].childNodes[1].value;
        } else {
                editPropsVal = editedElem.parentNode.parentNode.childNodes[1].childNodes[1].value;
        }

	editPropsObj = hiddenStringToEditPropsMap.get(trim(editPropsVal));

	divElem = tableNameToEditPopUpMap.get(curTableName);

	if (editPropsObj.dataType == "bool") {
		elem = getElemWithAttr(divElem, 'input', 'id', 'RadioElemTrue');
		if (elem.checked != true) {
			elem = getElemWithAttr(divElem, 'input', 'id', 'RadioElemFalse');
		}
		value = elem.nextSibling.data;
	} else {
		// It is a text elem
		value = getElemWithAttr(divElem, 'input', 'id', 'TextElem').value;
	}
	
	editedElem.childNodes[0].data = value;
}

function displayInfoPopUp(resType) {
	var info_div, tmpElem;

	setWindowSizeVars();
	info_div = tableNameToInfoPopUpMap.get(curTableName);
	info_div.style.visibility = "visible";
	var top = getTopMousePoint(editedElem);
	info_div.style.top = top + "px";
	var left = getLeftMousePoint(editedElem);
	left -= 503;
	info_div.style.left = left + "px";

        var newHeight = windowHeightSize + document.body.scrollTop;
        if ((newHeight - top) < 0 || (newHeight - top) < 141) {
                window.scrollBy(0, (newHeight - top) - 10);
        } else if (top < document.body.scrollTop) {
                window.scrollBy(0, - (document.body.scrollTop - top + 10));
        }

	tmpElem = getElemWithAttr(info_div, 'img', 'id', 'InfoPopUpInfoImage');

	if (resType == 0) {
		tmpElem.setAttribute("src", info_large_img_url);
		tmpElem.style.visibility = "visible";
	} else {
		tmpElem.setAttribute("src", err_large_img_url);
		tmpElem.style.visibility = "visible";
	}

	return false;
}

function handleInfoPopUpClose() {
	var info_image;
	var info_div;

	if (curTableName == "") {
		// No table was edited.
		return;
	}

	info_div = tableNameToInfoPopUpMap.get(curTableName);

	if (!info_div) {
		return;
	}

	info_div.style.visibility = "hidden";
	info_image = getElemWithAttr(info_div, 'img', 'id', 'InfoPopUpInfoImage');
	info_image.style.visibility = "hidden";
}

function handleInfoPopUpClick() {
	infoDivClick = true;
}

function ctrim(str) {
	var newStr = "";

	for(var i = 0; i < str.length ; i++) {
		if (!isWhitespace(str.charAt(i))) {
			newStr = newStr + str.charAt(i);
		}
	}

	return newStr;
}

function ltrim(str) { 
	for(var k = 0; k < str.length && isWhitespace(str.charAt(k)); k++);
	return str.substring(k, str.length);
}
function rtrim(str) {
	for(var j=str.length-1; j>=0 && isWhitespace(str.charAt(j)) ; j--) ;
	return str.substring(0,j+1);
}
function trim(str) {
	return ltrim(rtrim(str));
}
function isWhitespace(charToCheck) {
	var whitespaceChars = " \t\n\r\f";
	return (whitespaceChars.indexOf(charToCheck) != -1);
}

</SCRIPT>

</jato:pagelet>
