<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/**
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceGroupsStatusPagelet.jsp	1.8	08/07/14 SMI"
 */
--%>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<jato:pagelet>


<SCRIPT LANGUAGE="JavaScript"> 
   sHasResources = false;

function getHiddenResGrpFieldValue(hiddenTextFieldName) {
    // Element name (prefix) of selection checkbox.
    var elementName = "<cc:text name='SelectionNameText'/>";

    // Document form.
    var form = document.scForm;

    var nameItem, ItemNumIndex, selectedRow, prefix, resGrpComp, clusterNameComp, ej = null, retValue = null;
    var sValue, s1, s2, s3, s4, tmpS1, tmpS2, sManagedonline = false;
    var sManagedoffline = false, sUnmanaged = false, sSuspended = false, sNSuspended = false;
    
    var countChecked = 0;

    var managed = "<%=com.sun.cluster.common.ClusterState.CLSTATE_MANAGED%>";
    var unmanaged = "<%=com.sun.cluster.common.ClusterState.CLSTATE_UNMANAGED%>";
    var online =  "<%=com.sun.cluster.common.ClusterState.CLSTATE_ONLINE%>";
    var offline = "<%=com.sun.cluster.common.ClusterState.CLSTATE_OFFLINE%>";
    var suspended = "<%=com.sun.cluster.common.ClusterState.CLSTATE_SUSPENDED%>";
    var nsuspended = "<%=com.sun.cluster.common.ClusterState.CLSTATE_NOTSUSPENDED%>";
    var hasresources = "1";
    var clusterName = "";
    
    for (i = 0; i < form.elements.length; i++) {
      var e = form.elements[i];
      if ((e.name.indexOf(elementName) != -1)&&(e.type=="checkbox")) {
        if (e.checked) {
	  countChecked = countChecked + 1;
	  nameItem = e.name;
	  ItemNumIndex = nameItem.lastIndexOf("Checkbox")+8;
	  selectedRow = nameItem.substring(ItemNumIndex, nameItem.length);
          prefix =  elementName.substring(0,elementName.indexOf("Table.SelectionCheckbox"));
          resGrpComp = prefix + "TableTiledView[" + selectedRow + "]." + hiddenTextFieldName;
          ej = form.elements[resGrpComp];
          
          
          clusterNameComp = prefix + "TableTiledView[" + selectedRow + "].HiddenClusterName";
          if (countChecked == 1) {
              clusterName = form.elements[clusterNameComp].value;
          } else if (clusterName != form.elements[clusterNameComp].value) {
              return '';
          }
          
	  if (hiddenTextFieldName == 'HiddenResGrpName'){
		if (retValue == null)
			retValue = ej.value;
		else 
			retValue = retValue + ", " + ej.value;
	  }
          else if (hiddenTextFieldName == 'HiddenResGrpBtnState'){
		sValue =  ej.value; 
		s1 = sValue.substring(0,sValue.indexOf(" "));
		tmpS1 = sValue.substring(sValue.indexOf(" ")+1, sValue.length);
		s2 = tmpS1.substring(0, tmpS1.indexOf(" "));
		tmpS2 = tmpS1.substring(tmpS1.indexOf(" ")+1, tmpS1.length);
                s3 = tmpS2.substring(0, tmpS2.indexOf(" "));
                s4 = tmpS2.substring(tmpS2.indexOf(" ")+1, tmpS2.length);
		if ((s1==managed)&&(s2==online)) sManagedonline = true;			
		if ((s1==managed)&&(s2==offline)) sManagedoffline = true;
		if (s1==unmanaged) sUnmanaged = true;
		if (s3==suspended) sSuspended = true;
		if (s3==nsuspended) sNSuspended = true;
                if (s4==hasresources)  {
                    sHasResources = true; 
                } else {
                    sHasResources = false;
                }
	  }
        }
      }
    }


    if (countChecked==0) return '';

    if (hiddenTextFieldName == 'HiddenResGrpBtnState'){
	if ((sManagedonline) && (!sManagedoffline) && (!sUnmanaged))
		retValue = "1";
	else if ((!sManagedonline) && (sManagedoffline) && (!sUnmanaged))
		retValue = "2";
	else if ((!sManagedonline) && (!sManagedoffline) && (sUnmanaged))
		retValue = "3";
	else if ((sManagedonline) && (sManagedoffline) && (!sUnmanaged))
		retValue = "4";
	else retValue = "5";
	if ((sSuspended) && (!sNSuspended))
		retValue = retValue + "1";
	else if ((!sSuspended) && (sNSuspended))
		retValue = retValue + "0";
	else retValue = retValue + "2";
	if (countChecked > 1) retValue = retValue + "1";
	else retValue = retValue + "0";
	
    }
    
    return retValue;  
    
 }


 function displayResGrpTblBtnsConfirmation(operation) {

  var msg = "null";

  getHiddenResGrpFieldValue("HiddenResGrpName");
  
  if (operation == '0') {
    unRegisterEvents();
    return false;

  } else if (operation == '1') {
    msg = '<cc:text name="StaticText" 
                      defaultValue="rgm.resourcegroup.operation.bringOnline.confirm"
                      bundleID="scBundle"/>';

  } else if (operation == '2') {
    msg = '<cc:text name="StaticText" 
                      defaultValue="rgm.resourcegroup.operation.takeOffline.confirm"
                      bundleID="scBundle"/>';
  
  } else if (operation == '3') {
    msg = '<cc:text name="StaticText" 
                      defaultValue="rgm.resourcegroup.operation.manage.confirm"
                      bundleID="scBundle"/>';
  
  } else if (operation == '4') {
    msg = '<cc:text name="StaticText" 
                      defaultValue="rgm.resourcegroup.operation.unmanage.confirm"
                      bundleID="scBundle"/>';
  } else if (operation == '5') {
    msg = '<cc:text name="StaticText"
		defaultValue="rgm.resourcegroup.operation.suspendRGs.confirm"
		bundleID="scBundle"/>';
  
  } else if (operation == '6') {
    msg = '<cc:text name="StaticText"
		    defaultValue="rgm.resourcegroup.operation.fastsuspendRGs.confirm"
		    bundleID="scBundle"/>';

  } else if (operation == '7') {
    msg = '<cc:text name="StaticText"
		    defaultValue="rgm.resourcegroup.operation.resumeRGs.confirm"
		    bundleID="scBundle"/>';

  } else if (operation == 'restart') {
    msg = '<cc:text name="StaticText" 
                      defaultValue="rgm.resourcegroup.operation.restart.confirm"
                      bundleID="scBundle"/>';

  } else if (operation == 'remove') {
    if (sHasResources == true) {
        msg = '<cc:text name="StaticText" 
                      defaultValue="rgm.resourcegroup.operation.remove.force.confirm"
                      bundleID="scBundle"/>';
     } else {
        msg = '<cc:text name="StaticText" 
                      defaultValue="rgm.resourcegroup.operation.remove.confirm"
                      bundleID="scBundle"/>';
     }
  } 

  msg = msg.replace('{0}', getHiddenResGrpFieldValue("HiddenResGrpName"));
  
  var result = confirm(msg + '\n <cc:text name="StaticText"
                                          defaultValue="selectionConfirmation"
                                          bundleID="scBundle"/>');
  if (result) {
   unRegisterEvents();
  }   
  
  return result;
 }

// This function will toggle the disabled state of action buttons
// depending on single or multiple selections.
function toggleResGrpTblBtnsDisabledState() {
    // Action button.
    var restartActionButton = "<cc:text name='RestartActionButtonNameText'/>";
    var switchPrimActionButton = "<cc:text name='SwitchPrimActionButtonNameText'/>";
    var deleteActionButton = "<cc:text name='DeleteActionButtonNameText'/>";
    var rgJumpMenu = "<cc:text name='RGsActionMenuNameText'/>";
    

    var combinedState = getHiddenResGrpFieldValue("HiddenResGrpBtnState");  

   if (combinedState!=''){
    if (combinedState.charAt(0) == '1') {
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,1);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",false,2);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,3);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,4);
        ccSetButtonDisabled(restartActionButton, "scForm", false);

        if (combinedState.charAt(2) == '1'){
	    ccSetButtonDisabled(switchPrimActionButton, "scForm", true);
	    ccSetButtonDisabled(deleteActionButton, "scForm", true);
	}
	else {
	    ccSetButtonDisabled(switchPrimActionButton, "scForm", false);
	    ccSetButtonDisabled(deleteActionButton, "scForm", false);
	}
    }
    if (combinedState.charAt(0) == '2') {

        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",false,1);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,2);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,3);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",false,4);
        ccSetButtonDisabled(restartActionButton, "scForm", false); 

        if (combinedState.charAt(2) == '1'){
	    ccSetButtonDisabled(switchPrimActionButton, "scForm", true);
	    ccSetButtonDisabled(deleteActionButton, "scForm", true);
	}
	else {
	    ccSetButtonDisabled(switchPrimActionButton, "scForm", false);
	    ccSetButtonDisabled(deleteActionButton, "scForm", false);
	}
    }
    if (combinedState.charAt(0) == '3') {
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,1);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,2);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",false,3);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,4);
        ccSetButtonDisabled(restartActionButton, "scForm", true);
        ccSetButtonDisabled(switchPrimActionButton, "scForm", true);

        if (combinedState.charAt(2) == '1') {
	    ccSetButtonDisabled(deleteActionButton, "scForm", true);
        } else  {
	    ccSetButtonDisabled(deleteActionButton, "scForm", false);
        }
    }
    if (combinedState.charAt(0) == '4') {
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,1);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,2);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,3);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,4);
        ccSetButtonDisabled(restartActionButton, "scForm", false);
        ccSetButtonDisabled(switchPrimActionButton, "scForm", true); 
        ccSetButtonDisabled(deleteActionButton, "scForm", true);
    }
    if (combinedState.charAt(0) == '5') {
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,1);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,2);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,3);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,4);       
        ccSetButtonDisabled(restartActionButton, "scForm", true);
        ccSetButtonDisabled(switchPrimActionButton, "scForm", true);
        ccSetButtonDisabled(deleteActionButton, "scForm", true);
    }
    if (combinedState.charAt(1) == '1') {
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,5);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,6);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",false,7);

    }
    if (combinedState.charAt(1) == '0') {
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",false,5);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",false,6);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,7);

    }
    if (combinedState.charAt(1) == '2') {
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,5);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,6);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,7);
    }
   } else {
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,1);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,2);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,3);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,4);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,5);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,6);
        ccSetDropDownMenuOptionDisabled(rgJumpMenu,"scForm",true,7);
        ccSetButtonDisabled(restartActionButton, "scForm", true);
	ccSetButtonDisabled(switchPrimActionButton, "scForm", true);
	ccSetButtonDisabled(deleteActionButton, "scForm", true);
   }
}	
</SCRIPT>

<!-- Action Table -->
<cc:actiontable name="Table"
                bundleID="scBundle"
                title="rgm.resourcegroups.status.table.title"
                summary="rgm.resourcegroups.status.table.summary"
                empty="rgm.resourcegroups.status.table.empty"
                selectionJavascript="setTimeout('toggleResGrpTblBtnsDisabledState()', 0)"
                selectionType="multiple"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="true"
                showSelectionSortIcon="false"
                showSortingRow="true"
                page="1"/>

</jato:pagelet>
