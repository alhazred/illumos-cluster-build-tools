<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)tasksPage.js	1.9	08/05/20 SMI"
 *
 */
--%>

<script language="JavaScript" type="text/javascript">
var hide  = true;
var tleft;
var ttop; 
var ileft;
var colnum; // number of tasks
var errMsg;

//
// The prized coordinate locating function.
// From "JavaScript & DHTML Cookbook" by Danny Goodman (O'Reilly, 2003).
// Used with permission
//
function setTaskNumber(num){
    colnum = num;
}
function setTaskError(msg){
    errMsg = msg;
}

function getElementPosition(elemID) {
    var offsetTrail = document.getElementById(elemID);
    var offsetLeft = 0;
    var offsetTop = 0;


    while (offsetTrail) {
        offsetLeft += offsetTrail.offsetLeft;
        offsetTop += offsetTrail.offsetTop;
        offsetTrail = offsetTrail.offsetParent;
    }
    if (navigator.userAgent.indexOf("Mac") != -1 && 
        typeof document.body.leftMargin != "undefined") {
        offsetLeft += document.body.leftMargin;
        offsetTop += document.body.topMargin;
    }
   tleft=offsetLeft;
   ttop=offsetTop;

   //return {left:offsetLeft, top:offsetTop}; 

}
function getElementPosition2(elemID) {
    var offsetTrail = document.getElementById(elemID);
    var offsetLeft = 0;
    var offsetTop = 0;


    while (offsetTrail) {
        offsetLeft += offsetTrail.offsetLeft;
        offsetTop += offsetTrail.offsetTop;
        offsetTrail = offsetTrail.offsetParent;
    }
    if (navigator.userAgent.indexOf("Mac") != -1 && 
        typeof document.body.leftMargin != "undefined") {
        offsetLeft += document.body.leftMargin;
        offsetTop += document.body.topMargin;
    }
   ileft=offsetLeft;

   //return {left:offsetLeft, top:offsetTop}; 

}


function closeAll(num) {
  for(i=1;i<=colnum;i++) {
    if(document.getElementById("info"+i) && document.getElementById("togImg"+i)) {
      document.getElementById("info"+i).style.display = "none";   
      document.getElementById("togImg"+i).src = "/SunClusterManager/images/task/rightToggle.gif";
    }
  }
  document.getElementById("i"+num).focus();
}
function showDiv(num) {
document.getElementById("info"+num).style.display = "block";
}

function hideAllMenus() {
  for(i=1;i<=colnum;i++) {
    if(document.getElementById("info"+i) && document.getElementById("togImg"+i)) {
      document.getElementById("info"+i).style.display = "none";
      document.getElementById("togImg"+i).src = "/SunClusterManager/images/task/rightToggle.gif";  
    }
  }
}


// Toggle functions
function disableLink() {
  alert(errMsg);
  return false;
}

function onTaskClick(){
 // disable only those hrefs which open new wizard windows
  var ctr = colnum;
  for (i=0;i<colnum;i++){
      document.links[ctr].onclick = disableLink;
      ctr = ctr + 2;
  }
}


function test(num) {
  getElementPosition2("togImg"+num);
  if (document.getElementById("info"+num).style.display != "block") {
    for(i=1;i<=colnum;i++) {
      if(i!=num && document.getElementById("togImg"+i) && document.getElementById("info"+i)) {
        document.getElementById("togImg"+i).src = "/SunClusterManager/images/task/rightToggle.gif";
        document.getElementById("info"+i).style.display = "none";
      }
    }
    document.getElementById("togImg"+num).src = "/SunClusterManager/images/task/rightToggle-selected.gif";

    getElementPosition("gif"+num);



    document.getElementById("info"+num).style.display = "block";
    document.getElementById("info"+num).style.top = (ttop + 10) + 'px';
    document.getElementById("info"+num).style.left = (tleft -1) + 'px';
    document.getElementById("info"+num).style.width = (ileft - tleft) + 29 + 'px';
    
    document.getElementById("close"+num).focus();
  }
  else if (document.getElementById("info"+num).style.display = "block"){
    for(i=1;i<=colnum;i++) {
      if(document.getElementById("togImg"+i)) {
        document.getElementById("togImg"+i).src = "/SunClusterManager/images/task/rightToggle.gif";
      }
    }
    document.getElementById("info"+num).style.display = "none";
  }
}


// Hover Functions

function hoverImg(num) {
  if (document.getElementById("info"+num).style.display != "block") {
    document.getElementById("togImg"+num).src = "/SunClusterManager/images/task/rightToggle-rollover.gif"
  }
  else { 
    document.getElementById("togImg"+num).src = "/SunClusterManager/images/task/rightToggle-selected.gif";
  }
}
function outImg(num) {
  if (document.getElementById("info"+num).style.display != "block") {
    document.getElementById("togImg"+num).src = "/SunClusterManager/images/task/rightToggle.gif";
  }
  else {
    document.getElementById("togImg"+num).src = "/SunClusterManager/images/task/rightToggle-selected.gif";
  }
}

</script>
