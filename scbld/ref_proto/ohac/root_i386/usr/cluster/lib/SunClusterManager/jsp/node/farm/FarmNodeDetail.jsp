<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)FarmNodeDetail.jsp	1.3	08/05/20 SMI"
 */
--%>

<%@page info="FarmNodeDetail" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.node.farm.FarmNodeDetailViewBean">

<!-- Page header component -->
<spm:header pageTitle="node.farm.detail.title" copyrightYear="2005" baseName="com.sun.cluster.spm.resources.Resources" bundleID="scBundle" event="ESC_cluster_ipmp_group_state,ESC_cluster_ipmp_group_member_change,ESC_cluster_tp_path_config_change,ESC_cluster_node_state_change,ESC_cluster_quorum,ESC_cluster_dcs_primaries_changing,ESC_cluster_dcs_config_change,ESC_cluster_dcs_state_change" onLoad="reloadTree()">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<!-- Bread Crumb component -->
<cc:breadcrumbs name="BreadCrumb" bundleID="scBundle" />

<!-- Page Title -->
<cc:pagetitle name="GenericPageTitle" bundleID="scBundle" pageTitleText="node.farmStatus.pageTitle" showPageTitleSeparator="true" pageTitleHelpMessage="node.farmStatus.help"/>

<!-- Farm Node Details -->
<div class="ConMgn"> 

<table border="0" cellpadding="0" cellspacing="0">
  <!-- Farm Node Name  -->
  <tr>
    <td valign="top">
      <div class="ConTblCl1Div">&nbsp;
        <cc:label name="FarmNodeNameLabel" bundleID="scBundle"
                  defaultValue="node.farm.label.name"
                  styleLevel="2"
                  elementName="FarmNodeName" />
      </div>
    </td>
    <td valign="top">
      <div class="ConTblCl2Div">
        <span id="psLbl2" class="ConDefTxt">
          <cc:text name="FarmNodeName" bundleID="scBundle"/>
        </span>
      </div>
    </td>
  </tr>
  <!-- Farm node state -->
  <tr>
    <td valign="top">
	<div class="ConTblCl1Div">&nbsp;
	<cc:label name="FarmNodeStateLabel" bundleID="scBundle"
		  defaultValue="node.farm.label.state"
		  styleLevel="2"
		  elementName="FarmNodeState" />
	</div>
    </td>
    <td valign="top">
	<div class="ConTblCl2Div">
	<span id="psLbl2" class="ConDefTxt">
	  <cc:text name="FarmNodeState" bundleID="scBundle"/>
	</span>
	</div>
    </td>
  </tr>
  <!-- Farm node monitoring state -->
  <tr>
    <td valign="top">
	<div class="ConTblCl1Div">&nbsp;
	<cc:label name="FarmNodeMntrLabel" bundleID="scBundle"
		  defaultValue="node.farm.label.mntr.state"
		  styleLevel="2"
		  elementName="FarmNodeMntrState" />
	</div>
    </td>
    <td valign="top">
	<div class="ConTblCl2Div">
	<span id="psLbl2" class="ConDefTxt">
	  <cc:text name="FarmNodeMntrState" bundleID="scBundle"/>
	</span>
	</div>
    </td>
  </tr>
  <!-- Farm InterConnect -->
  <tr>
    <td valign="top">
	<div class="ConTblCl1Div">&nbsp;
	<cc:label name="FarmNodeIntrCntLabel" bundleID="scBundle"
		  defaultValue="node.farm.label.interconnect"
		  styleLevel="2"
		  elementName="FarmNodeIntrCnt" />
	</div>
    </td>
    <td valign="top">
	<div class="ConTblCl2Div">
	<span id="psLbl2" class="ConDefTxt">
	  <cc:text name="FarmNodeIntrCnt" bundleID="scBundle"/>
	</span>
	</div>
    </td>
  </tr>
</table>
</div>

<div><img src="/com_sun_web_ui/images/other/dot.gif"
          width="1" height="10" alt=""></div>


<spm:rbac auths="solaris.cluster.device.read">
<jato:containerView name="StatusTableView">
<jsp:include page="../../devicegroup/DeviceGroupTablePagelet.jsp"/>
</jato:containerView>
</spm:rbac>

<br>

</jato:form>
</spm:header>
</jato:useViewBean>
