<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ZonesStatus.jsp 1.4     08/05/20 SMI"
 */
--%>

<%@page info="ZonesStatus" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.zone.ZonesStatusViewBean">

<!-- Page header component -->
<spm:header 	pageTitle="zones.status.title" 
		copyrightYear="2005" 
		baseName="com.sun.cluster.spm.resources.Resources" 
		bundleID="scBundle" 
		event="ESC_cluster_zones_state" 
		onLoad="reloadTree()">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<!-- PageTitle -->
<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
 pageTitleText="zones.status.title" 
 showPageTitleSeparator="true"
 pageTitleHelpMessage="zones.status.help">

<div><img src="/com_sun_web_ui/images/other/dot.gif" width="1" height="10" alt=""></div>

<jato:containerView name="TableView">

<!-- Action Table -->
<cc:actiontable
name="Table"
bundleID="scBundle"
title="zones.tableTitle"
summary="zones.tableSummary"
empty="zones.tableEmpty"
rowSelectionType="none"
showAdvancedSortingIcon="false"
showLowerActions="false"
showPaginationControls="false"
showPaginationIcon="false"
showSelectionIcons="false"
showSortingRow="false"
page="1"/>

</jato:containerView>

</cc:pagetitle>
</jato:form>
</spm:header>
</jato:useViewBean>
