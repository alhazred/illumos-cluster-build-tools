#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident  "@(#)precompile.sh 1.2     08/05/20 SMI"

console_config=`cat /etc/webconsole/console/config.properties`
console_dombase=`printf "%s\n" $console_config|nawk -F= '/console_dombase=/ {print $2}'`
tomcat_home=`printf "%s\n" $console_config|nawk -F= '/container_home=/ {print $2}'`
console_home=`printf "%s\n" $console_config|nawk -F= '/console_home=/ {print $2}'`
jato_home=`printf "%s\n" $console_config|nawk -F= '/jato_home=/ {print $2}'`
java_home=`printf "%s\n" $console_config|nawk -F= '/java_home=/ {print $2}'`

java=${java_home}/bin/java
tools_jar=${java_home}/lib/tools.jar
webapp=SunClusterManager
webapp_dir="/usr/cluster/lib/${webapp}"
jspc=org.apache.jasper.JspC


jato_jar=${jato_home}/jato.jar
webconsole_home=${console_home}/lib
jar_dirs="${tomcat_home} ${webconsole_home}"
work_dir="${console_dombase}/console/work/com_sun_web_console/localhost/${webapp}"

jars=`find ${jar_dirs} -name "*.jar"`
jars=`echo $jars | sed 's/ /:/g` 
jars=${jars}:${jato_jar}:${tools_jar}:.

args="-s -v -l -compile -d ${work_dir} -webapp ${webapp_dir}"
${java} -classpath ${jars} ${jspc} ${args}
