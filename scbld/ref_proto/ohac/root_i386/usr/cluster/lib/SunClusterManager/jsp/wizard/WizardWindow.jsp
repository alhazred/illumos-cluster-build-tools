<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)WizardWindow.jsp 1.4     08/05/20 SMI"
 */
--%>
<%@ page language="java" %> 
<%@page import="com.sun.web.ui.common.CCI18N" %>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>

<%
    // Parameters
    //
    // wizWinMsthdSrc - application image for masthead
    // wizWinMsthdAlt - alternate src
    // wizWinBaseName - resource file path
    // wizWinBundleId - bundleId
    // wizWinTitle - page title
    //
    // Not used here but available
    //
    // wizard info - a list of application wizard defined name value
    //           pairs relevant to the application "backend" wizard class
    //		 For example the XML wizard needs the xml file path and
    //		 a name. So we'll use 
    //
    //		 wizName for the name
    //		 wizXmlFile for the XML file path
    //
    //		 wizModelClassName - is a framework defined name
    //		 for the application wizard class. The wizard info
    //		 parameters will be passed to this class

    /*
     * Eventually when proper default keys are place in this resource
     * file. Should also use define constants from a CCWizard class
     *
     */
    String pMsthdSrc =
	request.getParameter("WizardWindow.wizWinMsthdSrc") == null ?
	"wizard.window.secondary.src" :
	request.getParameter("WizardWindow.wizWinMsthdSrc");
    String pBaseName =
	request.getParameter("WizardWindow.wizWinBaseName") == null ?
	"com.sun.web.ui.resources.Resources" :
	request.getParameter("WizardWindow.wizWinBaseName");
    String pBundleId =
	request.getParameter("WizardWindow.wizWinBundleId") == null ?
	"tagBundle" : request.getParameter("WizardWindow.wizWinBundleId");

    // Get query parameters.
    String pWinTitle = 
	(request.getParameter("WizardWindow.wizWinTitle") != null)
	    ? request.getParameter("WizardWindow.wizWinTitle")
	    : request.getParameter("WizardTitle");
    if (pWinTitle == null) pWinTitle = "wizard.window.title";

    String pMsthdAlt = 
	(request.getParameter("WizardWindow.wizWinMsthdAlt") != null)
	    ? request.getParameter("WizardWindow.wizWinMsthdAlt")
	    : "wizard.window.secondary.alt";
%>

<jato:useViewBean
	className="com.sun.cluster.spm.wizard.WizardWindowViewBean">

<!-- Header -->
<cc:header name="Header"
	pageTitle="<%=pWinTitle %>"
	copyrightYear="2004"
	onResize="javascript: resize_hack()"
	onLoad="javascript: wizOnLoad(WizardWindow_Wizard)"
	baseName="<%=pBaseName %>"
	bundleID="<%=pBundleId %>"
	preserveFocus="true"
	preserveScroll="true"
    	isPopup="true">

<cc:form name="wizWinForm" method="post">

<!-- Secondary Masthead -->
<cc:secondarymasthead name="Masthead" bundleID="<%=pBundleId %>"
    src="<%=pMsthdSrc %>" alt="<%=pMsthdAlt %>"/>

<!-- Wizard -->
<!-- bundleId may be overloaded from the wizard class or model -->
<spm:wizard name="Wizard" />

<cc:hidden name="wizName"/>
<cc:hidden name="wizClassName"/>
<cc:hidden name="wizWinMsthdSrc"/>
<cc:hidden name="wizWinMsthdAlt"/>
<cc:hidden name="wizWinBaseName"/>
<cc:hidden name="wizWinBundleId"/>
<cc:hidden name="wizWinTitle"/>
<cc:hidden name="wizWinName"/>
<cc:hidden name="wizBtnForm"/>
<cc:hidden name="wizRefreshCmdChild"/>

</cc:form>
</cc:header>
</jato:useViewBean>
