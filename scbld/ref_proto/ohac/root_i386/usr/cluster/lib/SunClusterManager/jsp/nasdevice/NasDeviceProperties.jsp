<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)NasDeviceProperties.jsp	1.5	08/05/20 SMI"
 */
--%>

<%@page info="NasDeviceProperties" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean 
    className="com.sun.cluster.spm.nasdevice.NasDevicePropertiesViewBean">

<!-- Page header component -->
<spm:header pageTitle="devicegroup.title"
           copyrightYear="2003" 
           baseName="com.sun.cluster.spm.resources.Resources" 
           bundleID="scBundle" onLoad="reloadTree()"
           event="ESC_cluster_dcs_primaries_changing,ESC_cluster_dcs_state_change">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<!-- Bread Crumb -->
<cc:breadcrumbs name="NasDevsPropsBreadCrumb" bundleID="scBundle" />

<SCRIPT LANGUAGE="JavaScript">
 function displayConfirmation() {
  var header = '<cc:text name="StaticText"
                       defaultValue="devicegroup.operation.deleteNas.confirm"
                       bundleID="scBundle"/>';

  var msg = header.replace('{0}', '<cc:text name="NDName"/>');

  var result = confirm(msg + '\n <cc:text name="StaticText"
                                          defaultValue="selectionConfirmation"
                                          bundleID="scBundle"/>');

  if (result) {
    unRegisterEvents() ;
  }   

  return result;

 }	
</SCRIPT>

<!-- Alert -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
              pageTitleText="devicegroup.properties.pageTitle.title"
              showPageTitleSeparator="true"
              pageTitleHelpMessage="devicegroup.properties.pageTitle.nasDetails.help"/>

<BR>
<div class="ConMgn"> 
 <!-- Buttons -->
 <table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <spm:rbac auths="solaris.cluster.modify">
        <jato:content name="Edit">
        <cc:button name="DGPropertyEditButton" bundleID="scBundle"
                   defaultValue="devicegroup.properties.button.edit"/>
        </jato:content>
      </spm:rbac>
      <spm:rbac auths="solaris.cluster.admin">
        <jato:content name="Delete">
          <cc:button name="NDDeleteButton" bundleID="scBundle"
                     defaultValue="devicegroup.properties.button.deleteNas"
                     onClick="return displayConfirmation()"/>
        </jato:content>
      </spm:rbac>
    </td>
  </tr>
 </table>

<!-- Summary -->
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
 <tr>
   <td valign="top">
     <div class="ConTblCl1Div">&nbsp;
       <span class="LblLev2Txt">
         <label>
           <cc:text name="StaticText" bundleID="scBundle"
                    defaultValue="devicegroup.properties.nas.name"/>
         </label>
       </span>
     </div>
   </td>
   <td valign="top">
     <div class="ConTblCl2Div">
       <span id="psLbl2" class="ConDefTxt">
         <cc:text name="NDName"/>
       </span>
     </div>
   </td>
 </tr>
 <tr>
   <td valign="top">
     <div class="ConTblCl1Div">&nbsp;
       <span class="LblLev2Txt">
         <label>
           <cc:text name="StaticText" bundleID="scBundle"
                    defaultValue="devicegroup.properties.nas.type"/>
         </label>
       </span>
     </div>
   </td>
   <td>
     <div class="ConTblCl2Div">
       <span id="psLbl2" class="ConDefTxt">
         <cc:text name="DeviceType"/>
       </span>
     </div>
   </td>
 </tr>
 <tr>
   <td valign="top">			
     <div class="ConTblCl1Div">&nbsp;
       <span class="LblLev2Txt">
         <label>
           <cc:text name="StaticText" bundleID="scBundle"
                    defaultValue="devicegroup.properties.nas.vendor"/>
         </label>
       </span>
     </div>
   </td>
   <td valign="top">
     <div class="ConTblCl2Div">
       <span id="psLbl2" class="ConDefTxt">
         <cc:text name="Vendor"/>
       </span>
     </div>
   </td>  
 </tr>
<jato:content name="NasProperties">
 <tr>
   <td valign="top">    	
	<div class="ConTblCl1Div">&nbsp;
	<span class="LblLev2Txt">
	 <label>
           <cc:text name="StaticText" bundleID="scBundle"
                    defaultValue="devicegroup.properties.nas.login"/>
	 </label>
	</span>
     </div>
	</td>
   <td valign="top">
	<div class="ConTblCl2Div">
	<span id="psLbl2" class="ConDefTxt">
	 <cc:text name="Login"/>
	</span>
     </div>
   </td> 
 </tr>
 <tr>
   <td valign="top">            
        <div class="ConTblCl1Div">&nbsp;
        <span class="LblLev2Txt">
         <label>
           <cc:text name="StaticText" bundleID="scBundle"
                    defaultValue="devicegroup.properties.nas.passwd"/>
         </label>
        </span>
     </div>
        </td>
   <td valign="top">
        <div class="ConTblCl2Div">
        <span id="psLbl2" class="ConDefTxt">
         <cc:text name="Passwd"/>
        </span>
     </div>
   </td> 
 </tr>
</jato:content>
 <tr>
   <td valign="top">            
        <div class="ConTblCl1Div">&nbsp;
        <span class="LblLev2Txt">
         <label>
           <cc:text name="StaticText" bundleID="scBundle"
                    defaultValue="devicegroup.properties.nas.exports"/>
         </label>
        </span>
     </div>
        </td>
   <td valign="top">
        <div class="ConTblCl2Div">
        <span id="psLbl2" class="ConDefTxt">
         <cc:text name="ExportList"/>
        </span>
     </div>
   </td> 
 </tr>
<tbody>
</table>
</div>

<div><img src="/com_sun_web_ui/images/other/dot.gif" width="1" height="10" alt=""></div>

</jato:form>
</spm:header>
</jato:useViewBean>
