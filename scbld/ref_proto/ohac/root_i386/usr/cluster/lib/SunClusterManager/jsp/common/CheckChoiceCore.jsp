<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckChoiceCore.jsp	1.5	08/05/20 SMI"
 */
--%>


<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>


<jato:pagelet>

<!-- Page header component -->
<cc:header pageTitle="quorumCheck.title" copyrightYear="2002" baseName="com.sun.cluster.spm.resources.Resources" bundleID="testBundle">

<jato:form name="checkForm" method="post">

<!-- Secondary Masthead -->
<cc:secondarymasthead name="GenericSecondaryMasthead" title=""
   bundleID="testBundle" />

<div class="content-layout">
<table BORDER="0" width="100%">
  <tr>
    <td>
    <!-- Alert -->
    <cc:alertinline name="Alert" bundleID="testBundle" />
    </td>
  </tr>
</table>

  <!-- Buttons -->
<jato:content name="CancelButton">
<table>
  <tr>
    <td>
    <cc:button name="OkButton" bundleID="testBundle" defaultValue="checkChoice.ok"/>
    </td>
    <td>
      <cc:button name="CancelButton" bundleID="testBundle" defaultValue="checkChoice.cancel"/>

    </td>
  </tr>
</table>
</jato:content>
 
<jato:content name="ConfirmationButton">
   <cc:button name="ConfirmationButton" bundleID="testBundle" defaultValue="checkChoice.ok"/>
</jato:content>

</div>

<jato:content name="CommandInfo">
<br> <br>
<hr>
<b>
 <cc:text name="CommandLabel" bundleID="testBundle" 
  defaultValue="checkChoice.commandLabel"/>
 </b>
</jato:content>

<jato:content name="ErrorInfo">
 <br>
 <b>
 <cc:text name="CommandLabel" bundleID="testBundle" 
  defaultValue="checkChoice.errorLabel"/>
</b>
</jato:content>

</jato:form>

</cc:header>

</jato:pagelet>
