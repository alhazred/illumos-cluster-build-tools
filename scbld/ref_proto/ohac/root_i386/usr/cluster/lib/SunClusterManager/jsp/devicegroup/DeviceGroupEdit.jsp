<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)DeviceGroupEdit.jsp	1.5	08/05/20 SMI"
 */
--%>

<%@page info="DeviceGroupEdit" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.devicegroup.DeviceGroupEditViewBean">

<!-- Page header component -->
<spm:header pageTitle="devicegroup.title" 
            copyrightYear="2002" 
            baseName="com.sun.cluster.spm.resources.Resources" 
            bundleID="scBundle">

<jato:form name="scForm" method="post">

<!-- Bread Crumb -->
<cc:breadcrumbs name="DGEditBreadCrumb" bundleID="scBundle" useGrayBg="true" />

<!-- A set of javascript functions to handle the MoveUp and MoveDown buttons
     without painfully reloading the page -->
<script language="JavaScript">
function updateHiddenField(list) {
  var hidden = document.scForm.elements['DeviceGroupEdit.PrimaryValueHidden'];
  
  hidden.value = '';
  for (i = 0; i < list.length - 1; i++) {
    if (i > 0) {
      hidden.value = hidden.value + ',';
    }
    hidden.value = hidden.value + list.options[i].value;
  }
}

function moveUp() {
  var list = document.scForm.elements['DeviceGroupEdit.PrimaryValue'];
  
  if ((list.selectedIndex >= 0) &&
      (list.options[list.selectedIndex].value != 'ignoreMe') &&
      (list.selectedIndex != 0)) {
      tmpText  = list.options[list.selectedIndex - 1].text;
      tmpValue = list.options[list.selectedIndex - 1].value;
      list.options[list.selectedIndex - 1].text =
          list.options[list.selectedIndex].text;
      list.options[list.selectedIndex - 1].value =
          list.options[list.selectedIndex].value;
      list.options[list.selectedIndex].text  = tmpText;
      list.options[list.selectedIndex].value = tmpValue;

      list.options[list.selectedIndex - 1].selected = true;
      updateHiddenField(list);
  }
}

function moveDown() {
  var list = document.scForm.elements['DeviceGroupEdit.PrimaryValue'];
  
  if ((list.selectedIndex >= 0) &&
      (list.options[list.selectedIndex].value != 'ignoreMe') &&
      (list.selectedIndex != list.length - 2)) {
      tmpText  = list.options[list.selectedIndex + 1].text;
      tmpValue = list.options[list.selectedIndex + 1].value;
      list.options[list.selectedIndex + 1].text =
          list.options[list.selectedIndex].text;
      list.options[list.selectedIndex + 1].value =
          list.options[list.selectedIndex].value;
      list.options[list.selectedIndex].text  = tmpText;
      list.options[list.selectedIndex].value = tmpValue;

      list.options[list.selectedIndex + 1].selected = true;
      updateHiddenField(list);
  }
}
</script>

<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<!-- PageTitle -->
<cc:pagetitle name="GenericAddPageTitle" bundleID="scBundle"
              pageTitleText="devicegroup.edit.pageTitle.title" 
              showPageTitleSeparator="true"
              showPageButtonsTop="false"
              pageTitleHelpMessage="devicegroup.edit.pageTitle.help">

<!-- Set the layout style -->
<div class="content-layout">

<!-- PropertySheet -->
<cc:propertysheet name="PropertySheet" bundleID="scBundle" 
                  showJumpLinks="false"/>

</div>

</cc:pagetitle>
</jato:form>
</spm:header>
</jato:useViewBean>
