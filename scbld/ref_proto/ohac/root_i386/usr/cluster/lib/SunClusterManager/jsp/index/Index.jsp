<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Index.jsp	1.7	08/05/20 SMI"
 */
--%>

<%@page info="Index" language="java"%>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<jato:useViewBean className="com.sun.cluster.spm.index.IndexViewBean">

<HTML>
<HEAD>
<TITLE>Sun Cluster Manager</TITLE>

<FRAMESET FRAMESPACING="2" TITLE="Sun Cluster Management" ROWS="105,*" BORDER="0">
    <FRAME SRC="/SunClusterManager/banner/Banner" NAME="banner" TITLE="Banner" 
	   SCROLLING="NO" MARGINWIDTH="0" MARGINHEIGHT="0">
    <FRAMESET FRAMESPACING="2" COLS="205,*" NAME="menu_content" 
	      TITLE="Navigation Menu" BORDER="2">
        <FRAME SRC="/SunClusterManager/html/blank.html" NAME="menu" TITLE="Navigation Menu" 
	       SCROLLING="auto" MARGINWIDTH="6" MARGINHEIGHT="2">
	<FRAME SRC="/SunClusterManager/html/blank.html" NAME="content" TITLE="Content" 
	       SCROLLING="auto" MARGINWIDTH="0" MARGINHEIGHT="0">
    </FRAMESET>
</FRAMESET>

<body onload="checkJava();">

<SCRIPT LANGUAGE="JavaScript">

  function checkJava() {
    if (!navigator.javaEnabled()) {
	parent.menu.location="/SunClusterManager/tree/Tree" ;
	parent.content.location="/SunClusterManager/cluster/ClusterStatus?FromTree=yes" ;
    }
  }
</SCRIPT>

</body>

</HEAD>
</HTML>

</jato:useViewBean>
