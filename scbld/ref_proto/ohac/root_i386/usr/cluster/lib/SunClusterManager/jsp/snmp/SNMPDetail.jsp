<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)SNMPDetail.jsp	1.9	08/05/20 SMI"
 */
--%>

<%@page info="SNMP" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.snmp.SNMPDetailViewBean">

<!-- Page header component -->
<spm:header pageTitle="snmp.title" copyrightYear="2005"
            baseName="com.sun.cluster.spm.resources.Resources"
            bundleID="scBundle"
            onLoad="reloadTree()">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<SCRIPT LANGUAGE="JavaScript">

function toggleHostDisabledState() {
     // Action button and menu names.
     var actionButton = "<cc:text name='HostActionButtonNameText'/>";
 
     // Element name (prefix) of selection checkbox.
     var elementName = "<cc:text name='HostSelectionNameText'/>";
 
     // Document form.
     var form = document.scForm;
 
     // Flag indicating to disable action button and menu options.
     var disabled = true;
 
     // Set flag if a selection has been made.
     for (i = 0; i < form.elements.length; i++) {
         var e = form.elements[i];
 
         if (e.name.indexOf(elementName) != -1) {
             if (e.checked) {
                 disabled = false;
                 break;
             }
         }
     }
 
     // Toggle action button disabled state.
     ccSetButtonDisabled(actionButton, "scForm", disabled);
 }

function toggleUserDisabledState() {
     // Action button and menu names.
     var deleteButton = "<cc:text name='UserActionButtonNameText'/>";
     var defaultButton = "<cc:text name='DefaultActionButtonNameText'/>";
 
     // Element name (prefix) of selection checkbox.
     var elementName = "<cc:text name='UserSelectionNameText'/>";
 
     // Document form.
     var form = document.scForm;
 
     // Flag indicating to disable action button and menu options.
     var deleteDisabled = true;
     var defaultDisabled = true;

     var numChecked = 0;
 
     // Set flag if a selection has been made.
     for (i = 0; i < form.elements.length; i++) {
         var e = form.elements[i];
 
         if (e.name.indexOf(elementName) != -1) {
             if (e.checked) {
		 numChecked++;
                 deleteDisabled = false;
             }
         }
     }

     if (numChecked == 1) {
	 defaultDisabled = false;
     }
 
     // Toggle action button disabled state.
     ccSetButtonDisabled(deleteButton, "scForm", deleteDisabled);
     ccSetButtonDisabled(defaultButton, "scForm", defaultDisabled);
 }

function displayConfirmation() {

     var result = confirm('<cc:text name="ConfirmMsgStaticText" defaultValue="snmp.default.confirmation" bundleID="scBundle"/>');
     if (result) {
          unRegisterEvents();
     }
     return result;
 }
</SCRIPT>

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
              pageTitleText="snmp.detail.pageTitle.title"
              showPageTitleSeparator="true"
              pageTitleHelpMessage="snmp.detail.pageTitle.help"/>

<div><img src="/com_sun_web_ui/images/other/dot.gif"
          width="1" height="10" alt=""></div>

<!-- Command Alert -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<div><img src="/com_sun_web_ui/images/other/dot.gif"
          width="1" height="10" alt=""></div>

<div class="ConMgn"> 
  <table border="0" cellpadding="0" cellspacing="0">
    <!-- Node Name  -->
    <tr>
      <td valign="top">
        <div class="ConTblCl1Div">&nbsp;
          <cc:label name="NodeLabel" bundleID="scBundle"
                    defaultValue="snmp.detail.eventmib.node.label"
                    styleLevel="2"
                    elementName="ProtocolValue" />
        </div>
      </td>
      <td valign="top">
        <div class="ConTblCl2Div">
          <span id="psLbl2" class="ConDefTxt">
            <cc:text name="NodeValue" bundleID="scBundle"/>
          </span>
        </div>
      </td>
    </tr>
    <!-- SNMP Protocol Version  -->
    <tr>
      <td valign="top">
        <div class="ConTblCl1Div">&nbsp;
          <cc:label name="ProtocolLabel" bundleID="scBundle"
                    defaultValue="snmp.detail.eventmib.protocol.label"
                    styleLevel="2"
                    elementName="ProtocolValue" />
        </div>
      </td>
      <td valign="top">
        <div class="ConTblCl2Div">
          <span id="psLbl2" class="ConDefTxt">
            <cc:text name="ProtocolValue" bundleID="scBundle"/>
          </span>
        </div>
      </td>
    </tr>
  </table>
</div>

<div><img src="/com_sun_web_ui/images/other/dot.gif"
          width="1" height="10" alt=""></div>

<spm:rbac auths="solaris.cluster.read">
  <cc:actiontable name="SNMPHostsActionTable"
                  bundleID="scBundle"
                  title="snmp.detail.hosts.table.title"
                  summary="snmp.detail.hosts.table.summary"
                  empty="snmp.detail.hosts.table.empty"
		  selectionJavascript="setTimeout('toggleHostDisabledState()', 0)"
                  selectionType="multiple"
                  showAdvancedSortingIcon="false"
                  showLowerActions="false"
                  showPaginationControls="false"
                  showPaginationIcon="false"
                  showSelectionIcons="true"
                  showSortingRow="false"
                  page="1"/>
</spm:rbac>

<div><img src="/com_sun_web_ui/images/other/dot.gif"
          width="1" height="10" alt=""></div>

<spm:rbac auths="solaris.cluster.read">
  <cc:actiontable name="SNMPUsersActionTable"
                  bundleID="scBundle"
                  title="snmp.detail.users.table.title"
                  summary="snmp.detail.users.table.summary"
                  empty="snmp.detail.users.table.empty"
		  selectionJavascript="setTimeout('toggleUserDisabledState()', 0)"
                  selectionType="multiple"
                  showAdvancedSortingIcon="false"
                  showLowerActions="false"
                  showPaginationControls="false"
                  showPaginationIcon="false"
                  showSelectionIcons="true"
                  showSortingRow="false"
                  page="1"/>

</spm:rbac>

</jato:form>
</spm:header>
</jato:useViewBean>
