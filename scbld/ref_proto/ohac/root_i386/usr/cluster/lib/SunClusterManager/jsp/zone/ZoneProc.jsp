<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ZoneProc.jsp	1.4	08/07/14 SMI"
 */
--%>

<%@page info="Zone" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.zone.ZoneProcViewBean">

<!-- Page header component -->
<spm:header	pageTitle="zones.title"
		copyrightYear="2005"
		baseName="com.sun.cluster.spm.resources.Resources"
		bundleID="scBundle"
		event="ESC_cluster_zones_state"
		onLoad="reloadTree">

<jato:form name="scForm" method="post">


<!-- Bread Crumb component -->
<cc:breadcrumbs name="BreadCrumb" bundleID="scBundle" />

<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle" pageTitleText="zoneProcesses.pageTitle" showPageTitleSeparator="true" pageTitleHelpMessage="zoneProcesses.help"/>

<!-- Set the layout style -->

<jato:containerView name="ZoneHeaderView">
<jsp:include page="ZoneHeaderPagelet.jsp"/>
</jato:containerView>

<!-- Action Table -->
<cc:actiontable
name="Table"
bundleID="scBundle"
title="zones.procTableTitle"
summary="zones.procTableSummary"
empty="zones.procTableEmpty"
rowSelectionType="none"
showAdvancedSortingIcon="false"
showLowerActions="false"
showPaginationControls="true"
showPaginationIcon="true"
showSelectionIcons="false"
showSortingRow="true"
maxRows="10"
page="1"/>


</jato:form>
</spm:header>
</jato:useViewBean>
