<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)SystemLogDate.jsp	1.5	08/05/20 SMI"
 */
--%>

<%@page info="SystemLogDate" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.node.SystemLogDateViewBean">

<!-- Page header component -->
<spm:header pageTitle="systemLogDate.pageTitle" copyrightYear="2002" baseName="com.sun.cluster.spm.resources.Resources" bundleID="scBundle" 
event="ESC_cluster_node_state_change">

<jato:form name="scForm" method="post">

<SCRIPT LANGUAGE="JavaScript">

<!-- Begin
function refreshOpener() {
  
 if (!window.opener) { 
   return;
 } 

 var type;


 // Get the type of button clicked from the opener 

 type = window.opener.dateType;

 var value;

 value = document.scForm.elements["<cc:text name='DateName'/>" + ".startDate"].value;

 var param;

 if (type == 'start') {
   param = '&startDate=';
 } else {
   param = '&stopDate=';
 }
 var url = window.opener.location.href;
 if (window.opener.location.href.indexOf(param) != -1) {
   if (type == 'start') {
     url=url.replace(/&startDate=.*&/g, "");
     url=url.replace(/&startDate=.*/g, "");
   } else {
     url=url.replace(/&stopDate=.*&/g, "");
     url=url.replace(/&stopDate=.*/g, "");
   }
 }
 if (window.opener.closed == 0) {
   if (type == 'start') {
     window.opener.document.testForm.elements["NodeSyslog.TableView.AdvancedFilterView.StartDate"].value = value;
   } else {
     window.opener.document.testForm.elements["NodeSyslog.TableView.AdvancedFilterView.StopDate"].value = value;
   }
 }
   
}
// End -->
</SCRIPT>
  
<cc:pagetitle name="PageTitle" bundleID="scBundle" pageTitleText="systemLogDate.pageTitle" showPageTitleSeparator="true" pageTitleHelpMessage="systemLogDate.help" showPageButtonsTop="false">

<div class="content-layout">
 <cc:datetime name="DateTime" bundleID="testBundle" />
</div>

</cc:pagetitle>

</jato:form>
</spm:header>
</jato:useViewBean>
