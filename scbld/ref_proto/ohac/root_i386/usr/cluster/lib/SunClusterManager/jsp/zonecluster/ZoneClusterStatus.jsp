<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>

<%--
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ZoneClusterStatus.jsp	1.4	08/07/10 SMI"
 */
--%>

<%@page info="ZoneClusterStatus" language="java"%>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.zonecluster.ZoneClusterStatusViewBean">

<!-- Define the resource bundle, html, head, meta, stylesheet and body tags -->
<spm:header pageTitle="zcStatus.pageTitle" 
            copyrightYear="2007"
            baseName="com.sun.cluster.spm.resources.Resources" 
            bundleID="scBundle" 
            event="ESC_zonecluster_state_change,ESC_cluster_quorum,ESC_cluster_rg,ESC_cluster_node_state_change,ESC_cluster_node_config_change,ESC_cluster_dcs_primaries_changing,ESC_cluster_dcs_config_change,ESC_cluster_dcs_state_change"
            onLoad="reloadTree()">


<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<!-- Alert Inline for command result -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<cc:pagetitle   name="GenericPageTitle" 
                bundleID="scBundle" 
                pageTitleText="zcStatus.pageTitle" 
                showPageTitleSeparator="true" 
                pageTitleHelpMessage="zcStatus.help"/>
                
<br>
<div class="ConMgn">
  <table border="0" cellpadding="0" cellspacing="0">
    <!-- Cluster Name  -->
    <tr>
      <td valign="top">
	<div class="ConTblCl1Div">&nbsp;
	  <cc:label name="NameLabel" 
                    bundleID="scBundle"
                    defaultValue="clusterConfiguration.field0"
                    styleLevel="2"
                    elementName="NameValue" />
	</div>
	</td>
      <td valign="top">
	<div class="ConTblCl2Div">
	  <span id="psLbl2" class="ConDefTxt">
            <cc:text name="NameValue" bundleID="scBundle"/>
	  </span>
	</div>
	</td>
    </tr>
  </table>
</div>

<div><img src="/com_sun_web_ui/images/other/dot.gif"
	  width="1" height="10" alt=""></div>


<spm:rbac auths="solaris.cluster.read">
  <jato:containerView name="zcNodesInfoView">
    <jsp:include page="../zonecluster/ZoneClusterNodesInfoPagelet.jsp"/>
  </jato:containerView>

<br>
</spm:rbac>

<spm:rbac auths="solaris.cluster.read">
<jato:containerView name="ResourceGroupsStatusView">
<jsp:include page="../rgm/ResourceGroupsStatusPagelet.jsp"/>
</jato:containerView>
<br>
</spm:rbac>

</jato:form>

</spm:header>
</jato:useViewBean>

