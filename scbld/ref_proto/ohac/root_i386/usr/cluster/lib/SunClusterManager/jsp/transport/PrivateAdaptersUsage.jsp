<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/**
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)PrivateAdaptersUsage.jsp	1.3	08/05/20 SMI"
 */
--%>
<%@ page language="java" %>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.transport.PrivateAdaptersUsageViewBean">

<!-- Page header component -->
<spm:header pageTitle="transport.usage.title" copyrightYear="2006"
  baseName="com.sun.cluster.spm.resources.Resources"
  bundleID="scBundle"
  onLoad="reloadTree()"
  event="ESC_cluster_rg,ESC_cluster_r_config_change,ESC_cluster_slm_thr_state">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<spm:SLMcheck name="SLMCheck" summary="slmcheck.message.summary"
  detail="slmcheck.message.detail">

<!-- Alert Inline for resource groups status -->
<spm:alertinline name="Alert" bundleID="scBundle" />

<!-- Page Title -->
<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
  pageTitleText="transport.adapters.usage.title"
  showPageTitleSeparator="true"
  pageTitleHelpMessage="transport.adapters.usage.help">

<div><img src="/com_sun_web_ui/images/other/dot.gif"
  width="1" height="10" alt=""></div>

<jato:containerView name="PrivateAdaptersUsageView">
  <jsp:include page="PrivateAdaptersUsagePagelet.jsp" />
</jato:containerView>

</cc:pagetitle>
</spm:SLMcheck>
</jato:form>
</spm:header>
</jato:useViewBean>

