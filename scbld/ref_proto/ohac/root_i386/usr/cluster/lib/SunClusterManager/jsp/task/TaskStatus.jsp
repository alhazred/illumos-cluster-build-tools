<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)TaskStatus.jsp	1.6	08/05/20 SMI"
 */
--%>

<%@page info="TaskStatus" language="java" %> 
<%@include file="../../js/tasksPage.js"%>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<script type="text/javascript" src="/com_sun_web_ui/js/browserVersion.js">
</script>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.task.TaskStatusViewBean">


<!-- Page header component -->
<spm:header 	pageTitle="taskStatus.title" 
		copyrightYear="2007" 
		baseName="com.sun.cluster.spm.resources.Resources" 
		bundleID="scBundle" 
		event="ESC_cluster_dpm_"
		onResize="hideAllMenus();">

<jato:form name="scForm" method="post">

<!-- Alert -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<spm:taskpage	name="ConfigureDataServiceWizard" 
		bundleID="scBundle"
		titleText="taskStatus.title"/>
</jato:form>
</spm:header>
</jato:useViewBean>
