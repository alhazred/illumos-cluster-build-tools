<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/**
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceGroupSwitch.jsp	1.5	08/05/20 SMI"
 */
--%>
<%@ page language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.rgm.ResourceGroupSwitchViewBean">

<!-- Page header component -->
<spm:header pageTitle="rgm.resourcegroup.title"
            copyrightYear="2003"
            baseName="com.sun.cluster.spm.resources.Resources"
            bundleID="scBundle">

<jato:form name="scForm" method="post">

<script language="javascript">
  function togglePagetitleActionButton() {

    // ok/add button and checkbox or radio button name
    var addButton = "<cc:text name='AddButtonNameText'/>";
    var viewName = "<cc:text name='ViewNameText'/>";
    var checkboxName = viewName + ".PrimariesTable.SelectionCheckbox";
    var radiobuttonName = viewName + ".PrimariesTable.SelectionRadiobutton";

    // Document form.
    var form = document.scForm;

    // Flag indicating to disable action button and menu options.
    var disabled = true;

    // Set flag if a selection has been made.
    for (i = 0; i < form.elements.length; i++) {
        var e = form.elements[i];

        if ((e.name.indexOf(checkboxName) != -1 || e.name.indexOf(radiobuttonName) != -1) 
                && e.name.indexOf("jato_boolean") == -1) {
            if (e.checked) {
                disabled = false;
                break;
            }
        }
    }
    ccSetButtonDisabled(addButton, "scForm", disabled);
  }
</script>
 
<!-- Page Title -->
<cc:pagetitle name="GenericAddPageTitle" bundleID="scBundle"
        pageTitleText="rgm.resourcegroup.switch.scalable.pageTitle.title"
        showPageTitleSeparator="true"
        showPageButtonsTop="false"
        pageTitleHelpMessage="rgm.resourcegroup.switch.scalable.pageTitle.help">

<br>

<div><img src="/com_sun_web_ui/images/other/dot.gif"
          width="1" height="10" alt=""></div>

<!-- Explanation text -->
<div class="content-layout">
  <cc:text name="Explanations" bundleID="scBundle"
           defaultValue="rgm.resourcegroup.switch.scalable.explanations" />
</div>

<div><img src="/com_sun_web_ui/images/other/dot.gif"
          width="1" height="10" alt=""></div>

<!-- Action Table -->
<cc:actiontable name="PrimariesTable"
                bundleID="scBundle"
                title="rgm.resourcegroup.switch.table.title"
                summary="rgm.resourcegroup.switch.table.summary"
                empty="rgm.resourcegroup.switch.table.empty"
                selectionType="multiple"
                selectionJavascript="togglePagetitleActionButton()"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="true"
                showSortingRow="false"
                page="1"/>

</cc:pagetitle>
</jato:form>
</spm:header>
</jato:useViewBean>
