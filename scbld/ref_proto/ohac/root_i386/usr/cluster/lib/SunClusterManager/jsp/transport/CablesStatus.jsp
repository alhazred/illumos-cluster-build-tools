<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CablesStatus.jsp	1.5	08/05/20 SMI"
 */
--%>

<%@page info="CablesStatus" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.transport.CablesStatusViewBean">

<!-- Page header component -->
<spm:header 	pageTitle="transport.cables.status.title" 
		copyrightYear="2004" 
		baseName="com.sun.cluster.spm.resources.Resources" 
		bundleID="scBundle" 
		event="ESC_cluster_tp" 
		onLoad="reloadTree()">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<!-- Command Alert -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<!-- Selection Error Alert -->
<spm:alertinline name="SelectionErrorAlert" bundleID="scBundle" />

<!-- PageTitle -->
<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
 pageTitleText="transport.cables.status.title" 
 showPageTitleSeparator="true"
 pageTitleHelpMessage="transport.cables.status.help">


<div><img src="/com_sun_web_ui/images/other/dot.gif" width="1" height="10" alt=""></div>

<jato:containerView name="TableView">

<SCRIPT LANGUAGE="JavaScript">
 
function getHiddenFieldValue(hiddenTextFieldName) {
    // Element name (prefix) of selection checkbox.
    var elementName = "<cc:text name='SelectionNameText'/>";

    // Document form.
    var form = document.scForm;
 
    for (i = 0; i < form.elements.length; i++) {
      var e = form.elements[i];
      if (e.name.indexOf(elementName) != -1) {
        if (e.checked) {
          var prefix =  elementName.substring(0,elementName.indexOf("Table.SelectionRadiobutton"));
          var cablesComp = prefix + "TableTiledView[" + e.value + "]." + hiddenTextFieldName;
          var ej = form.elements[cablesComp];
          return ej.value;
        }
      }
    }
    return '';
 }


function displayConfirmation(operation) {
  var msg ='';
  if (operation == 'enable') {
    msg = '<cc:text name="EnableConfirmationText" defaultValue="transport.cables.enable.confirm.message" bundleID="scBundle"/>';
  } else if (operation == 'disable') {
    msg = '<cc:text name="DisableConfirmationText" defaultValue="transport.cables.disable.confirm.message" bundleID="scBundle"/>';
  } else {
        if ((getHiddenFieldValue("HiddenCableBtnState").indexOf("1"))!= -1)
            msg = '<cc:text name="RemoveConfirmationText" defaultValue="transport.cables.removeenabled.confirm.message" bundleID="scBundle"/>';
        else
            msg = '<cc:text name="RemoveConfirmationText" defaultValue="transport.cables.removedisabled.confirm.message" bundleID="scBundle"/>';
  }
    
  msg = msg.replace('{0}', getHiddenFieldValue("HiddenCableEP1"));
  msg = msg.replace('{1}', getHiddenFieldValue("HiddenCableEP2"));
  return confirm(msg);
  
}

// This function will toggle the disabled state of action buttons
// depending on single or multiple selections.
function toggleDisabledState() {
    // Action button.
    var enableActionButton = "<cc:text name='EnableActionButtonNameText'/>";
    var disableActionButton = "<cc:text name='DisableActionButtonNameText'/>";
    var removeActionButton = "<cc:text name='RemoveActionButtonNameText'/>";

    // Element name (prefix) of selection checkbox.
    var elementName = "<cc:text name='SelectionNameText'/>";

    // Document form.
    var form = document.scForm;
    var selectedCableState = getHiddenFieldValue("HiddenCableBtnState");

    var enabled = "<%=com.sun.cluster.common.ClusterState.CLSTATE_ENABLED%>";
    var disabled = "<%=com.sun.cluster.common.ClusterState.CLSTATE_DISABLED%>";

    if (selectedCableState == enabled){
        ccSetButtonDisabled(enableActionButton, "scForm", true);
        ccSetButtonDisabled(disableActionButton, "scForm", false);
        ccSetButtonDisabled(removeActionButton, "scForm", false); 
        return;
    }
    else if (selectedCableState == disabled){
        ccSetButtonDisabled(enableActionButton, "scForm", false);
        ccSetButtonDisabled(disableActionButton, "scForm", true);
        ccSetButtonDisabled(removeActionButton, "scForm", false); 
        return;
    }

    ccSetButtonDisabled(enableActionButton, "scForm", true);
    ccSetButtonDisabled(disableActionButton, "scForm", true);
    ccSetButtonDisabled(removeActionButton, "scForm", true);
}	
</SCRIPT>

<!-- Action Table -->
<cc:actiontable
name="Table"
bundleID="scBundle"
title="transport.cables.TableTitle"
summary="transport.cables.TableSummary"
empty="transport.cables.emptyTable"
selectionJavascript="setTimeout('toggleDisabledState()', 0)"
selectionType="single"
showAdvancedSortingIcon="false"
showLowerActions="false"
showPaginationControls="false"
showPaginationIcon="false"
showSelectionIcons="true"
showSelectionSortIcon="false"
showSortingRow="false"
page="1"/>

</jato:containerView>

</cc:pagetitle>
</jato:form>
</spm:header>
</jato:useViewBean>

