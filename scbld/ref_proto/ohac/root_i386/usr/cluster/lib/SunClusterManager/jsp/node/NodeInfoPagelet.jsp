<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)NodeInfoPagelet.jsp	1.4	08/05/20 SMI"
 */
--%>

<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<jato:pagelet>
<script type="text/javascript">
// This function will toggle the disabled state of action buttons
// depending on single or multiple selections.
function toggleNodeTblBtnsDisabledState() {
    // Action button.
    var actionButton = "<cc:text name='ActionButtonNameText'/>";
 
    // Element name (prefix) of selection checkbox.
    var elementName = "<cc:text name='SelectionNameText'/>";

    // Document form.
    var form = document.scForm;

    // Flag indicating to disable action button and menu options.
    var disabled = true;

    // Set flag if a selection has been made.
    for (i = 0; i < form.elements.length; i++) {
        var e = form.elements[i];

        if (e.name.indexOf(elementName) != -1) {
	    if (e.checked) {
		disabled = false;
		break;
	    }
	}
    }

    // Toggle action button disabled state.
    ccSetButtonDisabled(actionButton, "scForm", disabled);
}

function displayNodeTblBtnsConfirmation() {
    
    var msg = '';
    msg = '<cc:text name="StaticText" defaultValue="nodes.evacuate.confirmation" bundleID="scBundle"/>';

    // Element name (prefix) of selection checkbox.
    var elementName = "<cc:text name='SelectionNameText'/>";

    // Document form.
    var form = document.scForm;
 
    for (i = 0; i < form.elements.length; i++) {
      var e = form.elements[i];
      if (e.name.indexOf(elementName) != -1) {
        if (e.checked) {
          var prefix =  elementName.substring(0,elementName.indexOf("Table.SelectionRadiobutton"));
          var nodeComp = prefix + "TableTiledView[" + e.value + "].HiddenNodeName";
          var ej = form.elements[nodeComp];
          msg = msg.replace('{0}', ej.value);  
          var result = confirm(msg);
          if (result) unRegisterEvents();
          return result;
            
        }
      }
    }
}

</script>
<!-- Action Table -->
<cc:actiontable
name="Table"
bundleID="scBundle"
title="nodes.tableTitle"
summary="nodes.tableSummary"
empty="nodes.tableEmpty"
selectionJavascript="setTimeout('toggleNodeTblBtnsDisabledState()', 0)"
selectionType="single"
showAdvancedSortingIcon="false"
showLowerActions="false"
showPaginationControls="false"
showPaginationIcon="false"
showSelectionIcons="true"
showSelectionSortIcon="false"
showSortingRow="false"
maxRows="10"
page="1"/>


</jato:pagelet>
