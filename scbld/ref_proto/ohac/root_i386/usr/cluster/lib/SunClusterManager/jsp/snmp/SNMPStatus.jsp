<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)SNMPStatus.jsp	1.6	08/05/20 SMI"
 */
--%>

<%@page info="SNMP" language="java" %> 
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%> 
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.snmp.SNMPStatusViewBean">

<!-- Page header component -->
<spm:header pageTitle="snmp.title" copyrightYear="2004"
            baseName="com.sun.cluster.spm.resources.Resources"
            bundleID="scBundle"
            event="ESC_cluster_node_config_change"
            onLoad="reloadTree()">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>


<SCRIPT LANGUAGE="JavaScript">

function getHiddenFieldValue(hiddenTextFieldName) {
    // Element name (prefix) of selection checkbox.
    var elementName = "<cc:text name='SelectionNameText'/>";

    // Document form.
    var form = document.scForm;
 
    for (i = 0; i < form.elements.length; i++) {
        var e = form.elements[i];
        if (e.name.indexOf(elementName) != -1) {
            if (e.checked) {
                var prefix =  elementName.substring(0,
                      elementName.indexOf("NodeTable.SelectionRadiobutton"));

                var adaptersComp = prefix + 
                      "TableTiledView[" + e.value + "]." + hiddenTextFieldName;
                var ej = form.elements[adaptersComp];
          return ej.value;
        }
      }
    }
    return '';
 }

// This function will toggle the state of action buttons

function toggleSnmpTblBtnsDisabledState() {
    // Enable Action button.
    var enableActionButton = "<cc:text name='EnableButtonNameText'/>";

    // Disable Action button.
    var disableActionButton = "<cc:text name='DisableButtonNameText'/>"; 

    // Protocol  Action button.
    var protocolActionButton = "<cc:text name='ProtocolButtonNameText'/>";

    // Element name (prefix) of selection checkbox.
    var elementName = "<cc:text name='SelectionNameText'/>";

    // Flag indicating to disable action button
     var disabled = true;

    // Document form.
    var form = document.scForm;

    var selectedModuleState = getHiddenFieldValue("HiddenModuleState");

    var enabled = "<%=com.sun.cluster.common.ClusterState.CLSTATE_ENABLED%>";
    var disabled = "<%=com.sun.cluster.common.ClusterState.CLSTATE_DISABLED%>";


    if (selectedModuleState == enabled){
        ccSetButtonDisabled(enableActionButton, "scForm", true);
        ccSetButtonDisabled(disableActionButton, "scForm", false);
        ccSetButtonDisabled(protocolActionButton, "scForm", false);
        return;
    }
    else if (selectedModuleState == disabled){
        ccSetButtonDisabled(enableActionButton, "scForm", false);
        ccSetButtonDisabled(disableActionButton, "scForm", true);        
        ccSetButtonDisabled(protocolActionButton, "scForm", false);
        return;
    }

    ccSetButtonDisabled(enableActionButton, "scForm", true);
    ccSetButtonDisabled(disableActionButton, "scForm", true); 
    ccSetButtonDisabled(protocolActionButton, "scForm", true);
    
}


 function displayConfirmation() {
     var result = confirm('<cc:text name="StaticText"
                           defaultValue="selectionConfirmation"
                           bundleID="scBundle"/>');
     if (result) {
          unRegisterEvents();
     }
     return result;
  
 }
</SCRIPT>

<cc:pagetitle name="GenericPageTitle" bundleID="scBundle"
              pageTitleText="snmp.status.pageTitle.title"
              showPageTitleSeparator="true"
              pageTitleHelpMessage="snmp.status.pageTitle.help"/>

<div><img src="/com_sun_web_ui/images/other/dot.gif"
          width="1" height="10" alt=""></div>

<BR>

<!-- Status Alert -->
<spm:alertinline name="StatusAlert" bundleID="scBundle" />

<!-- Command Alert -->
<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<BR>

<spm:rbac auths="solaris.cluster.read">
  <cc:actiontable name="NodeTable"
                  bundleID="scBundle"
                  title="snmp.status.node.table.title"
                  summary="snmp.status.node.table.summary"
                  empty="snmp.status.node.table.empty"
                  rowSelectionType="single"
                  selectionJavascript="setTimeout('toggleSnmpTblBtnsDisabledState()', 0)"
                  showAdvancedSortingIcon="false"
                  showLowerActions="false"
                  showPaginationControls="false"
                  showPaginationIcon="false"
                  showSelectionIcons="true"
                  showSortingRow="true"
                  page="1"/>
</spm:rbac>

</jato:form>
</spm:header>
</jato:useViewBean>
