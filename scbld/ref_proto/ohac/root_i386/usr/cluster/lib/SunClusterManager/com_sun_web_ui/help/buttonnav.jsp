<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@taglib uri="http://www.sun.com/web/ui" prefix="ui" %>
<f:view>
  <ui:page>
    <ui:head title="#{JavaHelpBean.buttonNavHeadTitle}" />      
    <ui:body styleClass="HlpBtnNavBdy">
      <ui:form id="helpButtonNavForm">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td nowrap="nowrap">
              <div class="HlpBtnDiv">
                <ui:imageHyperlink id="HelpBackButton" alt="#{JavaHelpBean.backButtonText}" icon="#{JavaHelpBean.backButtonIcon}" toolTip="#{JavaHelpBean.backButtonText}" onClick="javascript:window.parent.contentFrame.focus(); window.parent.contentFrame.history.back(); return false;" />
                <ui:image id="Spacer1" icon="DOT" width="5" height="1" />
                <ui:imageHyperlink id="HelpForwardButton" alt="#{JavaHelpBean.forwardButtonText}" icon="#{JavaHelpBean.forwardButtonIcon}" toolTip="#{JavaHelpBean.forwardButtonText}" onClick="javascript:window.parent.contentFrame.focus(); window.parent.contentFrame.history.forward(); return false;" />
                <ui:image id="Spacer2" icon="DOT" width="10" height="1" />
                <ui:imageHyperlink id="PrintButton" alt="#{JavaHelpBean.printButtonText}" icon="#{JavaHelpBean.printButtonIcon}" toolTip="#{JavaHelpBean.printButtonText}" onClick="window.parent.contentFrame.focus(); window.parent.contentFrame.print()" />
              </div>
            </td>
          </tr>
        </table>
      </ui:form>      
    </ui:body> 
  </ui:page>
</f:view>
