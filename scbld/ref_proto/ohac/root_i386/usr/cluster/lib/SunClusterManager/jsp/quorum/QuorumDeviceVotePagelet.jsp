<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)QuorumDeviceVotePagelet.jsp	1.7	08/05/20 SMI"
 */
--%>

<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<jato:pagelet>

<SCRIPT LANGUAGE="JavaScript">

function getHiddenQuorumFieldValue(hiddenTextFieldName) {
    // Element name (prefix) of selection checkbox.
    var elementName = "<cc:text name='SelectionNameText'/>";

    // Document form.
    var form = document.scForm;
 
    for (i = 0; i < form.elements.length; i++) {
        var e = form.elements[i];
        if (e.name.indexOf(elementName) != -1) {
            if (e.checked) {
                var prefix =  elementName.substring(0,
                      elementName.indexOf("DeviceVoteTable.SelectionRadiobutton"));

                var quorumComp = prefix + 
                      "TableTiledView[" + e.value + "]." + hiddenTextFieldName;
                var ej = form.elements[quorumComp];
          return ej.value;
        }
      }
    }
    return '';
 }


// This function will toggle the disabled state of action buttons
// depending on single or multiple selections.
function toggleQuoDevTblBtnsDisabledState() {
    // Action button.
    var resetActionButton = "<cc:text name='ResetActionButtonNameText'/>";
    var maintenanceActionButton ="<cc:text name='MaintenanceActionButtonNameText'/>";
    var removeActionButton = "<cc:text name='RemoveActionButtonNameText'/>";

    // Element name (prefix) of selection checkbox.
    var elementName = "<cc:text name='SelectionNameText'/>";

    // Document form.
    var form = document.scForm;

    // Flag indicating to disable action button and menu options.
    var disabled = true;


    var selectedModuleState = getHiddenQuorumFieldValue("HiddenQuorumDeviceState");

    var enabled = "<%=com.sun.cluster.common.ClusterState.CLSTATE_ONLINE%>";
    var disabled = "<%=com.sun.cluster.common.ClusterState.CLSTATE_OFFLINE%>";


    if (selectedModuleState == enabled){
        ccSetButtonDisabled(resetActionButton, "scForm", true);
        ccSetButtonDisabled(maintenanceActionButton, "scForm", false);
        ccSetButtonDisabled(removeActionButton, "scForm", true);
        return;
    }
    else if (selectedModuleState == disabled){
        ccSetButtonDisabled(resetActionButton, "scForm", false);
        ccSetButtonDisabled(maintenanceActionButton, "scForm", true);        
        ccSetButtonDisabled(removeActionButton, "scForm", false);
        return;
    }

    ccSetButtonDisabled(resetActionButton, "scForm", true);
    ccSetButtonDisabled(maintenanceActionButton, "scForm", true); 
    ccSetButtonDisabled(removeActionButton, "scForm", true);


}

function displayQuoDevTblBtnsConfirmation(operation) {
  
  var msg = '<cc:text name="StaticText" 
                      defaultValue="quorum.operation.reset.confirm"
                      bundleID="scBundle"/>';
  
  if (operation == 'maintenance') {
    msg = '<cc:text name="StaticText" 
                    defaultValue="quorum.operation.maintenance.confirm"
                    bundleID="scBundle"/>';
  
  } else if (operation == 'remove') {
    msg = '<cc:text name="StaticText" 
                    defaultValue="quorum.operation.remove.confirm"
                    bundleID="scBundle"/>';
  
  }

  // Element name (prefix) of selection checkbox.
    var elementName = "<cc:text name='SelectionNameText'/>";

    // Document form.
    var form = document.scForm;
 
    for (i = 0; i < form.elements.length; i++) {
      var e = form.elements[i];
      if (e.name.indexOf(elementName) != -1) {
        if (e.checked) {
          var prefix =  elementName.substring(0,elementName.indexOf("DeviceVoteTable.SelectionRadiobutton"));
          var quorumComp = prefix + "TableTiledView[" + e.value + "].HiddenQuorumDeviceName";
          var ej = form.elements[quorumComp];
          msg = msg.replace('{0}', ej.value);  
          var result = confirm(msg + '\n <cc:text name="StaticText"
                                          defaultValue="selectionConfirmation"
                                          bundleID="scBundle"/>');

          if (result) {
            unRegisterEvents();
          }   
          return result;
        }
      }
    }
}
</SCRIPT>

<!-- Action Table -->
<cc:actiontable name="DeviceVoteTable"
                bundleID="scBundle"
                title="quorum.status.device.table.title"
                summary="quorum.status.device.table.summary"
                empty="quorum.status.device.table.empty"
                selectionJavascript="setTimeout('toggleQuoDevTblBtnsDisabledState()', 0)"
                selectionType="single"
                showAdvancedSortingIcon="false"
                showLowerActions="false"
                showPaginationControls="false"
                showPaginationIcon="false"
                showSelectionIcons="true"
                showSelectionSortIcon="false"
                showSortingRow="false"
                page="1"/>

</jato:pagelet>
