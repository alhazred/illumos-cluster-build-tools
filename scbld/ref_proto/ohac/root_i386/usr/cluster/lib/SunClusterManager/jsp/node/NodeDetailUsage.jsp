<%--
 --
 -- CDDL HEADER START
 --
 -- The contents of this file are subject to the terms of the
 -- Common Development and Distribution License (the License).
 -- You may not use this file except in compliance with the License.
 --
 -- You can obtain a copy of the license at usr/src/CDDL.txt
 -- or http://www.opensolaris.org/os/licensing.
 -- See the License for the specific language governing permissions
 -- and limitations under the License.
 --
 -- When distributing Covered Code, include this CDDL HEADER in each
 -- file and include the License file at usr/src/CDDL.txt.
 -- If applicable, add the following below this CDDL HEADER, with the
 -- fields enclosed by brackets [] replaced with your own identifying
 -- information: Portions Copyright [yyyy] [name of copyright owner]
 --
 -- CDDL HEADER END
 --
 --%>
<%--
/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)NodeDetailUsage.jsp	1.5	08/05/20 SMI"
 */
--%>

<%@page info="Node" language="java" %>
<%@taglib uri="/WEB-INF/tld/com_iplanet_jato/jato.tld" prefix="jato"%>
<%@taglib uri="/WEB-INF/tld/com_sun_cluster_spm/spm.tld" prefix="spm"%>
<%@taglib uri="/WEB-INF/tld/com_sun_web_ui/cc.tld" prefix="cc"%>

<!-- Set the viewbean -->
<jato:useViewBean className="com.sun.cluster.spm.node.NodeDetailUsageViewBean">

<!-- Define the resource bundle, html, head, meta, stylesheet and body tags -->
<spm:header pageTitle="clusterStatus.title"
  copyrightYear="2004"
  baseName="com.sun.cluster.spm.resources.Resources"
  bundleID="scBundle"
  event="ESC_cluster_node_config_change,ESC_cluster_slm_thr_state"
  onLoad="reloadTree()">

<jato:form name="scForm" method="post">

<!-- Hidden Field for the tree -->
<cc:hidden name="TreeNodeHiddenField"/>

<!-- Navigation Tabs -->
<cc:tabs name="GenericTabs" bundleID="scBundle" />

<spm:SLMcheck name="SLMCheck" summary="slmcheck.message.summary"
  detail="slmcheck.message.detail">

<spm:alertinline name="CommandResultAlert" bundleID="scBundle" />

<cc:pagetitle name="GenericPageTitle"
  bundleID="scBundle"
  pageTitleText="nodeDetailUsage.pageTitle"
  showPageTitleSeparator="true"
  pageTitleHelpMessage="nodeDetailUsage.help" />

<br>

<div class="ConMgn">
  <table border="0" cellpadding="0" cellspacing="0">
    <tr><td>
      <cc:button name="GraphButton"
        bundleID="scBundle"
        defaultValue="usage.graph.GraphButton.label"
        onClick="javascript: GraphWindow(); return false" />
    </td></tr>
  </table>
</div>

<jato:containerView name="NodeHeaderView">
  <jsp:include page="NodeHeaderPagelet.jsp" />
</jato:containerView>

<br>

<!-- Action Table -->
<cc:actiontable
  name="DetailMonitorTable"
  bundleID="scBundle"
  title="detail.monitor.table.title"
  summary="detail.monitor.table.summary"
  empty="detail.monitor.table.empty"
  rowSelectionType="none"
  showAdvancedSortingIcon="false"
  showLowerActions="false"
  showPaginationControls="false"
  showPaginationIcon="false"
  showSelectionIcons="false"
  showSortingRow="false"
  page="1"/>

<br>

<spm:rbac auths="solaris.cluster.read">
  <jato:containerView name="ResourceGroupsUsageView">
    <jsp:include page="../rgm/ResourceGroupsUsagePagelet.jsp" />
  </jato:containerView>
</spm:rbac>

<br>

<%-- comment out for performance. It's also can be viewed from node page
<spm:rbac auths="solaris.cluster.read">
  <jato:containerView name="TransportUsageView">
    <jsp:include page="../transport/TransportUsagePagelet.jsp" />
  </jato:containerView>
</spm:rbac>
--%>

</spm:SLMcheck>
</jato:form>
</spm:header>
</jato:useViewBean>


