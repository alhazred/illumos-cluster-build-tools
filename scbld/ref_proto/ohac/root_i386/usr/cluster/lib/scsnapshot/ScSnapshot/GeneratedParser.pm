package ScSnapshot::GeneratedParser;
use Parse::RecDescent;

{ my $ERRORS;


package Parse::RecDescent::namespace000001;
use strict;
use vars qw($skip $AUTOLOAD  );
$skip = '\s*';


{
local $SIG{__WARN__} = sub {0};
# PRETEND TO BE IN Parse::RecDescent NAMESPACE
*Parse::RecDescent::namespace000001::AUTOLOAD	= sub
{
	no strict 'refs';
	$AUTOLOAD =~ s/^Parse::RecDescent::namespace000001/Parse::RecDescent/;
	goto &{$AUTOLOAD};
}
}

push @Parse::RecDescent::namespace000001::ISA, 'Parse::RecDescent';
# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgresourcepropertyattributelist
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgresourcepropertyattributelist"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgresourcepropertyattributelist]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgresourcepropertyattributelist})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rgresourcepropertyattribute rgresourcepropertyattributelist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourcepropertyattributelist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourcepropertyattributelist});
		%item = (__RULE__ => q{rgresourcepropertyattributelist});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rgresourcepropertyattribute]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourcepropertyattributelist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourcepropertyattribute($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourcepropertyattribute]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourcepropertyattributelist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourcepropertyattribute]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertyattributelist})
						if defined $::RD_TRACE;
		$item{q{rgresourcepropertyattribute}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rgresourcepropertyattributelist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourcepropertyattributelist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rgresourcepropertyattributelist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourcepropertyattributelist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourcepropertyattributelist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourcepropertyattributelist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourcepropertyattributelist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertyattributelist})
						if defined $::RD_TRACE;
		$item{q{rgresourcepropertyattributelist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rgresourcepropertyattribute rgresourcepropertyattributelist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertyattributelist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: []},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourcepropertyattributelist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourcepropertyattributelist});
		%item = (__RULE__ => q{rgresourcepropertyattributelist});
		my $repcount = 0;



		Parse::RecDescent::_trace(q{>>Matched production: []<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertyattributelist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgresourcepropertyattributelist})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgresourcepropertyattributelist})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgresourcepropertyattributelist});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgresourcepropertyattributelist})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgresourceattribute
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgresourceattribute"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgresourceattribute]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgresourceattribute})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/.*Res /i /R_description:/i, or /type version:/i, or /resource group name:/i, or /enabled:/i, or /monitor enabled:/i, or /resource type:/i, or /resource project name:/i, or /offline restart dependencies:/i, or /restart dependencies:/i, or /strong dependencies:/i, or /weak dependencies:/i]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourceattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourceattribute});
		%item = (__RULE__ => q{rgresourceattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/.*Res /i]}, Parse::RecDescent::_tracefirst($text),
					  q{rgresourceattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:.*Res )//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [_alternation_1_of_production_1_of_rule_rgresourceattribute]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourceattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{/R_description:/i, or /type version:/i, or /resource group name:/i, or /enabled:/i, or /monitor enabled:/i, or /resource type:/i, or /resource project name:/i, or /offline restart dependencies:/i, or /restart dependencies:/i, or /strong dependencies:/i, or /weak dependencies:/i})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::_alternation_1_of_production_1_of_rule_rgresourceattribute($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [_alternation_1_of_production_1_of_rule_rgresourceattribute]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourceattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [_alternation_1_of_production_1_of_rule_rgresourceattribute]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourceattribute})
						if defined $::RD_TRACE;
		$item{q{_alternation_1_of_production_1_of_rule_rgresourceattribute}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [/.*Res /i /R_description:/i, or /type version:/i, or /resource group name:/i, or /enabled:/i, or /monitor enabled:/i, or /resource type:/i, or /resource project name:/i, or /offline restart dependencies:/i, or /restart dependencies:/i, or /strong dependencies:/i, or /weak dependencies:/i]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourceattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgresourceattribute})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgresourceattribute})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgresourceattribute});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgresourceattribute})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgresourcename
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgresourcename"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgresourcename]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgresourcename})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/.*Res name:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourcename})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourcename});
		%item = (__RULE__ => q{rgresourcename});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/.*Res name:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{rgresourcename})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:.*Res name:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourcename})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourcename})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcename})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcename})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->debug_ppc($item[1], $item[2]);
	$thisparser->_rgresourcename($item[2]);
};
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/.*Res name:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcename})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgresourcename})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgresourcename})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgresourcename});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgresourcename})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::def
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"def"};
	
	Parse::RecDescent::_trace(q{Trying rule: [def]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{def})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rtdef]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{def})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{def});
		%item = (__RULE__ => q{def});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rtdef]},
				  Parse::RecDescent::_tracefirst($text),
				  q{def})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtdef($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtdef]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{def})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtdef]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{def})
						if defined $::RD_TRACE;
		$item{q{rtdef}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rtdef]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{def})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rgdef]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{def})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{def});
		%item = (__RULE__ => q{def});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rgdef]},
				  Parse::RecDescent::_tracefirst($text),
				  q{def})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgdef($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgdef]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{def})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgdef]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{def})
						if defined $::RD_TRACE;
		$item{q{rgdef}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rgdef]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{def})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [<error...>]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{def})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[2];
		
		my $_savetext;
		@item = (q{def});
		%item = (__RULE__ => q{def});
		my $repcount = 0;


		

		Parse::RecDescent::_trace(q{Trying directive: [<error...>]},
					Parse::RecDescent::_tracefirst($text),
					  q{def})
						if defined $::RD_TRACE; 
		$_tok = do { if (1) { do {
		my $rule = $item[0];
		   $rule =~ s/_/ /g;
		#WAS: Parse::RecDescent::_error("Invalid $rule: " . $expectation->message() ,$thisline);
		push @{$thisparser->{errors}}, ["Invalid $rule: " . $expectation->message() ,$thisline];
		} unless  $_noactions; undef } else {0} };
		if (defined($_tok))
		{
			Parse::RecDescent::_trace(q{>>Matched directive<< (return value: [}
						. $_tok . q{])},
						Parse::RecDescent::_tracefirst($text))
							if defined $::RD_TRACE;
		}
		else
		{
			Parse::RecDescent::_trace(q{<<Didn't match directive>>},
						Parse::RecDescent::_tracefirst($text))
							if defined $::RD_TRACE;
		}
		
		last unless defined $_tok;
		push @item, $item{__DIRECTIVE1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [<error...>]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{def})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{def})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{def})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{def});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{def})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::startrule
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"startrule"};
	
	Parse::RecDescent::_trace(q{Trying rule: [startrule]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{startrule})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [def eofile]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{startrule})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{startrule});
		%item = (__RULE__ => q{startrule});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying repeated subrule: [def]},
				  Parse::RecDescent::_tracefirst($text),
				  q{startrule})
					if defined $::RD_TRACE;
		$expectation->is(q{})->at($text);
		
		unless (defined ($_tok = $thisparser->_parserepeat($text, \&Parse::RecDescent::namespace000001::def, 1, 100000000, $_noactions,$expectation,undef))) 
		{
			Parse::RecDescent::_trace(q{<<Didn't match repeated subrule: [def]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{startrule})
							if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched repeated subrule: [def]<< (}
					. @$_tok . q{ times)},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{startrule})
						if defined $::RD_TRACE;
		$item{q{def}} = $_tok;
		push @item, $_tok;
		


		Parse::RecDescent::_trace(q{Trying subrule: [eofile]},
				  Parse::RecDescent::_tracefirst($text),
				  q{startrule})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{eofile})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::eofile($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [eofile]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{startrule})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [eofile]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{startrule})
						if defined $::RD_TRACE;
		$item{q{eofile}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{startrule})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do { $return = $item[1] };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [def eofile]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{startrule})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{startrule})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{startrule})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{startrule});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{startrule})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtparam
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtparam"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtparam]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtparam})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rtparamname rtparamattributelist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtparam})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtparam});
		%item = (__RULE__ => q{rtparam});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rtparamname]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtparam})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtparamname($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtparamname]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtparam})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtparamname]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparam})
						if defined $::RD_TRACE;
		$item{q{rtparamname}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rtparamattributelist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtparam})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rtparamattributelist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtparamattributelist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtparamattributelist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtparam})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtparamattributelist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparam})
						if defined $::RD_TRACE;
		$item{q{rtparamattributelist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rtparamname rtparamattributelist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparam})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtparam})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtparam})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtparam});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtparam})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute"};
	
	Parse::RecDescent::_trace(q{Trying rule: [_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/class:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/class:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:class:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgresourcepropertyattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/class:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/description:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/description:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:description:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgresourcepropertyattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/description:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/pernode:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[2];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/pernode:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:pernode:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgresourcepropertyattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/pernode:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/type:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[3];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/type:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:type:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgresourcepropertyattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/type:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/value:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[4];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/value:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:value:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgresourcepropertyattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/value:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgresourcepropertyattribute
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgresourcepropertyattribute"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgresourcepropertyattribute]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgresourcepropertyattribute})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/.*Res property /i /class:/i, or /description:/i, or /pernode:/i, or /type:/i, or /value:/i]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourcepropertyattribute});
		%item = (__RULE__ => q{rgresourcepropertyattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/.*Res property /i]}, Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:.*Res property )//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourcepropertyattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{/class:/i, or /description:/i, or /pernode:/i, or /type:/i, or /value:/i})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourcepropertyattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$item{q{_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [/.*Res property /i /class:/i, or /description:/i, or /pernode:/i, or /type:/i, or /value:/i]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgresourcepropertyattribute})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgresourcepropertyattribute})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgresourcepropertyattribute});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgresourcepropertyattribute})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtparamname
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtparamname"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtparamname]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtparamname})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/.*Res Type param name:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtparamname})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtparamname});
		%item = (__RULE__ => q{rtparamname});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/.*Res Type param name:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{rtparamname})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:.*Res Type param name:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtparamname})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtparamname})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamname})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamname})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->debug_ppc($item[1], $item[2]);
	$thisparser->_rtparamname($item[2]);
};
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/.*Res Type param name:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamname})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtparamname})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtparamname})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtparamname});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtparamname})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::_alternation_1_of_production_1_of_rule_rtparamattribute
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"_alternation_1_of_production_1_of_rule_rtparamattribute"};
	
	Parse::RecDescent::_trace(q{Trying rule: [_alternation_1_of_production_1_of_rule_rtparamattribute]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/extension:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/extension:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:extension:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/extension:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/per-node:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/per-node:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:per-node:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/per-node:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/description:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[2];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/description:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:description:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/description:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/tunability:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[3];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/tunability:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:tunability:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/tunability:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/type:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[4];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/type:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:type:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/type:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/min int value:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[5];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/min int value:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:min int value:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/min int value:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/max int value:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[6];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/max int value:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:max int value:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/max int value:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/min length:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[7];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/min length:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:min length:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/min length:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/max length:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[8];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/max length:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:max length:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/max length:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/min array length:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[9];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/min array length:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:min array length:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/min array length:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/max array length:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[10];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/max array length:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:max array length:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/max array length:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/enum list:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[11];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/enum list:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:enum list:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/enum list:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/default:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[12];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/default:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:default:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtparamattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/default:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{_alternation_1_of_production_1_of_rule_rtparamattribute})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{_alternation_1_of_production_1_of_rule_rtparamattribute});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{_alternation_1_of_production_1_of_rule_rtparamattribute})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgresourcepropertyname
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgresourcepropertyname"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgresourcepropertyname]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgresourcepropertyname})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/.*Res property name:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourcepropertyname})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourcepropertyname});
		%item = (__RULE__ => q{rgresourcepropertyname});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/.*Res property name:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertyname})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:.*Res property name:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourcepropertyname})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourcepropertyname})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertyname})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertyname})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgresourcepropertyname($item[2]);
};
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/.*Res property name:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertyname})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgresourcepropertyname})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgresourcepropertyname})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgresourcepropertyname});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgresourcepropertyname})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtattributelist
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtattributelist"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtattributelist]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtattributelist})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rtattribute rtattributelist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtattributelist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtattributelist});
		%item = (__RULE__ => q{rtattributelist});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rtattribute]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtattributelist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtattribute($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtattribute]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtattributelist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtattribute]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtattributelist})
						if defined $::RD_TRACE;
		$item{q{rtattribute}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rtattributelist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtattributelist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rtattributelist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtattributelist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtattributelist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtattributelist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtattributelist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtattributelist})
						if defined $::RD_TRACE;
		$item{q{rtattributelist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rtattribute rtattributelist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtattributelist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: []},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtattributelist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtattributelist});
		%item = (__RULE__ => q{rtattributelist});
		my $repcount = 0;



		Parse::RecDescent::_trace(q{>>Matched production: []<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtattributelist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtattributelist})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtattributelist})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtattributelist});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtattributelist})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgresourceproperty
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgresourceproperty"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgresourceproperty]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgresourceproperty})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rgresourcepropertyname rgresourcepropertyattributelist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourceproperty})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourceproperty});
		%item = (__RULE__ => q{rgresourceproperty});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rgresourcepropertyname]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourceproperty})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourcepropertyname($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourcepropertyname]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourceproperty})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourcepropertyname]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourceproperty})
						if defined $::RD_TRACE;
		$item{q{rgresourcepropertyname}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rgresourcepropertyattributelist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourceproperty})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rgresourcepropertyattributelist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourcepropertyattributelist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourcepropertyattributelist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourceproperty})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourcepropertyattributelist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourceproperty})
						if defined $::RD_TRACE;
		$item{q{rgresourcepropertyattributelist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rgresourcepropertyname rgresourcepropertyattributelist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourceproperty})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgresourceproperty})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgresourceproperty})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgresourceproperty});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgresourceproperty})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtparams
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtparams"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtparams]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtparams})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/.*Res Type param table/i rtparamlist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtparams})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtparams});
		%item = (__RULE__ => q{rtparams});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/.*Res Type param table/i]}, Parse::RecDescent::_tracefirst($text),
					  q{rtparams})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:.*Res Type param table)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [rtparamlist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtparams})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rtparamlist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtparamlist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtparamlist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtparams})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtparamlist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparams})
						if defined $::RD_TRACE;
		$item{q{rtparamlist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [/.*Res Type param table/i rtparamlist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparams})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtparams})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtparams})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtparams});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtparams})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtattribute
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtattribute"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtattribute]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtattribute})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/.*Res Type /i /description:/i, or /base directory:/i, or /single instance:/i, or /init nodes:/i, or /failover:/i, or /proxy:/i, or /version:/i, or /API version:/i, or /installed on nodes:/i, or /packages:/i, or /system.*/i, or /upgrade.*/i]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtattribute});
		%item = (__RULE__ => q{rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/.*Res Type /i]}, Parse::RecDescent::_tracefirst($text),
					  q{rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:.*Res Type )//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [_alternation_1_of_production_1_of_rule_rtattribute]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{/description:/i, or /base directory:/i, or /single instance:/i, or /init nodes:/i, or /failover:/i, or /proxy:/i, or /version:/i, or /API version:/i, or /installed on nodes:/i, or /packages:/i, or /system.*/i, or /upgrade.*/i})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::_alternation_1_of_production_1_of_rule_rtattribute($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [_alternation_1_of_production_1_of_rule_rtattribute]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [_alternation_1_of_production_1_of_rule_rtattribute]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtattribute})
						if defined $::RD_TRACE;
		$item{q{_alternation_1_of_production_1_of_rule_rtattribute}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [/.*Res Type /i /description:/i, or /base directory:/i, or /single instance:/i, or /init nodes:/i, or /failover:/i, or /proxy:/i, or /version:/i, or /API version:/i, or /installed on nodes:/i, or /packages:/i, or /system.*/i, or /upgrade.*/i]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtattribute})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtattribute})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtattribute});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtattribute})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::_alternation_1_of_production_1_of_rule_rtattribute
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"_alternation_1_of_production_1_of_rule_rtattribute"};
	
	Parse::RecDescent::_trace(q{Trying rule: [_alternation_1_of_production_1_of_rule_rtattribute]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{_alternation_1_of_production_1_of_rule_rtattribute})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/description:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/description:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:description:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/description:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/base directory:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/base directory:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:base directory:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/base directory:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/single instance:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[2];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/single instance:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:single instance:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/single instance:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/init nodes:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[3];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/init nodes:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:init nodes:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/init nodes:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/failover:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[4];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/failover:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:failover:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/failover:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/proxy:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[5];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/proxy:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:proxy:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/proxy:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/version:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[6];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/version:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:version:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/version:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/API version:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[7];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/API version:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:API version:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/API version:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/installed on nodes:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[8];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/installed on nodes:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:installed on nodes:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtlistattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/installed on nodes:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/packages:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[9];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/packages:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:packages:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/packages:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/system.*/i]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[10];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/system.*/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:system.*)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/system.*/i]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/upgrade.*/i]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[11];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/upgrade.*/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:upgrade.*)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/upgrade.*/i]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{_alternation_1_of_production_1_of_rule_rtattribute})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{_alternation_1_of_production_1_of_rule_rtattribute})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{_alternation_1_of_production_1_of_rule_rtattribute});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{_alternation_1_of_production_1_of_rule_rtattribute})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtdef
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtdef"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtdef]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtdef})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rtname rtattributelist rtmethods rtparams]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtdef})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtdef});
		%item = (__RULE__ => q{rtdef});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rtname]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtdef})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtname($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtname]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtdef})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtname]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtdef})
						if defined $::RD_TRACE;
		$item{q{rtname}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rtattributelist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtdef})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rtattributelist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtattributelist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtattributelist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtdef})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtattributelist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtdef})
						if defined $::RD_TRACE;
		$item{q{rtattributelist}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rtmethods]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtdef})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rtmethods})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtmethods($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtmethods]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtdef})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtmethods]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtdef})
						if defined $::RD_TRACE;
		$item{q{rtmethods}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rtparams]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtdef})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rtparams})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtparams($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtparams]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtdef})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtparams]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtdef})
						if defined $::RD_TRACE;
		$item{q{rtparams}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rtname rtattributelist rtmethods rtparams]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtdef})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtdef})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtdef})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtdef});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtdef})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtmethodlist
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtmethodlist"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtmethodlist]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtmethodlist})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rtmethod rtmethodlist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtmethodlist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtmethodlist});
		%item = (__RULE__ => q{rtmethodlist});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rtmethod]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtmethodlist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtmethod($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtmethod]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtmethodlist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtmethod]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtmethodlist})
						if defined $::RD_TRACE;
		$item{q{rtmethod}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rtmethodlist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtmethodlist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rtmethodlist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtmethodlist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtmethodlist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtmethodlist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtmethodlist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtmethodlist})
						if defined $::RD_TRACE;
		$item{q{rtmethodlist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rtmethod rtmethodlist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtmethodlist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: []},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtmethodlist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtmethodlist});
		%item = (__RULE__ => q{rtmethodlist});
		my $repcount = 0;



		Parse::RecDescent::_trace(q{>>Matched production: []<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtmethodlist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtmethodlist})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtmethodlist})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtmethodlist});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtmethodlist})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgresource
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgresource"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgresource]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgresource})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rgresourcename rgresourceattributelist rgresourcepropertylist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresource})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresource});
		%item = (__RULE__ => q{rgresource});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rgresourcename]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresource})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourcename($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourcename]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresource})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourcename]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresource})
						if defined $::RD_TRACE;
		$item{q{rgresourcename}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rgresourceattributelist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresource})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rgresourceattributelist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourceattributelist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourceattributelist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresource})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourceattributelist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresource})
						if defined $::RD_TRACE;
		$item{q{rgresourceattributelist}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rgresourcepropertylist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresource})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rgresourcepropertylist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourcepropertylist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourcepropertylist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresource})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourcepropertylist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresource})
						if defined $::RD_TRACE;
		$item{q{rgresourcepropertylist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rgresourcename rgresourceattributelist rgresourcepropertylist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresource})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgresource})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgresource})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgresource});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgresource})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgname
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgname"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgname]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgname})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/Res Group name:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgname})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgname});
		%item = (__RULE__ => q{rgname});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/Res Group name:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{rgname})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:Res Group name:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgname})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgname})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgname})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgname})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->debug_ppc($item[1], $item[2]);
	$thisparser->_rgname($item[2]);
};
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/Res Group name:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgname})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgname})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgname})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgname});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgname})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::_alternation_1_of_production_1_of_rule_rtmethod
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"_alternation_1_of_production_1_of_rule_rtmethod"};
	
	Parse::RecDescent::_trace(q{Trying rule: [_alternation_1_of_production_1_of_rule_rtmethod]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/START:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtmethod});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/START:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:START:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtmethod(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/START:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/STOP:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtmethod});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/STOP:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:STOP:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtmethod(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/STOP:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/VALIDATE:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[2];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtmethod});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/VALIDATE:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:VALIDATE:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtmethod(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/VALIDATE:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/UPDATE:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[3];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtmethod});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/UPDATE:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:UPDATE:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtmethod(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/UPDATE:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/INIT:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[4];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtmethod});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/INIT:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:INIT:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtmethod(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/INIT:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/FINI:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[5];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtmethod});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/FINI:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:FINI:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtmethod(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/FINI:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/BOOT:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[6];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtmethod});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/BOOT:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:BOOT:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtmethod(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/BOOT:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/MONITOR START:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[7];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtmethod});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/MONITOR START:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:MONITOR START:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtmethod(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/MONITOR START:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/MONITOR STOP:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[8];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtmethod});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/MONITOR STOP:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:MONITOR STOP:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtmethod(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/MONITOR STOP:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/MONITOR CHECK:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[9];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtmethod});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/MONITOR CHECK:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:MONITOR CHECK:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtmethod(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/MONITOR CHECK:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/PRENET START:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[10];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtmethod});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/PRENET START:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:PRENET START:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtmethod(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/PRENET START:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/POSTNET STOP:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[11];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rtmethod});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/POSTNET STOP:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:POSTNET STOP:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rtmethod(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/POSTNET STOP:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{_alternation_1_of_production_1_of_rule_rtmethod})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{_alternation_1_of_production_1_of_rule_rtmethod})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{_alternation_1_of_production_1_of_rule_rtmethod});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{_alternation_1_of_production_1_of_rule_rtmethod})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtname
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtname"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtname]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtname})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/Res Type name:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtname})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtname});
		%item = (__RULE__ => q{rtname});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/Res Type name:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{rtname})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:Res Type name:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtname})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtname})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtname})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtname})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->debug_ppc($item[1], $item[2]);
	$thisparser->_rtname($item[2]);
};
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/Res Type name:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtname})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtname})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtname})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtname});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtname})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgdef
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgdef"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgdef]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgdef})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rgname rgattributelist rgresourcelist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgdef})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgdef});
		%item = (__RULE__ => q{rgdef});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rgname]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgdef})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgname($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgname]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgdef})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgname]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgdef})
						if defined $::RD_TRACE;
		$item{q{rgname}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rgattributelist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgdef})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rgattributelist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgattributelist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgattributelist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgdef})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgattributelist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgdef})
						if defined $::RD_TRACE;
		$item{q{rgattributelist}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rgresourcelist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgdef})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rgresourcelist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourcelist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourcelist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgdef})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourcelist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgdef})
						if defined $::RD_TRACE;
		$item{q{rgresourcelist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rgname rgattributelist rgresourcelist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgdef})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgdef})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgdef})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgdef});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgdef})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::_alternation_1_of_production_1_of_rule_rgresourceattribute
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"_alternation_1_of_production_1_of_rule_rgresourceattribute"};
	
	Parse::RecDescent::_trace(q{Trying rule: [_alternation_1_of_production_1_of_rule_rgresourceattribute]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/R_description:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/R_description:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:R_description:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do { $thisparser->_rgresourceattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/R_description:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/type version:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/type version:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:type version:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do { $thisparser->_rgresourceattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/type version:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/resource group name:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[2];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/resource group name:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:resource group name:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do { $thisparser->_rgresourceattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/resource group name:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/enabled:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[3];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/enabled:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:enabled:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do { $thisparser->_rgresourceattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/enabled:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/monitor enabled:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[4];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/monitor enabled:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:monitor enabled:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do { $thisparser->_rgresourceattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/monitor enabled:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/resource type:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[5];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/resource type:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:resource type:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do { $thisparser->_rgresourceattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/resource type:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/resource project name:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[6];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/resource project name:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:resource project name:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do { $thisparser->_rgresourceattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/resource project name:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/offline restart dependencies:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[7];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/offline restart dependencies:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:offline restart dependencies:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do { $thisparser->_rgresourcelistattribute(("resource_dependencies_offline_restart:",$item[2])); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/offline restart dependencies:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/restart dependencies:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[8];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/restart dependencies:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:restart dependencies:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do { $thisparser->_rgresourcelistattribute(("resource_dependencies_restart:",$item[2])); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/restart dependencies:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/strong dependencies:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[9];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/strong dependencies:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:strong dependencies:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do { $thisparser->_rgresourcelistattribute(("resource_dependencies:",$item[2])); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/strong dependencies:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/weak dependencies:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[10];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/weak dependencies:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:weak dependencies:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do { $thisparser->_rgresourcelistattribute(("resource_dependencies_weak:",$item[2])); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/weak dependencies:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{_alternation_1_of_production_1_of_rule_rgresourceattribute});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{_alternation_1_of_production_1_of_rule_rgresourceattribute})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtmethods
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtmethods"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtmethods]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtmethods})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/.*Res Type methods/i rtmethodlist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtmethods})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtmethods});
		%item = (__RULE__ => q{rtmethods});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/.*Res Type methods/i]}, Parse::RecDescent::_tracefirst($text),
					  q{rtmethods})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:.*Res Type methods)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [rtmethodlist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtmethods})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rtmethodlist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtmethodlist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtmethodlist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtmethods})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtmethodlist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtmethods})
						if defined $::RD_TRACE;
		$item{q{rtmethodlist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [/.*Res Type methods/i rtmethodlist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtmethods})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtmethods})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtmethods})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtmethods});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtmethods})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::data
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"data"};
	
	Parse::RecDescent::_trace(q{Trying rule: [data]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{data})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		local $skip = defined($skip) ? $skip : $Parse::RecDescent::skip;
		Parse::RecDescent::_trace(q{Trying production: [<skip:'[ \t]+'> /.*/i]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{data})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{data});
		%item = (__RULE__ => q{data});
		my $repcount = 0;


		

		Parse::RecDescent::_trace(q{Trying directive: [<skip:'[ \t]+'>]},
					Parse::RecDescent::_tracefirst($text),
					  q{data})
						if defined $::RD_TRACE; 
		$_tok = do { my $oldskip = $skip; $skip='[ \t]+'; $oldskip };
		if (defined($_tok))
		{
			Parse::RecDescent::_trace(q{>>Matched directive<< (return value: [}
						. $_tok . q{])},
						Parse::RecDescent::_tracefirst($text))
							if defined $::RD_TRACE;
		}
		else
		{
			Parse::RecDescent::_trace(q{<<Didn't match directive>>},
						Parse::RecDescent::_tracefirst($text))
							if defined $::RD_TRACE;
		}
		
		last unless defined $_tok;
		push @item, $item{__DIRECTIVE1__}=$_tok;
		

		Parse::RecDescent::_trace(q{Trying terminal: [/.*/i]}, Parse::RecDescent::_tracefirst($text),
					  q{data})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{/.*/i})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:.*)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		


		Parse::RecDescent::_trace(q{>>Matched production: [<skip:'[ \t]+'> /.*/i]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{data})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{data})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{data})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{data});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{data})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtmethod
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtmethod"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtmethod]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtmethod})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/.*Res Type /i /START:/i, or /STOP:/i, or /VALIDATE:/i, or /UPDATE:/i, or /INIT:/i, or /FINI:/i, or /BOOT:/i, or /MONITOR START:/i, or /MONITOR STOP:/i, or /MONITOR CHECK:/i, or /PRENET START:/i, or /POSTNET STOP:/i]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtmethod})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtmethod});
		%item = (__RULE__ => q{rtmethod});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/.*Res Type /i]}, Parse::RecDescent::_tracefirst($text),
					  q{rtmethod})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:.*Res Type )//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [_alternation_1_of_production_1_of_rule_rtmethod]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtmethod})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{/START:/i, or /STOP:/i, or /VALIDATE:/i, or /UPDATE:/i, or /INIT:/i, or /FINI:/i, or /BOOT:/i, or /MONITOR START:/i, or /MONITOR STOP:/i, or /MONITOR CHECK:/i, or /PRENET START:/i, or /POSTNET STOP:/i})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::_alternation_1_of_production_1_of_rule_rtmethod($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [_alternation_1_of_production_1_of_rule_rtmethod]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtmethod})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [_alternation_1_of_production_1_of_rule_rtmethod]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtmethod})
						if defined $::RD_TRACE;
		$item{q{_alternation_1_of_production_1_of_rule_rtmethod}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [/.*Res Type /i /START:/i, or /STOP:/i, or /VALIDATE:/i, or /UPDATE:/i, or /INIT:/i, or /FINI:/i, or /BOOT:/i, or /MONITOR START:/i, or /MONITOR STOP:/i, or /MONITOR CHECK:/i, or /PRENET START:/i, or /POSTNET STOP:/i]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtmethod})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtmethod})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtmethod})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtmethod});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtmethod})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgresourceattributelist
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgresourceattributelist"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgresourceattributelist]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgresourceattributelist})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rgresourceattribute rgresourceattributelist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourceattributelist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourceattributelist});
		%item = (__RULE__ => q{rgresourceattributelist});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rgresourceattribute]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourceattributelist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourceattribute($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourceattribute]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourceattributelist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourceattribute]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourceattributelist})
						if defined $::RD_TRACE;
		$item{q{rgresourceattribute}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rgresourceattributelist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourceattributelist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rgresourceattributelist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourceattributelist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourceattributelist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourceattributelist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourceattributelist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourceattributelist})
						if defined $::RD_TRACE;
		$item{q{rgresourceattributelist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rgresourceattribute rgresourceattributelist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourceattributelist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: []},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourceattributelist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourceattributelist});
		%item = (__RULE__ => q{rgresourceattributelist});
		my $repcount = 0;



		Parse::RecDescent::_trace(q{>>Matched production: []<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourceattributelist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgresourceattributelist})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgresourceattributelist})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgresourceattributelist});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgresourceattributelist})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::eofile
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"eofile"};
	
	Parse::RecDescent::_trace(q{Trying rule: [eofile]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{eofile})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/^\\Z/]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{eofile})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{eofile});
		%item = (__RULE__ => q{eofile});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/^\\Z/]}, Parse::RecDescent::_tracefirst($text),
					  q{eofile})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:^\Z)//)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/^\\Z/]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{eofile})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{eofile})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{eofile})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{eofile});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{eofile})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgresourcelist
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgresourcelist"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgresourcelist]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgresourcelist})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rgresource rgresourcelist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourcelist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourcelist});
		%item = (__RULE__ => q{rgresourcelist});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rgresource]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourcelist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresource($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresource]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourcelist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresource]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcelist})
						if defined $::RD_TRACE;
		$item{q{rgresource}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rgresourcelist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourcelist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rgresourcelist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourcelist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourcelist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourcelist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourcelist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcelist})
						if defined $::RD_TRACE;
		$item{q{rgresourcelist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rgresource rgresourcelist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcelist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: []},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourcelist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourcelist});
		%item = (__RULE__ => q{rgresourcelist});
		my $repcount = 0;



		Parse::RecDescent::_trace(q{>>Matched production: []<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcelist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgresourcelist})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgresourcelist})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgresourcelist});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgresourcelist})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgattribute
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgattribute"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgattribute]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgattribute})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/.*Res Group /i /RG_description:/i, or /mode:/i, or /management state:/i, or /RG_project_name:/i, or /RG_SLM_type:/i, or /RG_SLM_projectname:/i, or /RG_SLM_pset_type:/i, or /RG_SLM_CPU_SHARES:/i, or /RG_SLM_PSET_MIN:/i, or /auto_start_on_new_cluster:/i, or /failback:/i, or /nodelist:/i, or /maximum_primaries:/i, or /desired_primaries:/i, or /RG_dependencies:/i, or /network dependencies:/i, or /global_resources_used:/i, or /pingpong_interval:/i, or /pathprefix:/i, or /RG_affinities:/i, or /system:/i, or /Suspend_automatic_recovery:.*/i]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgattribute});
		%item = (__RULE__ => q{rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/.*Res Group /i]}, Parse::RecDescent::_tracefirst($text),
					  q{rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:.*Res Group )//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [_alternation_1_of_production_1_of_rule_rgattribute]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{/RG_description:/i, or /mode:/i, or /management state:/i, or /RG_project_name:/i, or /RG_SLM_type:/i, or /RG_SLM_projectname:/i, or /RG_SLM_pset_type:/i, or /RG_SLM_CPU_SHARES:/i, or /RG_SLM_PSET_MIN:/i, or /auto_start_on_new_cluster:/i, or /failback:/i, or /nodelist:/i, or /maximum_primaries:/i, or /desired_primaries:/i, or /RG_dependencies:/i, or /network dependencies:/i, or /global_resources_used:/i, or /pingpong_interval:/i, or /pathprefix:/i, or /RG_affinities:/i, or /system:/i, or /Suspend_automatic_recovery:.*/i})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::_alternation_1_of_production_1_of_rule_rgattribute($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [_alternation_1_of_production_1_of_rule_rgattribute]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [_alternation_1_of_production_1_of_rule_rgattribute]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgattribute})
						if defined $::RD_TRACE;
		$item{q{_alternation_1_of_production_1_of_rule_rgattribute}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [/.*Res Group /i /RG_description:/i, or /mode:/i, or /management state:/i, or /RG_project_name:/i, or /RG_SLM_type:/i, or /RG_SLM_projectname:/i, or /RG_SLM_pset_type:/i, or /RG_SLM_CPU_SHARES:/i, or /RG_SLM_PSET_MIN:/i, or /auto_start_on_new_cluster:/i, or /failback:/i, or /nodelist:/i, or /maximum_primaries:/i, or /desired_primaries:/i, or /RG_dependencies:/i, or /network dependencies:/i, or /global_resources_used:/i, or /pingpong_interval:/i, or /pathprefix:/i, or /RG_affinities:/i, or /system:/i, or /Suspend_automatic_recovery:.*/i]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgattribute})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgattribute})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgattribute});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgattribute})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::_alternation_1_of_production_1_of_rule_rgattribute
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"_alternation_1_of_production_1_of_rule_rgattribute"};
	
	Parse::RecDescent::_trace(q{Trying rule: [_alternation_1_of_production_1_of_rule_rgattribute]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/RG_description:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/RG_description:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:RG_description:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/RG_description:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/mode:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/mode:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:mode:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(("rg_mode:", $item[2])); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/mode:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/management state:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[2];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/management state:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:management state:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/management state:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/RG_project_name:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[3];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/RG_project_name:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:RG_project_name:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/RG_project_name:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/RG_SLM_type:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[4];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/RG_SLM_type:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:RG_SLM_type:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/RG_SLM_type:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/RG_SLM_projectname:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[5];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/RG_SLM_projectname:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:RG_SLM_projectname:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/RG_SLM_projectname:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/RG_SLM_pset_type:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[6];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/RG_SLM_pset_type:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:RG_SLM_pset_type:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/RG_SLM_pset_type:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/RG_SLM_CPU_SHARES:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[7];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/RG_SLM_CPU_SHARES:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:RG_SLM_CPU_SHARES:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/RG_SLM_CPU_SHARES:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/RG_SLM_PSET_MIN:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[8];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/RG_SLM_PSET_MIN:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:RG_SLM_PSET_MIN:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/RG_SLM_PSET_MIN:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/auto_start_on_new_cluster:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[9];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/auto_start_on_new_cluster:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:auto_start_on_new_cluster:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/auto_start_on_new_cluster:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/failback:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[10];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/failback:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:failback:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/failback:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/nodelist:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[11];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/nodelist:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:nodelist:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rglistattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/nodelist:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/maximum_primaries:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[12];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/maximum_primaries:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:maximum_primaries:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/maximum_primaries:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/desired_primaries:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[13];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/desired_primaries:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:desired_primaries:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/desired_primaries:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/RG_dependencies:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[14];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/RG_dependencies:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:RG_dependencies:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rglistattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/RG_dependencies:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/network dependencies:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[15];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/network dependencies:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:network dependencies:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(("implicit_network_dependencies:",$item[2])); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/network dependencies:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/global_resources_used:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[16];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/global_resources_used:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:global_resources_used:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/global_resources_used:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/pingpong_interval:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[17];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/pingpong_interval:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:pingpong_interval:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/pingpong_interval:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/pathprefix:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[18];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/pathprefix:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:pathprefix:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rgattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/pathprefix:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/RG_affinities:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[19];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/RG_affinities:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:RG_affinities:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rglistattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/RG_affinities:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/system:/i data]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[20];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/system:/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:system:)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [data]},
				  Parse::RecDescent::_tracefirst($text),
				  q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{data})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::data($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [data]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{_alternation_1_of_production_1_of_rule_rgattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [data]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$item{q{data}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying action},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		

		$_tok = ($_noactions) ? 0 : do {
	$thisparser->_rglistattribute(@item[1,2]); };
		unless (defined $_tok)
		{
			Parse::RecDescent::_trace(q{<<Didn't match action>> (return value: [undef])})
					if defined $::RD_TRACE;
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched action<< (return value: [}
					  . $_tok . q{])}, $text)
						if defined $::RD_TRACE;
		push @item, $_tok;
		$item{__ACTION1__}=$_tok;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/system:/i data]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/Suspend_automatic_recovery:.*/i]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[21];
		$text = $_[1];
		my $_savetext;
		@item = (q{_alternation_1_of_production_1_of_rule_rgattribute});
		%item = (__RULE__ => q{_alternation_1_of_production_1_of_rule_rgattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/Suspend_automatic_recovery:.*/i]}, Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:Suspend_automatic_recovery:.*)//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		


		Parse::RecDescent::_trace(q{>>Matched production: [/Suspend_automatic_recovery:.*/i]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{_alternation_1_of_production_1_of_rule_rgattribute})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{_alternation_1_of_production_1_of_rule_rgattribute})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{_alternation_1_of_production_1_of_rule_rgattribute});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{_alternation_1_of_production_1_of_rule_rgattribute})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtparamattributelist
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtparamattributelist"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtparamattributelist]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtparamattributelist})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rtparamattribute rtparamattributelist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtparamattributelist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtparamattributelist});
		%item = (__RULE__ => q{rtparamattributelist});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rtparamattribute]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtparamattributelist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtparamattribute($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtparamattribute]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtparamattributelist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtparamattribute]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamattributelist})
						if defined $::RD_TRACE;
		$item{q{rtparamattribute}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rtparamattributelist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtparamattributelist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rtparamattributelist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtparamattributelist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtparamattributelist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtparamattributelist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtparamattributelist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamattributelist})
						if defined $::RD_TRACE;
		$item{q{rtparamattributelist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rtparamattribute rtparamattributelist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamattributelist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: []},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtparamattributelist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtparamattributelist});
		%item = (__RULE__ => q{rtparamattributelist});
		my $repcount = 0;



		Parse::RecDescent::_trace(q{>>Matched production: []<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamattributelist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtparamattributelist})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtparamattributelist})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtparamattributelist});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtparamattributelist})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtparamlist
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtparamlist"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtparamlist]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtparamlist})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rtparam rtparamlist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtparamlist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtparamlist});
		%item = (__RULE__ => q{rtparamlist});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rtparam]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtparamlist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtparam($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtparam]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtparamlist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtparam]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamlist})
						if defined $::RD_TRACE;
		$item{q{rtparam}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rtparamlist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtparamlist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rtparamlist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rtparamlist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rtparamlist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtparamlist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rtparamlist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamlist})
						if defined $::RD_TRACE;
		$item{q{rtparamlist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rtparam rtparamlist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamlist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: []},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtparamlist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtparamlist});
		%item = (__RULE__ => q{rtparamlist});
		my $repcount = 0;



		Parse::RecDescent::_trace(q{>>Matched production: []<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamlist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtparamlist})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtparamlist})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtparamlist});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtparamlist})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rtparamattribute
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rtparamattribute"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rtparamattribute]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rtparamattribute})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [/.*Res Type param /i /extension:/i, or /per-node:/i, or /description:/i, or /tunability:/i, or /type:/i, or /min int value:/i, or /max int value:/i, or /min length:/i, or /max length:/i, or /min array length:/i, or /max array length:/i, or /enum list:/i, or /default:/i]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rtparamattribute})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rtparamattribute});
		%item = (__RULE__ => q{rtparamattribute});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying terminal: [/.*Res Type param /i]}, Parse::RecDescent::_tracefirst($text),
					  q{rtparamattribute})
						if defined $::RD_TRACE;
		$lastsep = "";
		$expectation->is(q{})->at($text);
		

		unless ($text =~ s/\A($skip)/$lastsep=$1 and ""/e and   $text =~ s/\A(?:.*Res Type param )//i)
		{
			
			$expectation->failed();
			Parse::RecDescent::_trace(q{<<Didn't match terminal>>},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;

			last;
		}
		Parse::RecDescent::_trace(q{>>Matched terminal<< (return value: [}
						. $& . q{])},
						  Parse::RecDescent::_tracefirst($text))
					if defined $::RD_TRACE;
		push @item, $item{__PATTERN1__}=$&;
		

		Parse::RecDescent::_trace(q{Trying subrule: [_alternation_1_of_production_1_of_rule_rtparamattribute]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rtparamattribute})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{/extension:/i, or /per-node:/i, or /description:/i, or /tunability:/i, or /type:/i, or /min int value:/i, or /max int value:/i, or /min length:/i, or /max length:/i, or /min array length:/i, or /max array length:/i, or /enum list:/i, or /default:/i})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::_alternation_1_of_production_1_of_rule_rtparamattribute($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [_alternation_1_of_production_1_of_rule_rtparamattribute]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rtparamattribute})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [_alternation_1_of_production_1_of_rule_rtparamattribute]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamattribute})
						if defined $::RD_TRACE;
		$item{q{_alternation_1_of_production_1_of_rule_rtparamattribute}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [/.*Res Type param /i /extension:/i, or /per-node:/i, or /description:/i, or /tunability:/i, or /type:/i, or /min int value:/i, or /max int value:/i, or /min length:/i, or /max length:/i, or /min array length:/i, or /max array length:/i, or /enum list:/i, or /default:/i]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rtparamattribute})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rtparamattribute})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rtparamattribute})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rtparamattribute});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rtparamattribute})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgattributelist
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgattributelist"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgattributelist]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgattributelist})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rgattribute rgattributelist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgattributelist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgattributelist});
		%item = (__RULE__ => q{rgattributelist});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rgattribute]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgattributelist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgattribute($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgattribute]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgattributelist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgattribute]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgattributelist})
						if defined $::RD_TRACE;
		$item{q{rgattribute}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rgattributelist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgattributelist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rgattributelist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgattributelist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgattributelist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgattributelist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgattributelist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgattributelist})
						if defined $::RD_TRACE;
		$item{q{rgattributelist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rgattribute rgattributelist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgattributelist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: []},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgattributelist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgattributelist});
		%item = (__RULE__ => q{rgattributelist});
		my $repcount = 0;



		Parse::RecDescent::_trace(q{>>Matched production: []<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgattributelist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgattributelist})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgattributelist})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgattributelist});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgattributelist})
	}
	$_[1] = $text;
	return $return;
}

# ARGS ARE: ($parser, $text; $repeating, $_noactions, \@args)
sub Parse::RecDescent::namespace000001::rgresourcepropertylist
{
	my $thisparser = $_[0];
	$ERRORS = 0;
	my $thisrule = $thisparser->{"rules"}{"rgresourcepropertylist"};
	
	Parse::RecDescent::_trace(q{Trying rule: [rgresourcepropertylist]},
				  Parse::RecDescent::_tracefirst($_[1]),
				  q{rgresourcepropertylist})
					if defined $::RD_TRACE;

	
	my $err_at = @{$thisparser->{errors}};

	my $score;
	my $score_return;
	my $_tok;
	my $return = undef;
	my $_matched=0;
	my $commit=0;
	my @item = ();
	my %item = ();
	my $repeating =  defined($_[2]) && $_[2];
	my $_noactions = defined($_[3]) && $_[3];
 	my @arg =        defined $_[4] ? @{ &{$_[4]} } : ();
	my %arg =        ($#arg & 01) ? @arg : (@arg, undef);
	my $text;
	my $lastsep="";
	my $expectation = new Parse::RecDescent::Expectation($thisrule->expected());
	$expectation->at($_[1]);
	
	my $thisline;
	tie $thisline, q{Parse::RecDescent::LineCounter}, \$text, $thisparser;

	

	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: [rgresourceproperty rgresourcepropertylist]},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourcepropertylist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[0];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourcepropertylist});
		%item = (__RULE__ => q{rgresourcepropertylist});
		my $repcount = 0;


		Parse::RecDescent::_trace(q{Trying subrule: [rgresourceproperty]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourcepropertylist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourceproperty($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourceproperty]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourcepropertylist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourceproperty]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertylist})
						if defined $::RD_TRACE;
		$item{q{rgresourceproperty}} = $_tok;
		push @item, $_tok;
		
		}

		Parse::RecDescent::_trace(q{Trying subrule: [rgresourcepropertylist]},
				  Parse::RecDescent::_tracefirst($text),
				  q{rgresourcepropertylist})
					if defined $::RD_TRACE;
		if (1) { no strict qw{refs};
		$expectation->is(q{rgresourcepropertylist})->at($text);
		unless (defined ($_tok = Parse::RecDescent::namespace000001::rgresourcepropertylist($thisparser,$text,$repeating,$_noactions,undef)))
		{
			
			Parse::RecDescent::_trace(q{<<Didn't match subrule: [rgresourcepropertylist]>>},
						  Parse::RecDescent::_tracefirst($text),
						  q{rgresourcepropertylist})
							if defined $::RD_TRACE;
			$expectation->failed();
			last;
		}
		Parse::RecDescent::_trace(q{>>Matched subrule: [rgresourcepropertylist]<< (return value: [}
					. $_tok . q{]},
					  
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertylist})
						if defined $::RD_TRACE;
		$item{q{rgresourcepropertylist}} = $_tok;
		push @item, $_tok;
		
		}


		Parse::RecDescent::_trace(q{>>Matched production: [rgresourceproperty rgresourcepropertylist]<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertylist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


	while (!$_matched && !$commit)
	{
		
		Parse::RecDescent::_trace(q{Trying production: []},
					  Parse::RecDescent::_tracefirst($_[1]),
					  q{rgresourcepropertylist})
						if defined $::RD_TRACE;
		my $thisprod = $thisrule->{"prods"}[1];
		$text = $_[1];
		my $_savetext;
		@item = (q{rgresourcepropertylist});
		%item = (__RULE__ => q{rgresourcepropertylist});
		my $repcount = 0;



		Parse::RecDescent::_trace(q{>>Matched production: []<<},
					  Parse::RecDescent::_tracefirst($text),
					  q{rgresourcepropertylist})
						if defined $::RD_TRACE;
		$_matched = 1;
		last;
	}


        unless ( $_matched || defined($return) || defined($score) )
	{
		

		$_[1] = $text;	# NOT SURE THIS IS NEEDED
		Parse::RecDescent::_trace(q{<<Didn't match rule>>},
					 Parse::RecDescent::_tracefirst($_[1]),
					 q{rgresourcepropertylist})
					if defined $::RD_TRACE;
		return undef;
	}
	if (!defined($return) && defined($score))
	{
		Parse::RecDescent::_trace(q{>>Accepted scored production<<}, "",
					  q{rgresourcepropertylist})
						if defined $::RD_TRACE;
		$return = $score_return;
	}
	splice @{$thisparser->{errors}}, $err_at;
	$return = $item[$#item] unless defined $return;
	if (defined $::RD_TRACE)
	{
		Parse::RecDescent::_trace(q{>>Matched rule<< (return value: [} .
					  $return . q{])}, "",
					  q{rgresourcepropertylist});
		Parse::RecDescent::_trace(q{(consumed: [} .
					  Parse::RecDescent::_tracemax(substr($_[1],0,-length($text))) . q{])}, 
					  Parse::RecDescent::_tracefirst($text),
					  , q{rgresourcepropertylist})
	}
	$_[1] = $text;
	return $return;
}
}
package ScSnapshot::GeneratedParser; sub new { my $self = bless( {
                 '_AUTOTREE' => undef,
                 'rules' => {
                              'rgresourcepropertyattributelist' => bless( {
                                                                            'impcount' => 0,
                                                                            'line' => '232',
                                                                            'prods' => [
                                                                                         bless( {
                                                                                                  'dircount' => 0,
                                                                                                  'uncommit' => undef,
                                                                                                  'patcount' => 0,
                                                                                                  'strcount' => 0,
                                                                                                  'number' => 0,
                                                                                                  'error' => undef,
                                                                                                  'line' => undef,
                                                                                                  'items' => [
                                                                                                               bless( {
                                                                                                                        'line' => '232',
                                                                                                                        'subrule' => 'rgresourcepropertyattribute',
                                                                                                                        'argcode' => undef,
                                                                                                                        'implicit' => undef,
                                                                                                                        'matchrule' => 0,
                                                                                                                        'lookahead' => 0
                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                               bless( {
                                                                                                                        'line' => '232',
                                                                                                                        'subrule' => 'rgresourcepropertyattributelist',
                                                                                                                        'argcode' => undef,
                                                                                                                        'implicit' => undef,
                                                                                                                        'matchrule' => 0,
                                                                                                                        'lookahead' => 0
                                                                                                                      }, 'Parse::RecDescent::Subrule' )
                                                                                                             ],
                                                                                                  'actcount' => 0
                                                                                                }, 'Parse::RecDescent::Production' ),
                                                                                         bless( {
                                                                                                  'dircount' => 0,
                                                                                                  'uncommit' => undef,
                                                                                                  'patcount' => 0,
                                                                                                  'strcount' => 0,
                                                                                                  'number' => 1,
                                                                                                  'error' => undef,
                                                                                                  'line' => '232',
                                                                                                  'items' => [],
                                                                                                  'actcount' => 0
                                                                                                }, 'Parse::RecDescent::Production' )
                                                                                       ],
                                                                            'calls' => [
                                                                                         'rgresourcepropertyattribute',
                                                                                         'rgresourcepropertyattributelist'
                                                                                       ],
                                                                            'opcount' => 0,
                                                                            'changed' => 0,
                                                                            'vars' => '',
                                                                            'name' => 'rgresourcepropertyattributelist'
                                                                          }, 'Parse::RecDescent::Rule' ),
                              'rgresourceattribute' => bless( {
                                                                'impcount' => 1,
                                                                'line' => '201',
                                                                'prods' => [
                                                                             bless( {
                                                                                      'dircount' => 0,
                                                                                      'uncommit' => undef,
                                                                                      'patcount' => 1,
                                                                                      'strcount' => 0,
                                                                                      'number' => 0,
                                                                                      'error' => undef,
                                                                                      'line' => undef,
                                                                                      'items' => [
                                                                                                   bless( {
                                                                                                            'description' => '/.*Res /i',
                                                                                                            'pattern' => '.*Res ',
                                                                                                            'mod' => 'i',
                                                                                                            'hashname' => '__PATTERN1__',
                                                                                                            'lookahead' => 0,
                                                                                                            'ldelim' => '/',
                                                                                                            'line' => '201',
                                                                                                            'rdelim' => '/'
                                                                                                          }, 'Parse::RecDescent::Token' ),
                                                                                                   bless( {
                                                                                                            'line' => '222',
                                                                                                            'subrule' => '_alternation_1_of_production_1_of_rule_rgresourceattribute',
                                                                                                            'argcode' => undef,
                                                                                                            'implicit' => '/R_description:/i, or /type version:/i, or /resource group name:/i, or /enabled:/i, or /monitor enabled:/i, or /resource type:/i, or /resource project name:/i, or /offline restart dependencies:/i, or /restart dependencies:/i, or /strong dependencies:/i, or /weak dependencies:/i',
                                                                                                            'matchrule' => 0,
                                                                                                            'lookahead' => 0
                                                                                                          }, 'Parse::RecDescent::Subrule' )
                                                                                                 ],
                                                                                      'actcount' => 0
                                                                                    }, 'Parse::RecDescent::Production' )
                                                                           ],
                                                                'calls' => [
                                                                             '_alternation_1_of_production_1_of_rule_rgresourceattribute'
                                                                           ],
                                                                'opcount' => 0,
                                                                'changed' => 0,
                                                                'vars' => '',
                                                                'name' => 'rgresourceattribute'
                                                              }, 'Parse::RecDescent::Rule' ),
                              'rgresourcename' => bless( {
                                                           'impcount' => 0,
                                                           'line' => '194',
                                                           'prods' => [
                                                                        bless( {
                                                                                 'dircount' => 0,
                                                                                 'uncommit' => undef,
                                                                                 'patcount' => 1,
                                                                                 'strcount' => 0,
                                                                                 'number' => 0,
                                                                                 'error' => undef,
                                                                                 'line' => undef,
                                                                                 'items' => [
                                                                                              bless( {
                                                                                                       'description' => '/.*Res name:/i',
                                                                                                       'pattern' => '.*Res name:',
                                                                                                       'mod' => 'i',
                                                                                                       'hashname' => '__PATTERN1__',
                                                                                                       'lookahead' => 0,
                                                                                                       'ldelim' => '/',
                                                                                                       'line' => '194',
                                                                                                       'rdelim' => '/'
                                                                                                     }, 'Parse::RecDescent::Token' ),
                                                                                              bless( {
                                                                                                       'line' => '194',
                                                                                                       'subrule' => 'data',
                                                                                                       'argcode' => undef,
                                                                                                       'implicit' => undef,
                                                                                                       'matchrule' => 0,
                                                                                                       'lookahead' => 0
                                                                                                     }, 'Parse::RecDescent::Subrule' ),
                                                                                              bless( {
                                                                                                       'line' => '194',
                                                                                                       'code' => '{
	$thisparser->debug_ppc($item[1], $item[2]);
	$thisparser->_rgresourcename($item[2]);
}',
                                                                                                       'hashname' => '__ACTION1__',
                                                                                                       'lookahead' => 0
                                                                                                     }, 'Parse::RecDescent::Action' )
                                                                                            ],
                                                                                 'actcount' => 1
                                                                               }, 'Parse::RecDescent::Production' )
                                                                      ],
                                                           'calls' => [
                                                                        'data'
                                                                      ],
                                                           'opcount' => 0,
                                                           'changed' => 0,
                                                           'vars' => '',
                                                           'name' => 'rgresourcename'
                                                         }, 'Parse::RecDescent::Rule' ),
                              'def' => bless( {
                                                'impcount' => 0,
                                                'line' => '34',
                                                'prods' => [
                                                             bless( {
                                                                      'dircount' => 0,
                                                                      'uncommit' => undef,
                                                                      'patcount' => 0,
                                                                      'strcount' => 0,
                                                                      'number' => 0,
                                                                      'error' => undef,
                                                                      'line' => undef,
                                                                      'items' => [
                                                                                   bless( {
                                                                                            'line' => '34',
                                                                                            'subrule' => 'rtdef',
                                                                                            'argcode' => undef,
                                                                                            'implicit' => undef,
                                                                                            'matchrule' => 0,
                                                                                            'lookahead' => 0
                                                                                          }, 'Parse::RecDescent::Subrule' )
                                                                                 ],
                                                                      'actcount' => 0
                                                                    }, 'Parse::RecDescent::Production' ),
                                                             bless( {
                                                                      'dircount' => 0,
                                                                      'uncommit' => undef,
                                                                      'patcount' => 0,
                                                                      'strcount' => 0,
                                                                      'number' => 1,
                                                                      'error' => undef,
                                                                      'line' => '34',
                                                                      'items' => [
                                                                                   bless( {
                                                                                            'line' => '34',
                                                                                            'subrule' => 'rgdef',
                                                                                            'argcode' => undef,
                                                                                            'implicit' => undef,
                                                                                            'matchrule' => 0,
                                                                                            'lookahead' => 0
                                                                                          }, 'Parse::RecDescent::Subrule' )
                                                                                 ],
                                                                      'actcount' => 0
                                                                    }, 'Parse::RecDescent::Production' ),
                                                             bless( {
                                                                      'dircount' => 1,
                                                                      'uncommit' => 0,
                                                                      'patcount' => 0,
                                                                      'strcount' => 0,
                                                                      'number' => 2,
                                                                      'error' => 1,
                                                                      'line' => '34',
                                                                      'items' => [
                                                                                   bless( {
                                                                                            'line' => '34',
                                                                                            'commitonly' => '',
                                                                                            'hashname' => '__DIRECTIVE1__',
                                                                                            'lookahead' => 0,
                                                                                            'msg' => ''
                                                                                          }, 'Parse::RecDescent::Error' )
                                                                                 ],
                                                                      'actcount' => 0
                                                                    }, 'Parse::RecDescent::Production' )
                                                           ],
                                                'calls' => [
                                                             'rtdef',
                                                             'rgdef'
                                                           ],
                                                'opcount' => 0,
                                                'changed' => 0,
                                                'vars' => '',
                                                'name' => 'def'
                                              }, 'Parse::RecDescent::Rule' ),
                              'startrule' => bless( {
                                                      'impcount' => 0,
                                                      'line' => '31',
                                                      'prods' => [
                                                                   bless( {
                                                                            'dircount' => 0,
                                                                            'uncommit' => undef,
                                                                            'patcount' => 0,
                                                                            'strcount' => 0,
                                                                            'number' => 0,
                                                                            'error' => undef,
                                                                            'line' => undef,
                                                                            'items' => [
                                                                                         bless( {
                                                                                                  'min' => 1,
                                                                                                  'argcode' => undef,
                                                                                                  'matchrule' => 0,
                                                                                                  'lookahead' => 0,
                                                                                                  'subrule' => 'def',
                                                                                                  'line' => '32',
                                                                                                  'expected' => undef,
                                                                                                  'max' => 100000000,
                                                                                                  'repspec' => 's'
                                                                                                }, 'Parse::RecDescent::Repetition' ),
                                                                                         bless( {
                                                                                                  'line' => '32',
                                                                                                  'subrule' => 'eofile',
                                                                                                  'argcode' => undef,
                                                                                                  'implicit' => undef,
                                                                                                  'matchrule' => 0,
                                                                                                  'lookahead' => 0
                                                                                                }, 'Parse::RecDescent::Subrule' ),
                                                                                         bless( {
                                                                                                  'line' => '32',
                                                                                                  'code' => '{ $return = $item[1] }',
                                                                                                  'hashname' => '__ACTION1__',
                                                                                                  'lookahead' => 0
                                                                                                }, 'Parse::RecDescent::Action' )
                                                                                       ],
                                                                            'actcount' => 1
                                                                          }, 'Parse::RecDescent::Production' )
                                                                 ],
                                                      'calls' => [
                                                                   'def',
                                                                   'eofile'
                                                                 ],
                                                      'opcount' => 0,
                                                      'changed' => 0,
                                                      'vars' => '',
                                                      'name' => 'startrule'
                                                    }, 'Parse::RecDescent::Rule' ),
                              'rtparam' => bless( {
                                                    'impcount' => 0,
                                                    'line' => '101',
                                                    'prods' => [
                                                                 bless( {
                                                                          'dircount' => 0,
                                                                          'uncommit' => undef,
                                                                          'patcount' => 0,
                                                                          'strcount' => 0,
                                                                          'number' => 0,
                                                                          'error' => undef,
                                                                          'line' => undef,
                                                                          'items' => [
                                                                                       bless( {
                                                                                                'line' => '101',
                                                                                                'subrule' => 'rtparamname',
                                                                                                'argcode' => undef,
                                                                                                'implicit' => undef,
                                                                                                'matchrule' => 0,
                                                                                                'lookahead' => 0
                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                       bless( {
                                                                                                'line' => '101',
                                                                                                'subrule' => 'rtparamattributelist',
                                                                                                'argcode' => undef,
                                                                                                'implicit' => undef,
                                                                                                'matchrule' => 0,
                                                                                                'lookahead' => 0
                                                                                              }, 'Parse::RecDescent::Subrule' )
                                                                                     ],
                                                                          'actcount' => 0
                                                                        }, 'Parse::RecDescent::Production' )
                                                               ],
                                                    'calls' => [
                                                                 'rtparamname',
                                                                 'rtparamattributelist'
                                                               ],
                                                    'opcount' => 0,
                                                    'changed' => 0,
                                                    'vars' => '',
                                                    'name' => 'rtparam'
                                                  }, 'Parse::RecDescent::Rule' ),
                              '_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute' => bless( {
                                                                                                               'impcount' => 0,
                                                                                                               'line' => '239',
                                                                                                               'prods' => [
                                                                                                                            bless( {
                                                                                                                                     'dircount' => 0,
                                                                                                                                     'uncommit' => undef,
                                                                                                                                     'patcount' => 1,
                                                                                                                                     'strcount' => 0,
                                                                                                                                     'number' => 0,
                                                                                                                                     'error' => undef,
                                                                                                                                     'line' => undef,
                                                                                                                                     'items' => [
                                                                                                                                                  bless( {
                                                                                                                                                           'description' => '/class:/i',
                                                                                                                                                           'pattern' => 'class:',
                                                                                                                                                           'mod' => 'i',
                                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                                           'lookahead' => 0,
                                                                                                                                                           'ldelim' => '/',
                                                                                                                                                           'line' => '239',
                                                                                                                                                           'rdelim' => '/'
                                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                                  bless( {
                                                                                                                                                           'line' => '239',
                                                                                                                                                           'subrule' => 'data',
                                                                                                                                                           'argcode' => undef,
                                                                                                                                                           'implicit' => undef,
                                                                                                                                                           'matchrule' => 0,
                                                                                                                                                           'lookahead' => 0
                                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                                  bless( {
                                                                                                                                                           'line' => '239',
                                                                                                                                                           'code' => '{
	$thisparser->_rgresourcepropertyattribute(@item[1,2]); }',
                                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                                           'lookahead' => 0
                                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                                ],
                                                                                                                                     'actcount' => 1
                                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                                            bless( {
                                                                                                                                     'dircount' => 0,
                                                                                                                                     'uncommit' => undef,
                                                                                                                                     'patcount' => 1,
                                                                                                                                     'strcount' => 0,
                                                                                                                                     'number' => 1,
                                                                                                                                     'error' => undef,
                                                                                                                                     'line' => '240',
                                                                                                                                     'items' => [
                                                                                                                                                  bless( {
                                                                                                                                                           'description' => '/description:/i',
                                                                                                                                                           'pattern' => 'description:',
                                                                                                                                                           'mod' => 'i',
                                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                                           'lookahead' => 0,
                                                                                                                                                           'ldelim' => '/',
                                                                                                                                                           'line' => '241',
                                                                                                                                                           'rdelim' => '/'
                                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                                  bless( {
                                                                                                                                                           'line' => '241',
                                                                                                                                                           'subrule' => 'data',
                                                                                                                                                           'argcode' => undef,
                                                                                                                                                           'implicit' => undef,
                                                                                                                                                           'matchrule' => 0,
                                                                                                                                                           'lookahead' => 0
                                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                                  bless( {
                                                                                                                                                           'line' => '241',
                                                                                                                                                           'code' => '{
	$thisparser->_rgresourcepropertyattribute(@item[1,2]); }',
                                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                                           'lookahead' => 0
                                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                                ],
                                                                                                                                     'actcount' => 1
                                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                                            bless( {
                                                                                                                                     'dircount' => 0,
                                                                                                                                     'uncommit' => undef,
                                                                                                                                     'patcount' => 1,
                                                                                                                                     'strcount' => 0,
                                                                                                                                     'number' => 2,
                                                                                                                                     'error' => undef,
                                                                                                                                     'line' => '242',
                                                                                                                                     'items' => [
                                                                                                                                                  bless( {
                                                                                                                                                           'description' => '/pernode:/i',
                                                                                                                                                           'pattern' => 'pernode:',
                                                                                                                                                           'mod' => 'i',
                                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                                           'lookahead' => 0,
                                                                                                                                                           'ldelim' => '/',
                                                                                                                                                           'line' => '243',
                                                                                                                                                           'rdelim' => '/'
                                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                                  bless( {
                                                                                                                                                           'line' => '243',
                                                                                                                                                           'subrule' => 'data',
                                                                                                                                                           'argcode' => undef,
                                                                                                                                                           'implicit' => undef,
                                                                                                                                                           'matchrule' => 0,
                                                                                                                                                           'lookahead' => 0
                                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                                  bless( {
                                                                                                                                                           'line' => '243',
                                                                                                                                                           'code' => '{
	$thisparser->_rgresourcepropertyattribute(@item[1,2]); }',
                                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                                           'lookahead' => 0
                                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                                ],
                                                                                                                                     'actcount' => 1
                                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                                            bless( {
                                                                                                                                     'dircount' => 0,
                                                                                                                                     'uncommit' => undef,
                                                                                                                                     'patcount' => 1,
                                                                                                                                     'strcount' => 0,
                                                                                                                                     'number' => 3,
                                                                                                                                     'error' => undef,
                                                                                                                                     'line' => '244',
                                                                                                                                     'items' => [
                                                                                                                                                  bless( {
                                                                                                                                                           'description' => '/type:/i',
                                                                                                                                                           'pattern' => 'type:',
                                                                                                                                                           'mod' => 'i',
                                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                                           'lookahead' => 0,
                                                                                                                                                           'ldelim' => '/',
                                                                                                                                                           'line' => '245',
                                                                                                                                                           'rdelim' => '/'
                                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                                  bless( {
                                                                                                                                                           'line' => '245',
                                                                                                                                                           'subrule' => 'data',
                                                                                                                                                           'argcode' => undef,
                                                                                                                                                           'implicit' => undef,
                                                                                                                                                           'matchrule' => 0,
                                                                                                                                                           'lookahead' => 0
                                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                                  bless( {
                                                                                                                                                           'line' => '245',
                                                                                                                                                           'code' => '{
	$thisparser->_rgresourcepropertyattribute(@item[1,2]); }',
                                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                                           'lookahead' => 0
                                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                                ],
                                                                                                                                     'actcount' => 1
                                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                                            bless( {
                                                                                                                                     'dircount' => 0,
                                                                                                                                     'uncommit' => undef,
                                                                                                                                     'patcount' => 1,
                                                                                                                                     'strcount' => 0,
                                                                                                                                     'number' => 4,
                                                                                                                                     'error' => undef,
                                                                                                                                     'line' => '246',
                                                                                                                                     'items' => [
                                                                                                                                                  bless( {
                                                                                                                                                           'description' => '/value:/i',
                                                                                                                                                           'pattern' => 'value:',
                                                                                                                                                           'mod' => 'i',
                                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                                           'lookahead' => 0,
                                                                                                                                                           'ldelim' => '/',
                                                                                                                                                           'line' => '247',
                                                                                                                                                           'rdelim' => '/'
                                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                                  bless( {
                                                                                                                                                           'line' => '247',
                                                                                                                                                           'subrule' => 'data',
                                                                                                                                                           'argcode' => undef,
                                                                                                                                                           'implicit' => undef,
                                                                                                                                                           'matchrule' => 0,
                                                                                                                                                           'lookahead' => 0
                                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                                  bless( {
                                                                                                                                                           'line' => '247',
                                                                                                                                                           'code' => '{
	$thisparser->_rgresourcepropertyattribute(@item[1,2]); }',
                                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                                           'lookahead' => 0
                                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                                ],
                                                                                                                                     'actcount' => 1
                                                                                                                                   }, 'Parse::RecDescent::Production' )
                                                                                                                          ],
                                                                                                               'calls' => [
                                                                                                                            'data'
                                                                                                                          ],
                                                                                                               'opcount' => 0,
                                                                                                               'changed' => 0,
                                                                                                               'vars' => '',
                                                                                                               'name' => '_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute'
                                                                                                             }, 'Parse::RecDescent::Rule' ),
                              'rgresourcepropertyattribute' => bless( {
                                                                        'impcount' => 1,
                                                                        'line' => '234',
                                                                        'prods' => [
                                                                                     bless( {
                                                                                              'dircount' => 0,
                                                                                              'uncommit' => undef,
                                                                                              'patcount' => 1,
                                                                                              'strcount' => 0,
                                                                                              'number' => 0,
                                                                                              'error' => undef,
                                                                                              'line' => undef,
                                                                                              'items' => [
                                                                                                           bless( {
                                                                                                                    'description' => '/.*Res property /i',
                                                                                                                    'pattern' => '.*Res property ',
                                                                                                                    'mod' => 'i',
                                                                                                                    'hashname' => '__PATTERN1__',
                                                                                                                    'lookahead' => 0,
                                                                                                                    'ldelim' => '/',
                                                                                                                    'line' => '234',
                                                                                                                    'rdelim' => '/'
                                                                                                                  }, 'Parse::RecDescent::Token' ),
                                                                                                           bless( {
                                                                                                                    'line' => '243',
                                                                                                                    'subrule' => '_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute',
                                                                                                                    'argcode' => undef,
                                                                                                                    'implicit' => '/class:/i, or /description:/i, or /pernode:/i, or /type:/i, or /value:/i',
                                                                                                                    'matchrule' => 0,
                                                                                                                    'lookahead' => 0
                                                                                                                  }, 'Parse::RecDescent::Subrule' )
                                                                                                         ],
                                                                                              'actcount' => 0
                                                                                            }, 'Parse::RecDescent::Production' )
                                                                                   ],
                                                                        'calls' => [
                                                                                     '_alternation_1_of_production_1_of_rule_rgresourcepropertyattribute'
                                                                                   ],
                                                                        'opcount' => 0,
                                                                        'changed' => 0,
                                                                        'vars' => '',
                                                                        'name' => 'rgresourcepropertyattribute'
                                                                      }, 'Parse::RecDescent::Rule' ),
                              'rtparamname' => bless( {
                                                        'impcount' => 0,
                                                        'line' => '105',
                                                        'prods' => [
                                                                     bless( {
                                                                              'dircount' => 0,
                                                                              'uncommit' => undef,
                                                                              'patcount' => 1,
                                                                              'strcount' => 0,
                                                                              'number' => 0,
                                                                              'error' => undef,
                                                                              'line' => undef,
                                                                              'items' => [
                                                                                           bless( {
                                                                                                    'description' => '/.*Res Type param name:/i',
                                                                                                    'pattern' => '.*Res Type param name:',
                                                                                                    'mod' => 'i',
                                                                                                    'hashname' => '__PATTERN1__',
                                                                                                    'lookahead' => 0,
                                                                                                    'ldelim' => '/',
                                                                                                    'line' => '105',
                                                                                                    'rdelim' => '/'
                                                                                                  }, 'Parse::RecDescent::Token' ),
                                                                                           bless( {
                                                                                                    'line' => '105',
                                                                                                    'subrule' => 'data',
                                                                                                    'argcode' => undef,
                                                                                                    'implicit' => undef,
                                                                                                    'matchrule' => 0,
                                                                                                    'lookahead' => 0
                                                                                                  }, 'Parse::RecDescent::Subrule' ),
                                                                                           bless( {
                                                                                                    'line' => '105',
                                                                                                    'code' => '{
	$thisparser->debug_ppc($item[1], $item[2]);
	$thisparser->_rtparamname($item[2]);
}',
                                                                                                    'hashname' => '__ACTION1__',
                                                                                                    'lookahead' => 0
                                                                                                  }, 'Parse::RecDescent::Action' )
                                                                                         ],
                                                                              'actcount' => 1
                                                                            }, 'Parse::RecDescent::Production' )
                                                                   ],
                                                        'calls' => [
                                                                     'data'
                                                                   ],
                                                        'opcount' => 0,
                                                        'changed' => 0,
                                                        'vars' => '',
                                                        'name' => 'rtparamname'
                                                      }, 'Parse::RecDescent::Rule' ),
                              '_alternation_1_of_production_1_of_rule_rtparamattribute' => bless( {
                                                                                                    'impcount' => 0,
                                                                                                    'line' => '223',
                                                                                                    'prods' => [
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 0,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => undef,
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/extension:/i',
                                                                                                                                                'pattern' => 'extension:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '223',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '223',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '223',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 1,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => '224',
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/per-node:/i',
                                                                                                                                                'pattern' => 'per-node:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '225',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '225',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '225',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 2,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => '226',
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/description:/i',
                                                                                                                                                'pattern' => 'description:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '227',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '227',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '227',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 3,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => '228',
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/tunability:/i',
                                                                                                                                                'pattern' => 'tunability:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '229',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '229',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '229',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 4,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => '230',
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/type:/i',
                                                                                                                                                'pattern' => 'type:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '231',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '231',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '231',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 5,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => '232',
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/min int value:/i',
                                                                                                                                                'pattern' => 'min int value:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '233',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '233',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '233',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 6,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => '234',
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/max int value:/i',
                                                                                                                                                'pattern' => 'max int value:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '235',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '235',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '235',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 7,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => '236',
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/min length:/i',
                                                                                                                                                'pattern' => 'min length:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '237',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '237',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '237',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 8,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => '238',
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/max length:/i',
                                                                                                                                                'pattern' => 'max length:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '239',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '239',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '239',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 9,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => '240',
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/min array length:/i',
                                                                                                                                                'pattern' => 'min array length:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '241',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '241',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '241',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 10,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => '242',
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/max array length:/i',
                                                                                                                                                'pattern' => 'max array length:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '243',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '243',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '243',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 11,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => '244',
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/enum list:/i',
                                                                                                                                                'pattern' => 'enum list:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '245',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '245',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '245',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                                                 bless( {
                                                                                                                          'dircount' => 0,
                                                                                                                          'uncommit' => undef,
                                                                                                                          'patcount' => 1,
                                                                                                                          'strcount' => 0,
                                                                                                                          'number' => 12,
                                                                                                                          'error' => undef,
                                                                                                                          'line' => '246',
                                                                                                                          'items' => [
                                                                                                                                       bless( {
                                                                                                                                                'description' => '/default:/i',
                                                                                                                                                'pattern' => 'default:',
                                                                                                                                                'mod' => 'i',
                                                                                                                                                'hashname' => '__PATTERN1__',
                                                                                                                                                'lookahead' => 0,
                                                                                                                                                'ldelim' => '/',
                                                                                                                                                'line' => '247',
                                                                                                                                                'rdelim' => '/'
                                                                                                                                              }, 'Parse::RecDescent::Token' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '247',
                                                                                                                                                'subrule' => 'data',
                                                                                                                                                'argcode' => undef,
                                                                                                                                                'implicit' => undef,
                                                                                                                                                'matchrule' => 0,
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                       bless( {
                                                                                                                                                'line' => '247',
                                                                                                                                                'code' => '{
	$thisparser->_rtparamattribute(@item[1,2]); }',
                                                                                                                                                'hashname' => '__ACTION1__',
                                                                                                                                                'lookahead' => 0
                                                                                                                                              }, 'Parse::RecDescent::Action' )
                                                                                                                                     ],
                                                                                                                          'actcount' => 1
                                                                                                                        }, 'Parse::RecDescent::Production' )
                                                                                                               ],
                                                                                                    'calls' => [
                                                                                                                 'data'
                                                                                                               ],
                                                                                                    'opcount' => 0,
                                                                                                    'changed' => 0,
                                                                                                    'vars' => '',
                                                                                                    'name' => '_alternation_1_of_production_1_of_rule_rtparamattribute'
                                                                                                  }, 'Parse::RecDescent::Rule' ),
                              'rgresourcepropertyname' => bless( {
                                                                   'impcount' => 0,
                                                                   'line' => '228',
                                                                   'prods' => [
                                                                                bless( {
                                                                                         'dircount' => 0,
                                                                                         'uncommit' => undef,
                                                                                         'patcount' => 1,
                                                                                         'strcount' => 0,
                                                                                         'number' => 0,
                                                                                         'error' => undef,
                                                                                         'line' => undef,
                                                                                         'items' => [
                                                                                                      bless( {
                                                                                                               'description' => '/.*Res property name:/i',
                                                                                                               'pattern' => '.*Res property name:',
                                                                                                               'mod' => 'i',
                                                                                                               'hashname' => '__PATTERN1__',
                                                                                                               'lookahead' => 0,
                                                                                                               'ldelim' => '/',
                                                                                                               'line' => '228',
                                                                                                               'rdelim' => '/'
                                                                                                             }, 'Parse::RecDescent::Token' ),
                                                                                                      bless( {
                                                                                                               'line' => '228',
                                                                                                               'subrule' => 'data',
                                                                                                               'argcode' => undef,
                                                                                                               'implicit' => undef,
                                                                                                               'matchrule' => 0,
                                                                                                               'lookahead' => 0
                                                                                                             }, 'Parse::RecDescent::Subrule' ),
                                                                                                      bless( {
                                                                                                               'line' => '228',
                                                                                                               'code' => '{
	$thisparser->_rgresourcepropertyname($item[2]);
}',
                                                                                                               'hashname' => '__ACTION1__',
                                                                                                               'lookahead' => 0
                                                                                                             }, 'Parse::RecDescent::Action' )
                                                                                                    ],
                                                                                         'actcount' => 1
                                                                                       }, 'Parse::RecDescent::Production' )
                                                                              ],
                                                                   'calls' => [
                                                                                'data'
                                                                              ],
                                                                   'opcount' => 0,
                                                                   'changed' => 0,
                                                                   'vars' => '',
                                                                   'name' => 'rgresourcepropertyname'
                                                                 }, 'Parse::RecDescent::Rule' ),
                              'rtattributelist' => bless( {
                                                            'impcount' => 0,
                                                            'line' => '43',
                                                            'prods' => [
                                                                         bless( {
                                                                                  'dircount' => 0,
                                                                                  'uncommit' => undef,
                                                                                  'patcount' => 0,
                                                                                  'strcount' => 0,
                                                                                  'number' => 0,
                                                                                  'error' => undef,
                                                                                  'line' => undef,
                                                                                  'items' => [
                                                                                               bless( {
                                                                                                        'line' => '43',
                                                                                                        'subrule' => 'rtattribute',
                                                                                                        'argcode' => undef,
                                                                                                        'implicit' => undef,
                                                                                                        'matchrule' => 0,
                                                                                                        'lookahead' => 0
                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                               bless( {
                                                                                                        'line' => '43',
                                                                                                        'subrule' => 'rtattributelist',
                                                                                                        'argcode' => undef,
                                                                                                        'implicit' => undef,
                                                                                                        'matchrule' => 0,
                                                                                                        'lookahead' => 0
                                                                                                      }, 'Parse::RecDescent::Subrule' )
                                                                                             ],
                                                                                  'actcount' => 0
                                                                                }, 'Parse::RecDescent::Production' ),
                                                                         bless( {
                                                                                  'dircount' => 0,
                                                                                  'uncommit' => undef,
                                                                                  'patcount' => 0,
                                                                                  'strcount' => 0,
                                                                                  'number' => 1,
                                                                                  'error' => undef,
                                                                                  'line' => '43',
                                                                                  'items' => [],
                                                                                  'actcount' => 0
                                                                                }, 'Parse::RecDescent::Production' )
                                                                       ],
                                                            'calls' => [
                                                                         'rtattribute',
                                                                         'rtattributelist'
                                                                       ],
                                                            'opcount' => 0,
                                                            'changed' => 0,
                                                            'vars' => '',
                                                            'name' => 'rtattributelist'
                                                          }, 'Parse::RecDescent::Rule' ),
                              'rgresourceproperty' => bless( {
                                                               'impcount' => 0,
                                                               'line' => '226',
                                                               'prods' => [
                                                                            bless( {
                                                                                     'dircount' => 0,
                                                                                     'uncommit' => undef,
                                                                                     'patcount' => 0,
                                                                                     'strcount' => 0,
                                                                                     'number' => 0,
                                                                                     'error' => undef,
                                                                                     'line' => undef,
                                                                                     'items' => [
                                                                                                  bless( {
                                                                                                           'line' => '226',
                                                                                                           'subrule' => 'rgresourcepropertyname',
                                                                                                           'argcode' => undef,
                                                                                                           'implicit' => undef,
                                                                                                           'matchrule' => 0,
                                                                                                           'lookahead' => 0
                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                  bless( {
                                                                                                           'line' => '226',
                                                                                                           'subrule' => 'rgresourcepropertyattributelist',
                                                                                                           'argcode' => undef,
                                                                                                           'implicit' => undef,
                                                                                                           'matchrule' => 0,
                                                                                                           'lookahead' => 0
                                                                                                         }, 'Parse::RecDescent::Subrule' )
                                                                                                ],
                                                                                     'actcount' => 0
                                                                                   }, 'Parse::RecDescent::Production' )
                                                                          ],
                                                               'calls' => [
                                                                            'rgresourcepropertyname',
                                                                            'rgresourcepropertyattributelist'
                                                                          ],
                                                               'opcount' => 0,
                                                               'changed' => 0,
                                                               'vars' => '',
                                                               'name' => 'rgresourceproperty'
                                                             }, 'Parse::RecDescent::Rule' ),
                              'rtparams' => bless( {
                                                     'impcount' => 0,
                                                     'line' => '97',
                                                     'prods' => [
                                                                  bless( {
                                                                           'dircount' => 0,
                                                                           'uncommit' => undef,
                                                                           'patcount' => 1,
                                                                           'strcount' => 0,
                                                                           'number' => 0,
                                                                           'error' => undef,
                                                                           'line' => undef,
                                                                           'items' => [
                                                                                        bless( {
                                                                                                 'description' => '/.*Res Type param table/i',
                                                                                                 'pattern' => '.*Res Type param table',
                                                                                                 'mod' => 'i',
                                                                                                 'hashname' => '__PATTERN1__',
                                                                                                 'lookahead' => 0,
                                                                                                 'ldelim' => '/',
                                                                                                 'line' => '97',
                                                                                                 'rdelim' => '/'
                                                                                               }, 'Parse::RecDescent::Token' ),
                                                                                        bless( {
                                                                                                 'line' => '97',
                                                                                                 'subrule' => 'rtparamlist',
                                                                                                 'argcode' => undef,
                                                                                                 'implicit' => undef,
                                                                                                 'matchrule' => 0,
                                                                                                 'lookahead' => 0
                                                                                               }, 'Parse::RecDescent::Subrule' )
                                                                                      ],
                                                                           'actcount' => 0
                                                                         }, 'Parse::RecDescent::Production' )
                                                                ],
                                                     'calls' => [
                                                                  'rtparamlist'
                                                                ],
                                                     'opcount' => 0,
                                                     'changed' => 0,
                                                     'vars' => '',
                                                     'name' => 'rtparams'
                                                   }, 'Parse::RecDescent::Rule' ),
                              'rtattribute' => bless( {
                                                        'impcount' => 1,
                                                        'line' => '45',
                                                        'prods' => [
                                                                     bless( {
                                                                              'dircount' => 0,
                                                                              'uncommit' => undef,
                                                                              'patcount' => 1,
                                                                              'strcount' => 0,
                                                                              'number' => 0,
                                                                              'error' => undef,
                                                                              'line' => undef,
                                                                              'items' => [
                                                                                           bless( {
                                                                                                    'description' => '/.*Res Type /i',
                                                                                                    'pattern' => '.*Res Type ',
                                                                                                    'mod' => 'i',
                                                                                                    'hashname' => '__PATTERN1__',
                                                                                                    'lookahead' => 0,
                                                                                                    'ldelim' => '/',
                                                                                                    'line' => '45',
                                                                                                    'rdelim' => '/'
                                                                                                  }, 'Parse::RecDescent::Token' ),
                                                                                           bless( {
                                                                                                    'line' => '66',
                                                                                                    'subrule' => '_alternation_1_of_production_1_of_rule_rtattribute',
                                                                                                    'argcode' => undef,
                                                                                                    'implicit' => '/description:/i, or /base directory:/i, or /single instance:/i, or /init nodes:/i, or /failover:/i, or /proxy:/i, or /version:/i, or /API version:/i, or /installed on nodes:/i, or /packages:/i, or /system.*/i, or /upgrade.*/i',
                                                                                                    'matchrule' => 0,
                                                                                                    'lookahead' => 0
                                                                                                  }, 'Parse::RecDescent::Subrule' )
                                                                                         ],
                                                                              'actcount' => 0
                                                                            }, 'Parse::RecDescent::Production' )
                                                                   ],
                                                        'calls' => [
                                                                     '_alternation_1_of_production_1_of_rule_rtattribute'
                                                                   ],
                                                        'opcount' => 0,
                                                        'changed' => 0,
                                                        'vars' => '',
                                                        'name' => 'rtattribute'
                                                      }, 'Parse::RecDescent::Rule' ),
                              '_alternation_1_of_production_1_of_rule_rtattribute' => bless( {
                                                                                               'impcount' => 0,
                                                                                               'line' => '227',
                                                                                               'prods' => [
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 0,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => undef,
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/description:/i',
                                                                                                                                           'pattern' => 'description:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '227',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '227',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '227',
                                                                                                                                           'code' => '{
	$thisparser->_rtattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 1,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '228',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/base directory:/i',
                                                                                                                                           'pattern' => 'base directory:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '229',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '229',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '229',
                                                                                                                                           'code' => '{
	$thisparser->_rtattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 2,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '230',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/single instance:/i',
                                                                                                                                           'pattern' => 'single instance:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '231',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '231',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '231',
                                                                                                                                           'code' => '{
	$thisparser->_rtattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 3,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '232',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/init nodes:/i',
                                                                                                                                           'pattern' => 'init nodes:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '233',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '233',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '233',
                                                                                                                                           'code' => '{
	$thisparser->_rtattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 4,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '234',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/failover:/i',
                                                                                                                                           'pattern' => 'failover:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '235',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '235',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '235',
                                                                                                                                           'code' => '{
	$thisparser->_rtattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 5,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '236',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/proxy:/i',
                                                                                                                                           'pattern' => 'proxy:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '237',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '237',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '237',
                                                                                                                                           'code' => '{
	$thisparser->_rtattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 6,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '238',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/version:/i',
                                                                                                                                           'pattern' => 'version:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '239',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '239',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '239',
                                                                                                                                           'code' => '{
	$thisparser->_rtattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 7,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '240',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/API version:/i',
                                                                                                                                           'pattern' => 'API version:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '241',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '241',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '241',
                                                                                                                                           'code' => '{
	$thisparser->_rtattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 8,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '242',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/installed on nodes:/i',
                                                                                                                                           'pattern' => 'installed on nodes:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '243',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '243',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '243',
                                                                                                                                           'code' => '{
	$thisparser->_rtlistattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 9,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '244',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/packages:/i',
                                                                                                                                           'pattern' => 'packages:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '245',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '245',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '245',
                                                                                                                                           'code' => '{
	$thisparser->_rtattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 10,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '246',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/system.*/i',
                                                                                                                                           'pattern' => 'system.*',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '247',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' )
                                                                                                                                ],
                                                                                                                     'actcount' => 0
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 11,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '247',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/upgrade.*/i',
                                                                                                                                           'pattern' => 'upgrade.*',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '248',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' )
                                                                                                                                ],
                                                                                                                     'actcount' => 0
                                                                                                                   }, 'Parse::RecDescent::Production' )
                                                                                                          ],
                                                                                               'calls' => [
                                                                                                            'data'
                                                                                                          ],
                                                                                               'opcount' => 0,
                                                                                               'changed' => 0,
                                                                                               'vars' => '',
                                                                                               'name' => '_alternation_1_of_production_1_of_rule_rtattribute'
                                                                                             }, 'Parse::RecDescent::Rule' ),
                              'rtdef' => bless( {
                                                  'impcount' => 0,
                                                  'line' => '36',
                                                  'prods' => [
                                                               bless( {
                                                                        'dircount' => 0,
                                                                        'uncommit' => undef,
                                                                        'patcount' => 0,
                                                                        'strcount' => 0,
                                                                        'number' => 0,
                                                                        'error' => undef,
                                                                        'line' => undef,
                                                                        'items' => [
                                                                                     bless( {
                                                                                              'line' => '36',
                                                                                              'subrule' => 'rtname',
                                                                                              'argcode' => undef,
                                                                                              'implicit' => undef,
                                                                                              'matchrule' => 0,
                                                                                              'lookahead' => 0
                                                                                            }, 'Parse::RecDescent::Subrule' ),
                                                                                     bless( {
                                                                                              'line' => '36',
                                                                                              'subrule' => 'rtattributelist',
                                                                                              'argcode' => undef,
                                                                                              'implicit' => undef,
                                                                                              'matchrule' => 0,
                                                                                              'lookahead' => 0
                                                                                            }, 'Parse::RecDescent::Subrule' ),
                                                                                     bless( {
                                                                                              'line' => '36',
                                                                                              'subrule' => 'rtmethods',
                                                                                              'argcode' => undef,
                                                                                              'implicit' => undef,
                                                                                              'matchrule' => 0,
                                                                                              'lookahead' => 0
                                                                                            }, 'Parse::RecDescent::Subrule' ),
                                                                                     bless( {
                                                                                              'line' => '36',
                                                                                              'subrule' => 'rtparams',
                                                                                              'argcode' => undef,
                                                                                              'implicit' => undef,
                                                                                              'matchrule' => 0,
                                                                                              'lookahead' => 0
                                                                                            }, 'Parse::RecDescent::Subrule' )
                                                                                   ],
                                                                        'actcount' => 0
                                                                      }, 'Parse::RecDescent::Production' )
                                                             ],
                                                  'calls' => [
                                                               'rtname',
                                                               'rtattributelist',
                                                               'rtmethods',
                                                               'rtparams'
                                                             ],
                                                  'opcount' => 0,
                                                  'changed' => 0,
                                                  'vars' => '',
                                                  'name' => 'rtdef'
                                                }, 'Parse::RecDescent::Rule' ),
                              'rtmethodlist' => bless( {
                                                         'impcount' => 0,
                                                         'line' => '70',
                                                         'prods' => [
                                                                      bless( {
                                                                               'dircount' => 0,
                                                                               'uncommit' => undef,
                                                                               'patcount' => 0,
                                                                               'strcount' => 0,
                                                                               'number' => 0,
                                                                               'error' => undef,
                                                                               'line' => undef,
                                                                               'items' => [
                                                                                            bless( {
                                                                                                     'line' => '70',
                                                                                                     'subrule' => 'rtmethod',
                                                                                                     'argcode' => undef,
                                                                                                     'implicit' => undef,
                                                                                                     'matchrule' => 0,
                                                                                                     'lookahead' => 0
                                                                                                   }, 'Parse::RecDescent::Subrule' ),
                                                                                            bless( {
                                                                                                     'line' => '70',
                                                                                                     'subrule' => 'rtmethodlist',
                                                                                                     'argcode' => undef,
                                                                                                     'implicit' => undef,
                                                                                                     'matchrule' => 0,
                                                                                                     'lookahead' => 0
                                                                                                   }, 'Parse::RecDescent::Subrule' )
                                                                                          ],
                                                                               'actcount' => 0
                                                                             }, 'Parse::RecDescent::Production' ),
                                                                      bless( {
                                                                               'dircount' => 0,
                                                                               'uncommit' => undef,
                                                                               'patcount' => 0,
                                                                               'strcount' => 0,
                                                                               'number' => 1,
                                                                               'error' => undef,
                                                                               'line' => '70',
                                                                               'items' => [],
                                                                               'actcount' => 0
                                                                             }, 'Parse::RecDescent::Production' )
                                                                    ],
                                                         'calls' => [
                                                                      'rtmethod',
                                                                      'rtmethodlist'
                                                                    ],
                                                         'opcount' => 0,
                                                         'changed' => 0,
                                                         'vars' => '',
                                                         'name' => 'rtmethodlist'
                                                       }, 'Parse::RecDescent::Rule' ),
                              'rgresource' => bless( {
                                                       'impcount' => 0,
                                                       'line' => '192',
                                                       'prods' => [
                                                                    bless( {
                                                                             'dircount' => 0,
                                                                             'uncommit' => undef,
                                                                             'patcount' => 0,
                                                                             'strcount' => 0,
                                                                             'number' => 0,
                                                                             'error' => undef,
                                                                             'line' => undef,
                                                                             'items' => [
                                                                                          bless( {
                                                                                                   'line' => '192',
                                                                                                   'subrule' => 'rgresourcename',
                                                                                                   'argcode' => undef,
                                                                                                   'implicit' => undef,
                                                                                                   'matchrule' => 0,
                                                                                                   'lookahead' => 0
                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                          bless( {
                                                                                                   'line' => '192',
                                                                                                   'subrule' => 'rgresourceattributelist',
                                                                                                   'argcode' => undef,
                                                                                                   'implicit' => undef,
                                                                                                   'matchrule' => 0,
                                                                                                   'lookahead' => 0
                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                          bless( {
                                                                                                   'line' => '192',
                                                                                                   'subrule' => 'rgresourcepropertylist',
                                                                                                   'argcode' => undef,
                                                                                                   'implicit' => undef,
                                                                                                   'matchrule' => 0,
                                                                                                   'lookahead' => 0
                                                                                                 }, 'Parse::RecDescent::Subrule' )
                                                                                        ],
                                                                             'actcount' => 0
                                                                           }, 'Parse::RecDescent::Production' )
                                                                  ],
                                                       'calls' => [
                                                                    'rgresourcename',
                                                                    'rgresourceattributelist',
                                                                    'rgresourcepropertylist'
                                                                  ],
                                                       'opcount' => 0,
                                                       'changed' => 0,
                                                       'vars' => '',
                                                       'name' => 'rgresource'
                                                     }, 'Parse::RecDescent::Rule' ),
                              'rgname' => bless( {
                                                   'impcount' => 0,
                                                   'line' => '139',
                                                   'prods' => [
                                                                bless( {
                                                                         'dircount' => 0,
                                                                         'uncommit' => undef,
                                                                         'patcount' => 1,
                                                                         'strcount' => 0,
                                                                         'number' => 0,
                                                                         'error' => undef,
                                                                         'line' => undef,
                                                                         'items' => [
                                                                                      bless( {
                                                                                               'description' => '/Res Group name:/i',
                                                                                               'pattern' => 'Res Group name:',
                                                                                               'mod' => 'i',
                                                                                               'hashname' => '__PATTERN1__',
                                                                                               'lookahead' => 0,
                                                                                               'ldelim' => '/',
                                                                                               'line' => '139',
                                                                                               'rdelim' => '/'
                                                                                             }, 'Parse::RecDescent::Token' ),
                                                                                      bless( {
                                                                                               'line' => '139',
                                                                                               'subrule' => 'data',
                                                                                               'argcode' => undef,
                                                                                               'implicit' => undef,
                                                                                               'matchrule' => 0,
                                                                                               'lookahead' => 0
                                                                                             }, 'Parse::RecDescent::Subrule' ),
                                                                                      bless( {
                                                                                               'line' => '139',
                                                                                               'code' => '{
	$thisparser->debug_ppc($item[1], $item[2]);
	$thisparser->_rgname($item[2]);
}',
                                                                                               'hashname' => '__ACTION1__',
                                                                                               'lookahead' => 0
                                                                                             }, 'Parse::RecDescent::Action' )
                                                                                    ],
                                                                         'actcount' => 1
                                                                       }, 'Parse::RecDescent::Production' )
                                                              ],
                                                   'calls' => [
                                                                'data'
                                                              ],
                                                   'opcount' => 0,
                                                   'changed' => 0,
                                                   'vars' => '',
                                                   'name' => 'rgname'
                                                 }, 'Parse::RecDescent::Rule' ),
                              '_alternation_1_of_production_1_of_rule_rtmethod' => bless( {
                                                                                            'impcount' => 0,
                                                                                            'line' => '225',
                                                                                            'prods' => [
                                                                                                         bless( {
                                                                                                                  'dircount' => 0,
                                                                                                                  'uncommit' => undef,
                                                                                                                  'patcount' => 1,
                                                                                                                  'strcount' => 0,
                                                                                                                  'number' => 0,
                                                                                                                  'error' => undef,
                                                                                                                  'line' => undef,
                                                                                                                  'items' => [
                                                                                                                               bless( {
                                                                                                                                        'description' => '/START:/i',
                                                                                                                                        'pattern' => 'START:',
                                                                                                                                        'mod' => 'i',
                                                                                                                                        'hashname' => '__PATTERN1__',
                                                                                                                                        'lookahead' => 0,
                                                                                                                                        'ldelim' => '/',
                                                                                                                                        'line' => '225',
                                                                                                                                        'rdelim' => '/'
                                                                                                                                      }, 'Parse::RecDescent::Token' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '225',
                                                                                                                                        'subrule' => 'data',
                                                                                                                                        'argcode' => undef,
                                                                                                                                        'implicit' => undef,
                                                                                                                                        'matchrule' => 0,
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '225',
                                                                                                                                        'code' => '{
	$thisparser->_rtmethod(@item[1,2]); }',
                                                                                                                                        'hashname' => '__ACTION1__',
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Action' )
                                                                                                                             ],
                                                                                                                  'actcount' => 1
                                                                                                                }, 'Parse::RecDescent::Production' ),
                                                                                                         bless( {
                                                                                                                  'dircount' => 0,
                                                                                                                  'uncommit' => undef,
                                                                                                                  'patcount' => 1,
                                                                                                                  'strcount' => 0,
                                                                                                                  'number' => 1,
                                                                                                                  'error' => undef,
                                                                                                                  'line' => '226',
                                                                                                                  'items' => [
                                                                                                                               bless( {
                                                                                                                                        'description' => '/STOP:/i',
                                                                                                                                        'pattern' => 'STOP:',
                                                                                                                                        'mod' => 'i',
                                                                                                                                        'hashname' => '__PATTERN1__',
                                                                                                                                        'lookahead' => 0,
                                                                                                                                        'ldelim' => '/',
                                                                                                                                        'line' => '227',
                                                                                                                                        'rdelim' => '/'
                                                                                                                                      }, 'Parse::RecDescent::Token' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '227',
                                                                                                                                        'subrule' => 'data',
                                                                                                                                        'argcode' => undef,
                                                                                                                                        'implicit' => undef,
                                                                                                                                        'matchrule' => 0,
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '227',
                                                                                                                                        'code' => '{
	$thisparser->_rtmethod(@item[1,2]); }',
                                                                                                                                        'hashname' => '__ACTION1__',
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Action' )
                                                                                                                             ],
                                                                                                                  'actcount' => 1
                                                                                                                }, 'Parse::RecDescent::Production' ),
                                                                                                         bless( {
                                                                                                                  'dircount' => 0,
                                                                                                                  'uncommit' => undef,
                                                                                                                  'patcount' => 1,
                                                                                                                  'strcount' => 0,
                                                                                                                  'number' => 2,
                                                                                                                  'error' => undef,
                                                                                                                  'line' => '228',
                                                                                                                  'items' => [
                                                                                                                               bless( {
                                                                                                                                        'description' => '/VALIDATE:/i',
                                                                                                                                        'pattern' => 'VALIDATE:',
                                                                                                                                        'mod' => 'i',
                                                                                                                                        'hashname' => '__PATTERN1__',
                                                                                                                                        'lookahead' => 0,
                                                                                                                                        'ldelim' => '/',
                                                                                                                                        'line' => '229',
                                                                                                                                        'rdelim' => '/'
                                                                                                                                      }, 'Parse::RecDescent::Token' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '229',
                                                                                                                                        'subrule' => 'data',
                                                                                                                                        'argcode' => undef,
                                                                                                                                        'implicit' => undef,
                                                                                                                                        'matchrule' => 0,
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '229',
                                                                                                                                        'code' => '{
	$thisparser->_rtmethod(@item[1,2]); }',
                                                                                                                                        'hashname' => '__ACTION1__',
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Action' )
                                                                                                                             ],
                                                                                                                  'actcount' => 1
                                                                                                                }, 'Parse::RecDescent::Production' ),
                                                                                                         bless( {
                                                                                                                  'dircount' => 0,
                                                                                                                  'uncommit' => undef,
                                                                                                                  'patcount' => 1,
                                                                                                                  'strcount' => 0,
                                                                                                                  'number' => 3,
                                                                                                                  'error' => undef,
                                                                                                                  'line' => '230',
                                                                                                                  'items' => [
                                                                                                                               bless( {
                                                                                                                                        'description' => '/UPDATE:/i',
                                                                                                                                        'pattern' => 'UPDATE:',
                                                                                                                                        'mod' => 'i',
                                                                                                                                        'hashname' => '__PATTERN1__',
                                                                                                                                        'lookahead' => 0,
                                                                                                                                        'ldelim' => '/',
                                                                                                                                        'line' => '231',
                                                                                                                                        'rdelim' => '/'
                                                                                                                                      }, 'Parse::RecDescent::Token' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '231',
                                                                                                                                        'subrule' => 'data',
                                                                                                                                        'argcode' => undef,
                                                                                                                                        'implicit' => undef,
                                                                                                                                        'matchrule' => 0,
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '231',
                                                                                                                                        'code' => '{
	$thisparser->_rtmethod(@item[1,2]); }',
                                                                                                                                        'hashname' => '__ACTION1__',
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Action' )
                                                                                                                             ],
                                                                                                                  'actcount' => 1
                                                                                                                }, 'Parse::RecDescent::Production' ),
                                                                                                         bless( {
                                                                                                                  'dircount' => 0,
                                                                                                                  'uncommit' => undef,
                                                                                                                  'patcount' => 1,
                                                                                                                  'strcount' => 0,
                                                                                                                  'number' => 4,
                                                                                                                  'error' => undef,
                                                                                                                  'line' => '232',
                                                                                                                  'items' => [
                                                                                                                               bless( {
                                                                                                                                        'description' => '/INIT:/i',
                                                                                                                                        'pattern' => 'INIT:',
                                                                                                                                        'mod' => 'i',
                                                                                                                                        'hashname' => '__PATTERN1__',
                                                                                                                                        'lookahead' => 0,
                                                                                                                                        'ldelim' => '/',
                                                                                                                                        'line' => '233',
                                                                                                                                        'rdelim' => '/'
                                                                                                                                      }, 'Parse::RecDescent::Token' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '233',
                                                                                                                                        'subrule' => 'data',
                                                                                                                                        'argcode' => undef,
                                                                                                                                        'implicit' => undef,
                                                                                                                                        'matchrule' => 0,
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '233',
                                                                                                                                        'code' => '{
	$thisparser->_rtmethod(@item[1,2]); }',
                                                                                                                                        'hashname' => '__ACTION1__',
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Action' )
                                                                                                                             ],
                                                                                                                  'actcount' => 1
                                                                                                                }, 'Parse::RecDescent::Production' ),
                                                                                                         bless( {
                                                                                                                  'dircount' => 0,
                                                                                                                  'uncommit' => undef,
                                                                                                                  'patcount' => 1,
                                                                                                                  'strcount' => 0,
                                                                                                                  'number' => 5,
                                                                                                                  'error' => undef,
                                                                                                                  'line' => '234',
                                                                                                                  'items' => [
                                                                                                                               bless( {
                                                                                                                                        'description' => '/FINI:/i',
                                                                                                                                        'pattern' => 'FINI:',
                                                                                                                                        'mod' => 'i',
                                                                                                                                        'hashname' => '__PATTERN1__',
                                                                                                                                        'lookahead' => 0,
                                                                                                                                        'ldelim' => '/',
                                                                                                                                        'line' => '235',
                                                                                                                                        'rdelim' => '/'
                                                                                                                                      }, 'Parse::RecDescent::Token' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '235',
                                                                                                                                        'subrule' => 'data',
                                                                                                                                        'argcode' => undef,
                                                                                                                                        'implicit' => undef,
                                                                                                                                        'matchrule' => 0,
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '235',
                                                                                                                                        'code' => '{
	$thisparser->_rtmethod(@item[1,2]); }',
                                                                                                                                        'hashname' => '__ACTION1__',
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Action' )
                                                                                                                             ],
                                                                                                                  'actcount' => 1
                                                                                                                }, 'Parse::RecDescent::Production' ),
                                                                                                         bless( {
                                                                                                                  'dircount' => 0,
                                                                                                                  'uncommit' => undef,
                                                                                                                  'patcount' => 1,
                                                                                                                  'strcount' => 0,
                                                                                                                  'number' => 6,
                                                                                                                  'error' => undef,
                                                                                                                  'line' => '236',
                                                                                                                  'items' => [
                                                                                                                               bless( {
                                                                                                                                        'description' => '/BOOT:/i',
                                                                                                                                        'pattern' => 'BOOT:',
                                                                                                                                        'mod' => 'i',
                                                                                                                                        'hashname' => '__PATTERN1__',
                                                                                                                                        'lookahead' => 0,
                                                                                                                                        'ldelim' => '/',
                                                                                                                                        'line' => '237',
                                                                                                                                        'rdelim' => '/'
                                                                                                                                      }, 'Parse::RecDescent::Token' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '237',
                                                                                                                                        'subrule' => 'data',
                                                                                                                                        'argcode' => undef,
                                                                                                                                        'implicit' => undef,
                                                                                                                                        'matchrule' => 0,
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '237',
                                                                                                                                        'code' => '{
	$thisparser->_rtmethod(@item[1,2]); }',
                                                                                                                                        'hashname' => '__ACTION1__',
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Action' )
                                                                                                                             ],
                                                                                                                  'actcount' => 1
                                                                                                                }, 'Parse::RecDescent::Production' ),
                                                                                                         bless( {
                                                                                                                  'dircount' => 0,
                                                                                                                  'uncommit' => undef,
                                                                                                                  'patcount' => 1,
                                                                                                                  'strcount' => 0,
                                                                                                                  'number' => 7,
                                                                                                                  'error' => undef,
                                                                                                                  'line' => '238',
                                                                                                                  'items' => [
                                                                                                                               bless( {
                                                                                                                                        'description' => '/MONITOR START:/i',
                                                                                                                                        'pattern' => 'MONITOR START:',
                                                                                                                                        'mod' => 'i',
                                                                                                                                        'hashname' => '__PATTERN1__',
                                                                                                                                        'lookahead' => 0,
                                                                                                                                        'ldelim' => '/',
                                                                                                                                        'line' => '239',
                                                                                                                                        'rdelim' => '/'
                                                                                                                                      }, 'Parse::RecDescent::Token' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '239',
                                                                                                                                        'subrule' => 'data',
                                                                                                                                        'argcode' => undef,
                                                                                                                                        'implicit' => undef,
                                                                                                                                        'matchrule' => 0,
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '239',
                                                                                                                                        'code' => '{
	$thisparser->_rtmethod(@item[1,2]); }',
                                                                                                                                        'hashname' => '__ACTION1__',
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Action' )
                                                                                                                             ],
                                                                                                                  'actcount' => 1
                                                                                                                }, 'Parse::RecDescent::Production' ),
                                                                                                         bless( {
                                                                                                                  'dircount' => 0,
                                                                                                                  'uncommit' => undef,
                                                                                                                  'patcount' => 1,
                                                                                                                  'strcount' => 0,
                                                                                                                  'number' => 8,
                                                                                                                  'error' => undef,
                                                                                                                  'line' => '240',
                                                                                                                  'items' => [
                                                                                                                               bless( {
                                                                                                                                        'description' => '/MONITOR STOP:/i',
                                                                                                                                        'pattern' => 'MONITOR STOP:',
                                                                                                                                        'mod' => 'i',
                                                                                                                                        'hashname' => '__PATTERN1__',
                                                                                                                                        'lookahead' => 0,
                                                                                                                                        'ldelim' => '/',
                                                                                                                                        'line' => '241',
                                                                                                                                        'rdelim' => '/'
                                                                                                                                      }, 'Parse::RecDescent::Token' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '241',
                                                                                                                                        'subrule' => 'data',
                                                                                                                                        'argcode' => undef,
                                                                                                                                        'implicit' => undef,
                                                                                                                                        'matchrule' => 0,
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '241',
                                                                                                                                        'code' => '{
	$thisparser->_rtmethod(@item[1,2]); }',
                                                                                                                                        'hashname' => '__ACTION1__',
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Action' )
                                                                                                                             ],
                                                                                                                  'actcount' => 1
                                                                                                                }, 'Parse::RecDescent::Production' ),
                                                                                                         bless( {
                                                                                                                  'dircount' => 0,
                                                                                                                  'uncommit' => undef,
                                                                                                                  'patcount' => 1,
                                                                                                                  'strcount' => 0,
                                                                                                                  'number' => 9,
                                                                                                                  'error' => undef,
                                                                                                                  'line' => '242',
                                                                                                                  'items' => [
                                                                                                                               bless( {
                                                                                                                                        'description' => '/MONITOR CHECK:/i',
                                                                                                                                        'pattern' => 'MONITOR CHECK:',
                                                                                                                                        'mod' => 'i',
                                                                                                                                        'hashname' => '__PATTERN1__',
                                                                                                                                        'lookahead' => 0,
                                                                                                                                        'ldelim' => '/',
                                                                                                                                        'line' => '243',
                                                                                                                                        'rdelim' => '/'
                                                                                                                                      }, 'Parse::RecDescent::Token' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '243',
                                                                                                                                        'subrule' => 'data',
                                                                                                                                        'argcode' => undef,
                                                                                                                                        'implicit' => undef,
                                                                                                                                        'matchrule' => 0,
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '243',
                                                                                                                                        'code' => '{
	$thisparser->_rtmethod(@item[1,2]); }',
                                                                                                                                        'hashname' => '__ACTION1__',
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Action' )
                                                                                                                             ],
                                                                                                                  'actcount' => 1
                                                                                                                }, 'Parse::RecDescent::Production' ),
                                                                                                         bless( {
                                                                                                                  'dircount' => 0,
                                                                                                                  'uncommit' => undef,
                                                                                                                  'patcount' => 1,
                                                                                                                  'strcount' => 0,
                                                                                                                  'number' => 10,
                                                                                                                  'error' => undef,
                                                                                                                  'line' => '244',
                                                                                                                  'items' => [
                                                                                                                               bless( {
                                                                                                                                        'description' => '/PRENET START:/i',
                                                                                                                                        'pattern' => 'PRENET START:',
                                                                                                                                        'mod' => 'i',
                                                                                                                                        'hashname' => '__PATTERN1__',
                                                                                                                                        'lookahead' => 0,
                                                                                                                                        'ldelim' => '/',
                                                                                                                                        'line' => '245',
                                                                                                                                        'rdelim' => '/'
                                                                                                                                      }, 'Parse::RecDescent::Token' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '245',
                                                                                                                                        'subrule' => 'data',
                                                                                                                                        'argcode' => undef,
                                                                                                                                        'implicit' => undef,
                                                                                                                                        'matchrule' => 0,
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '245',
                                                                                                                                        'code' => '{
	$thisparser->_rtmethod(@item[1,2]); }',
                                                                                                                                        'hashname' => '__ACTION1__',
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Action' )
                                                                                                                             ],
                                                                                                                  'actcount' => 1
                                                                                                                }, 'Parse::RecDescent::Production' ),
                                                                                                         bless( {
                                                                                                                  'dircount' => 0,
                                                                                                                  'uncommit' => undef,
                                                                                                                  'patcount' => 1,
                                                                                                                  'strcount' => 0,
                                                                                                                  'number' => 11,
                                                                                                                  'error' => undef,
                                                                                                                  'line' => '246',
                                                                                                                  'items' => [
                                                                                                                               bless( {
                                                                                                                                        'description' => '/POSTNET STOP:/i',
                                                                                                                                        'pattern' => 'POSTNET STOP:',
                                                                                                                                        'mod' => 'i',
                                                                                                                                        'hashname' => '__PATTERN1__',
                                                                                                                                        'lookahead' => 0,
                                                                                                                                        'ldelim' => '/',
                                                                                                                                        'line' => '247',
                                                                                                                                        'rdelim' => '/'
                                                                                                                                      }, 'Parse::RecDescent::Token' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '247',
                                                                                                                                        'subrule' => 'data',
                                                                                                                                        'argcode' => undef,
                                                                                                                                        'implicit' => undef,
                                                                                                                                        'matchrule' => 0,
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                                                               bless( {
                                                                                                                                        'line' => '247',
                                                                                                                                        'code' => '{
	$thisparser->_rtmethod(@item[1,2]); }',
                                                                                                                                        'hashname' => '__ACTION1__',
                                                                                                                                        'lookahead' => 0
                                                                                                                                      }, 'Parse::RecDescent::Action' )
                                                                                                                             ],
                                                                                                                  'actcount' => 1
                                                                                                                }, 'Parse::RecDescent::Production' )
                                                                                                       ],
                                                                                            'calls' => [
                                                                                                         'data'
                                                                                                       ],
                                                                                            'opcount' => 0,
                                                                                            'changed' => 0,
                                                                                            'vars' => '',
                                                                                            'name' => '_alternation_1_of_production_1_of_rule_rtmethod'
                                                                                          }, 'Parse::RecDescent::Rule' ),
                              'rtname' => bless( {
                                                   'impcount' => 0,
                                                   'line' => '38',
                                                   'prods' => [
                                                                bless( {
                                                                         'dircount' => 0,
                                                                         'uncommit' => undef,
                                                                         'patcount' => 1,
                                                                         'strcount' => 0,
                                                                         'number' => 0,
                                                                         'error' => undef,
                                                                         'line' => undef,
                                                                         'items' => [
                                                                                      bless( {
                                                                                               'description' => '/Res Type name:/i',
                                                                                               'pattern' => 'Res Type name:',
                                                                                               'mod' => 'i',
                                                                                               'hashname' => '__PATTERN1__',
                                                                                               'lookahead' => 0,
                                                                                               'ldelim' => '/',
                                                                                               'line' => '38',
                                                                                               'rdelim' => '/'
                                                                                             }, 'Parse::RecDescent::Token' ),
                                                                                      bless( {
                                                                                               'line' => '38',
                                                                                               'subrule' => 'data',
                                                                                               'argcode' => undef,
                                                                                               'implicit' => undef,
                                                                                               'matchrule' => 0,
                                                                                               'lookahead' => 0
                                                                                             }, 'Parse::RecDescent::Subrule' ),
                                                                                      bless( {
                                                                                               'line' => '38',
                                                                                               'code' => '{
	$thisparser->debug_ppc($item[1], $item[2]);
	$thisparser->_rtname($item[2]);
}',
                                                                                               'hashname' => '__ACTION1__',
                                                                                               'lookahead' => 0
                                                                                             }, 'Parse::RecDescent::Action' )
                                                                                    ],
                                                                         'actcount' => 1
                                                                       }, 'Parse::RecDescent::Production' )
                                                              ],
                                                   'calls' => [
                                                                'data'
                                                              ],
                                                   'opcount' => 0,
                                                   'changed' => 0,
                                                   'vars' => '',
                                                   'name' => 'rtname'
                                                 }, 'Parse::RecDescent::Rule' ),
                              'rgdef' => bless( {
                                                  'impcount' => 0,
                                                  'line' => '137',
                                                  'prods' => [
                                                               bless( {
                                                                        'dircount' => 0,
                                                                        'uncommit' => undef,
                                                                        'patcount' => 0,
                                                                        'strcount' => 0,
                                                                        'number' => 0,
                                                                        'error' => undef,
                                                                        'line' => undef,
                                                                        'items' => [
                                                                                     bless( {
                                                                                              'line' => '137',
                                                                                              'subrule' => 'rgname',
                                                                                              'argcode' => undef,
                                                                                              'implicit' => undef,
                                                                                              'matchrule' => 0,
                                                                                              'lookahead' => 0
                                                                                            }, 'Parse::RecDescent::Subrule' ),
                                                                                     bless( {
                                                                                              'line' => '137',
                                                                                              'subrule' => 'rgattributelist',
                                                                                              'argcode' => undef,
                                                                                              'implicit' => undef,
                                                                                              'matchrule' => 0,
                                                                                              'lookahead' => 0
                                                                                            }, 'Parse::RecDescent::Subrule' ),
                                                                                     bless( {
                                                                                              'line' => '137',
                                                                                              'subrule' => 'rgresourcelist',
                                                                                              'argcode' => undef,
                                                                                              'implicit' => undef,
                                                                                              'matchrule' => 0,
                                                                                              'lookahead' => 0
                                                                                            }, 'Parse::RecDescent::Subrule' )
                                                                                   ],
                                                                        'actcount' => 0
                                                                      }, 'Parse::RecDescent::Production' )
                                                             ],
                                                  'calls' => [
                                                               'rgname',
                                                               'rgattributelist',
                                                               'rgresourcelist'
                                                             ],
                                                  'opcount' => 0,
                                                  'changed' => 0,
                                                  'vars' => '',
                                                  'name' => 'rgdef'
                                                }, 'Parse::RecDescent::Rule' ),
                              '_alternation_1_of_production_1_of_rule_rgresourceattribute' => bless( {
                                                                                                       'impcount' => 0,
                                                                                                       'line' => '227',
                                                                                                       'prods' => [
                                                                                                                    bless( {
                                                                                                                             'dircount' => 0,
                                                                                                                             'uncommit' => undef,
                                                                                                                             'patcount' => 1,
                                                                                                                             'strcount' => 0,
                                                                                                                             'number' => 0,
                                                                                                                             'error' => undef,
                                                                                                                             'line' => undef,
                                                                                                                             'items' => [
                                                                                                                                          bless( {
                                                                                                                                                   'description' => '/R_description:/i',
                                                                                                                                                   'pattern' => 'R_description:',
                                                                                                                                                   'mod' => 'i',
                                                                                                                                                   'hashname' => '__PATTERN1__',
                                                                                                                                                   'lookahead' => 0,
                                                                                                                                                   'ldelim' => '/',
                                                                                                                                                   'line' => '227',
                                                                                                                                                   'rdelim' => '/'
                                                                                                                                                 }, 'Parse::RecDescent::Token' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '227',
                                                                                                                                                   'subrule' => 'data',
                                                                                                                                                   'argcode' => undef,
                                                                                                                                                   'implicit' => undef,
                                                                                                                                                   'matchrule' => 0,
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '228',
                                                                                                                                                   'code' => '{ $thisparser->_rgresourceattribute(@item[1,2]); }',
                                                                                                                                                   'hashname' => '__ACTION1__',
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Action' )
                                                                                                                                        ],
                                                                                                                             'actcount' => 1
                                                                                                                           }, 'Parse::RecDescent::Production' ),
                                                                                                                    bless( {
                                                                                                                             'dircount' => 0,
                                                                                                                             'uncommit' => undef,
                                                                                                                             'patcount' => 1,
                                                                                                                             'strcount' => 0,
                                                                                                                             'number' => 1,
                                                                                                                             'error' => undef,
                                                                                                                             'line' => '228',
                                                                                                                             'items' => [
                                                                                                                                          bless( {
                                                                                                                                                   'description' => '/type version:/i',
                                                                                                                                                   'pattern' => 'type version:',
                                                                                                                                                   'mod' => 'i',
                                                                                                                                                   'hashname' => '__PATTERN1__',
                                                                                                                                                   'lookahead' => 0,
                                                                                                                                                   'ldelim' => '/',
                                                                                                                                                   'line' => '229',
                                                                                                                                                   'rdelim' => '/'
                                                                                                                                                 }, 'Parse::RecDescent::Token' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '229',
                                                                                                                                                   'subrule' => 'data',
                                                                                                                                                   'argcode' => undef,
                                                                                                                                                   'implicit' => undef,
                                                                                                                                                   'matchrule' => 0,
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '230',
                                                                                                                                                   'code' => '{ $thisparser->_rgresourceattribute(@item[1,2]); }',
                                                                                                                                                   'hashname' => '__ACTION1__',
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Action' )
                                                                                                                                        ],
                                                                                                                             'actcount' => 1
                                                                                                                           }, 'Parse::RecDescent::Production' ),
                                                                                                                    bless( {
                                                                                                                             'dircount' => 0,
                                                                                                                             'uncommit' => undef,
                                                                                                                             'patcount' => 1,
                                                                                                                             'strcount' => 0,
                                                                                                                             'number' => 2,
                                                                                                                             'error' => undef,
                                                                                                                             'line' => '230',
                                                                                                                             'items' => [
                                                                                                                                          bless( {
                                                                                                                                                   'description' => '/resource group name:/i',
                                                                                                                                                   'pattern' => 'resource group name:',
                                                                                                                                                   'mod' => 'i',
                                                                                                                                                   'hashname' => '__PATTERN1__',
                                                                                                                                                   'lookahead' => 0,
                                                                                                                                                   'ldelim' => '/',
                                                                                                                                                   'line' => '231',
                                                                                                                                                   'rdelim' => '/'
                                                                                                                                                 }, 'Parse::RecDescent::Token' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '231',
                                                                                                                                                   'subrule' => 'data',
                                                                                                                                                   'argcode' => undef,
                                                                                                                                                   'implicit' => undef,
                                                                                                                                                   'matchrule' => 0,
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '232',
                                                                                                                                                   'code' => '{ $thisparser->_rgresourceattribute(@item[1,2]); }',
                                                                                                                                                   'hashname' => '__ACTION1__',
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Action' )
                                                                                                                                        ],
                                                                                                                             'actcount' => 1
                                                                                                                           }, 'Parse::RecDescent::Production' ),
                                                                                                                    bless( {
                                                                                                                             'dircount' => 0,
                                                                                                                             'uncommit' => undef,
                                                                                                                             'patcount' => 1,
                                                                                                                             'strcount' => 0,
                                                                                                                             'number' => 3,
                                                                                                                             'error' => undef,
                                                                                                                             'line' => '232',
                                                                                                                             'items' => [
                                                                                                                                          bless( {
                                                                                                                                                   'description' => '/enabled:/i',
                                                                                                                                                   'pattern' => 'enabled:',
                                                                                                                                                   'mod' => 'i',
                                                                                                                                                   'hashname' => '__PATTERN1__',
                                                                                                                                                   'lookahead' => 0,
                                                                                                                                                   'ldelim' => '/',
                                                                                                                                                   'line' => '233',
                                                                                                                                                   'rdelim' => '/'
                                                                                                                                                 }, 'Parse::RecDescent::Token' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '233',
                                                                                                                                                   'subrule' => 'data',
                                                                                                                                                   'argcode' => undef,
                                                                                                                                                   'implicit' => undef,
                                                                                                                                                   'matchrule' => 0,
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '234',
                                                                                                                                                   'code' => '{ $thisparser->_rgresourceattribute(@item[1,2]); }',
                                                                                                                                                   'hashname' => '__ACTION1__',
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Action' )
                                                                                                                                        ],
                                                                                                                             'actcount' => 1
                                                                                                                           }, 'Parse::RecDescent::Production' ),
                                                                                                                    bless( {
                                                                                                                             'dircount' => 0,
                                                                                                                             'uncommit' => undef,
                                                                                                                             'patcount' => 1,
                                                                                                                             'strcount' => 0,
                                                                                                                             'number' => 4,
                                                                                                                             'error' => undef,
                                                                                                                             'line' => '234',
                                                                                                                             'items' => [
                                                                                                                                          bless( {
                                                                                                                                                   'description' => '/monitor enabled:/i',
                                                                                                                                                   'pattern' => 'monitor enabled:',
                                                                                                                                                   'mod' => 'i',
                                                                                                                                                   'hashname' => '__PATTERN1__',
                                                                                                                                                   'lookahead' => 0,
                                                                                                                                                   'ldelim' => '/',
                                                                                                                                                   'line' => '235',
                                                                                                                                                   'rdelim' => '/'
                                                                                                                                                 }, 'Parse::RecDescent::Token' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '235',
                                                                                                                                                   'subrule' => 'data',
                                                                                                                                                   'argcode' => undef,
                                                                                                                                                   'implicit' => undef,
                                                                                                                                                   'matchrule' => 0,
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '236',
                                                                                                                                                   'code' => '{ $thisparser->_rgresourceattribute(@item[1,2]); }',
                                                                                                                                                   'hashname' => '__ACTION1__',
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Action' )
                                                                                                                                        ],
                                                                                                                             'actcount' => 1
                                                                                                                           }, 'Parse::RecDescent::Production' ),
                                                                                                                    bless( {
                                                                                                                             'dircount' => 0,
                                                                                                                             'uncommit' => undef,
                                                                                                                             'patcount' => 1,
                                                                                                                             'strcount' => 0,
                                                                                                                             'number' => 5,
                                                                                                                             'error' => undef,
                                                                                                                             'line' => '236',
                                                                                                                             'items' => [
                                                                                                                                          bless( {
                                                                                                                                                   'description' => '/resource type:/i',
                                                                                                                                                   'pattern' => 'resource type:',
                                                                                                                                                   'mod' => 'i',
                                                                                                                                                   'hashname' => '__PATTERN1__',
                                                                                                                                                   'lookahead' => 0,
                                                                                                                                                   'ldelim' => '/',
                                                                                                                                                   'line' => '237',
                                                                                                                                                   'rdelim' => '/'
                                                                                                                                                 }, 'Parse::RecDescent::Token' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '237',
                                                                                                                                                   'subrule' => 'data',
                                                                                                                                                   'argcode' => undef,
                                                                                                                                                   'implicit' => undef,
                                                                                                                                                   'matchrule' => 0,
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '238',
                                                                                                                                                   'code' => '{ $thisparser->_rgresourceattribute(@item[1,2]); }',
                                                                                                                                                   'hashname' => '__ACTION1__',
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Action' )
                                                                                                                                        ],
                                                                                                                             'actcount' => 1
                                                                                                                           }, 'Parse::RecDescent::Production' ),
                                                                                                                    bless( {
                                                                                                                             'dircount' => 0,
                                                                                                                             'uncommit' => undef,
                                                                                                                             'patcount' => 1,
                                                                                                                             'strcount' => 0,
                                                                                                                             'number' => 6,
                                                                                                                             'error' => undef,
                                                                                                                             'line' => '238',
                                                                                                                             'items' => [
                                                                                                                                          bless( {
                                                                                                                                                   'description' => '/resource project name:/i',
                                                                                                                                                   'pattern' => 'resource project name:',
                                                                                                                                                   'mod' => 'i',
                                                                                                                                                   'hashname' => '__PATTERN1__',
                                                                                                                                                   'lookahead' => 0,
                                                                                                                                                   'ldelim' => '/',
                                                                                                                                                   'line' => '239',
                                                                                                                                                   'rdelim' => '/'
                                                                                                                                                 }, 'Parse::RecDescent::Token' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '239',
                                                                                                                                                   'subrule' => 'data',
                                                                                                                                                   'argcode' => undef,
                                                                                                                                                   'implicit' => undef,
                                                                                                                                                   'matchrule' => 0,
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '240',
                                                                                                                                                   'code' => '{ $thisparser->_rgresourceattribute(@item[1,2]); }',
                                                                                                                                                   'hashname' => '__ACTION1__',
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Action' )
                                                                                                                                        ],
                                                                                                                             'actcount' => 1
                                                                                                                           }, 'Parse::RecDescent::Production' ),
                                                                                                                    bless( {
                                                                                                                             'dircount' => 0,
                                                                                                                             'uncommit' => undef,
                                                                                                                             'patcount' => 1,
                                                                                                                             'strcount' => 0,
                                                                                                                             'number' => 7,
                                                                                                                             'error' => undef,
                                                                                                                             'line' => '240',
                                                                                                                             'items' => [
                                                                                                                                          bless( {
                                                                                                                                                   'description' => '/offline restart dependencies:/i',
                                                                                                                                                   'pattern' => 'offline restart dependencies:',
                                                                                                                                                   'mod' => 'i',
                                                                                                                                                   'hashname' => '__PATTERN1__',
                                                                                                                                                   'lookahead' => 0,
                                                                                                                                                   'ldelim' => '/',
                                                                                                                                                   'line' => '241',
                                                                                                                                                   'rdelim' => '/'
                                                                                                                                                 }, 'Parse::RecDescent::Token' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '241',
                                                                                                                                                   'subrule' => 'data',
                                                                                                                                                   'argcode' => undef,
                                                                                                                                                   'implicit' => undef,
                                                                                                                                                   'matchrule' => 0,
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '242',
                                                                                                                                                   'code' => '{ $thisparser->_rgresourcelistattribute(("resource_dependencies_offline_restart:",$item[2])); }',
                                                                                                                                                   'hashname' => '__ACTION1__',
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Action' )
                                                                                                                                        ],
                                                                                                                             'actcount' => 1
                                                                                                                           }, 'Parse::RecDescent::Production' ),
                                                                                                                    bless( {
                                                                                                                             'dircount' => 0,
                                                                                                                             'uncommit' => undef,
                                                                                                                             'patcount' => 1,
                                                                                                                             'strcount' => 0,
                                                                                                                             'number' => 8,
                                                                                                                             'error' => undef,
                                                                                                                             'line' => '242',
                                                                                                                             'items' => [
                                                                                                                                          bless( {
                                                                                                                                                   'description' => '/restart dependencies:/i',
                                                                                                                                                   'pattern' => 'restart dependencies:',
                                                                                                                                                   'mod' => 'i',
                                                                                                                                                   'hashname' => '__PATTERN1__',
                                                                                                                                                   'lookahead' => 0,
                                                                                                                                                   'ldelim' => '/',
                                                                                                                                                   'line' => '243',
                                                                                                                                                   'rdelim' => '/'
                                                                                                                                                 }, 'Parse::RecDescent::Token' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '243',
                                                                                                                                                   'subrule' => 'data',
                                                                                                                                                   'argcode' => undef,
                                                                                                                                                   'implicit' => undef,
                                                                                                                                                   'matchrule' => 0,
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '244',
                                                                                                                                                   'code' => '{ $thisparser->_rgresourcelistattribute(("resource_dependencies_restart:",$item[2])); }',
                                                                                                                                                   'hashname' => '__ACTION1__',
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Action' )
                                                                                                                                        ],
                                                                                                                             'actcount' => 1
                                                                                                                           }, 'Parse::RecDescent::Production' ),
                                                                                                                    bless( {
                                                                                                                             'dircount' => 0,
                                                                                                                             'uncommit' => undef,
                                                                                                                             'patcount' => 1,
                                                                                                                             'strcount' => 0,
                                                                                                                             'number' => 9,
                                                                                                                             'error' => undef,
                                                                                                                             'line' => '244',
                                                                                                                             'items' => [
                                                                                                                                          bless( {
                                                                                                                                                   'description' => '/strong dependencies:/i',
                                                                                                                                                   'pattern' => 'strong dependencies:',
                                                                                                                                                   'mod' => 'i',
                                                                                                                                                   'hashname' => '__PATTERN1__',
                                                                                                                                                   'lookahead' => 0,
                                                                                                                                                   'ldelim' => '/',
                                                                                                                                                   'line' => '245',
                                                                                                                                                   'rdelim' => '/'
                                                                                                                                                 }, 'Parse::RecDescent::Token' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '245',
                                                                                                                                                   'subrule' => 'data',
                                                                                                                                                   'argcode' => undef,
                                                                                                                                                   'implicit' => undef,
                                                                                                                                                   'matchrule' => 0,
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '246',
                                                                                                                                                   'code' => '{ $thisparser->_rgresourcelistattribute(("resource_dependencies:",$item[2])); }',
                                                                                                                                                   'hashname' => '__ACTION1__',
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Action' )
                                                                                                                                        ],
                                                                                                                             'actcount' => 1
                                                                                                                           }, 'Parse::RecDescent::Production' ),
                                                                                                                    bless( {
                                                                                                                             'dircount' => 0,
                                                                                                                             'uncommit' => undef,
                                                                                                                             'patcount' => 1,
                                                                                                                             'strcount' => 0,
                                                                                                                             'number' => 10,
                                                                                                                             'error' => undef,
                                                                                                                             'line' => '246',
                                                                                                                             'items' => [
                                                                                                                                          bless( {
                                                                                                                                                   'description' => '/weak dependencies:/i',
                                                                                                                                                   'pattern' => 'weak dependencies:',
                                                                                                                                                   'mod' => 'i',
                                                                                                                                                   'hashname' => '__PATTERN1__',
                                                                                                                                                   'lookahead' => 0,
                                                                                                                                                   'ldelim' => '/',
                                                                                                                                                   'line' => '247',
                                                                                                                                                   'rdelim' => '/'
                                                                                                                                                 }, 'Parse::RecDescent::Token' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '247',
                                                                                                                                                   'subrule' => 'data',
                                                                                                                                                   'argcode' => undef,
                                                                                                                                                   'implicit' => undef,
                                                                                                                                                   'matchrule' => 0,
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                          bless( {
                                                                                                                                                   'line' => '248',
                                                                                                                                                   'code' => '{ $thisparser->_rgresourcelistattribute(("resource_dependencies_weak:",$item[2])); }',
                                                                                                                                                   'hashname' => '__ACTION1__',
                                                                                                                                                   'lookahead' => 0
                                                                                                                                                 }, 'Parse::RecDescent::Action' )
                                                                                                                                        ],
                                                                                                                             'actcount' => 1
                                                                                                                           }, 'Parse::RecDescent::Production' )
                                                                                                                  ],
                                                                                                       'calls' => [
                                                                                                                    'data'
                                                                                                                  ],
                                                                                                       'opcount' => 0,
                                                                                                       'changed' => 0,
                                                                                                       'vars' => '',
                                                                                                       'name' => '_alternation_1_of_production_1_of_rule_rgresourceattribute'
                                                                                                     }, 'Parse::RecDescent::Rule' ),
                              'rtmethods' => bless( {
                                                      'impcount' => 0,
                                                      'line' => '68',
                                                      'prods' => [
                                                                   bless( {
                                                                            'dircount' => 0,
                                                                            'uncommit' => undef,
                                                                            'patcount' => 1,
                                                                            'strcount' => 0,
                                                                            'number' => 0,
                                                                            'error' => undef,
                                                                            'line' => undef,
                                                                            'items' => [
                                                                                         bless( {
                                                                                                  'description' => '/.*Res Type methods/i',
                                                                                                  'pattern' => '.*Res Type methods',
                                                                                                  'mod' => 'i',
                                                                                                  'hashname' => '__PATTERN1__',
                                                                                                  'lookahead' => 0,
                                                                                                  'ldelim' => '/',
                                                                                                  'line' => '68',
                                                                                                  'rdelim' => '/'
                                                                                                }, 'Parse::RecDescent::Token' ),
                                                                                         bless( {
                                                                                                  'line' => '68',
                                                                                                  'subrule' => 'rtmethodlist',
                                                                                                  'argcode' => undef,
                                                                                                  'implicit' => undef,
                                                                                                  'matchrule' => 0,
                                                                                                  'lookahead' => 0
                                                                                                }, 'Parse::RecDescent::Subrule' )
                                                                                       ],
                                                                            'actcount' => 0
                                                                          }, 'Parse::RecDescent::Production' )
                                                                 ],
                                                      'calls' => [
                                                                   'rtmethodlist'
                                                                 ],
                                                      'opcount' => 0,
                                                      'changed' => 0,
                                                      'vars' => '',
                                                      'name' => 'rtmethods'
                                                    }, 'Parse::RecDescent::Rule' ),
                              'data' => bless( {
                                                 'impcount' => 0,
                                                 'line' => '245',
                                                 'prods' => [
                                                              bless( {
                                                                       'dircount' => 1,
                                                                       'uncommit' => undef,
                                                                       'patcount' => 1,
                                                                       'strcount' => 0,
                                                                       'number' => 0,
                                                                       'error' => undef,
                                                                       'line' => undef,
                                                                       'items' => [
                                                                                    bless( {
                                                                                             'line' => '245',
                                                                                             'code' => 'my $oldskip = $skip; $skip=\'[ \\t]+\'; $oldskip',
                                                                                             'hashname' => '__DIRECTIVE1__',
                                                                                             'lookahead' => 0,
                                                                                             'name' => '<skip:\'[ \\t]+\'>'
                                                                                           }, 'Parse::RecDescent::Directive' ),
                                                                                    bless( {
                                                                                             'description' => '/.*/i',
                                                                                             'pattern' => '.*',
                                                                                             'mod' => 'i',
                                                                                             'hashname' => '__PATTERN1__',
                                                                                             'lookahead' => 0,
                                                                                             'ldelim' => '/',
                                                                                             'line' => '245',
                                                                                             'rdelim' => '/'
                                                                                           }, 'Parse::RecDescent::Token' )
                                                                                  ],
                                                                       'actcount' => 0
                                                                     }, 'Parse::RecDescent::Production' )
                                                            ],
                                                 'calls' => [],
                                                 'opcount' => 0,
                                                 'changed' => 0,
                                                 'vars' => '',
                                                 'name' => 'data'
                                               }, 'Parse::RecDescent::Rule' ),
                              'rtmethod' => bless( {
                                                     'impcount' => 1,
                                                     'line' => '72',
                                                     'prods' => [
                                                                  bless( {
                                                                           'dircount' => 0,
                                                                           'uncommit' => undef,
                                                                           'patcount' => 1,
                                                                           'strcount' => 0,
                                                                           'number' => 0,
                                                                           'error' => undef,
                                                                           'line' => undef,
                                                                           'items' => [
                                                                                        bless( {
                                                                                                 'description' => '/.*Res Type /i',
                                                                                                 'pattern' => '.*Res Type ',
                                                                                                 'mod' => 'i',
                                                                                                 'hashname' => '__PATTERN1__',
                                                                                                 'lookahead' => 0,
                                                                                                 'ldelim' => '/',
                                                                                                 'line' => '72',
                                                                                                 'rdelim' => '/'
                                                                                               }, 'Parse::RecDescent::Token' ),
                                                                                        bless( {
                                                                                                 'line' => '95',
                                                                                                 'subrule' => '_alternation_1_of_production_1_of_rule_rtmethod',
                                                                                                 'argcode' => undef,
                                                                                                 'implicit' => '/START:/i, or /STOP:/i, or /VALIDATE:/i, or /UPDATE:/i, or /INIT:/i, or /FINI:/i, or /BOOT:/i, or /MONITOR START:/i, or /MONITOR STOP:/i, or /MONITOR CHECK:/i, or /PRENET START:/i, or /POSTNET STOP:/i',
                                                                                                 'matchrule' => 0,
                                                                                                 'lookahead' => 0
                                                                                               }, 'Parse::RecDescent::Subrule' )
                                                                                      ],
                                                                           'actcount' => 0
                                                                         }, 'Parse::RecDescent::Production' )
                                                                ],
                                                     'calls' => [
                                                                  '_alternation_1_of_production_1_of_rule_rtmethod'
                                                                ],
                                                     'opcount' => 0,
                                                     'changed' => 0,
                                                     'vars' => '',
                                                     'name' => 'rtmethod'
                                                   }, 'Parse::RecDescent::Rule' ),
                              'rgresourceattributelist' => bless( {
                                                                    'impcount' => 0,
                                                                    'line' => '199',
                                                                    'prods' => [
                                                                                 bless( {
                                                                                          'dircount' => 0,
                                                                                          'uncommit' => undef,
                                                                                          'patcount' => 0,
                                                                                          'strcount' => 0,
                                                                                          'number' => 0,
                                                                                          'error' => undef,
                                                                                          'line' => undef,
                                                                                          'items' => [
                                                                                                       bless( {
                                                                                                                'line' => '199',
                                                                                                                'subrule' => 'rgresourceattribute',
                                                                                                                'argcode' => undef,
                                                                                                                'implicit' => undef,
                                                                                                                'matchrule' => 0,
                                                                                                                'lookahead' => 0
                                                                                                              }, 'Parse::RecDescent::Subrule' ),
                                                                                                       bless( {
                                                                                                                'line' => '199',
                                                                                                                'subrule' => 'rgresourceattributelist',
                                                                                                                'argcode' => undef,
                                                                                                                'implicit' => undef,
                                                                                                                'matchrule' => 0,
                                                                                                                'lookahead' => 0
                                                                                                              }, 'Parse::RecDescent::Subrule' )
                                                                                                     ],
                                                                                          'actcount' => 0
                                                                                        }, 'Parse::RecDescent::Production' ),
                                                                                 bless( {
                                                                                          'dircount' => 0,
                                                                                          'uncommit' => undef,
                                                                                          'patcount' => 0,
                                                                                          'strcount' => 0,
                                                                                          'number' => 1,
                                                                                          'error' => undef,
                                                                                          'line' => '199',
                                                                                          'items' => [],
                                                                                          'actcount' => 0
                                                                                        }, 'Parse::RecDescent::Production' )
                                                                               ],
                                                                    'calls' => [
                                                                                 'rgresourceattribute',
                                                                                 'rgresourceattributelist'
                                                                               ],
                                                                    'opcount' => 0,
                                                                    'changed' => 0,
                                                                    'vars' => '',
                                                                    'name' => 'rgresourceattributelist'
                                                                  }, 'Parse::RecDescent::Rule' ),
                              'eofile' => bless( {
                                                   'impcount' => 0,
                                                   'line' => '247',
                                                   'prods' => [
                                                                bless( {
                                                                         'dircount' => 0,
                                                                         'uncommit' => undef,
                                                                         'patcount' => 1,
                                                                         'strcount' => 0,
                                                                         'number' => 0,
                                                                         'error' => undef,
                                                                         'line' => undef,
                                                                         'items' => [
                                                                                      bless( {
                                                                                               'description' => '/^\\\\Z/',
                                                                                               'pattern' => '^\\Z',
                                                                                               'mod' => '',
                                                                                               'hashname' => '__PATTERN1__',
                                                                                               'lookahead' => 0,
                                                                                               'ldelim' => '/',
                                                                                               'line' => '247',
                                                                                               'rdelim' => '/'
                                                                                             }, 'Parse::RecDescent::Token' )
                                                                                    ],
                                                                         'actcount' => 0
                                                                       }, 'Parse::RecDescent::Production' )
                                                              ],
                                                   'calls' => [],
                                                   'opcount' => 0,
                                                   'changed' => 0,
                                                   'vars' => '',
                                                   'name' => 'eofile'
                                                 }, 'Parse::RecDescent::Rule' ),
                              'rgresourcelist' => bless( {
                                                           'impcount' => 0,
                                                           'line' => '190',
                                                           'prods' => [
                                                                        bless( {
                                                                                 'dircount' => 0,
                                                                                 'uncommit' => undef,
                                                                                 'patcount' => 0,
                                                                                 'strcount' => 0,
                                                                                 'number' => 0,
                                                                                 'error' => undef,
                                                                                 'line' => undef,
                                                                                 'items' => [
                                                                                              bless( {
                                                                                                       'line' => '190',
                                                                                                       'subrule' => 'rgresource',
                                                                                                       'argcode' => undef,
                                                                                                       'implicit' => undef,
                                                                                                       'matchrule' => 0,
                                                                                                       'lookahead' => 0
                                                                                                     }, 'Parse::RecDescent::Subrule' ),
                                                                                              bless( {
                                                                                                       'line' => '190',
                                                                                                       'subrule' => 'rgresourcelist',
                                                                                                       'argcode' => undef,
                                                                                                       'implicit' => undef,
                                                                                                       'matchrule' => 0,
                                                                                                       'lookahead' => 0
                                                                                                     }, 'Parse::RecDescent::Subrule' )
                                                                                            ],
                                                                                 'actcount' => 0
                                                                               }, 'Parse::RecDescent::Production' ),
                                                                        bless( {
                                                                                 'dircount' => 0,
                                                                                 'uncommit' => undef,
                                                                                 'patcount' => 0,
                                                                                 'strcount' => 0,
                                                                                 'number' => 1,
                                                                                 'error' => undef,
                                                                                 'line' => '190',
                                                                                 'items' => [],
                                                                                 'actcount' => 0
                                                                               }, 'Parse::RecDescent::Production' )
                                                                      ],
                                                           'calls' => [
                                                                        'rgresource',
                                                                        'rgresourcelist'
                                                                      ],
                                                           'opcount' => 0,
                                                           'changed' => 0,
                                                           'vars' => '',
                                                           'name' => 'rgresourcelist'
                                                         }, 'Parse::RecDescent::Rule' ),
                              'rgattribute' => bless( {
                                                        'impcount' => 1,
                                                        'line' => '146',
                                                        'prods' => [
                                                                     bless( {
                                                                              'dircount' => 0,
                                                                              'uncommit' => undef,
                                                                              'patcount' => 1,
                                                                              'strcount' => 0,
                                                                              'number' => 0,
                                                                              'error' => undef,
                                                                              'line' => undef,
                                                                              'items' => [
                                                                                           bless( {
                                                                                                    'description' => '/.*Res Group /i',
                                                                                                    'pattern' => '.*Res Group ',
                                                                                                    'mod' => 'i',
                                                                                                    'hashname' => '__PATTERN1__',
                                                                                                    'lookahead' => 0,
                                                                                                    'ldelim' => '/',
                                                                                                    'line' => '146',
                                                                                                    'rdelim' => '/'
                                                                                                  }, 'Parse::RecDescent::Token' ),
                                                                                           bless( {
                                                                                                    'line' => '188',
                                                                                                    'subrule' => '_alternation_1_of_production_1_of_rule_rgattribute',
                                                                                                    'argcode' => undef,
                                                                                                    'implicit' => '/RG_description:/i, or /mode:/i, or /management state:/i, or /RG_project_name:/i, or /RG_SLM_type:/i, or /RG_SLM_projectname:/i, or /RG_SLM_pset_type:/i, or /RG_SLM_CPU_SHARES:/i, or /RG_SLM_PSET_MIN:/i, or /auto_start_on_new_cluster:/i, or /failback:/i, or /nodelist:/i, or /maximum_primaries:/i, or /desired_primaries:/i, or /RG_dependencies:/i, or /network dependencies:/i, or /global_resources_used:/i, or /pingpong_interval:/i, or /pathprefix:/i, or /RG_affinities:/i, or /system:/i, or /Suspend_automatic_recovery:.*/i',
                                                                                                    'matchrule' => 0,
                                                                                                    'lookahead' => 0
                                                                                                  }, 'Parse::RecDescent::Subrule' )
                                                                                         ],
                                                                              'actcount' => 0
                                                                            }, 'Parse::RecDescent::Production' )
                                                                   ],
                                                        'calls' => [
                                                                     '_alternation_1_of_production_1_of_rule_rgattribute'
                                                                   ],
                                                        'opcount' => 0,
                                                        'changed' => 0,
                                                        'vars' => '',
                                                        'name' => 'rgattribute'
                                                      }, 'Parse::RecDescent::Rule' ),
                              '_alternation_1_of_production_1_of_rule_rgattribute' => bless( {
                                                                                               'impcount' => 0,
                                                                                               'line' => '206',
                                                                                               'prods' => [
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 0,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => undef,
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/RG_description:/i',
                                                                                                                                           'pattern' => 'RG_description:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '206',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '206',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '206',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 1,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '207',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/mode:/i',
                                                                                                                                           'pattern' => 'mode:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '208',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '208',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '208',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(("rg_mode:", $item[2])); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 2,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '209',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/management state:/i',
                                                                                                                                           'pattern' => 'management state:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '210',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '210',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '210',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 3,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '211',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/RG_project_name:/i',
                                                                                                                                           'pattern' => 'RG_project_name:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '212',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '212',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '212',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 4,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '213',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/RG_SLM_type:/i',
                                                                                                                                           'pattern' => 'RG_SLM_type:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '214',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '214',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '214',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 5,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '215',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/RG_SLM_projectname:/i',
                                                                                                                                           'pattern' => 'RG_SLM_projectname:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '216',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '216',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '216',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 6,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '217',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/RG_SLM_pset_type:/i',
                                                                                                                                           'pattern' => 'RG_SLM_pset_type:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '218',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '218',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '218',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 7,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '219',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/RG_SLM_CPU_SHARES:/i',
                                                                                                                                           'pattern' => 'RG_SLM_CPU_SHARES:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '220',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '220',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '220',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 8,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '221',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/RG_SLM_PSET_MIN:/i',
                                                                                                                                           'pattern' => 'RG_SLM_PSET_MIN:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '222',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '222',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '222',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 9,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '223',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/auto_start_on_new_cluster:/i',
                                                                                                                                           'pattern' => 'auto_start_on_new_cluster:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '224',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '224',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '224',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 10,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '225',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/failback:/i',
                                                                                                                                           'pattern' => 'failback:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '226',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '226',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '226',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 11,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '227',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/nodelist:/i',
                                                                                                                                           'pattern' => 'nodelist:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '228',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '228',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '228',
                                                                                                                                           'code' => '{
	$thisparser->_rglistattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 12,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '229',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/maximum_primaries:/i',
                                                                                                                                           'pattern' => 'maximum_primaries:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '230',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '230',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '230',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 13,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '231',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/desired_primaries:/i',
                                                                                                                                           'pattern' => 'desired_primaries:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '232',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '232',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '232',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 14,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '233',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/RG_dependencies:/i',
                                                                                                                                           'pattern' => 'RG_dependencies:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '234',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '234',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '234',
                                                                                                                                           'code' => '{
	$thisparser->_rglistattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 15,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '235',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/network dependencies:/i',
                                                                                                                                           'pattern' => 'network dependencies:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '236',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '236',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '236',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(("implicit_network_dependencies:",$item[2])); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 16,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '237',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/global_resources_used:/i',
                                                                                                                                           'pattern' => 'global_resources_used:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '238',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '238',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '238',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 17,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '239',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/pingpong_interval:/i',
                                                                                                                                           'pattern' => 'pingpong_interval:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '240',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '240',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '240',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 18,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '241',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/pathprefix:/i',
                                                                                                                                           'pattern' => 'pathprefix:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '242',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '242',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '242',
                                                                                                                                           'code' => '{
	$thisparser->_rgattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 19,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '243',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/RG_affinities:/i',
                                                                                                                                           'pattern' => 'RG_affinities:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '244',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '244',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '244',
                                                                                                                                           'code' => '{
	$thisparser->_rglistattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 20,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '245',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/system:/i',
                                                                                                                                           'pattern' => 'system:',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '246',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '246',
                                                                                                                                           'subrule' => 'data',
                                                                                                                                           'argcode' => undef,
                                                                                                                                           'implicit' => undef,
                                                                                                                                           'matchrule' => 0,
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Subrule' ),
                                                                                                                                  bless( {
                                                                                                                                           'line' => '246',
                                                                                                                                           'code' => '{
	$thisparser->_rglistattribute(@item[1,2]); }',
                                                                                                                                           'hashname' => '__ACTION1__',
                                                                                                                                           'lookahead' => 0
                                                                                                                                         }, 'Parse::RecDescent::Action' )
                                                                                                                                ],
                                                                                                                     'actcount' => 1
                                                                                                                   }, 'Parse::RecDescent::Production' ),
                                                                                                            bless( {
                                                                                                                     'dircount' => 0,
                                                                                                                     'uncommit' => undef,
                                                                                                                     'patcount' => 1,
                                                                                                                     'strcount' => 0,
                                                                                                                     'number' => 21,
                                                                                                                     'error' => undef,
                                                                                                                     'line' => '247',
                                                                                                                     'items' => [
                                                                                                                                  bless( {
                                                                                                                                           'description' => '/Suspend_automatic_recovery:.*/i',
                                                                                                                                           'pattern' => 'Suspend_automatic_recovery:.*',
                                                                                                                                           'mod' => 'i',
                                                                                                                                           'hashname' => '__PATTERN1__',
                                                                                                                                           'lookahead' => 0,
                                                                                                                                           'ldelim' => '/',
                                                                                                                                           'line' => '248',
                                                                                                                                           'rdelim' => '/'
                                                                                                                                         }, 'Parse::RecDescent::Token' )
                                                                                                                                ],
                                                                                                                     'actcount' => 0
                                                                                                                   }, 'Parse::RecDescent::Production' )
                                                                                                          ],
                                                                                               'calls' => [
                                                                                                            'data'
                                                                                                          ],
                                                                                               'opcount' => 0,
                                                                                               'changed' => 0,
                                                                                               'vars' => '',
                                                                                               'name' => '_alternation_1_of_production_1_of_rule_rgattribute'
                                                                                             }, 'Parse::RecDescent::Rule' ),
                              'rtparamattributelist' => bless( {
                                                                 'impcount' => 0,
                                                                 'line' => '103',
                                                                 'prods' => [
                                                                              bless( {
                                                                                       'dircount' => 0,
                                                                                       'uncommit' => undef,
                                                                                       'patcount' => 0,
                                                                                       'strcount' => 0,
                                                                                       'number' => 0,
                                                                                       'error' => undef,
                                                                                       'line' => undef,
                                                                                       'items' => [
                                                                                                    bless( {
                                                                                                             'line' => '103',
                                                                                                             'subrule' => 'rtparamattribute',
                                                                                                             'argcode' => undef,
                                                                                                             'implicit' => undef,
                                                                                                             'matchrule' => 0,
                                                                                                             'lookahead' => 0
                                                                                                           }, 'Parse::RecDescent::Subrule' ),
                                                                                                    bless( {
                                                                                                             'line' => '103',
                                                                                                             'subrule' => 'rtparamattributelist',
                                                                                                             'argcode' => undef,
                                                                                                             'implicit' => undef,
                                                                                                             'matchrule' => 0,
                                                                                                             'lookahead' => 0
                                                                                                           }, 'Parse::RecDescent::Subrule' )
                                                                                                  ],
                                                                                       'actcount' => 0
                                                                                     }, 'Parse::RecDescent::Production' ),
                                                                              bless( {
                                                                                       'dircount' => 0,
                                                                                       'uncommit' => undef,
                                                                                       'patcount' => 0,
                                                                                       'strcount' => 0,
                                                                                       'number' => 1,
                                                                                       'error' => undef,
                                                                                       'line' => '103',
                                                                                       'items' => [],
                                                                                       'actcount' => 0
                                                                                     }, 'Parse::RecDescent::Production' )
                                                                            ],
                                                                 'calls' => [
                                                                              'rtparamattribute',
                                                                              'rtparamattributelist'
                                                                            ],
                                                                 'opcount' => 0,
                                                                 'changed' => 0,
                                                                 'vars' => '',
                                                                 'name' => 'rtparamattributelist'
                                                               }, 'Parse::RecDescent::Rule' ),
                              'rtparamlist' => bless( {
                                                        'impcount' => 0,
                                                        'line' => '99',
                                                        'prods' => [
                                                                     bless( {
                                                                              'dircount' => 0,
                                                                              'uncommit' => undef,
                                                                              'patcount' => 0,
                                                                              'strcount' => 0,
                                                                              'number' => 0,
                                                                              'error' => undef,
                                                                              'line' => undef,
                                                                              'items' => [
                                                                                           bless( {
                                                                                                    'line' => '99',
                                                                                                    'subrule' => 'rtparam',
                                                                                                    'argcode' => undef,
                                                                                                    'implicit' => undef,
                                                                                                    'matchrule' => 0,
                                                                                                    'lookahead' => 0
                                                                                                  }, 'Parse::RecDescent::Subrule' ),
                                                                                           bless( {
                                                                                                    'line' => '99',
                                                                                                    'subrule' => 'rtparamlist',
                                                                                                    'argcode' => undef,
                                                                                                    'implicit' => undef,
                                                                                                    'matchrule' => 0,
                                                                                                    'lookahead' => 0
                                                                                                  }, 'Parse::RecDescent::Subrule' )
                                                                                         ],
                                                                              'actcount' => 0
                                                                            }, 'Parse::RecDescent::Production' ),
                                                                     bless( {
                                                                              'dircount' => 0,
                                                                              'uncommit' => undef,
                                                                              'patcount' => 0,
                                                                              'strcount' => 0,
                                                                              'number' => 1,
                                                                              'error' => undef,
                                                                              'line' => '99',
                                                                              'items' => [],
                                                                              'actcount' => 0
                                                                            }, 'Parse::RecDescent::Production' )
                                                                   ],
                                                        'calls' => [
                                                                     'rtparam',
                                                                     'rtparamlist'
                                                                   ],
                                                        'opcount' => 0,
                                                        'changed' => 0,
                                                        'vars' => '',
                                                        'name' => 'rtparamlist'
                                                      }, 'Parse::RecDescent::Rule' ),
                              'rtparamattribute' => bless( {
                                                             'impcount' => 1,
                                                             'line' => '110',
                                                             'prods' => [
                                                                          bless( {
                                                                                   'dircount' => 0,
                                                                                   'uncommit' => undef,
                                                                                   'patcount' => 1,
                                                                                   'strcount' => 0,
                                                                                   'number' => 0,
                                                                                   'error' => undef,
                                                                                   'line' => undef,
                                                                                   'items' => [
                                                                                                bless( {
                                                                                                         'description' => '/.*Res Type param /i',
                                                                                                         'pattern' => '.*Res Type param ',
                                                                                                         'mod' => 'i',
                                                                                                         'hashname' => '__PATTERN1__',
                                                                                                         'lookahead' => 0,
                                                                                                         'ldelim' => '/',
                                                                                                         'line' => '110',
                                                                                                         'rdelim' => '/'
                                                                                                       }, 'Parse::RecDescent::Token' ),
                                                                                                bless( {
                                                                                                         'line' => '135',
                                                                                                         'subrule' => '_alternation_1_of_production_1_of_rule_rtparamattribute',
                                                                                                         'argcode' => undef,
                                                                                                         'implicit' => '/extension:/i, or /per-node:/i, or /description:/i, or /tunability:/i, or /type:/i, or /min int value:/i, or /max int value:/i, or /min length:/i, or /max length:/i, or /min array length:/i, or /max array length:/i, or /enum list:/i, or /default:/i',
                                                                                                         'matchrule' => 0,
                                                                                                         'lookahead' => 0
                                                                                                       }, 'Parse::RecDescent::Subrule' )
                                                                                              ],
                                                                                   'actcount' => 0
                                                                                 }, 'Parse::RecDescent::Production' )
                                                                        ],
                                                             'calls' => [
                                                                          '_alternation_1_of_production_1_of_rule_rtparamattribute'
                                                                        ],
                                                             'opcount' => 0,
                                                             'changed' => 0,
                                                             'vars' => '',
                                                             'name' => 'rtparamattribute'
                                                           }, 'Parse::RecDescent::Rule' ),
                              'rgattributelist' => bless( {
                                                            'impcount' => 0,
                                                            'line' => '144',
                                                            'prods' => [
                                                                         bless( {
                                                                                  'dircount' => 0,
                                                                                  'uncommit' => undef,
                                                                                  'patcount' => 0,
                                                                                  'strcount' => 0,
                                                                                  'number' => 0,
                                                                                  'error' => undef,
                                                                                  'line' => undef,
                                                                                  'items' => [
                                                                                               bless( {
                                                                                                        'line' => '144',
                                                                                                        'subrule' => 'rgattribute',
                                                                                                        'argcode' => undef,
                                                                                                        'implicit' => undef,
                                                                                                        'matchrule' => 0,
                                                                                                        'lookahead' => 0
                                                                                                      }, 'Parse::RecDescent::Subrule' ),
                                                                                               bless( {
                                                                                                        'line' => '144',
                                                                                                        'subrule' => 'rgattributelist',
                                                                                                        'argcode' => undef,
                                                                                                        'implicit' => undef,
                                                                                                        'matchrule' => 0,
                                                                                                        'lookahead' => 0
                                                                                                      }, 'Parse::RecDescent::Subrule' )
                                                                                             ],
                                                                                  'actcount' => 0
                                                                                }, 'Parse::RecDescent::Production' ),
                                                                         bless( {
                                                                                  'dircount' => 0,
                                                                                  'uncommit' => undef,
                                                                                  'patcount' => 0,
                                                                                  'strcount' => 0,
                                                                                  'number' => 1,
                                                                                  'error' => undef,
                                                                                  'line' => '144',
                                                                                  'items' => [],
                                                                                  'actcount' => 0
                                                                                }, 'Parse::RecDescent::Production' )
                                                                       ],
                                                            'calls' => [
                                                                         'rgattribute',
                                                                         'rgattributelist'
                                                                       ],
                                                            'opcount' => 0,
                                                            'changed' => 0,
                                                            'vars' => '',
                                                            'name' => 'rgattributelist'
                                                          }, 'Parse::RecDescent::Rule' ),
                              'rgresourcepropertylist' => bless( {
                                                                   'impcount' => 0,
                                                                   'line' => '224',
                                                                   'prods' => [
                                                                                bless( {
                                                                                         'dircount' => 0,
                                                                                         'uncommit' => undef,
                                                                                         'patcount' => 0,
                                                                                         'strcount' => 0,
                                                                                         'number' => 0,
                                                                                         'error' => undef,
                                                                                         'line' => undef,
                                                                                         'items' => [
                                                                                                      bless( {
                                                                                                               'line' => '224',
                                                                                                               'subrule' => 'rgresourceproperty',
                                                                                                               'argcode' => undef,
                                                                                                               'implicit' => undef,
                                                                                                               'matchrule' => 0,
                                                                                                               'lookahead' => 0
                                                                                                             }, 'Parse::RecDescent::Subrule' ),
                                                                                                      bless( {
                                                                                                               'line' => '224',
                                                                                                               'subrule' => 'rgresourcepropertylist',
                                                                                                               'argcode' => undef,
                                                                                                               'implicit' => undef,
                                                                                                               'matchrule' => 0,
                                                                                                               'lookahead' => 0
                                                                                                             }, 'Parse::RecDescent::Subrule' )
                                                                                                    ],
                                                                                         'actcount' => 0
                                                                                       }, 'Parse::RecDescent::Production' ),
                                                                                bless( {
                                                                                         'dircount' => 0,
                                                                                         'uncommit' => undef,
                                                                                         'patcount' => 0,
                                                                                         'strcount' => 0,
                                                                                         'number' => 1,
                                                                                         'error' => undef,
                                                                                         'line' => '224',
                                                                                         'items' => [],
                                                                                         'actcount' => 0
                                                                                       }, 'Parse::RecDescent::Production' )
                                                                              ],
                                                                   'calls' => [
                                                                                'rgresourceproperty',
                                                                                'rgresourcepropertylist'
                                                                              ],
                                                                   'opcount' => 0,
                                                                   'changed' => 0,
                                                                   'vars' => '',
                                                                   'name' => 'rgresourcepropertylist'
                                                                 }, 'Parse::RecDescent::Rule' )
                            },
                 'namespace' => 'Parse::RecDescent::namespace000001',
                 '_check' => {
                               'prevline' => '',
                               'prevcolumn' => '',
                               'thiscolumn' => '',
                               'prevoffset' => '',
                               'thisoffset' => '',
                               'itempos' => ''
                             },
                 '_AUTOACTION' => undef,
                 'localvars' => '',
                 'startcode' => ''
               }, 'Parse::RecDescent' );
}