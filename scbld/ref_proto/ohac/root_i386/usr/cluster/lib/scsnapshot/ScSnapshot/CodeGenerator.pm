#! /usr/perl5/bin/perl -w
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident   "@(#)CodeGenerator.pm 1.3     08/05/20 SMI"
# 
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#
# Module: ScSnapshot::CodeGenerator
# 

package ScSnapshot::CodeGenerator;
use strict;
use Sun::Solaris::Utils qw(gettext);

#
# Constructor.
# Arguments:
#   1) The instance of ScSnapshot::InternalViewDumper to be used for
#      generating the code.
#      Cannot be undefined.
#
sub new
{
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = {};
    $self->{DUMPER} = shift;
    $self->{EXIT_CODE} = shift;
    bless ($self, $class);
    return $self;
}

#
# Generates the code representing a given view into the given file.
# If the file is not specified, the standard output is used instead.
# Arguments:
#   1) The view for which the code should be generated.
#      Cannot be undefined.
#   2) The name of the destination file.
#      May be undefined.
#
sub generate 
{
    my $self = shift;
    my $view = shift;
    my $filename = shift;
    if (defined($filename)) {

      # If a similar file is already present, rename it with .old sufffix
      if(open(OUTFILE, "<" . $filename)) {
	
	if(system('/usr/bin/mv', $filename, $filename . ".old") != 0) {
	  print STDERR gettext ("FATAL ERROR: Unable to rename file:")
	    . "\n\t[" . $filename . "] --> [" . $filename . ".old]\n";
	  exit $self->{EXIT_CODE}->getCode("EIO");
	}
	close(OUTFILE);
      }
      
      if(!open(OUTFILE, ">" . $filename)) {
	print STDERR gettext("FATAL ERROR: Unable to open file for writing:") 
	  . "\n\t[" . $filename . "]\n";
	exit $self->{EXIT_CODE}->getCode("EIO");
      }
      
      $self->{DUMPER}->dump($view, *OUTFILE);
      close(OUTFILE);
    } else {
      $self->{DUMPER}->dump($view, *STDOUT);
    }
} 

# Return success for module load
1; 
