#! /usr/perl5/bin/perl -w
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident   "@(#)MetadataRepository.pm 1.3     08/05/20 SMI"
# 
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# 
# Module: ScSnapshot::MetadataRepository
# 
# Provides access to the information stored in the metadata repository.
#

package ScSnapshot::MetadataRepository;
use strict;

#
# Constructor
# Arguments:
#  1) Name of the file containing the metadata repository.
#     Cannot be undefined.
#
sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self  = {};
  $self->{FILENAME} = shift;
  $self->{REPOSITORY} = undef;
  bless ($self, $class);
  $self->_init();
  return $self;
}

#
# Reads the file containing the metadata repository and fills in the
# hash holding the default values. Invoked by the class constructor.
#
sub _init {
  my $self = shift;
  $self->{REPOSITORY} = { ResourceGroups => {} , Resources => {} };
  open(DATAFILE, "<" . $self->filename) || 
    die ("INTERNAL ERROR: Cannot open metadata repository: $self->filename : $!"); 
  while (my $line = readline(*DATAFILE)) { 
    # drop commented lines and empty lines
    unless ($line =~ /^\#/ || $line =~ /^\s$/) {   
      my $properties; 
      # look for rg properties and remove trailing newline if any
      if (($line =~ s/^rg:(.*)\n/$1/) || ($line =~ s/^rg:(.*)/$1/)) {
	$properties = $self->resourcegroups();
      }
      # look for resource properties and remove trailing newline if any
      elsif (($line =~ s/^r:(.*)\n/$1/) || ($line =~ s/^r:(.*)/$1/)) {
	$properties = $self->resources();
      }
      
      # ignore malformed lines
      if ($properties) { 
	# look for properties with default value
	if ($line =~ s/(.*) \((.*)\)/$1 $2/) {
	  my $name = $line;
	  my $value = $line;
	  $name =~ s/ .*//;
	  $value =~ s/.* //;
	  $value =~ s/"(.*)"/$1/; # unquote value if needed
	  $value =~ s/null//; # replace value "null" by the empty 
	                      # string if needed
	  $properties->{$name} = $value;
	} else {
	  # no default value
	  $properties->{$line} = undef; 
	}
      }
    }
  }
  
  close(DATAFILE);
} 

#
# Gets/Sets the name of the file containing the metadata repository
#
sub filename {
  my $self = shift;
  if (@_) { 
    $self->{FILENAME} = shift;
  }
  return $self->{FILENAME};
}

#
# Gets/Sets the sub-hash of the metadata repository holding default values 
# for the resource groups.
#
sub resourcegroups {
  my $self = shift;
  if (@_) { 
    $self->{REPOSITORY}->{ResourceGroups} = shift;
  }
  return $self->{REPOSITORY}->{ResourceGroups};
}

#
# Gets/Sets the sub-hash of the metadata repository holding default values 
# for the resources.
#
sub resources {
  my $self = shift;
  if (@_) { 
    $self->{REPOSITORY}->{Resources} = shift;
  }
  return $self->{REPOSITORY}->{Resources};
}

# Return success for module load
1; 
