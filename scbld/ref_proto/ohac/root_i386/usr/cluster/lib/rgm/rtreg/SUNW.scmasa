#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#
# Copyright 2003-2004 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)SUNW.scmasa	1.3	08/05/20 SMI"
#
#

# Sun Cluster Data Services Builder template version 1.0
# Registration information and Paramtable for scmasa
#
# NOTE: Keywords are case insensitive, i.e. users may use any
# capitalization style they wish
#

RESOURCE_TYPE = "scmasa";
VENDOR_ID = SUNW;
RT_DESCRIPTION = "Sun Cluster Managability and Serviceability (CMASS) resource";

RT_VERSION ="1.0"; 
API_VERSION = 2;	 
FAILOVER = TRUE;

INIT_NODES = RG_PRIMARIES;

RT_BASEDIR=/usr/cluster/lib/rgm/rt/hamasa;

START				=	scmasa_svc_start;
STOP				=	scmasa_svc_stop;

VALIDATE			=	scmasa_validate;
UPDATE	 			=	scmasa_update;

MONITOR_START			=	scmasa_monitor_start;
MONITOR_STOP			=	scmasa_monitor_stop;
MONITOR_CHECK			=	scmasa_monitor_check;


# The paramtable is a list of bracketed resource property declarations 
# that come after the resource-type declarations
# The property-name declaration must be the first attribute
# after the open curly of a paramtable entry
#
# The following are the system defined properties. Each of the system defined
# properties have a default value set for each of the attributes. Look at 
# man rt_reg(4) for a detailed explanation.
#
{  
	PROPERTY = Start_timeout; 
	MIN = 60;
	DEFAULT = 300;
}
{
	PROPERTY = Stop_timeout; 
	MIN = 60;
	DEFAULT = 300;
}
{ 
	PROPERTY = Validate_timeout; 
	MIN = 60;
	DEFAULT = 300; 
}
{
        PROPERTY = Update_timeout;
	MIN = 60;
        DEFAULT = 300;
}
{ 
	PROPERTY = Monitor_Start_timeout; 
	MIN = 60;
	DEFAULT = 300;
}
{ 
	PROPERTY = Monitor_Stop_timeout; 
	MIN = 60;
	DEFAULT = 300;
}
{ 
	PROPERTY = Monitor_Check_timeout; 
	MIN = 60;
	DEFAULT = 300; 
}
{
        PROPERTY = FailOver_Mode;
        DEFAULT = SOFT;
        TUNABLE = ANYTIME;
}
{
        PROPERTY = Network_resources_used;
        TUNABLE = WHEN_DISABLED;
	DEFAULT = "";
}
{ 
	PROPERTY = Thorough_Probe_Interval; 
	MAX = 3600; 
	DEFAULT = 60; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = Retry_Count; 
	MAX = 10; 
	DEFAULT = 2; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = Retry_Interval; 
	MAX = 3600; 
	DEFAULT = 300; 
	TUNABLE = ANYTIME;
}

{
	PROPERTY = Port_list;
	DEFAULT = "8082/tcp";
	TUNABLE = AT_CREATION;
}

{
        PROPERTY = Scalable;
	DEFAULT=false;
        TUNABLE = AT_CREATION;
}

{
        PROPERTY = Load_balancing_policy;
        DEFAULT = LB_WEIGHTED;
        TUNABLE = AT_CREATION;
}

{
        PROPERTY = Load_balancing_weights;
        DEFAULT = "";
        TUNABLE = ANYTIME;
}

#
# Extension Properties
#


# These two control the restarting of the fault monitor itself
# (not the server daemon) by PMF.
{
	PROPERTY = Monitor_retry_count;
	EXTENSION;
	INT;
	DEFAULT = 4;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Number of PMF restarts allowed for the fault monitor";
}

{
	PROPERTY = Monitor_retry_interval;
	EXTENSION;
	INT;
	DEFAULT = 2;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time window (minutes) for fault monitor restarts";
}

# Time out value for the probe
{
	PROPERTY = Probe_timeout;
	EXTENSION;
	INT;
	MIN = 2;
	DEFAULT = 30;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time out value for the probe (seconds)";
}

# Child process monitoring level for PMF (-C option of pmfadm)
# Default of -1 means: Do NOT use the -C option to PMFADM
# A value of 0-> indicates the level of child process monitoring
# by PMF that is desired.
{
	PROPERTY = Child_mon_level;
	EXTENSION;
	INT;
	DEFAULT = -1;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Child monitoring level for PMF";
}

# This is an optional property, which determines whether to failover when
# retry_count is exceeded during retry_interval.
#
{
	PROPERTY = Failover_enabled;
	EXTENSION;
	BOOLEAN;
	DEFAULT = TRUE;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "Determines whether to failover when retry_count is exceeded during retry_interval";
}

# This is an optional property, which determines which failover_group
# should be unlocked / locked by the cmass container when this resource
# is activated / de-activated. If this property is left empty (which is
# the default), the name of the resource is used as the cmas_failover_group
# name.
#
{
	PROPERTY = Cmas_failover_group;
	EXTENSION;
	STRING;
	DEFAULT = "";
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "Name of the CMASS failover group to use";
}
