#
# Copyright (c) 2008, Sun Microsystems, Inc.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#   * Neither the name of Sun Microsystems, Inc. nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#
# Gate Specific Code -- BEGIN
# ident  "@(#)Makefile.xfs 1.15     08/02/23 SMI"
# Gate Specific Code -- END
# Sun Cluster Data Services Builder template version 1.0
#
# Makefile for data service: XFS
#

# Set this to match the path to where your C compiler binary is.
CC=cc
LDFLAGS += -L/usr/cluster/lib -R/usr/cluster/lib
INCLFLAGS = -I/usr/cluster/include -I/usr/cluster/include/rgm

LDLIBS	+= -lc -lnsl -lsocket -lresolv -ldsdev -lscha -ldl

# System commands
RMCMD=/usr/bin/rm
MVCMD=/usr/bin/mv
PKGMKCMD=/usr/bin/pkgmk

WORKING_DIR=

# Source and header files for XFS
INCL = xfs.h
COMMON_SRCS = xfs.c xfs_cmdline_subst.c
PROG_SRCS = xfs_svc_start.c \
	xfs_svc_stop.c \
	xfs_validate.c \
	xfs_update.c \
	xfs_monitor_start.c \
	xfs_monitor_stop.c \
	xfs_monitor_check.c \
	xfs_probe.c 

# objects
COMMON_OBJS = $(COMMON_SRCS:%.c=%.o)
OBJS = $(PROG_SRCS:%.c=%.o)

PROGS = $(PROG_SRCS:%.c=%)

# Make targets

all: $(PROGS)

# Make install rule will install XFS into WORKING_DIR
pkg: all
	$(PKGMKCMD) -o -d ../pkg -f ../etc/prototype -b $(WORKING_DIR)

%.o: %.c
	$(CC) -c $< $(INCLFLAGS)

$(PROGS): $(COMMON_OBJS) $(OBJS)
	$(CC) $(@:%=%.o) $(COMMON_OBJS) -o $@ $(LDFLAGS) $(LDLIBS)
	$(MVCMD) $@ ../bin

clean:
	$(RMCMD) -f $(OBJS) $(PROGS) $(COMMON_OBJS)

