#!/bin/ksh
#
# Copyright (c) 2008, Sun Microsystems, Inc.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#   * Neither the name of Sun Microsystems, Inc. nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#
# Gate Specific Code -- BEGIN
# ident	"@(#)dns_svc_start.ksh	1.15	08/02/23 SMI"
# Gate Specific Code -- END
# Sun Cluster Data Services Builder template version 1.0
#
# Start Method for DNS.
#
# The data service is started under the control of PMF. Prior to starting the 
# process(es) for DNS some sanity checks are done. The PMF starts the service
# with an action script and retry count of zero.
# The pmf action script will handle the local restarts/failover of the resource 
###############################################################################
alias scds_syslog="/usr/cluster/lib/sc/scds_syslog"
SYSLOG_TAG="[SUNW.dns]"
###############################################################################
# Parse program arguments.
#
function parse_args # [args ...]
{
	typeset opt

	while getopts 'R:G:T:' opt
	do
		case "$opt" in

		R)
		# Name of the DNS resource.
		RESOURCE_NAME=$OPTARG
		;;

		G)
		# Name of the resource group in which the resource is
		# configured.
		RESOURCEGROUP_NAME=$OPTARG
		;;

		T)
		# Name of the resource type.
		RESOURCETYPE_NAME=$OPTARG
		;;

		*)
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"ERROR: Option %s unknown" \
		$OPTARG
		exit 1
		;;

		esac
	done
	SYSLOG_TAG="[${RESOURCETYPE_NAME},${RESOURCE_NAME},${RESOURCEGROUP_NAME}]"
}

###############################################################################
# MAIN
##############################################################################

export PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/proc/bin:$PATH

# Parse the arguments that have been passed to this method
parse_args "$@"

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.svc

# We need to know the full path for the gethostnames utility which resides
# in the directory <RT_BASEDIR>. Get this from the RT_BASEDIR property of the
# resource type.
RT_BASEDIR=`scha_resource_get -O RT_BASEDIR -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

hostnames=`$RT_BASEDIR/gethostnames -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
	-T $RESOURCETYPE_NAME`

start_cmd_args=

start_cmd_prog=`echo $start_cmd_args | nawk '{print $1}'`

if [[ ! -f $start_cmd_prog || ! -x $start_cmd_prog ]]; then
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"File %s is missing or not executable" \
		$start_cmd_prog
	exit 1
fi

# Action script to be passed to pmfadm
action=$RT_BASEDIR/dns_pmf_action.ksh

# User added code -- BEGIN vvvvvvvvvvvvvvv
# User added code -- END   ^^^^^^^^^^^^^^^

# start the daemon under the control of Sun Cluster Process Monitor
# Facility. Let it crash and invoke the action script provided. The
# action script will do a resource restart/failover and will return
# non zero exit status to cease the process monitor facility restarts.

pmfadm -c $PMF_TAG -a "$action -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
-T $RESOURCETYPE_NAME" $start_cmd_args

# Log a message indicating that DNS has been started.
if [ $? -eq 0 ]; then
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		"DNS successfully started"
fi

# User added code -- BEGIN vvvvvvvvvvvvvvv
# User added code -- END   ^^^^^^^^^^^^^^^

exit 0
