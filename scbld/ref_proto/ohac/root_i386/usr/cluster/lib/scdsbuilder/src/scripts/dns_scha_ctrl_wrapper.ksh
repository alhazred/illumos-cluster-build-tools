#!/bin/ksh
#
# Copyright (c) 2008, Sun Microsystems, Inc.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#   * Neither the name of Sun Microsystems, Inc. nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#
# Gate Specific Code -- BEGIN
# ident	"@(#)dns_scha_ctrl_wrapper.ksh	1.3	08/02/23 SMI"
# Gate Specific Code -- END
# Sun Cluster Data Services Builder template version 1.0
#
# scha control wrapper script for DNS.
#
###############################################################################
alias scds_syslog="/usr/cluster/lib/sc/scds_syslog"
alias scha_control="/usr/cluster/bin/scha_control"
SYSLOG_TAG="[SUNW.dns]"
###############################################################################
# Parse program arguments.
#
function parse_args # [args ...]
{
	typeset opt

	while getopts 'R:G:T:O:' opt
	do
		case "$opt" in

		R)
		# Name of the DNS resource.
		RESOURCE_NAME=$OPTARG
		;;

		G)
		# Name of the resource group in which the resource is
		# configured.
		RESOURCEGROUP_NAME=$OPTARG
		;;

		T)
		# Name of the resource type.
		RESOURCETYPE_NAME=$OPTARG
		;;

		O)
		# scha_control option.
		OPTION=$OPTARG
		;;

		*)
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"ERROR: Option %s unknown" \
		$OPTARG
		exit 1
		;;

		esac
	done
	SYSLOG_TAG="[${RESOURCETYPE_NAME},${RESOURCE_NAME},${RESOURCEGROUP_NAME}]"
}

###############################################################################
# MAIN
##############################################################################

rc=1
parse_args "$@"
if [ $OPTION == "RESOURCE_RESTART" ]; then
	scha_control -O RESOURCE_RESTART -G $RESOURCEGROUP_NAME \
			-R $RESOURCE_NAME
	rc=$?
	if [[ $rc -ne 0 ]]; then
		# don't log a trace if the error is SCHA_ERR_CHECKS
		# Probe may fail repeatedly with Failover_mode setting
		# preventing giveover [or restart], and we don't want
		# to fill up syslog
		if [[ $rc -ne 16 ]]; then
			scds_syslog -p error -t "${SYSLOG_TAG}" -m \
				"Restart operation failed for Resource %s" \
				"${RESOURCE_NAME}"
		fi
		# set the status to faulted state.
		scha_resource_setstatus -G $RESOURCEGROUP_NAME \
			-R $RESOURCE_NAME \
			-s FAULTED \
			-m "Restart failed."
	else
		scds_syslog -p info -t "${SYSLOG_TAG}" -m \
			"Successfully restarted service."
	fi
else
	# The fact that the number of resource restarts have exceeded
	# implies that the dataservice has already passed the max no
	# of retries allowed. Hence there is no point in trying to restart
	# the dataservice again. We might as well try to failover
	# to another node in the cluster.
	# The scha_control GIVEOVER attempt might fail for one of the
	# following reasons:
	# 5 - SCHA_ERR_RECONF: a cluster or RG reconfiguration is in progress
	# 16 - SCHA_ERR_CHECKS: a device group switchover is in progress; or
	# a resource dependency is not satisfied; or there is no other
	# healthy node currently available to master the resource group.
	# Any of these conditions may be resolved after a while, so we
	# sleep and retry
	count=0
	scha_control -O GIVEOVER -G $RESOURCEGROUP_NAME \
		-R $RESOURCE_NAME
	rc=$?
        while [[ $rc == 5 || $rc == 16 ]] && [[ $count -lt 20 ]]
        do
		scha_control -O GIVEOVER -G $RESOURCEGROUP_NAME \
			-R $RESOURCE_NAME
		rc=$?
                let count=$count+1
                sleep 30
        done
	# if the resource is not able to failover set it to 
	# faulted state
	# Ignore SCHA_ERR_STATE as the RG might be in reconfiguration
	# state
	if [[ $rc -ne 0 && $rc -ne 9 ]]; then
		scha_resource_setstatus -G $RESOURCEGROUP_NAME \
			-R $RESOURCE_NAME \
			-s FAULTED \
			-m "Unable to Failover to other node."
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Error in failing over the resource group:%s" \
			"${RESOURCEGROUP_NAME}"
	fi
fi
exit 0
