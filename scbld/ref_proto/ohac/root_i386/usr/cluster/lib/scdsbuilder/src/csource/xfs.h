/*
 * Copyright (c) 2008, Sun Microsystems, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Sun Microsystems, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef	_XFS_COMMON_H
#define	_XFS_COMMON_H

#pragma ident	"@(#)xfs.h	1.11	08/02/26 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/* Debug levels for error messages */
#define	DBG_LEVEL_HIGH		9
#define	DBG_LEVEL_MED		5
#define	DBG_LEVEL_LOW		1

#define	SCDS_CMD_SIZE		(8 * 1024)

#define	SCDS_ARRAY_SIZE		1024

#define	NON_TCP_PORT		-999

int svc_validate(scds_handle_t scds_handle);

int svc_start(scds_handle_t scds_handle);

int svc_stop(scds_handle_t scds_handle);

/* Network aware applications code -- BEGIN */
int svc_wait_network_aware(scds_handle_t scds_handle);
int svc_probe_network_aware(scds_handle_t scds_handle, char *hostname,
    int port, int proto, int timeout);
/* Network aware applications code -- END */

/* Stand alone applications code -- BEGIN */
int svc_wait_non_network_aware(scds_handle_t scds_handle);
int svc_probe_non_network_aware(scds_handle_t scds_handle, int timeout);
void set_status_msg(scds_handle_t scds_handle, char *msg);
/* Stand alone applications code -- END */

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

int preprocess_cmd(scds_handle_t scds_handle, char *cmd, int cmd_size);

/* User added code -- BEGIN vvvvvvvvvvvvvvv */
/* User added code -- END   ^^^^^^^^^^^^^^^ */

#ifdef __cplusplus
}
#endif

#endif /* _XFS_COMMON_H */
