/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SOL_VERSION_H
#define	_SOL_VERSION_H

#pragma ident	"@(#)sol_version.h	1.11	08/05/20 SMI"

/*
 * sol_version.h - Conversions between Solaris version and their release
 * dates
 */

#ifdef	__cplusplus
extern "C" {
#endif

#define	__s11 200710
#define	__s10 200411
#define	__s9 200205
#define	__s9u3 200304
#define	__s8u7 200202
/*
 * kernel release (i.e. 2.4.21-4 == 2.04.21.040 for RH30AS).
 */
#define	__rh40as	20609011
#define	__rh30as	20421040
#define	__rh72		20409310

#ifdef __cplusplus
}
#endif

#endif /* _SOL_VERSION_H */
