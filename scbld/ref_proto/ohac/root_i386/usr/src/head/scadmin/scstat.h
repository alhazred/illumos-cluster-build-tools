/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCSTAT_H
#define	_SCSTAT_H

#pragma ident	"@(#)scstat.h	1.30	08/07/24 SMI"

/*
 * This is the header file for libscstat.  All usrs of libscstat should
 * include this file. It contains functions scstat and Monitoring GUI needed
 * to retrieve current state of cluster components incluing:
 * 	Cluster
 *	Node
 *	Transport
 *		Path
 *	HA Device
 *	Quorum Device
 *	RG & Resource
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/errno.h>
#include <sys/types.h>
#include <sys/clconf_int.h>
#include <sys/quorum_int.h>
#include <sys/sc_syslog_msg.h>
#include <scadmin/scconf.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/pnm.h>

#define	SCSTAT_MAX_STRING_LEN	1024

/* Error codes returned by scstat functions. */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
typedef enum scstat_errno {
	SCSTAT_ENOERR,		/* normal return - no error */
	SCSTAT_EUSAGE,		/* syntax error */
	SCSTAT_ENOMEM,		/* not enough memory */
	SCSTAT_ENOTCLUSTER,	/* not a cluster node */
	SCSTAT_ENOTCONFIGURED,	/* not found in CCR */
	SCSTAT_ESERVICENAME,	/* dcs: invalid service name */
	SCSTAT_EINVAL,		/* scconf: invalid argument */
	SCSTAT_EPERM,		/* not root */
	SCSTAT_ECLUSTERRECONFIG, /* cluster is reconfiguring */
	SCSTAT_ERGRECONFIG,	/* RG is reconfiguring */
	SCSTAT_EOBSOLETE,	/* Resource/RG has been updated */
	SCSTAT_EUNEXPECTED,	/* internal or unexpected error */
	SCSTAT_ENGZONE,		/* OP not allowed in a non global zone */
	SCSTAT_ECZONE		/* OP not allowed in a clusterized zone */
} scstat_errno_t;

typedef enum scstat_access_status {
	SCSTAT_ACCESS_DISABLED = 0,	/* access disabled */
	SCSTAT_ACCESS_ENABLED = 1	/* access enabled */
} scstat_access_status_t;

typedef enum scstat_type {
	SCSTAT_TYPE_UNKNOWN,		/* unknown */
	SCSTAT_TYPE_CLUSTER,		/* cluster */
	SCSTAT_TYPE_NODE,		/* node */
	SCSTAT_TYPE_PATH,		/* path */
	SCSTAT_TYPE_HA_DEVICE_SERVICE, 	/* ha device service */
	SCSTAT_TYPE_DEVICE_QUORUM, 	/* device quorum */
	SCSTAT_TYPE_NODE_QUORUM,	/* node quorum */
	SCSTAT_TYPE_RT,			/* Resource type */
	SCSTAT_TYPE_RG,			/* RG */
	SCSTAT_TYPE_RESOURCE,		/* Resource */
	SCSTAT_TYPE_FRAMEWORK		/* ORB Framework */
} scstat_type_t;

/* States a resource can be in */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
typedef enum scstat_state_code {
	/* resource is running */
	SCSTAT_ONLINE = SC_STATE_ONLINE,

	/* resource stopped due to user action */
	SCSTAT_OFFLINE = SC_STATE_OFFLINE,

	/* resource stopped due to a failure */
	SCSTAT_FAULTED = SC_STATE_FAULTED,

	/* resource running with a minor problem */
	SCSTAT_DEGRADED = SC_STATE_DEGRADED,

	/* resource is in transition */
	SCSTAT_WAIT = SC_STATE_WAIT,

	/* resource state is unknown */
	SCSTAT_UNKNOWN = SC_STATE_UNKNOWN,

	/* resource is not monitored */
	SCSTAT_NOTMONITORED = SC_STATE_NOT_MONITORED
} scstat_state_code_t;

/* States a replica of a resource can be in */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
typedef enum scstat_node_pref {
	SCSTAT_PRIMARY,		/* replica is a primary */
	SCSTAT_SECONDARY,	/* replica is a secondary */
	SCSTAT_SPARE,		/* replica is a spare */
	SCSTAT_INACTIVE,	/* replica is inactive */
	SCSTAT_TRANSITION,	/* replica is changing state */
	SCSTAT_INVALID		/* replica is in an invalid state */
} scstat_node_pref_t;

/* component name */
typedef char *scstat_name_t;
typedef scstat_name_t scstat_cluster_name_t;	/* cluster name */
typedef scstat_name_t scstat_node_name_t;	/* node name */
typedef scstat_name_t scstat_adapter_name_t;	/* adapter name */
typedef scstat_name_t scstat_path_name_t;	/* path name */
typedef scstat_name_t scstat_ds_name_t;		/* device service name */
typedef scstat_name_t scstat_quorumdev_name_t;	/* quorum device name */
typedef scstat_name_t scstat_rs_name_t;		/* resource name */
typedef scstat_name_t scstat_rg_name_t;		/* rg name */

/* status string */
typedef char *scstat_statstr_t;
typedef scstat_statstr_t scstat_node_statstr_t;		/* node status */
typedef scstat_statstr_t scstat_path_statstr_t;		/* path status */
typedef scstat_statstr_t scstat_ds_statstr_t;		/* DS status */
typedef scstat_statstr_t scstat_node_quorum_statstr_t;	/* node quorum status */
typedef scstat_statstr_t scstat_quorumdev_statstr_t; 	/* quorum device stat */

/* resource status string by node */
typedef scstat_statstr_t scstat_rs_statstr_by_node_t;

/* resource state string by node */
typedef scstat_statstr_t scstat_rs_state_str_by_node_t;

/* rg status string by node */
typedef scstat_statstr_t scstat_rg_statstr_by_node_t;
/* vote count */
typedef uint_t scstat_vote_t;

/* Cluster node list */
typedef struct scstat_node_list_struct {
	scstat_node_name_t		scstat_node_name;	/* node name */
	struct scstat_node_list_struct *scstat_node_next;	/* next */
} scstat_node_list_t;

/* Cluster node access list */
typedef struct scstat_node_access_struct {
	/* node name */
	scstat_node_name_t			scstat_node_name;
	/* node access status */
	scstat_access_status_t			scstat_access_status;
	/* next */
	struct scstat_node_access_struct	*scstat_node_next;
} scstat_node_access_t;

/* Cluster path endpoint */
typedef struct scstat_path_epoint {
	scstat_adapter_name_t	scstat_adapter_name;	/* adapter name */
} scstat_path_epoint_t;

/* ha device node status list */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
typedef struct scstat_ds_node_state_struct {
	/* node name */
	scstat_node_name_t			scstat_node_name;
	/* node status */
	scstat_node_pref_t			scstat_node_state;
	/* next */
	struct scstat_ds_node_state_struct	*scstat_node_next;
} scstat_ds_node_state_t;

/* rg status list */
typedef struct scstat_rg_status_struct {
	/* node name */
	scstat_node_name_t		scstat_node_name;
	/* rg status */
	sc_state_code_t			scstat_rg_status_by_node;
	/* rg status string */
	scstat_rg_statstr_by_node_t	scstat_rg_statstr;
	/* next */
	struct scstat_rg_status_struct	*scstat_rg_status_by_node_next;
} scstat_rg_status_t;

/* resource status list */
typedef struct scstat_rs_status_struct {
	/* node name */
	scstat_node_name_t		scstat_node_name;
	/* resource status by node */
	sc_state_code_t			scstat_rs_status_by_node;
	/* resource status string */
	scstat_rs_statstr_by_node_t	scstat_rs_statstr;
	/* resource state string */
	scstat_rs_state_str_by_node_t	scstat_rs_state_str;
	/* next */
	struct scstat_rs_status_struct	*scstat_rs_status_by_node_next;
} scstat_rs_status_t;

/* resource list */
typedef struct scstat_rs_struct {
	scstat_rs_name_t	scstat_rs_name;		/* rs name */
	scstat_rs_status_t 	*scstat_rs_status_list;	/* resource status */
	struct scstat_rs_struct *scstat_rs_next;	/* next */
} scstat_rs_t;

/* Cluster node status */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
typedef struct scstat_node_struct {
	scstat_node_name_t	scstat_node_name;	/* node name */
	scstat_state_code_t	scstat_node_status;	/* cluster membership */
	scstat_node_statstr_t	scstat_node_statstr;	/* node status string */
	void			*pad;			/* Padding for */
							/* PSARC/1999/462. */
	struct scstat_node_struct *scstat_node_next;	/* next */
} scstat_node_t;

/* Farm node status */
typedef struct scstat_farm_node {
	scstat_node_name_t	scstat_node_name;	/* node name */
	scstat_node_statstr_t	scstat_node_statstr;	/* membership state */
	void			*pad;			/* Padding for */
	struct scstat_farm_node *scstat_node_next;	/* next */
} scstat_farm_node_t;

/* Cluster path status */
typedef struct scstat_path_struct {
	/* path name */
	scstat_path_name_t		scstat_path_name;
	/* path status */
	sc_state_code_t			scstat_path_status;
	/* path status string */
	scstat_path_statstr_t		scstat_path_statstr;
	/* endpoint */
	scstat_path_epoint_t		scstat_path_epoint1;
	/* endpoint */
	scstat_path_epoint_t		scstat_path_epoint2;
	/* next */
	struct scstat_path_struct	*scstat_path_next;
} scstat_path_t;

/* Cluster transport status */
typedef struct scstat_transport_struct {
	scstat_path_t		*scstat_path_list;	/* paths */
} scstat_transport_t;

/* Cluster ha device status */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
typedef struct scstat_ds_struct {
	/* ha device name */
	scstat_ds_name_t		scstat_ds_name;
	/* ha device status */
	scstat_state_code_t		scstat_ds_status;
	/* ha device statstr */
	scstat_ds_statstr_t		scstat_ds_statstr;
	/* node preference list */
	scstat_ds_node_state_t		*scstat_node_state_list;
	/* next */
	struct scstat_ds_struct		*scstat_ds_next;
} scstat_ds_t;

/* quorum device */
typedef struct scstat_quorumdev_struct {
	/* quorum device name */
	scstat_quorumdev_name_t		scstat_quorumdev_name;
	/* votes configured */
	scstat_vote_t			scstat_vote_configured;
	/* votes contributed */
	scstat_vote_t			scstat_vote_contributed;
	/* config data */
	scstat_node_access_t 		*scstat_node_access_list;
	/* quorum device status by probing - UP/DOWN */
	scstat_state_code_t			scstat_quorumdev_status;
	/* quorum device statstr */
	scstat_quorumdev_statstr_t	scstat_quorumdev_statstr;
	/* next */
	struct scstat_quorumdev_struct	*scstat_quorumdev_next;
} scstat_quorumdev_t;

/* node quorum */
typedef struct scstat_node_quorum_struct {
	/* node name */
	scstat_node_name_t			scstat_node_name;
	/* votes configured */
	scstat_vote_t				scstat_vote_configured;
	/* votes configured */
	scstat_vote_t				scstat_vote_contributed;
	/* node contributing or not - UP/DOWN */
	scstat_state_code_t			scstat_node_quorum_status;
	/* node quorum statstr */
	scstat_node_quorum_statstr_t		scstat_node_quorum_statstr;
	/* next */
	struct scstat_node_quorum_struct	*scstat_node_quorum_next;
} scstat_node_quorum_t;

/* Cluster quorum status */
typedef struct scstat_quorum_struct {
	/* current votes */
	scstat_vote_t		scstat_vote;
	/* votes configured */
	scstat_vote_t		scstat_vote_configured;
	/* votes needed */
	scstat_vote_t		scstat_vote_needed;
	/* node quorums */
	scstat_node_quorum_t 	*scstat_node_quorum_list;
	/* device quorums */
	scstat_quorumdev_t	*scstat_quorumdev_list;
} scstat_quorum_t;

/* RG status */
typedef struct scstat_rg_struct {
	scstat_rg_name_t	scstat_rg_name;		/* RG name */
	scstat_rg_status_t 	*scstat_rg_status_list;	/* RG status list */
	scstat_rs_t		*scstat_rs_list;	/* resource list */
	struct scstat_rg_struct *scstat_rg_next;	/* next */
} scstat_rg_t;

/* Cluster status */
typedef struct scstat_cluster_struct {
	/* cluster name */
	scstat_cluster_name_t		scstat_cluster_name;
	/* nodes */
	scstat_node_t 			*scstat_node_list;
	/* transport */
	scstat_transport_t		*scstat_transport;
	/* ha device */
	scstat_ds_t			*scstat_ds_list;
	/* quorum device */
	scstat_quorum_t			*scstat_quorum;
	/* rg */
	scstat_rg_t 			*scstat_rg_list;
	/* next */
	struct scstat_cluster_struct	*scstat_cluster_next;
} scstat_cluster_t;

/* IPMP group list struct - name, status and a list of adp structs */
typedef struct scstat_grp_stat_list {
	char			*grp_name; /* name of the grp */
	int			status;  /* up or down */
	pnm_grp_status_t	pgst; /* adp list in grp */
	struct scstat_grp_stat_list *next;
} scstat_grp_stat_list_t;

/* IPMP Node list struct - name, list of ipmp group structs */
typedef struct scstat_ipmp_stat_list {
	char			*name; /* node */
	scstat_grp_stat_list_t	*pgslt; /* grp list in node */
	struct scstat_ipmp_stat_list *next;
} scstat_ipmp_stat_list_t;

/*
 * scstat_get_default_status_string
 *
 * Get status string from status code. The supplied "text" should
 * be of at least SCSTAT_MAX_STRING_LEN.
 */
void scstat_get_default_status_string(const scstat_state_code_t, char *);

/*
 * scstat_get_access_status_string
 *
 * Get access status string. The supplied "text" should
 * be of at least SCSTAT_MAX_STRING_LEN.
 */
void scstat_get_access_status_string(const scstat_access_status_t, char *);

/*
 * scstat_get_preference_level_string
 *
 * Get preference level string. The supplied "text" should
 * be of at least SCSTAT_MAX_STRING_LEN.
 */
void scstat_get_preference_level_string(const scstat_node_pref_t, char *);

/*
 * scstat_strerr
 *
 * Map scstat_errno_t to a string.
 *
 * The supplied "errbuffer" should be of at least SCSTAT_MAX_STRING_LEN
 * in length.
 */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
void scstat_strerr(scstat_errno_t, char *);

/*
 * scstat_convert_scconf_error_code
 *
 * convert a scconf error code to scstat error code
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_ENOTCLUSTER	- not a cluster node
 *	SCSTAT_EINVAL		- scconf: invalid argument
 *	SCSTAT_EPERM		- not root
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_convert_scconf_error_code(const scconf_errno_t);

/*
 * scstat_get_path_endpoints
 *
 * Upon success, two adapter names are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 */
scstat_errno_t
scstat_get_path_endpoints(const char *, char **, char **);

/*
 * scstat_get_default_rgstate_string
 *
 * Get rgstate string from rgstate code. The supplied "text" should
 * be of at least SCSTAT_MAX_STRING_LEN.
 */
void scstat_get_default_rgstate_string(const scha_rgstate_t, char *);

/*
 * scstat_get_transport_adapter_name
 *
 *	Get name of a transport adapter.
 */
scstat_errno_t
scstat_get_transport_adapter_name(const char *, char *);

/*
 * scstat_get_cached_transport_adapter_name
 *
 *	Get name of a transport adapter from cached cluster configuration.
 */
scstat_errno_t
scstat_get_cached_transport_adapter_name(const scconf_cfg_cluster_t *,
    const char *, char *);

/*
 * Cluster status
 *
 *	scstat_get_cluster_status()
 *	scstat_get_cluster_status_with_cached_config()
 *	scstat_free_cluster_status()
 */

/*
 * Upon success, an object of scstat_cluster_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *      SCSTAT_ENOERR           - success
 *      SCSTAT_ENOMEM           - not enough memory
 *	SCSTAT_ENOTCLUSTER	- not a cluster node
 *	SCSTAT_ESERVICENAME	- invalid device group name
 *	SCSTAT_EINVAL		- scconf: invalid argument
 *	SCSTAT_EPERM		- not root
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *      SCCONF_EUNEXPECTED      - internal or unexpected error
 */
scstat_errno_t scstat_get_cluster_status(scstat_cluster_t **ppcluster);

/*
 * Get cluster status based on cached configuration.
 * Upon success, an object of scstat_cluster_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *      SCSTAT_ENOERR           - success
 *      SCSTAT_ENOMEM           - not enough memory
 *	SCSTAT_ENOTCLUSTER	- not a cluster node
 *	SCSTAT_ESERVICENAME	- invalid device group name
 *	SCSTAT_EINVAL		- scconf: invalid argument
 *	SCSTAT_EPERM		- not root
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *      SCCONF_EUNEXPECTED      - internal or unexpected error
 */
scstat_errno_t scstat_get_cluster_status_with_cached_config(\
    const scconf_cfg_cluster_t *, scstat_cluster_t **ppcluster);

/*
 * Free all memory associated with a scstat_cluster_t structure.
 */
void scstat_free_cluster_status(scstat_cluster_t *pcluster);

/*
 * node status
 *
 *	scstat_get_nodes()
 *	scstat_get_nodes_with_cached_config()
 *	scstat_free_nodes()
 *	scstat_get_farmnodes()
 *	scstat_free_farmnodes()
 *	scstat_get_node()
 *	scstat_get_node_with_cached_config()
 *	scstat_free_node()
 *	scstat_free_farmnode()
 */

/*
 * Upon success, a list of objects of scstat_node_t are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
scstat_errno_t scstat_get_nodes(scstat_node_t **pplnodes);

/*
 * Get status of all nodes based on cached configuration.
 * Upon success, a list of objects of scstat_node_t are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_nodes_with_cached_config(\
    const scconf_cfg_cluster_t *, scstat_node_t **);

/*
 * Free all memory associated with a scstat_node_t structure.
 */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
void scstat_free_nodes(scstat_node_t *plnodes);

/*
 * Get status of all farm nodes based on cached configuration.
 * Upon success, a list of objects of scstat_farm_node_t are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_farmnodes(scstat_farm_node_t **fnodes,
    uint_t *offline_count, uint_t *online_count);

/*
 * Free all memory associated with a scstat_farm_node_t structure.
 */
void scstat_free_farmnodes(scstat_farm_node_t *fnodes);

/*
 * Upon success, an object of scstat_node_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 * 	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_node(const scstat_node_name_t node_name,
    scstat_node_t **ppnode);

/*
 * Get status of a node based on cached configuration.
 * Upon success, an object of scstat_node_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 * 	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_node_with_cached_config(const scconf_cfg_cluster_t *,
    const scstat_node_name_t, scstat_node_t **);

/*
 * Free all memory associated with a scstat_node_t structure.
 */
void scstat_free_node(scstat_node_t *pnode);

/*
 * Free all memory associated with a scstat_farm_node_t structure.
 */
void scstat_free_farmnode(scstat_farm_node_t *fnode);

/*
 *
 * transport status
 *
 *	scstat_get_transport()
 *	scstat_free_transport()
 *	scstat_get_path()
 *	scstat_free_path()
 *
 */

/*
 * Upon success, an object of scstat_transport_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_transport(scstat_transport_t **);

/*
 * Free all memory associated with a scstat_transport_t structure.
 */
void scstat_free_transport(scstat_transport_t *);

/*
 * Upon success, an object of scstat_path_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_path(const scstat_path_name_t,
    scstat_path_t **);

/*
 * Free all memory associated with a scstat_path_t structure.
 */
void scstat_free_path(scstat_path_t *);

/*
 *
 * ha device status
 *
 *	scstat_get_ds_status()
 *	scstat_get_ds_status_with_cached_config()
 *	scstat_free_ds_status()
 *
 */

/*
 * If the device service name passed in is NULL, then this function returns
 * the status of all device services, otherwise it returns the status of the
 * device service specified.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_ESERVICENAME	- invalid device group name
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
scstat_errno_t scstat_get_ds_status(const scstat_ds_name_t *dsname,
    scstat_ds_t **dsstatus);

/*
 * Get device service status based on cached configuration.
 * If the device service name passed in is NULL, then this function returns
 * the status of all device services, otherwise it returns the status of the
 * device service specified.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_ESERVICENAME	- invalid device group name
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_ds_status_with_cached_config(\
    const scconf_cfg_cluster_t *, const scstat_ds_name_t *,
    scstat_ds_t **);

/*
 * Free memory associated with a scstat_ds_t structure.
 */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
void scstat_free_ds_status(scstat_ds_t *dsstatus);

/*
 *
 * quorum device status
 *	scstat_get_quorum()
 *	scstat_get_quorum_with_cached_config()
 *	scstat_free_quorum()
 *	scstat_get_quorumdev()
 *	scstat_get_quorumdev_with_cached_config()
 *	scstat_free_quorumdev()
 *	scstat_get_node_quorum()
 *	scstat_get_node_quorum_with_cached_config()
 *	scstat_free_node_quorum()
 *
 */

/*
 * Upon success, an object of scstat_quorum_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_quorum(scstat_quorum_t **);

/*
 * Get quorum status based on cached configuration.
 * Upon success, an object of scstat_quorum_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_quorum_with_cached_config(\
    const scconf_cfg_cluster_t *, scstat_quorum_t **);

/*
 * Free all memory associated with a scstat_quorum_t structure.
 */
void scstat_free_quorum(scstat_quorum_t *);

/*
 * Upon success, an object of scstat_quorumdev_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_quorumdev(
    const scstat_quorumdev_name_t,
    scstat_quorumdev_t **);

/*
 * Get quorum device status based on cached configuration.
 * Upon success, an object of scstat_quorumdev_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_quorumdev_with_cached_config(\
    const scconf_cfg_cluster_t *, const scstat_quorumdev_name_t,
    scstat_quorumdev_t **);

/*
 * Free all memory associated with a scstat_quorumdev_t structure.
 */
void scstat_free_quorumdev(scstat_quorumdev_t *);

/*
 * Upon success, an object of scstat_node_quorum_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_node_quorum(
    const scstat_node_name_t, scstat_node_quorum_t **);

/*
 * Get node quorum based on cached configuration.
 * Upon success, an object of scstat_node_quorum_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_node_quorum_with_cached_config(
    const scconf_cfg_cluster_t *,
    const scstat_node_name_t, scstat_node_quorum_t **);

/*
 * Free all memory associated with a scstat_node_quorum_t structure.
 */
void scstat_free_node_quorum(scstat_node_quorum_t *);


/*
 *
 * RG status
 *
 *	scstat_get_rgs()
 *	scstat_free_rgs()
 *	scstat_get_rg()
 *	scstat_free_rg()
 *	scstat_get_rss()
 *	scstat_free_rss()
 *	scstat_get_rs()
 *	scstat_free_rs()
 *
 */

/*
 * Upon success, a list of objects of scstat_rg_t are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_rgs(scstat_rg_t **);

/*
 * Free all memory associated with a scstat_rg_t structure.
 */
void scstat_free_rgs(scstat_rg_t *);

/*
 * Upon success, an object of scstat_rg_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_rg(const scstat_rg_name_t,
    scstat_rg_t **, boolean_t);

/*
 * Free all memory associated with a scstat_rg_t structure.
 */
void scstat_free_rg(scstat_rg_t *);

/*
 * Upon success, a list of objects of scstat_rs_t are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_rss(const scstat_rg_name_t,
    scstat_rs_t **);

/*
 * Free all memory associated with a scstat_rs_t structure.
 */
void scstat_free_rss(scstat_rs_t *);

/*
 * Upon success, an object of scstat_rs_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_rs(const scstat_rg_name_t,
    const scstat_rs_name_t,
    scstat_rs_t **, boolean_t);

/*
 * Free all memory associated with a scstat_rs_t structure.
 */
void scstat_free_rs(scstat_rs_t *);

/*
 * scstat_get_rg_boolprop
 *
 * Get the value of an boolean RG property.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_rg_boolprop(const char *rg_name, const char *tag,
    boolean_t *val);

/*
 * scstat_ipmp_stat_list_free()
 *
 * Free memory associated with the given scstat_ipmp_stat_list_t. The argument
 * is also freed.
 * INPUT: pointer to a scstat_ipmp_stat_list_t struct whose memory we want to
 * free.
 * RETURN: void.
 */
void	scstat_ipmp_stat_list_free(scstat_ipmp_stat_list_t *);

/*
 * scstat_get_ipmp_state()
 *
 * Return a scstat_ipmp_stat_list_t structure. This scstat_ipmp_stat_list_t
 * must be freed using scstat_ipmp_stat_list_free() after use. If the function
 * returns error then scstat_ipmp_stat_list_t is not valid. The
 * scstat_ipmp_stat_list_t struct contains the ipmp state of the whole cluster.
 * This is not MT-safe since it uses libpnm which is not MT-safe.
 * RETURN: 0 on sucess, non-zero on error.
 * INPUT: pointer to a scstat_ipmp_stat_list_t struct pointer - this struct has
 *        the general ipmp status of the cluster. This struct must be freed
 *        later by calling scstat_ipmp_stat_list_free().
 */
scstat_errno_t scstat_get_ipmp_state(scstat_ipmp_stat_list_t **);

#ifdef __cplusplus
}
#endif

#endif	/* _SCSTAT_H */
