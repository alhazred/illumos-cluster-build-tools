/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCRGADM_H
#define	_SCRGADM_H

#pragma ident	"@(#)scrgadm.h	1.19	08/07/28 SMI"

/*
 * This is the header file for libscrgadm.  All usrs of libscrgadm should
 * include this file.
 */

#ifdef __cplusplus
extern "C" {
#endif


#define	SCRGADM_RTR_DIR		"/usr/cluster/lib/rgm/rtreg"
#define	SCRGADM_RTR_OPT_DIR	"/opt/cluster/lib/rgm/rtreg"

#define	SCRGADM_RTLH_NAME	"SUNW.LogicalHostname"
#define	SCRGADM_RTSA_NAME	"SUNW.SharedAddress"
#define	SCRGADM_NETIFLIST	"NetIfList"
#define	SCRGADM_HOSTNAMELIST	"HostnameList"
#define	SCRGADM_AUXNODELIST	"AuxNodeList"

typedef enum rg_ip_type {
	IP_TYPE_SHARED = 0,
	IP_TYPE_EXCL,
	IP_TYPE_MIX,
	IP_TYPE_UNKNOWN
} rg_ip_type_t;

#include <rgm/rgm_scrgadm.h>
#include <rgm/rtreg.h>
#include <rgm/pnm.h>
#include <scadmin/scconf.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <libintl.h>


/*
 * scrgadm_process_rtreg()
 *
 * Input: RT name and optional RT reg file path
 *
 * Action:
 *	resolve path to file if needed
 *	parse file and fill RtrFileResult
 *	if !SCHA_ERR_NOERR then scha_errmsg_t will hold eror string
 *
 * Caller must call scrgadm_free_rtrresult(RtrFileResult)
 *	or free(scha_errmsg_t err_msg)
 *
 * Output:  RtrFileResult or scha_errmsg_t
 *
 * Returns:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *	SCHA_ERR_INTERNAL
 */
scha_errmsg_t
scrgadm_process_rtreg(char *rt_name, char *rt_file,
    RtrFileResult **rtr_results, char *zc_name);



/*
 * scrgadm_free_rtrresult()
 *
 * Input: RtrFileResult*
 *
 * Action: free the data structure created by scrgadm_process_rtreg()
 *
 * Output: none
 *
 * Returns: nothing
 */
void
scrgadm_free_rtrresult(RtrFileResult *rtr_results);

/*
 * Fetch type name by looking into the rtr file.
 *
 * Output: There should be enough space in full_rtname, the resourcetype
 *	name is copied there.
 *
 * Returns: SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *
 */
scha_errmsg_t
scrgadm_get_type_in_file(char *rtrfile, char *full_rtname);

/*
 * Sets zc_allowed parameter to 1 if either the GLOBAL_ZONE property
 * is set to TRUE or if the RTR File belongs to a System RT
 * Otherwise zc_allowed is set to 0
 */
scha_errmsg_t
scrgadm_get_globalzone_in_file(char *rtrfile, int *zc_allowed);

/*
 * scrgadm_set_lhrg_props()
 *
 * Input: none
 *
 * Action:
 *	create two aligned char arrays:
 *		required property names
 *		required property value-string
 *
 * Output: two char arrays
 *
 * Caller must free storage.
 *
 * Possible return values:
 *	none
 */
void
scrgadm_set_lhrg_props(char ***props, char ***values);


/*
 * scrgadm_set_sarg_props()
 *
 * Input: none
 *
 * Action:
 *	create two aligned char arrays:
 *		required property names
 *		required property value-string
 *
 * Output: two char arrays
 *
 * Caller must free storage.
 *
 * Possible return values:
 *	none
 */
void
scrgadm_set_sarg_props(char ***props, char ***values);

/*
 * scrgadm_rdep_list_to_string
 *
 *      Return a single string for the list of resource dependency names.
 *      The caller is responsible for freeing the list of names.
 */
char *
scrgadm_rdep_list_to_string(rdeplist_t *r_dependencies, char *null_str,
    char *separator);

/*
 * The following routines and definitions have been relocated from the
 * scrgadm.c file for modularity purposes.
 */

typedef name_t scrgadm_name_t;
typedef	namelist_t scrgadm_value_list_t;
typedef	namelist_t scrgadm_node_list_t;

/*
 * name_valuel_lists are used for properties:
 *	one property name may have multiple values
 *	a property can  have different values on
 *	different nodes.
 */
typedef struct scrgadm_name_valuel_list {
	scrgadm_name_t			name;
	scrgadm_value_list_t		*value_list;
	scrgadm_node_list_t		*node_list;
	boolean_t			is_per_node;
	struct scrgadm_name_valuel_list *next;
} scrgadm_name_valuel_list_t;

/*
 * node_ipmp_lists hold "netadapter@nodeidentifier"
 *	information from the command line in the 'raw' fields
 *	and resolved names in 'ipmp' and 'nodeid' fields
 */
typedef struct scrgadm_node_ipmp_list {
	scrgadm_name_t			raw_netid;
	scrgadm_name_t			ipmp_group;
	scrgadm_name_t			raw_node;
	scrgadm_name_t			zone;
	scconf_nodeid_t			nodeid;
	scrgadm_name_t			hostname;
	struct scrgadm_node_ipmp_list	*next;
} scrgadm_node_ipmp_list_t;

/*
 * scrgadm_hostname_lists hold hostname/IP
 *	information from the command line in the 'raw' field,
 *	resolved name and numeric IP in 'hostname' and 'ip_addr' fields
 */
typedef struct scrgadm_hostname_list {
	scrgadm_name_t			raw_name;
	scrgadm_name_t			hostname;
	in_addr_t			*ip_addr;
	in6_addr_t			*ip6_addr;
	struct scrgadm_hostname_list	*next;
} scrgadm_hostname_list_t;

/*
 * scrgadm_adapterinfo_list_t
 *
 * This structure is used for making the -n option optional when
 * creating LHRS and SARS. Each element in this list represents adapter
 * information for adapters on each node for a given RG.
 * See comments of process_netadapter_list() for more details.
 */
typedef struct scrgadm_adapterinfo_list {
	scrgadm_name_t			ai_node;	/* node name */
	scrgadm_name_t			ai_zone;	/* Zone name */
	scrgadm_name_t			ai_nodezone;	/* Node:Zone name */
	pnm_adapterinfo_t		*ai_list;	/* adapter list */
	struct scrgadm_adapterinfo_list	*next;
} scrgadm_adapterinfo_list_t;

/*
 * pnm_adapter_instances_t
 *
 * This structure is used for keeping the various adapters and their instances
 * together. It has a pointer to the v4 and v6 instance of the adapter.
 */
typedef struct pnm_adapter_instances {
	pnm_adapterinfo_t		*v4instp;
	pnm_adapterinfo_t		*v6instp;
	struct pnm_adapter_instances	*next;
} pnm_adapter_instances_t;

typedef enum adapter_host_status {
	ADAPTER_HOST_OK,		/* Adapter can host hostnames */
	ADAPTER_HOST_SUBNET,		/* Adapter is on wrong subnet */
	ADAPTER_HOST_UNINITIATED	/* Adapter has not been tested */
} adapter_host_status_t;

scha_errmsg_t process_netadapter_list(
    const scrgadm_name_t rg_name,
    scrgadm_hostname_list_t *lhnames,
    scrgadm_node_ipmp_list_t *lipmp,
    scrgadm_adapterinfo_list_t **ailistp,
    char *zc_name);

scha_errmsg_t validate_unique_haip(
    scrgadm_hostname_list_t *hostlist);

scha_errmsg_t convert_adapterinfo_to_ipmp(
    scrgadm_adapterinfo_list_t *ailist,
    scrgadm_node_ipmp_list_t **nalist,
    scrgadm_hostname_list_t *hnlist,
    char *zcname);

void free_adapterinfo_list(scrgadm_adapterinfo_list_t *);
boolean_t str_to_nodeid(char *, scconf_nodeid_t *);
scha_errmsg_t scconf_err_to_scha_err(scconf_errno_t);
scha_errmsg_t process_namelist_to_nodeids(namelist_t *);
scha_errmsg_t validate_auxnodelist(scrgadm_adapterinfo_list_t *,
    namelist_t *);
scha_errmsg_t load_node_ipmp_elt(scrgadm_node_ipmp_list_t *, char *);
scha_errmsg_t create_netiflist_val(const scrgadm_node_ipmp_list_t *,
    char *[]);
scha_errmsg_t create_hnlist_val(const scrgadm_hostname_list_t *,
    char **);
char *get_latest_rtname(const char *, char *);
void print_scha_error(scha_errmsg_t);
scha_errmsg_t validate_nodelist(name_t rg_name, char *zc_name);
scha_errmsg_t validate_farmnodelist(name_t rg_name);
scha_errmsg_t get_rg_nodetype(name_t rg_name, scconf_nodetype_t *rg_nodetype,
    char *zc_name);
scha_errmsg_t create_auxnodelist_val(const namelist_t *, char **);
rg_ip_type_t get_ip_type(name_t rg_name, char *zc_namep);

/* SC SLM addon start */
void slm_replace_dash_in_str(char *);
/* SC SLM addon end */

#ifdef __cplusplus
}
#endif

#endif /* _SCRGADM_H */
