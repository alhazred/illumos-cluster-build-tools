/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RGM_COMMON_H_
#define	_RGM_COMMON_H_

#pragma ident	"@(#)rgm_common.h	1.70	08/06/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif


#include <scha.h>
#include <rgm/scha_priv.h>


/*
 * The following typedef is redundant with the rgm::lni_t typedef.
 * librgm clients such as scrgadm and scswitch include this file, but
 * cannot include <h/rgm.h>.
 * For an 'unsigned long' in the idl file, the idl compiler declares a
 * uint32_t in the generated .h file.
 */
typedef uint32_t rgm_lni_t;
typedef uint32_t rrestart_t;
typedef uint32_t rrg_state;
typedef uint32_t rr_state;
typedef uint32_t rr_fm_status;

#define	MDB_RGM_LOGSIZE 150

/* Used in intercluster rg affinities. */
#define	WP "+"
#define	WN "-"
#define	SP "++"
#define	SPD "+++"
#define	SN "--"

/*
 * rgm_stamp_t -
 *	The library uses the sequence number in CCR as a stamp of the
 *	RGM object.  This sequence number is maintained by CCR facility.
 *	Every time the table is updated, this number gets bumped
 *	automatically.  For every update request, it is required that
 *	the caller passes the object stamp, which is retrieved from CCR
 *	lately, as an argument.  Attemping to update a table with a sequence
 *	number different from the current value in CCR table will result in
 *	an error return code and the update request will be aborted.
 */
typedef char *rgm_stamp_t;

typedef char *name_t;

/*
 * rgm_timelist_t
 * Linked list of timestamps, used in rg_xstate_t and rlist_t.
 * Used by the rebalance()
 * function on the president to remember the most recent times at which an RG
 * failed to start on each node.
 *
 * Also used on each slave to store history of scha_control
 * restart calls (both resource and RG restarts) that occurred locally on the
 * slave; to be retrieved by scha_resource_get(num_rg_restarts) and
 * scha_resource_get(num_resource_restarts) queries.
 */
typedef struct rgm_timelist {
	time_t			tl_time;	/* when did event most */
						/* recently occur? */
	struct rgm_timelist	*tl_next;	/* link to next struct */
} rgm_timelist_t;


/*
 * Possible values of Tunable in resource property definition which
 * indicats when the property value can be changed.
 */
typedef enum rgm_tune {
	TUNE_NONE = 0,
	TUNE_AT_CREATION,
	TUNE_ANYTIME,
	TUNE_WHEN_OFFLINE,
	TUNE_WHEN_UNMANAGED,
	TUNE_WHEN_UNMONITORED,
	TUNE_WHEN_DISABLED
} rgm_tune_t;

/*
 * Possible values of RT property 'Sysdefined_type'.
 * This property is specifically to support the implementation
 * of predefined RTs such as the Shared_address and Logical_hostname
 * resource types.
 */
typedef enum rgm_sysdeftype {
	SYST_NONE = 0,
	SYST_LOGICAL_HOSTNAME,
	SYST_SHARED_ADDRESS
} rgm_sysdeftype_t;

/*
 * namelist_t -
 *	for storing a list of hostnames, a list of pathnames
 */
typedef struct namelist {
	name_t			nl_name;
	struct namelist		*nl_next;
} namelist_t;

/*
 * namelist_all_t -
 *      Similar to namelist_t, but is used for properties where a
 *      wildcard "*" value is permitted.  Currently this is used only
 *	for the RG Global_resources_used property.
 *      A boolean flag 'is_ALL_value' is for identifying
 *      that asterisk (*) is specifed for the property value.
 *      If this flag is TRUE, 'names' will be a NULL pointer.
 */
typedef struct namelist_all {
	boolean_t		is_ALL_value;
	namelist_t		*names;
} namelist_all_t;


/*
 * nodeidlist_t -
 *	For storing the RG nodelist.
 *	In librgm, we store the nodeid and the zonename.
 *	For global zones, or where zones are not used, nl_zonename is NULL.
 *
 *	Inside the rgmd, the nodeid and zonename are converted to an rgm::lni_t
 *	(logical node id) and stored in the nl_lni field.
 */
typedef struct nodeidlist {
	unsigned int		nl_nodeid;
	name_t			nl_zonename;
	rgm_lni_t		nl_lni;	/* only used by rgmd */
	struct nodeidlist 	*nl_next;
} nodeidlist_t;


/*
 * nodeidlist_all_t -
 *	For storing the RT_Installed nodes.
 * 	A boolean flag 'is_ALL_value' is for identifying
 *	that asterisk (*) is specifed for the property value
 *	If this flag is TRUE, 'names' will be a NULL pointer.
 */
typedef struct nodeidlist_all {
	boolean_t	is_ALL_value;
	nodeidlist_t	*nodeids;
} nodeidlist_all_t;


/*
 * rgm_methods_t -
 *	Method names are properties of Resource Type.
 *	The timeout values of these methods are properties of Resource,
 *	which will be defined in Paramtable of Resource Type.
 */
typedef struct rgm_methods {
	name_t		m_start;		/* START */
	name_t		m_stop;			/* STOP */
	name_t		m_validate;		/* VALIDATE */
	name_t		m_update;		/* UPDATE */
	name_t		m_init;			/* INIT */
	name_t		m_fini;			/* FINI */
	name_t		m_boot;			/* BOOT */
	name_t		m_monitor_start;	/* MONITOR_START */
	name_t		m_monitor_stop;		/* MONITOR_STOP */
	name_t		m_monitor_check;	/* MONITOR_CHECK */
	name_t		m_prenet_start;		/* PRENET_START */
	name_t		m_postnet_stop;		/* POSTNET_STOP */
} rgm_methods_t;


/*
 * rgm_param_t -
 *	To store the resource property definitions listed in the Paramtable.
 */
typedef struct rgm_param {
	char		*p_name;
	boolean_t	p_extension;
	rgm_tune_t	p_tunable;
	scha_prop_type_t	p_type;
	int		p_defaultint;
			/* numeric form for int-type default values. */
	boolean_t	p_defaultbool;
			/* Should be B_TRUE or B_FALSE for booleans */
	char		*p_defaultstr;
			/* The STRING or ENUM version of the default value */
	namelist_t	*p_defaultarray;
	int		p_min;
			/* min INT type , minlen for STRING and STRINGARRY */
	int		p_max;
			/* max INT type , maxlen for STRING and STRINGARRY */
	int		p_arraymin;
			/* minimum array size for STRINGARRAY type */
	int		p_arraymax;
			/* maximum array size for STRINGARRAY type */
	namelist_t	*p_enumlist;
	char		*p_description;
	/* Flags to indicate whether optional elements are set */
	boolean_t	p_default_isset;
	boolean_t	p_min_isset;
	boolean_t	p_max_isset;
	boolean_t	p_arraymin_isset;
	boolean_t	p_arraymax_isset;
	boolean_t	p_per_node;
} rgm_param_t;

/*
 * rdep_locality_t -
 *
 *    This structure defines the two different semantics for applying the
 *    resource dependencies. With ANY_NODE semantics, the dependent would wait
 *    for the dependee to start on any of the multiple nodes before starting
 *    itself. With LOCAL_NODE semantics, the dependent would wait for the
 *    dependee to start on the same node.
 *
 */
typedef enum rdep_locality {
	FROM_RG_AFFINITIES,
	LOCAL_NODE,
	ANY_NODE
} rdep_locality_t;


/*
 *    With the introduction of the definition of LOCAL_NODE and ANY_NODE
 *    semantics to define a resource dependency, the rgm_dependencies_t
 *    structure would be extended as shown below.
 */
typedef struct rdeplist {
	name_t rl_name;
	rdep_locality_t locality_type;
	struct rdeplist *rl_next;
} rdeplist_t;
/*
 * rgm_dependencies_t -
 *
 *      This type is used to store the "r_dependencies".
 *
 *	dp_restart, dp_weak and dp_strong are all pointers to a NULL
 *	terminated array of pointers to NULL terminated strings.
 *
 *	dp_weak is used to store the resources that the specified resource
 *	weakly depends on, dp_strong is used to store the resources
 *	the specified resource strongly depends on, and dp_restart stores
 *	resources on which the specified resource has a restart dependency.
 *
 */
typedef struct rgm_dependencies {
	rdeplist_t	*dp_weak;
	rdeplist_t	*dp_strong;
	rdeplist_t	*dp_restart;
	rdeplist_t	*dp_offline_restart;
} rgm_dependencies_t;


/*
 * deptype_t
 *
 *    This type is used to represent the type of dependency.
 */
typedef enum deptype {
	DEPS_WEAK,
	DEPS_STRONG,
	DEPS_RESTART,
	DEPS_OFFLINE_RESTART
} deptype_t;

/*
 * rgm_rt_upgrade_t -
 *	for storing a list of rt_version and its tunability listed in
 *	RTR file (#$upgrade_from)
 */
typedef struct rgm_rt_upgrade {
	char			*rtu_version;
	rgm_tune_t		rtu_tunability;
	struct rgm_rt_upgrade	*rtu_next;
} rgm_rt_upgrade_t;


/*
 * rgm_rt_t
 *	Resource Type properties
 */
typedef struct rgm_rt {
	name_t			rt_name;
	name_t			rt_basedir;
	rgm_methods_t		rt_methods;
	boolean_t		rt_single_inst;
	scha_initnodes_flag_t	rt_init_nodes;
	nodeidlist_all_t	rt_instl_nodes; /* configurable at run time */
	boolean_t		rt_failover;
	boolean_t		rt_globalzone;
	boolean_t		rt_proxy;
	rgm_sysdeftype_t	rt_sysdeftype;
	namelist_t		*rt_dependencies;
	uint_t			rt_api_version;
	name_t			rt_version;
	namelist_t		*rt_pkglist;
	rgm_param_t		**rt_paramtable;
	char			*rt_description;
	rgm_stamp_t		rt_stamp;
	boolean_t		rt_sc30;
	rgm_rt_upgrade_t	*rt_upgrade_from;
	rgm_rt_upgrade_t	*rt_downgrade_to;
	struct rgm_rt		*rt_next; /* null terminated;used by rgmd */
	boolean_t		rt_system;
} rgm_rt_t;

/*
 * The value of the resource property is declared as void pointers for
 * C source files. The actual declarations are in rgm_cpp.h header.
 */
typedef void *value_t;
/*
 * rgm_property_t -
 *	To store the information of a single resource property value.
 */
typedef struct rgm_property {
	name_t			rp_key;
	value_t			rp_value;
	namelist_t		*rp_array_values; /* for SCHA_STRINGARRAY */
	scha_prop_type_t	rp_type;
	name_t			rp_description;
	boolean_t		is_per_node;
} rgm_property_t;


/*
 * mdb_props_t -
 * mdb snapshot of the rs property structures.
 */
typedef struct mdb_props {
	char  **value;
	uint_t size;
} mdb_props_t;


/*
 * Backup of resource On_off/Monitored switch and
 * state infomation.
 */
typedef struct mdb_rxback {
	char *mdb_onoffswitch;
	char *mdb_monswitch;
	mdb_props_t mdb_stdprops;
	mdb_props_t mdb_extprops;
} mdb_rxback_t;

typedef struct rgm_property_list {
	rgm_property_t			*rpl_property;
	struct rgm_property_list	*rpl_next;
} rgm_property_list_t;

/*
 * mdb_rxstate_t -
 * mdb snapshot of the rs state structure.
 * This structure has be kept in sync with the r_xstate
 * definition in rgm_state.h file.
 */
typedef struct mdb_rxstate {
	rgm_lni_t	lni;
	rr_state	rx_state;
	rr_fm_status	rx_fm_status;
	char			*rx_fm_status_msg;
	boolean_t		rx_restarting;
	rgm_timelist_t	*rx_rg_restarts;
	time_t		rx_last_restart_time;
	time_t		rx_throttle_restart_counter;
	rgm_timelist_t	*rx_r_restarts;
	rgm_timelist_t	*rx_blocked_restart;
	rrestart_t	rx_restart_flg;
	boolean_t	rx_ignore_failed_start;
	boolean_t	rx_ok_to_start;
} mdb_rxstate_t;


/*
 * mdb_rgxstate_t -
 * mdb snapshot of the rg state structure.
 * This structure has be kept in sync with the rg_xstate
 * definition in rgm_state.h file.
 */
typedef struct mdb_rgxstate {
	rgm_lni_t	lni;
	rrg_state rgx_state;
	rgm_timelist_t	*rgx_start_fail;
	boolean_t	rgx_net_relative_restart;
	boolean_t	rgx_postponed_stop;
	boolean_t	rgx_postponed_boot;
} mdb_rgxstate_t;


/*
 * mdb_rgxlogs_t -
 * In memory logging used to log each resource group state update.
 * mdb uses this log along with the resource group state backup to
 * construct the actual snapshot of the resource group state structure.
 * The Logging threshold is MDB_RGM_LOGSIZE defined in this file.
 */
typedef struct mdb_rgxlogs {
	mdb_rgxstate_t *rgx_state;
	uint_t size;
} mdb_rgxlogs_t;


/*
 * mdb_rxlogs_t -
 * In memory logging used to log each resource state update.
 * mdb uses this log along with the resource state backup to
 * construct the actual snapshot of the resource state structure.
 * The Logging threshold is MDB_RGM_LOGSIZE defined in this file.
 */
typedef struct mdb_rxlogs {
	mdb_rxstate_t *rx_state;
	uint_t	size;
} mdb_rxlogs_t;

/*
 * The On_off_switch and monitored_switch are implemented as hash maps
 * internal to RGM. C source files, include the below defined typedef,
 * a void *. The actual declarations are in rgm_cpp.h
 */
typedef void *switch_t;
/*
 * rgm_resource_t
 *	Structure of Resource properties.  All the properties defined in
 *	Paramtable are stored in r_properties field.
 */
typedef struct rgm_resource {
	name_t			r_name;
	name_t			r_type;
	name_t			r_rgname;
	switch_t		r_onoff_switch;
	switch_t		r_monitored_switch;
	rgm_dependencies_t	r_dependencies;
	/* Used	for inter cluster dependencies */
	rgm_dependencies_t	r_inter_cluster_dependents;
	rgm_property_list_t	*r_properties;		/* system defined */
	rgm_property_list_t	*r_ext_properties;	/* extension property */
	char			*r_description;
	name_t			r_project_name;		/* project name */
	name_t			r_type_version;
	mdb_rxback_t		rx_back;
} rgm_resource_t;

/*
 * rgm_rg_t
 */
typedef struct rgm_rg {
	name_t			rg_name;
	boolean_t		rg_unmanaged;		/* is rg unmanaged */
	nodeidlist_t		*rg_nodelist;
	int			rg_max_primaries;
	int			rg_desired_primaries;
	boolean_t		rg_failback;
	namelist_t		*rg_dependencies;
	scha_rgmode_t		rg_mode;
	boolean_t		rg_impl_net_depend;
	namelist_all_t		rg_glb_rsrcused;
	name_t			rg_pathprefix;
	char			*rg_description;
	rgm_stamp_t		rg_stamp;
	int			rg_ppinterval;
	namelist_t		*rg_affinities;
	namelist_t		*rg_affinitents;
	boolean_t		rg_auto_start;
	name_t			rg_project_name;
	boolean_t		rg_system;
	boolean_t		rg_suspended;
					/* is Automatic Recovery Suspended */
	/* SC SLM addon start */
	name_t			rg_slm_type;		/* type of SLM */
	name_t			rg_slm_pset_type;	/* type of pset */
	int			rg_slm_cpu;		/* RG_SLM_CPU_SHARES */
	int			rg_slm_cpu_min;		/* RG_SLM_PSET_MIN */
	/* SC SLM addon end */
} rgm_rg_t;


/*
 * scha_property_t, scha_resource_properties_t -
 *	These data structures are for supporting updating multiple properties
 *	within a single update function call.  The function of creating and
 *	updating a RG takes a array of pointers to the struct
 *	scha_property_t as one parameter.  Since RGM supports that resource
 *	may have an extension property which has the same name as a
 *	predefined property, the function of updating resource needs two
 *	lists: one for predefined properties and one for extension ones
 *	(scha_resource_properties_t).
 */
typedef struct scha_property {
	char		*sp_name;
	namelist_t	*sp_value;
	namelist_t	*sp_nodes;
	boolean_t	is_per_node;
} scha_property_t;


typedef struct scha_properties {
	scha_property_t		**srp_predefined;	/* system pre-defined */
	scha_property_t		**srp_extension;	/* extension property */
} scha_resource_properties_t;



/*
 * rgm_orbinit()
 *	ORB Initialization.
 *	Note: Every command and daemon which will call the functions in
 *	librgm should call rgm_orbinit() exactly once at the beginning
 *	of the program.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
extern scha_errmsg_t rgm_orbinit(void);


/*
 * rgm_error_msg()
 *	return a generic static error message for a given error code of
 *	scha_err_t.
 */
extern char *rgm_error_msg(scha_err_t);

/* Nodename/nodeid mapping (using libclconf interfaces) */
extern scha_errmsg_t get_nodename(uint_t nodeid, char **nodenamep);
extern int get_nodeid(char *nodename, uint_t *nodeidp);

/*
 * Free Memory
 */
void rgm_free_rg(rgm_rg_t *);
void rgm_free_rt(rgm_rt_t *);
void rgm_free_upglist(rgm_rt_upgrade_t *);
void rgm_free_resource(rgm_resource_t *);
void rgm_free_paramtable(rgm_param_t **);
void rgm_free_proplist(rgm_property_list_t *);
void rgm_free_property(rgm_property_t *);
void rgm_free_stamp(rgm_stamp_t);
void rgm_free_strarray(char **);
void rgm_free_nlist(namelist_t *);
void rgm_free_rdeplist(rdeplist_t *);
void rgm_free_nlist_all(namelist_all_t *);
void rgm_free_nodeid_list(nodeidlist_t *l);

scha_errmsg_t namelist_add_name(namelist_t **, name_t);
void compute_NRUlist_frm_deplist(rgm_resource_t *, namelist_t **, char *);
void var_strcat(char **ptr, const char *format, /* args */ ...);
rdeplist_t *rdeplist_copy(const rdeplist_t *src);

/*
 * rgm_rsrc_on_which_rg() - find the RG in which the given resource
 * is configured.
 */
scha_errmsg_t rgm_rsrc_on_which_rg(char *rname, char **rgname, char *cluster);

/*
 * parse & lookup logical nodename using libclconf interfaces
 */
scha_errmsg_t logicalnode_to_nodeidzone(const char *name, uint_t *nodeid,
    char **zonename);

scha_errmsg_t convert_namelist_to_nodeidlist(namelist_t *namelist_in,
    nodeidlist_t **nodeidlist_out);

scha_errmsg_t populate_zone_cred(uint_t *nodeid, char **zonename);
char *get_clustername_from_zone(const char *zone, scha_errmsg_t *err);
scha_errmsg_t convert_zc_namelist_to_nodeidlist(namelist_t *namelist_in,
    nodeidlist_t **nodeidlist_out, char *zcname);
boolean_t allow_zc(void);
#ifdef __cplusplus
}
#endif

#endif /* _RGM_COMMON_H_ */
