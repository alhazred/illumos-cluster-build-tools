/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCHA_STRINGS_H_
#define	_SCHA_STRINGS_H_

#pragma ident	"@(#)scha_strings.h	1.34	08/06/10 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include	<sys/syslog.h>
#include	<scha_err.h>

/*
 * This file contains macros for strings that are the standard names
 * for entities in the SC resource management architecture
 * that are not declared in the public API scha_tags.h file.
 * These strings include string names for macro values in scha_types.h
 * and the names of paramtable attributes that are needed for
 * implementation of the public commands and RT registration files.
 *
 * These macros extend the declarations in the public scha_tags.h
 * header files to complete the names of entities in the SC
 * Resource Management architecture.
 *
 */


/*
 * Maximum length for names of RGM Resource objects.
 */
#define	MAXRGMOBJNAMELEN	255
/*
 * Maximum length for names of RGM Resource Type and Resource Group objects.
 * CCR table creation will fail if greater than this value.
 */
#define	MAXCCRTBLNMLEN		241

/*
 * The maximum length of a property value(STRINGARRAY, STRING or ENUM type)
 * is 4 megabytes minus 1
 */
#define	MAXRGMVALUESLEN		4194303

/*
 * Value of current API Version,
 * Macro to check for supported API Versions
 */
#define	MIN_SCHA_API_VERSION		2
#define	CURRENT_SCHA_API_VERSION	9
#define	IS_SUPPORTED_SCHA_API_VERSION(n)	(n >= MIN_SCHA_API_VERSION && \
						n <= CURRENT_SCHA_API_VERSION)

/*
 * Property names that are not access tags
 */
#define	SCHA_RT_NAME			"RT_name"
#define	SCHA_RESOURCE_NAME		"Resource_name"
#define	SCHA_RG_NAME			"RG_name"
#define	SCHA_SYSDEFINED_TYPE		"Sysdefined_type"
#define	SCHA_STATUS_MSG			"Status_msg"

/*
 * Names used in RT registration file declarations.
 * Vendor_id and Resource_type together form the value of the
 * RT_name property.
 * "Property" is used to name a property in the paramtable.
 * RT_Upgrade flag is used to indicate this RT is created from a SC31 or
 * post-SC31 RTR file.  Upgrade_from and Downgrade_to are the prefix for
 * RT_upgrade tunability entries.
 */
#define	SCHA_VENDOR_ID			"Vendor_id"
#define	SCHA_RESOURCE_TYPE		"Resource_type"
#define	SCHA_PROPERTY			"Property"
#define	SCHA_UPGRADE_HEADER		"Upgrade"
#define	SCHA_UPGRADE_FROM		"Upgrade_from"
#define	SCHA_DOWNGRADE_TO		"Downgrade_to"

/*
 * names for boolean values
 */
#define	SCHA_TRUE			"TRUE"
#define	SCHA_FALSE			"FALSE"
/*
 * enum scha_switch value names
 */
#define	SCHA_DISABLED			"DISABLED"
#define	SCHA_ENABLED			"ENABLED"
/*
 * enum scha_rsstatus, enum scha_rsstate and enum scha_rgstate
 * value names.  Names overlap across these enum types.
 */
#define	SCHA_OK				"OK"
#define	SCHA_DEGRADED			"DEGRADED"
#define	SCHA_FAULTED			"FAULTED"
#define	SCHA_UNKNOWN			"UNKNOWN"
#define	SCHA_OFFLINE			"OFFLINE"
#define	SCHA_ONLINE			"ONLINE"
#define	SCHA_STOP_FAILED		"STOP_FAILED"
#define	SCHA_START_FAILED		"START_FAILED"
#define	SCHA_MONITOR_FAILED		"MONITOR_FAILED"
#define	SCHA_ONLINE_NOT_MONITORED	"ONLINE_NOT_MONITORED"
#define	SCHA_DETACHED			"DETACHED"
#define	SCHA_UNMANAGED			"UNMANAGED"
#define	SCHA_PENDING_ONLINE		"PENDING_ONLINE"
#define	SCHA_PENDING_ONLINE_BLOCKED	"PENDING_ONLINE_BLOCKED"
#define	SCHA_PENDING_OFFLINE		"PENDING_OFFLINE"
#define	SCHA_ERROR_STOP_FAILED		"ERROR_STOP_FAILED"
#define	SCHA_ONLINE_FAULTED		"ONLINE_FAULTED"
#define	SCHA_STARTING			"STARTING"
#define	SCHA_STOPPING			"STOPPING"

/*
 * enum scha_failover_mode value names
 */
#define	SCHA_NONE			"NONE"
#define	SCHA_HARD			"HARD"
#define	SCHA_SOFT			"SOFT"
#define	SCHA_RESTART_ONLY		"RESTART_ONLY"
#define	SCHA_LOG_ONLY			"LOG_ONLY"

/*
 * enum scha_initnodes_flag value names
 */
#define	SCHA_RG_PRIMARIES		"RG_PRIMARIES"
#define	SCHA_RT_INSTALLED_NODES		"RT_INSTALLED_NODES"
/*
 * enum scha_node_state value names
 */
#define	SCHA_UP				"UP"
#define	SCHA_DOWN			"DOWN"

/*
 * SCHA_FAILOVER is defined in scha_tags.h
 */
#define	SCHA_SCALABLE			"Scalable"

/*
 * Attribute names in Paramtable entries
 */
#define	SCHA_NAME			"Name"
#define	SCHA_EXTENSION			"Extension"
#define	SCHA_EXTENSION_NODE		"Extension_node"
#define	SCHA_DEFAULT			"Default"
#define	SCHA_TUNABLE			"Tunable"
/* SCHA_TYPE defined as "Type" is  also a resource property name */
#define	SCHA_ENUMLIST			"Enumlist"
#define	SCHA_MIN			"Min"
#define	SCHA_MAX			"Max"
#define	SCHA_MINLENGTH			"Minlength"
#define	SCHA_MAXLENGTH			"Maxlength"
#define	SCHA_ARRAY_MINSIZE		"Array_minsize"
#define	SCHA_ARRAY_MAXSIZE		"Array_maxsize"
#define	SCHA_DESCRIPTION		"Description"

/*
 * Names of values for paramtable Type attribute,
 * names for enum scha_prop_types including UINTARRAY and UINT which
 * are not types allowed for extension properties in paramtable etnries.
 */
#define	SCHA_STRING			"STRING"
#define	SCHA_BOOLEAN			"BOOLEAN"
#define	SCHA_ENUM			"ENUM"
#define	SCHA_INT			"INT"
#define	SCHA_STRINGARRAY		"STRINGARRAY"
#define	SCHA_UINTARRAY			"UINTARRAY"
#define	SCHA_UINT			"UINT"
/*
 * Names for values of Tunable paramtable attribute and tunability for
 * RT upgrade
 */
#define	SCHA_AT_CREATION		"AT_CREATION"
#define	SCHA_WHEN_DISABLED		"WHEN_DISABLED"
#define	SCHA_WHEN_OFFLINE		"WHEN_OFFLINE"
#define	SCHA_WHEN_UNMANAGED		"WHEN_UNMANAGED"
#define	SCHA_WHEN_UNMONITORED		"WHEN_UNMONITORED"
#define	SCHA_ANYTIME			"ANYTIME"
/*
 * Names for values of Sysdefined_type private RT property
 */
#define	SCHA_LOGICAL_HOSTNAME		"Logical_hostname"
#define	SCHA_SHARED_ADDRESS		"Shared_address"
/*
 * String representation for special "ALL" or "*" name-list value
 */
#define	SCHA_ALL_SPECIAL_VALUE		"*"

#define	SCHA_CLUSTER_LOGFACILITY	LOG_DAEMON

#define	SCHA_METHOD			"Invalid_method"

/* SC SLM addon start */
#define	SCHA_SLM_TYPE_MANUAL			"manual"
#define	SCHA_SLM_TYPE_AUTOMATED			"automated"
#define	SCHA_SLM_PSET_TYPE_DEFAULT		"default"
#define	SCHA_SLM_RG_CPU_SHARES_DEFAULT		1
#define	SCHA_SLM_RG_PSET_MIN_DEFAULT		0
#define	SCHA_SLM_PSET_TYPE_DEDICATED_WEAK	"weak"
#define	SCHA_SLM_PSET_TYPE_DEDICATED_STRONG	"strong"
/*
 * The below string is used when the tunablity of NRU is changed to
 * TUNE_ANYTIME in case if the user has explicitly set the NRU
 * tunablity to anything other than TUNE_ANYTIME in the RTR file.
 */

#define	SCHA_NRU_TUNABLITY_CHANGED "Network resources used tunablity is set "\
	"to ANYTIME"
/* SC SLM addon end */

/* the following are not be public api, only for use by zone clusters */
#define	SCHA_VERIFY_IP			"Verify_IP"
#define	SCHA_VERIFY_ADAPTER		"Verify_Adapter"
#define	SCHA_ZONE_TYPE			"Zone_Type"

#ifdef __cplusplus
}
#endif

#endif	/* _SCHA_STRINGS_H_ */
