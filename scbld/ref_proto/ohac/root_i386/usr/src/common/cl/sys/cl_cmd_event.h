/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file is the interface to cl_cmd_event.c
 */

#ifndef _CL_CMD_EVENT_H
#define	_CL_CMD_EVENT_H

#pragma ident	"@(#)cl_cmd_event.h	1.4	08/05/20 SMI"

/*
 * cl_cmd_event.h - interface callable by the user state
 * routines to publish events via Sun Cluster Events API
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/cl_events.h>

/*
 * cl_cmd_event_init(int, char**, int*)
 *
 * Input:
 *       argc      - this is the number of arguments passed to
 *                   the program
 *       argv      - this is a pointer to a pointer to an array
 *                   containing the parameters passed to a program
 *       exit_code - this is the exit value that the program returns.
 *
 * Action: This function initializes this library.
 *
 * Return: none
 */
void cl_cmd_event_init(int argc, char** argv, int* exit_code);

/*
 * cl_cmd_event_gen_end_event()
 *
 * Input: none
 *
 * Action: Calls sc_publish_event() via internal functions
 * to generate an event. It verifies that cl_cmd_event_init()
 * has been already called. So cl_cmd_event_init() should be
 * called prior to the first use of this function.
 * This function is typically registered with atexit()
 * for generating an event at the completion of a CLI.
 *
 * Return: none.
 */
void cl_cmd_event_gen_end_event(void);

/*
 * cl_cmd_event_gen_start_event()
 *
 * Input: none
 *
 * Action: Calls sc_publish_event() via internal functions
 * to generate an event. It verifies that cl_cmd_event_init()
 * has been already called. So cl_cmd_event_init() should be
 * called prior to the first use of this function.
 * This function is typically used for generating an event at
 * the start of a CLI.
 *
 * Return: cl_event_error_t
 */
cl_event_error_t cl_cmd_event_gen_start_event(void);

#ifdef __cplusplus
}
#endif

#endif	/* _CL_CMD_EVENT_H */
