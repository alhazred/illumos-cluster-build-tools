/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*	Copyright (c) 1984, 1986, 1987, 1988, 1989 AT&T */
/*	  All Rights Reserved   */

/*
 * The following notices accompanied the original version of this file:
 *
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#ifndef _OS_H
#define	_OS_H

#pragma ident	"@(#)os.h	1.125	08/07/17 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include <sys/os_compat.h>

#if defined(__cplusplus)

/*
 * os.h - specification of the OS services that Solaris MC components
 *	  can call from C++ in a portable way both in userland and in
 *	  the kernel.
 */

#include <sys/types.h>
#if __cplusplus < 199711
/* For emulation "bool" type till compiler supports built-in bool type */
#include <sys/boolean.h>
#endif

#ifndef linux
#define	BUG_4630016
#endif

#ifdef BUG_4630016
#ifndef _KERNEL
#define	_KERNEL
#include <sys/atomic.h>
#undef _KERNEL
#else // _KERNEL
#include <sys/atomic.h>
#endif // _KERNEL
#endif

#ifndef linux
#include <sys/systm.h>
#endif
#include <sys/time.h>
#include <orb/infrastructure/fork.h>

#if defined(DEBUG)
#define	INTR_DEBUG
#endif

extern "C" {
	void symtracedump(void);
}

#include <netdb.h>
#include <netinet/in.h>
#include <sys/cl_assert.h>

#ifdef linux
#include <pthread.h>
#include <semaphore.h>
#endif

#ifdef	_KERNEL
#ifndef	linux
#include <sys/thread.h>
#include <sys/mutex.h>
#include <sys/rwlock.h>
#include <sys/ksynch.h>
#include <sys/proc.h>
#include <sys/stack.h>
#include <sys/strsubr.h>
#include <sys/kmem.h>
#include <sys/debug.h>
#include <sys/bitmap.h>
#include <sys/kobj.h>
#include <sys/dirent.h>
#endif	/* linux */

#else	// _KERNEL

#include <stdio.h>	// for BUFSIZ
#include <stdlib.h>	// for atoi
#ifdef linux
#include <pthread.h>
#include <semaphore.h>
#else
#include <thread.h>
#include <synch.h>
#endif
#include <string.h>
#include <errno.h>
#include <strings.h>
#include <ctype.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>

#define	ATOMIC_HASH_SIZE	256
#define	ATOMIC_HASH_SHIFT	6
#define	ATOMIC_HASH(addr)	\
	(((uintptr_t)(addr)	>> ATOMIC_HASH_SHIFT) & ATOMIC_HASH_MASK)
#define	ATOMIC_HASH_MASK		(ATOMIC_HASH_SIZE - 1)

#define	ATOMIC_LOCK(addr)		&atomic_lock[ATOMIC_HASH(addr)]

#endif	// _KERNEL

#include <sys/sc_syslog_msg.h> // for sc_syslog_msg class

#ifdef u
#undef u
#endif

#ifdef paddr
#undef paddr
#endif

#ifndef	NULL
#define	NULL 0
#endif

//
// OS dependent functions needed for C/C++ programs
//
extern "C" int mkdirp(const char *path, mode_t mode);
extern "C" hrtime_t gethrtime();
#ifdef linux
extern "C" int getnetbyname_r(const char*, struct netent*, char*, size_t,
    struct netent**, int*);
extern "C" int getrpcbyname_r(const char*, struct rpcent*, char*, size_t,
	struct rpcent**) throw();
#else
extern "C" struct netent *getnetbyname_r(const char *, struct netent*,
	char*, int);
extern "C" struct rpcent *getrpcbyname_r(const char *, struct rpcent*,
	char*, int);
#endif
extern "C" struct hostent *getipnodebyname(const char *name, int af,
	int flags, int *error_num);
extern "C" void freehostent(struct hostent *hp);


//
// maxusers - unode version defines the maximum number of users
// a unode system supports. This really is used for system
// configuration.
//
extern int maxusers;
#ifndef linux
#include <sys/clconf.h>
#ifndef _KERNEL
extern void	clconf_init(void);
extern nodeid_t	clconf_get_nodeid(void);
#endif
#else
extern void clconf_init();
#endif

//
// The namespace os:: includes those OS services that Solaris MC components
// can call from C++ in a portable way. Both userland and kernel level
// implementations of this API are provided.
//
// The C equivalent of this API is defined in <os_c.h>
//

class os {
public:
#ifdef _KERNEL
	enum mem_alloc_type { SLEEP = KM_SLEEP, NO_SLEEP = KM_NOSLEEP };
#else
	enum mem_alloc_type { SLEEP, NO_SLEEP };
#endif
	enum mem_zero_type { DONTZERO, ZERO };

#ifdef _KERNEL
	typedef kthread_t *threadid_t;
#else
	typedef thread_t threadid_t;
#endif
	static os::threadid_t threadid();

	class condvar_t;	// forward declaration

	//
	// class mutex - synchronization mutex
	//
	class mutex_t {
		friend class condvar_t;
#ifndef	_KERNEL
		//
		// following friend class defines pthread_atfork handler
		// functions. This is defined friend of mutex class so that
		// the owner of the lock can be updated to new threadid
		// in the child process.
		//
		friend class pthread_atfork_handler;
#endif	// _KERNEL
	public:
		mutex_t();
		~mutex_t();
		void lock();
		int try_lock();
		void unlock();
		int lock_held();
		int lock_not_held();
		uint64_t owner();

	private:
		//
		// when a process fork's a child, it may inherit a lock
		// in a locked state, and the owner of the lock is set to
		// threadid of the locking  thread of the parent.
		// The owner of lock should be set to threadid of the locking
		// thread in the child process. This interface is called by
		// the child handler defined in fork.cc  so as to set the
		// owner of the lock to threadid in the child process.
		//
		// Note: this interface should be called only by
		// pthread_handler's defined in fork.cc.
		//
		void set_owner();
#ifdef	_KERNEL
		::kmutex_t _mutex;
#else	// _KERNEL
#ifdef linux
		::pthread_mutex_t _mutex;
#else
		::mutex_t _mutex;
#endif
		os::threadid_t _owner;	// For lock held and assertions
#endif	// _KERNEL

		// Disallow assignments and pass by value
		mutex_t(const mutex_t &);
		mutex_t &operator = (mutex_t &);
	};

	//
	// class rwlock_t - reader/writer lock
	//
	class rwlock_t {
	public:
		rwlock_t();
		~rwlock_t();
		void rdlock();
		void wrlock();
		int try_rdlock();
		int try_wrlock();
		void unlock();
		int lock_held();
		int read_held();
		int write_held();
#ifdef	_KERNEL
		//
		// The methods try_upgrade(), downgrade(), and rw_owner()
		// are not supported in userland.  There is no
		// corresponding Solaris system call to wrap around.
		//

		int try_upgrade();
		void downgrade();
		threadid_t rw_owner();
#endif
	private:
#ifdef	_KERNEL
		::krwlock_t _rwlock;
#else	// _KERNEL
#ifdef linux
		::pthread_rwlock_t _rwlock;
#else
		::rwlock_t _rwlock;
#endif
#endif	// _KERNEL

		// Disallow assignments and pass by value
		rwlock_t(const rwlock_t &);
		rwlock_t &operator = (rwlock_t &);
	};

	class systime;

	//
	// functions for atomic operations { see <sys/atomic.h> }
	// add delta atomically to tgt
	//
	static void atomic_add_16(uint16_t *tgt, int16_t delta);
	static void atomic_add_32(uint32_t *tgt, int32_t delta);
	static void atomic_add_64(uint64_t *tgt, int64_t delta);
	//
	// as above, but also return final value of target
	//
	static uint16_t atomic_add_16_nv(uint16_t *tgt, int16_t delta);
	static uint32_t atomic_add_32_nv(uint32_t *tgt, int32_t delta);
	static uint64_t atomic_add_64_nv(uint64_t *tgt, int64_t delta);

#ifdef _KERNEL
	//
	// compare and swap if tgt == cmp then set tgt to nv
	// and return old value of tgt
	//
	// If there are requirements for user-land versions of the
	// cas* functions, atomic_cas_32(3C), atomic_cas_64(3C) and
	// atomic_cas_ptr(3C) can be used.
	//
	static uint32_t cas32(uint32_t *tgt, uint32_t cmp, uint32_t nv);
	static uint64_t cas64(uint64_t *tgt, uint64_t cmp, uint64_t nv);
	//
	// as above, but with pointers
	//
	static void    *casptr(void **tgt, void *cmp, void *nval);
	//
	// end of atomic operations from <sys/atomic.h>
	//
#endif 	// _KERNEL

	//
	// Atomic copies (for guaranteed atomic copies across architectures)
	// This function makes no guarantees as to atomic reads.  If you want
	// to read atomically, use this function to make a local copy and read
	// that.  Both arguments must point to addresses that are 8 byte
	// aligned.
	//
	static void atomic_copy_64(uint64_t *tgt, uint64_t *src);

	//
	// variations of allocb/dupb that support os::SLEEP allocations
	// XX Currently implemented in _KERNEL only,
	// would be nice to have user land emulation too
	//
	static mblk_t *allocb(uint_t size, uint_t pri, os::mem_alloc_type flag);
	static mblk_t *dupb(mblk_t *mp, uint_t pri, os::mem_alloc_type flag);

	//
	// class condvar - condition variable
	//
	// The condvar can only be used with one lock.
	// The first lock used with the condvar becomes the only lock
	// which the condvar will accept subsequently.
	//
	class condvar_t {
	public:
		condvar_t();
		~condvar_t();

		//
		// These result values correspond to those returned by the
		// kernel so that kernel developers may use the actual values
		// if they prefer.
		//
		enum wait_result { SIGNALED = 0, TIMEDOUT = -1, NORMAL = 1 };

		// There are 4 variants on wait. The userland version of the
		// wait() and timedwait() can be woken by signals or fork. If
		// they are NORMAL is returned.
		//
		// For user, if there is no signal handler defined for the
		// signal in question, the program will segv.
		void	wait(mutex_t *lockp);
		// returns NORMAL or SIGNALED.
		wait_result wait_sig(mutex_t *lockp);
		// returns NORMAL or TIMEDOUT
		wait_result timedwait(mutex_t *lockp, systime *timeout);
		// returns NORMAL or SIGNALED or TIMEDOUT
		wait_result timedwait_sig(mutex_t *lockp, systime *timeout);

		void	signal();
		void	broadcast();

#ifdef DEBUG
		//
		// The condvar_t will no longer use the lock currently
		// associated with it and the condvar_t is idle.
		// Reset the condvar_t to its initial state.
		//
		void	reset();
#endif

	private:

#ifdef linux
		::pthread_cond_t _cv;
#else	/* linux */
#ifdef	_KERNEL
		::kcondvar_t _cv;
#else	// _KERNEL
		::cond_t _cv;
#endif
#endif	// _KERNEL

		// Called before wait.
		void debug_pre(os::mutex_t *lockp);
		// Called after wait.
		void debug_post();
#ifdef DEBUG
		int		num_waiting;
		os::mutex_t	*wait_lock;	// Only this lock can be used,
						// once set
#endif

		// Disallow assignments and pass by value
		condvar_t(const condvar_t &);
		condvar_t &operator = (condvar_t &);
	};

	// class sem_t
	class sem_t {
	public:
		sem_t(uint_t initval);
		sem_t();
		~sem_t();

		void	p();
		int	tryp();
		void	v();

	private:
#ifdef	_KERNEL
		::ksema_t _sem;
#else	// _KERNEL
#ifdef linux
		::sem_t _sem;
#else
		::sema_t _sem;
#endif
#endif	// _KERNEL

		// Disallow assignments and pass by value
		sem_t(const sem_t &);
		sem_t &operator = (sem_t &);
	};

	//
	// class notify_t
	//
	// Used to encapsulate a cv + flag (global mutex)
	//
	class notify_t {
	public :
		enum wait_state { WAITING, DONE };

		// constructor initialized to WAITING
		notify_t();

		// Set flag to DONE and signal all waiters
		// This does a cv.broadcast to wake up all waiters
		// There is no equivalent of cv.signal as the class needs to
		// be reinitialized before new waiter can wait on it.
		void signal();

		// Wait for flag to change state.
		void wait();

		// Timed Wait for flag to change state. Returns current state
		wait_state timedwait(os::systime &);

		// reinitialize, if old flag is WAITING, wait for old signal
		// Returns 0 on failure (nonblocking only), 1 on success
		int reinit(os::mem_alloc_type);

		// Return current state. Mostly for debug purposes
		wait_state get_state();
	private	:
		wait_state	flag;
		os::condvar_t	cv;
		static os::mutex_t notify_lock;

		// Disallow assignments and pass by value
		notify_t(const notify_t &);
		notify_t &operator = (notify_t &);
	};

	//
	// class thread
	//

	// XXX - need to declare an API to allow creation of a new thread.
	class thread {
	public:
#ifdef _KERNEL
		static os::threadid_t create(void *(*func)(void *));
#else
		static int create(void *, size_t, void *(*)(void *),
		    void *, long, threadid_t *);
#endif
		static void exit(void *);
		static int main(void);
		static int join(threadid_t, threadid_t *departed = NULL,
		    void **status = NULL);
		static threadid_t self(void) { return (os::threadid()); }
		static void sigsetmask(int, sigset_t *, sigset_t *);
		static int getconcurrency();
		static int setconcurrency(int);
	};

	static int drv_getparm(unsigned int, void *);
	static int drv_priv(cred_t *cr);
	static int sysinfo(int command, char *buf, long count);
	static struct passwd *getpwnam(const char  *name,  struct  passwd
	    *pwd, char *buffer, int buflen);
	static struct passwd *getpwuid(uid_t  uid,  struct  passwd  *pwd,
	    char *buffer, int buflen);

	//
	// Thread specific data
	//
	class tsd {
	public:
		typedef void (*freef_t)(void *value);
		tsd(freef_t = NULL);
		~tsd();
		void set_tsd(uintptr_t);		// set value
		uintptr_t get_tsd() const;		// get value
	private:
		uint_t _key;

		// Disallow assignments and pass by value
		tsd(const tsd &);
		tsd &operator = (tsd &);
	};

	//
	// debugging code to check for nonblocking contexts
	//
#ifndef linux
	static
#endif
class envchk {
	public:
		//
		// nbtype - defines different kinds of nonblocking context.
		// The system uses these enum values to identify when
		// certain operations cannot be performed.
		//
		enum nbtype {NB_INTR = 0x1,	// Interrupt
		    NB_INVO = 0x2,		// Invocation
		    NB_RECONF = 0x4};		// Node reconfiguration

		static void set_nonblocking(nbtype);
		static void clear_nonblocking(nbtype);
		static bool is_nonblocking(int);
		static void check_nonblocking(int, char *);
	private:
#ifdef INTR_DEBUG
		static tsd env;
#endif
		// Disallow assignments and pass by value
		envchk(const envchk &);
		envchk &operator = (envchk &);
	};
	typedef ::hrtime_t hrtime_t;
	static os::hrtime_t gethrtime();

	static void tracedump();

	typedef clock_t usec_t;

	static void usecsleep(os::usec_t sleeptime);

	//
	// Methods for time of day computations. Parts of it have been
	// adapted from userland ctime implementation and other parts
	// have been copied from other parts of the kernel to eliminate
	// dependencies.
	//
	class tod {
	public:
		// Maximum length of a date string including the terminating
		// NULL character returned by ctime_r.
		enum {CTIME_STR_LEN = 32};

		// Converts seconds since epoch to a printable date string.
		// Adapted from userland ctime_r.
		static char *ctime_r(const time_t *, char *, int);
	private:
#ifdef _KERNEL
		// Represents time of day. Taken from common/sys/time.h.
		typedef struct todinfo {
			int	tod_sec;	// seconds 0-59
			int	tod_min;	// minutes 0-59
			int	tod_hour;	// hours 0-23
			int	tod_dow;	// day of week 1-7
			int	tod_day;	// day of month 1-31
			int	tod_month;	// month 1-12
			int	tod_year;	// year 70+
		} todinfo_t;

		// Splits seconds since epoch into year, month, day,
		// hours, minutes, seconds.
		// Copied from usr/src/uts/common/os/timers.c
		static todinfo_t utc_to_tod(time_t);

		// The following methods are supporting methods for ctime_r
		// and have been adapted from their userland counterparts.
		static char *ct_numb(char *, int);
		static char *asctime_r(const todinfo_t *, char *, int);
#endif
	};

	//
	// miscellaneous library functions
	//
	static int strcmp(const char *s1, const char *s2);
	static int strncmp(const char *s1, const char *s2, size_t n);
	static int strncasecmp(const char *s1, const char *s2, size_t n);
	static char *strcpy(char *dst, const char *src);
	static char *strncpy(char *dst, const char *src, size_t n);
	static char *strstr(const char *s1, const char *s2);

	static size_t strspn(const char *string, const char *charset);
	static char *strpbrk(const char *string, const char *brkset);
	static char *strtok_r(char *string, const char *sepset, char **lasts);

	// strdup differs from the normal strdup in two ways:
	// os::strdup allocates memory using "new" and not malloc
	// The returns char * should be freed with delete [] ...
	// os::strdup returns NULL if either string is NULL or
	// the allocation failed (the kernel version does not return NULL)
	static char *strdup(const char *string);

	static size_t strlen(const char *str);
	static void sprintf(char *buf, const char *fmt, ...);
	static int snprintf(char *buf, size_t n, const char *fmt, ...);

	static void *bsearch(const void *key, const void *base,
	    size_t nel, size_t size,
	    int (*compar)(const void *, const void *));

	static void warning(const char *fmt, ...);
	static void printf(const char *fmt, ...);
	static void prom_printf(const char *fmt, ...); // make sure it gets out
	static void abort();
	static void panic(const char *, ...);
	static void log_syslog(const char *fmt, ...);
	static void symtracedump(void);

	static int cladm(int facility, int command, void *arg);

	static int create_dir(char *);
	static int rm_dir(char *);
	static int file_link(char *, char *);
	static int file_rename(char *, char *);
	static int file_unlink(char *);
	static int file_copy(char *, char *);

	// Read-only file stream object
	class rfile {
	public:
		rfile();
		~rfile();
		bool open(char *);
		bool isopen();
		size_t read(char *, size_t, size_t);
		int fgetc();
		int ungetc(int);
		void close();
	private:
#ifdef	_KERNEL
		struct _buf *f;
#else
		FILE *f;
#endif
	};

	static int mkdirp(const char *path, mode_t mode);
	static int find_unique_subdir(char *path, char *foundpath);

#ifdef	_KERNEL
	typedef ::dirent64_t dirent_t;
#else
#ifdef linux
	typedef struct dirent dirent_t;
#else
	typedef ::dirent_t dirent_t;
#endif
#endif

	// os interface for opendir/readdir
	class rdir {
	public:
		rdir();
		~rdir();
		bool open(char *);
		bool isopen();
		dirent_t *read();
		void close();
	private:
#ifdef	_KERNEL
		enum { maxlen = 1024 };	// buffer size

		vnode_t	*dvp;		// directory vnode
		char *rawdirp;		// buffer for directory contents
		struct dirent64 *nextp;	// pointer to next unread entry
		struct dirent64 *lastp;	// pointer to last entry in rawdirp
		offset_t doff;		// directory offset to read from
#else
		DIR *d;
#endif
	};

	// translate a string decimal internet address to a inaddr_t
	// NOTE: This function behaves differently than
	// the user-level library function inet_addr
	static in_addr_t inet_addr(const char *);

	// sc_syslog_msg interfaces

	//
	// The method calls in turn invoke openlog(3) and syslog(3) in
	// user-land and cmn_err(9F) in kernel-land.
	// The second parameter to openlog(3) viz. logopt cannot be specified
	// when invoking methods of sc_syslog_msg class. It is always set
	// to LOG_CONS by methods in sc_syslog_msg class.
	//
	// Other parameters to openlog(3) such as ident (1st parameter)
	// can be specified in the constructor
	// of sc_syslog_msg class for user-land modules.
	// facility is always set to LOG_DAEMON for user-land modules.
	//

	class sc_syslog_msg {
	public:
		//
		// Constructor
		//
		// Paramters:
		//
		// resource_type_specific_tag -
		// 	This tag will become part of the message id. of the
		//	logged message.
		//	- It should not be NULL.
		//	- Some resource_type_specific_tags
		//	  are defined in sc_syslog_msg.h
		//
		// resource_name -
		//	resource_type_specific_tag and resource_name are
		//	concatenated to create the <message tag> part of the
		//	message id.
		//	- It should not be NULL.
		//	- It should be unique clusterwide.
		//	  For example, if it is a message logged by CMM for
		//	  node 2, resource_name should be "2".
		//
		//

		sc_syslog_msg(
			const char 	*resource_type_specific_tag,
			const char	*resource_name,
			void		*);

		//
		// log() - Logs a message with the syslog facility
		//
		// Paramters:
		//
		// priority - priority of the logged message. see syslog(3)
		//		or cmn_err(9F) for additional information.
		//		Some common priority values are defined in
		//		sc_syslog_msg.h to avoid having to put ifdefs
		//		in the code that is shared in kernel and
		//		user-land.
		//
		// format - Formatted message as it is logged using syslog(3)
		//		or cmn_err(9F)
		//
		//
		sc_syslog_msg_status_t log(
			int		priority,
			sc_event_type_t	event_type,
			const char	*format,
			...);

		~sc_syslog_msg();
private:
		sc_syslog_msg_handle_t	msg_handle;

		// Disallow assignments and pass by value
		sc_syslog_msg(const sc_syslog_msg &);
		sc_syslog_msg &operator = (sc_syslog_msg &);
	}; // end of sc_syslog_msg

	//
	// class systime
	//
	// Simple class for dealing with system time.
	class systime {
		friend class condvar_t;
	public:
		// constructor
		systime(usec_t offset = 0);

		// Set the time of this object to some offset relative to now.
		void setreltime(usec_t offset);

		// Is this time in the past.
		bool is_past();

		// Is this time in the future.
		bool is_future();

		// Is this time before that.
		bool is_before(systime &);

		// Set the time.
		void set(systime &);

	private:
#ifdef	_KERNEL
		long		_time;
#else	// _KERNEL
		timestruc_t	_time;
#endif	// _KERNEL

		// Disallow assignments and pass by value
		systime(const systime &);
		systime &operator = (systime &);
	};

	//
	// class ctype
	//
	// simple routines for deriving character types
	//
	// NB: For the kernel these are optimized since they
	// need not perform internationalization.
	// The userland versions map directly to libc.
	//
	// This list isn't complete, but contains the methods
	// we currently use in the kernel.
	//
	// Since the is* functions are macros, we cannot simply
	// map the names one for one (they're all expanded inline).
	// Given that limitation, we've added an underscore.
	//
	class ctype {
	public:
		static int is_digit(int c);
		static int is_alpha(int c);
		static int is_xdigit(int c);
		static int is_upper(int c);
		static int is_lower(int c);
		static int is_space(int c);
	};

	static int atoi(const char *str);

	// Convert int to char *. Caller needs to make sure
	// the space pointed by char * is big enough. It returns the
	// number of digits copied.
	static int itoa(int, char *, uint_t base = 10);

	//
	// Find highest and lowest one bit set.
	//	Returns bit number + 1 of bit that is set, otherwise returns 0.
	// Low order bit is 0, high order bit is 31.
	//
	static int highbit(ulong_t);
	static int lowbit(ulong_t);

	//
	// copyin - replace with a bcopy function call and a sequence
	// operator (the comma) followed by the return value of success.
	// The unode copyin has no problems crossing from user to kernel space.
	//
	// copyout - replace with a bcopy function call and a sequence
	// operator (the comma) followed by the return value of success.
	// The unode copyout has no problems crossing from kernel to user space.
	//
#ifndef _KERNEL
	static int copyin(const void *src, void *dest, size_t size)
	    { bcopy(src, dest, size); return (0); }
	static int x_copyin(const void *src, void *dest, size_t size)
	    { bcopy(src, dest, size); return (0); }
	static int copyout(const void *src, void *dest, size_t size)
	    { bcopy(src, dest, size); return (0); }
	static int x_copyout(const void *src, void *dest, size_t size)
	    { bcopy(src, dest, size); return (0); }
	static int copyoutstr(const void *src, void *dest, size_t,
	    size_t *lencopied)
	    { *(lencopied) = strlen((const char *)src);
		bcopy(src, dest, *(lencopied)); return (0); }
#else
	static int copyin(const void *src, void *dest, size_t size)
	    { return (::copyin(src, dest, size)); }
	static int x_copyin(const void *src, void *dest, size_t size)
	    { return (::xcopyin(src, dest, size)); }
	static int copyout(const void *src, void *dest, size_t size)
	    { return (::copyout(src, dest, size)); }
	static int x_copyout(const void *src, void *dest, size_t size)
	    { return (::xcopyout(src, dest, size)); }
	static int copyoutstr(const void *src, void *dest, size_t size,
	    size_t *lencopied) {
		return (::copyoutstr((const char *)src, (char *)dest, size,
		lencopied));
	    }
#endif
	//
	// str2sig() only exists on Solaris.  On Linux, we need to
	// provide our own implementation.
	//
	static int str2sig(const char *s, int *sigp);

	// Abstraction of getnetbyname_r()
	static struct netent *getnetbyname_r(const char *name,
	    struct netent *result_buf, char *buffer, int *res);
	// Abstraction of getrpcbyname_r()
	static struct rpcent *getrpcbyname_r(const char *name,
		struct rpcent *result_buf, char *buffer, int buflen);

private:

#ifdef _KERNEL_ORB
	// Used by usecsleep()
	static mutex_t		sleep_mutex;
	static condvar_t	sleep_cv;
#endif

}; // class os

void *shared_new(size_t, os::mem_alloc_type, os::mem_zero_type);

//
// In a 32-bit kernel, size_t is defined as an unsigned long.  But the
// C++ compiler "knows" that size_t is an unsigned int, so if we define
// new operators using size_t we'll get compilation errors.  So we use
// galsize_t when defining new and delete operators.  Everywhere else
// we use size_t.
//
#if defined(_LP64) || !defined(_I32LPx)
typedef size_t galsize_t;
#else
typedef uint_t galsize_t;
#endif

#ifdef	_KERNEL

// If inlines are disabled (and won't be seen by including os_in.h)
// we need prototypes for the forms of operator new().

#ifdef NOINLINES
void *operator new(galsize_t, os::mem_zero_type);
void *operator new(galsize_t, os::mem_alloc_type);
void *operator new(galsize_t, os::mem_alloc_type, os::mem_zero_type);
#endif

// id for "not a thread"
// Disabling this as this results in 1 variable instance per .cc file!
// const os::threadid_t NO_THREAD = NULL;

#else	// _KERNEL

#ifdef NOINLINES
void *operator new(size_t, os::mem_zero_type);
void *operator new(size_t, os::mem_alloc_type);
void *operator new(size_t, os::mem_alloc_type, os::mem_zero_type);

#endif

// id for "not a thread"
const os::threadid_t NO_THREAD = 0;

extern os::mutex_t atomic_lock[ATOMIC_HASH_SIZE];

#endif	// _KERNEL

#include <sys/knewdel.h>

// If inlines are enabled the _in.h headers are included here,
// otherwise non-inline methods are built into libkos/libuos
// (see os_misc.cc).

#ifndef NOINLINES
#include <sys/os_in.h>

#ifdef DEBUGGER_PRINT
#include <dbg_os_in.h>
inline void *
/*CSTYLED*/
operator new[](galsize_t len, os::mem_alloc_type flag)
{
	extern void *shared_new(size_t, os::mem_alloc_type, os::mem_zero_type);

	return (shared_new((size_t)len, flag, os::DONTZERO));
}
#else
#ifdef 	_KERNEL
#include <sys/kos_in.h>
#else	// _KERNEL
#include <sys/uos_in.h>
#endif	// _KERNEL
#endif // DEBUGGER_PRINT

#endif	// _NOINLINES

#else /* __cplusplus */

/*
 * OS dependent functions needed for C programs
 */
#ifdef linux
extern int mkdirp(const char *path, mode_t mode);
extern hrtime_t gethrtime();
extern struct hostent *getipnodebyname(const char *name, int af,
	int flags, int *error_num);
extern void freehostent(struct hostent *hp);
#endif

#endif /* __cplusplus */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _OS_H */
