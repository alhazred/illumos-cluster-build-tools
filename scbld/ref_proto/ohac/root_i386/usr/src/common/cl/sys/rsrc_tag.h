/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSRC_TAG_H
#define	_RSRC_TAG_H

#pragma ident	"@(#)rsrc_tag.h	1.24	08/05/27 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * This header file defines the resource type tag used when logging a message
 * with sc_syslog_msg interface. The tags are also used by
 * stat_get_object_type() in scstat_cluster.c
 * (usr/src/lib/libscstat/common/scstat_cluster.c) to
 * map the tag into an object type. Be sure to sync up with
 * these two files when this file is modified.
 * Note: currently, stat_get_object_type() is used only by the scstat transport
 * module, which cares only about SC_SYSLOG_TRANSPORT_PATH_TAG.
 */

/*
 * Resource type tag for transport objects
 */

#define	SC_SYSLOG_TRANSPORT_PATH_TAG		"Cluster.Transport.Path"
#define	SC_SYSLOG_TRANSPORT_ADAPTER_TAG		"Cluster.Transport.Adapter"
#define	SC_SYSLOG_TRANSPORT_JUNCTION_TAG	"Cluster.Transport.Junction"
#define	SC_SYSLOG_TRANSPORT_CABLE_TAG		"Cluster.Transport.Cable"
#define	SC_SYSLOG_TRANSPORT_PORT_TAG		"Cluster.Transport.Port"

#define	SC_SYSLOG_TRANSPORT			"Cluster.Transport"
#define	SC_SYSLOG_TCP_TRANSPORT			"Cluster.Transport.Tcp"
#define	SC_SYSLOG_RSM_TRANSPORT			"Cluster.Transport.Rsm"

#define	SC_SYSLOG_PRIVIPD_TAG 			"Cluster.Transport.Privipd"
#define	SC_SYSLOG_CZNETD_TAG 			"Cluster.Transport.Cznetd"
/*
 * Resource type tag for ORB Framework objects
 */

#define	SC_SYSLOG_FRAMEWORK_TAG			"Cluster.Framework"
#define	SC_SYSLOG_FRAMEWORK_RSRC		"cl_comm"

/*
 * Resource type tag for cmm/quorum/ccr objects
 */
#define	SC_SYSLOG_CMM_NODE_TAG			"Cluster.CMM.Node"
#define	SC_SYSLOG_CMM_QUORUM_TAG		"Cluster.CMM.Quorum"
#define	SC_SYSLOG_CCR_CLCONF_TAG		"Cluster.CCR.CLCONF"

/*
 * Resource type tag for device group objects
 */
#define	SC_SYSLOG_DCS_DEVICE_SERVICE_TAG	"Cluster.DCS.DeviceService"
#define	SC_SYSLOG_DEVICE_SERVICE_TAG		"Cluster.DeviceService"

/*
 * Resource type tag for name server objects
 */
#define	SC_SYSLOG_NAMING_SERVICE_TAG		"Cluster.NameServer"

/*
 * Resource type tag for global mount objects
 */
#define	SC_SYSLOG_GLOBAL_MOUNT_TAG		"Cluster.GlobalMount"

/*
 * Resource type tag for file system objects
 */
#define	SC_SYSLOG_FILESYSTEM_TAG		"Cluster.Filesystem"

/*
 * Resource type tag for NAFO objects
 */
#define	SC_SYSLOG_NAFO_GROUP_TAG		"Cluster.NAFO.Group"
#define	SC_SYSLOG_NAFO_ADAPTER_TAG		"Cluster.NAFO.Adapter"

/*
 * Resource type tag for RGM objects
 */
#define	SC_SYSLOG_RGM_RG_TAG			"Cluster.RGM.RG"
#define	SC_SYSLOG_RGM_RS_TAG			"Cluster.RGM.RS"
#define	SC_SYSLOG_RGM_RT_TAG			"Cluster.RGM.RT"

/*
 * Resource type tag for FED, PMF, and RGM objects - used *only* for
 * event type MESSAGE.
 */
#define	SC_SYSLOG_RGM_RGMD_TAG			"Cluster.RGM.rgmd"
#define	SC_SYSLOG_RGM_SCHA_TAG			"Cluster.RGM.schalib"
#define	SC_SYSLOG_RGM_FED_TAG			"Cluster.RGM.fed"
#define	SC_SYSLOG_PMF_PMFD_TAG			"Cluster.PMF.pmfd"
#define	SC_SYSLOG_RGMPMF_TAG			"Cluster.RGMPMF.lib"
#define	SC_SYSLOG_RGM_ZONESD_TAG		"Cluster.RGM.zonesd"
#define	SC_SYSLOG_RGM_CANARY_TAG		"Cluster.RGM.sc_canary"

/*
 * Tags for PNM (NAFO groups and pnmd)
 */
#define	SC_SYSLOG_PNM_TAG			"Cluster.PNM"
#define	SC_SYSLOG_PNM_GROUP_TAG			"Cluster.PNM.Group"
#define	SC_SYSLOG_PNM_ADAPTER_TAG		"Cluster.PNM.Adapter"
#define	SC_SYSLOG_PNMPROXY_TAG			"Cluster.PNMproxy"

/*
 * Tags for UCMMD components
 */
#define	SC_SYSLOG_UCMMD_TAG			"Cluster.UCMMD"
#define	SC_SYSLOG_OPS_UCMMD_TAG			"Cluster.OPS.UCMMD"
#define	SC_SYSLOG_OPS_SKGXN_TAG			"Cluster.OPS.SKGXN"


/*
 * Tags for cl_apid (CRNP)
 */
#define	SC_SYSLOG_CLAPI_CLAPID_TAG "Cluster.CLAPI.clapid"
#define	SC_SYSLOG_CLAPI_CLEVENTD_TAG "Cluster.CLAPI.cleventd"

/*
 * Tag for sc_event_proxy
 */
#define	SC_SYSLOG_SC_EVENT_PROXY_TAG		"Cluster.event_proxy"

/*
 * Tags for sun cluster SMF delegated restarter
 */
#define	SC_SYSLOG_SMF_DR_TAG "Cluster.SMF.DR"

/*
 * Tags for zone cluster commands and libraries
 */
#define	SC_SYSLOG_ZC_LIB_TAG			"Cluster.ZC.libzccfg.so"
#define	SC_SYSLOG_ZC_SCZONECFG_TAG		"Cluster.ZC.sczonecfg"
#define	SC_SYSLOG_ZC_ZC_HELPER_TAG		"Cluster.ZC.zc_helper"
#define	SC_SYSLOG_ZC_ZCCFG_VERIFY_TAG		"Cluster.ZC.zccfg_verify"
#define	SC_SYSLOG_ZC_CMD_LOG_TAG		"Cluster.ZC.process_cmd_log"

#ifdef	__cplusplus
}
#endif

#endif	/* _RSRC_TAG_H */
