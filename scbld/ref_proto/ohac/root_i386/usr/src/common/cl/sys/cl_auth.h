/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file is the interface to cl_auth.c
 */

#ifndef _CL_AUTH_H
#define	_CL_AUTH_H

#pragma ident	"@(#)cl_auth.h	1.10	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/* Constants corresponding to different authorizations */
#define	CL_AUTH_APPINSTALL "solaris.cluster.appinstall"
#define	CL_AUTH_DEVICE_ADMIN "solaris.cluster.device.admin"
#define	CL_AUTH_DEVICE_MODIFY "solaris.cluster.device.modify"
#define	CL_AUTH_DEVICE_READ "solaris.cluster.device.read"
#define	CL_AUTH_NETWORK_READ "solaris.cluster.network.read"
#define	CL_AUTH_NETWORK_MODIFY "solaris.cluster.network.modify"
#define	CL_AUTH_NETWORK_ADMIN "solaris.cluster.network.admin"
#define	CL_AUTH_NODE_MODIFY "solaris.cluster.node.modify"
#define	CL_AUTH_NODE_READ "solaris.cluster.node.read"
#define	CL_AUTH_NODE_ADMIN "solaris.cluster.node.admin"
#define	CL_AUTH_QUORUM_MODIFY "solaris.cluster.quorum.modify"
#define	CL_AUTH_QUORUM_READ "solaris.cluster.quorum.read"
#define	CL_AUTH_QUORUM_ADMIN "solaris.cluster.quorum.admin"
#define	CL_AUTH_RESOURCE_ADMIN "solaris.cluster.resource.admin"
#define	CL_AUTH_RESOURCE_MODIFY "solaris.cluster.resource.modify"
#define	CL_AUTH_RESOURCE_READ "solaris.cluster.resource.read"
#define	CL_AUTH_SYSTEM_ADMIN "solaris.cluster.system.admin"
#define	CL_AUTH_SYSTEM_MODIFY "solaris.cluster.system.modify"
#define	CL_AUTH_SYSTEM_READ "solaris.cluster.system.read"
#define	CL_AUTH_TRANSPORT_MODIFY "solaris.cluster.transport.modify"
#define	CL_AUTH_TRANSPORT_READ "solaris.cluster.transport.read"
#define	CL_AUTH_TRANSPORT_ADMIN "solaris.cluster.transport.admin"
#define	CL_AUTH_CLUSTER_MODIFY "solaris.cluster.modify"
#define	CL_AUTH_CLUSTER_READ "solaris.cluster.read"
#define	CL_AUTH_CLUSTER_ADMIN "solaris.cluster.admin"

/*
 * void cl_auth_init(void)
 *
 * Initializes cl_auth library and keeps a copy of
 * euid & uid internally. This function should be
 * called typically at the beginning of main and
 * before any of the cl_auth library functions
 * is called.
 */
void cl_auth_init(void);

/*
 * void cl_auth_demote(void)
 *
 * sets euid of user to their uid
 * (also makes sure we have copy of old euid/uid)
 */

void cl_auth_demote(void);

/*
 * void cl_auth_promote(void)
 *
 * It restores the EUID privileges to the executable,
 * and sets the UID privileges to the EUID privileges,
 * allowing access (amongst other things) to the
 * ORB. After this function, the command basically
 * can run as "root" if the profile is set up with
 * suid root.
 */
void cl_auth_promote(void);

/*
 * cl_auth_is_authorized() : This function checks for
 * a particular authorization for the user of the
 * current process. This function will simply return
 * the flow of control to the caller and it's up
 * to the caller to decide what to do with the value
 * returned.
 *
 * Parameters :
 *   auth_name - a constant character string representing
 *               the authorization to be checked.
 *
 * Return value : returns 0 if not authorized, otherwise 1
 */
int cl_auth_is_authorized(const char *auth_name);


/*
 * cl_auth_check_command_opt_exit() : This function checks
 * whether the user of the current process has authorization
 * to execute a particular command with particular a option.
 * If it fails, it will exit with the error-code passed to it as
 * an argumentor; otherwise execution will continue in the caller's
 * code.
 *
 * Parameters :
 *   command_name - constant character string representing
 *                  the name of the command for which authorization
 *                  is being done.
 *   command_option - constant character string representing the
 *                    option of the command that needs authorization
 *   auth_name - a constant character string representing the
 *               the authorization to be cheched.
 *   cl_auth_err - an integer error code with which the function
 *                 will exit, if authorization checking fails.
 *
 * Return value : void
 */
void cl_auth_check_command_opt_exit(const char *command_name,
    const char *command_option,
    const char *auth_name,
    int cl_auth_err);

/*
 * cl_auth_get_initial_uid : This function returns the uid value
 * that was saved before demoting and promoting. Users that want
 * to know the original uid of the process, should make use of
 * this function.
 *
 * Parameters :
 *            None.
 *
 * Return value :
 *            The uid value of the process.
 */
uid_t cl_auth_get_initial_uid(void);

#ifdef __cplusplus
}
#endif

#endif	/* _CL_AUTH_H */
