/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _OS_COMPAT_H
#define	_OS_COMPAT_H

#pragma ident	"@(#)os_compat.h	1.15	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/sol_version.h>

#if OS_VERSION == __rh30as
#include <sys/os.RH30AS.h>
#elif OS_VERSION == __rh40as
#include <sys/os.RH40AS.h>
#elif OS_VERSION == __rh72
#include <sys/os.RH72.h>
#else
#include <sys/os.SunOS.h>
#endif

#ifdef __cplusplus
}
#endif

#endif /* _OS_COMPAT_H */
